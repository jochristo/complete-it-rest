/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobiweb.mobile.completeit.test.ejb;
/**
 *
 * @author john
 */
public class OpenEJBTest //extends TestCase
{
/*
    private Context ctx = null;
    private EJBContainer container = null;
    private Logger logger = Logger.getLogger("completeIt Logger");
    @EJB private ProfileJpaService service;
    @EJB private ProfileResource resource;
    
    // profile service lookups
    @EJB private ProfileService ps;
    @EJB private IServiceLocal isr;
    @EJB private UProfileService ups;

    private final String profileServiceJNDI = "java:global/completeit/ProfileJpaService!mobiweb.mobile.completeit.service.ProfileJpaService";
    private final String profileResourceJNDI = "java:global/completeit/ProfileResource!mobiweb.mobile.completeit.resource.ProfileResource";
    
    private final String psJNDI = "java:global/completeit/ProfileService!mobiweb.mobile.completeit.service.ProfileService";
    private final String isrJNDI = "java:global/completeit/ProfileService!mobiweb.mobile.completeit.service.IServiceLocal";
    
    @Override    
    protected void setUp() throws Exception {
        
        // Init EJB container
        Map<String, Object> properties;
        properties = new HashMap();
        properties.put(EJBContainer.MODULES, new File("target/classes"));        
        container = EJBContainer.createEJBContainer(properties);
        ctx = container.getContext();
        //service = (ProfileJpaService)ctx.lookup(profileServiceJNDI);
        //resource = (ProfileResource)ctx.lookup(profileResourceJNDI);
        
        ps = (ProfileService) ctx.lookup(psJNDI);
        isr = (IServiceLocal) ctx.lookup(isrJNDI);
        
        ups = (UProfileService) ctx.lookup("java:global/completeit/UProfileService!mobiweb.mobile.completeit.service.UProfileService");
    }
    
    @Override
    protected void tearDown() throws Exception {
        ctx.close();
        container.close();
    }    
   

    public void testSetUp()
    {
        assertNotNull(container);
        assertNotNull(ctx);        
    }
    
    
    public void testLookups() throws NamingException
    {
        assertNotNull(ctx.lookup(profileServiceJNDI));       
        //assertNotNull(ctx.lookup(profileResourceJNDI));       
        
        // test IService and Service
        assertNotNull(ps);
        assertNotNull(isr);
    }

    
    public void testServiceCanonicalName()
    {
        //assertNotNull(resource.getClassName());       
        //assertEquals("ProfileJpaService$$LocalBeanProxy", resource.getClassName());
    }

    public void atestCreateProfile() throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException
    {
        Profile p = new Profile();
        p.setNickname("johnny");
        p.setEmail("ichristod@gmail.com");
        p.setLanguage("English");        
        p.setPassword(PBKDF2Hash.getHash("#$#$@#$@#$")); 
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = sdf.parse("13/11/2000");        
        p.setDob(date);        
        //p.setLongitute(new BigDecimal(1.5));
        //p.setLatitude(new BigDecimal(2.5));
        Location location = new Location(1525.8787000,25.232365);
        p.setLocation(location);
        p.setPictureFilepath("johnny.png");
        ps.inserted(p);
        assertNotNull(p);
        
        // SECOND CREATION
        p = new Profile();
        p.setNickname("ioannis");
        p.setEmail("ichristod@hotmail.com");
        p.setLanguage("Greek");        
        p.setPassword(PBKDF2Hash.getHash("dwfsdf23sdf23rwf")); 
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        date = sdf.parse("13/11/2016");        
        p.setDob(date);
        location = new Location(12.9898,32.5598);
        p.setLocation(location);
        p.setPictureFilepath("my_pic.png");
        ps.inserted(p);
        assertNotNull(p);    
        
        
        List<Profile> listAll = ps.findAll();
        assertNotNull(listAll);        
    }

    public void atestDeleteProfile(String nickname)
    {
        
        //Profile p = service.findByParameterSingle(nickname);
        //assertNotNull(p);
        //assertEquals(p.getNickname(),nickname);
        //service.delete(p);
        //p = service.findByParameterSingle(nickname);
        //assertNull(p);        
        
    }
    
    @Override
    protected void runTest() throws Throwable {
        super.runTest();
    }
    
    public void atestIService()
    {
        
        //assertNotNull(ps.getEntityManager());
        //assertNotNull(isr.getEntityManager());
        
        //Profile p = new Profile();
        //p.setNickname("john");
        //p.setEmail("john@gmail.com");
        //p.setLanguage("English");
        //p.setLocation("Patra");
        //p.setPassword("dsfsdfsdfsdfwerweeesdfs");        
        //Profile p1 = (Profile) isr.inserted(p);    
        //assertNotNull(p1);
        //Profile pf = isr.getEntityManager().find(Profile.class, p1.getId());
        //assertNotNull(pf);
        //assertEquals(pf.getEmail(),p.getEmail());
        
        
    }
            
    public void atestCreateUProfileService()
    {
        
        assertNotNull(ups.getEntityManager());
        assertNotNull(isr.getEntityManager());
        
        UProfile p = new UProfile();
        p.setNickname("john");
        p.setEmail("john@gmail.com");
        p.setLanguage("English");
        p.setLocation("Patra");
        p.setPassword("dsfsdfsdfsdfwerweeesdfs");        
        UProfile p1 = (UProfile) ups.inserted(p);    
        assertNotNull(p1);
        UProfile pf = isr.getEntityManager().find(UProfile.class, p1.getId());
        assertNotNull(pf);
        assertEquals(pf.getEmail(),p.getEmail());        
        
    } 
*/
}
