<%-- 
    Document   : login
    Created on : Sep 26, 2016, 2:51:06 PM
    Author     : AdminAccount
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Complete-It Login</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">         
    <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">    
    <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">                
  <style>
    body {
      padding-top: 60px;      
      /* 60px to make the container go all the way to the bottom of the topbar */
    }
    
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
    }

    .modal-body{
        padding: 25px;
    }
    .modal-dialog {
        width: 500px;
    }
    .bootbox {
        background: none;
    }
    
    backdrop.fade.in {
        opacity: 0.6; 
    }    

    .row{
        margin-left: -15px;
    }
  </style>
  
<script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script> 
<script>
  
$(document).ready(function(){

    //console.log('doc ready');
    $('#error').show().hide();
    //console.log('element hidden');
    
    $('.close').click(function() {

       $('#error').show().hide();

    });
    
    
// Login Button CLICK Event
$("#loginForm").submit(function(e)
{
   
   var username = $('#username').val();
   console.log(username);
   var password = $('#password').val();   
   console.log(password);
   
        $.ajax({
            url : 'login',
            type: 'POST',
            data : { 
                action : "login", 
                username : username, 
                password : password
            },            
            dataType : 'json',
            beforeSend: function(){
               //$('#deleteFailure').addClass('hidden');
               //alert("beforeSend");
            },            
            success : function (response){
                if(response !== ''){ //login OK
                    message = 'Phrase created successfully!';
                    //alert(message);
                    var trimData = $.trim(JSON.stringify(response));                    
                    var obj = $.parseJSON(trimData);                                        
                    
                    // redraw table
                    //$('#phrases-table').dataTable().fnDraw();                    
                }
                else if(response === null){ // login error
                    message = 'login failed';
                    $('#errorMessage').html(message);                
                    $('#error').show();
                    //$('#deleteFailure').alert();
                    $("#error").fadeTo(2000, 500).slideUp(500, function(){
                    $("#error").slideUp(1000);                    
                    });
                }
            },
            error:function(responseText) {
                $('#errorMessage').html(responseText);
                $('#error').show();
            }
        }); 
        
         e.preventDefault();
   
});    
    
    
});
  
</script>      
  
 </head>
  
<body>
        
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse"
         data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span>
      </a> <a class="brand" href="#">Complete it</a>

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
    </div>
  </div>
</div>
        
<div class="container">  
    
<div style="width:600px;margin-left: auto;margin-right: auto;margin-top: 30px">  
<h2>Login form</h2>
</div>

 <div id="mylogin" style="border-radius:6px;width: 600px; background-color: #EEE; padding: 0px 0px 0px 0px; margin-left: auto;
    margin-right: auto; margin-top: 20px;">
     
    <!-- LOGIN ERROR-->
    <div class="alert alert-danger fade in" id="error" style="margin-bottom: 0px;">
      <a href="#" class="close" aria-label="close" style="right:0px">×</a>
      <strong id="errorMessage"> Incorrect username or password.</strong>
    </div>
     
  <!-- Login Form -->  
  <form id="loginForm" action="login" method="post" name="login" style="padding: 15px 20px 20px 20px;">
  <div class="form-group">
    <label for="email">Username</label>
    <input type="username" class="form-control" id="username" name="username" style="height:25px; width: 300px" required="true">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="password" name="password" style="height:25px;width: 300px" required="true">
  </div>  
  <button id="loginBtn" type="submit" class="btn btn-primary" style="margin: 5px 0 0 0;padding: 5px 15px 5px 15px;">Login</button>
</form>       
  </div>       
   
</div>
 
      
</div>
<!-- /container -->        
        
</body>
</html>
