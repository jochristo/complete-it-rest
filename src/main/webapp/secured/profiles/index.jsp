<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : AdminAccount
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Complete it - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">  
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">  
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">  
  <link rel="stylesheet" href="http://cdn.css.net/libs/startbootstrap-sb-admin-2/1.0.5/css/sb-admin-2.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>  
  <link rel="stylesheet" href="/complete-it/assets/css/sb-admin-2.min.css"/>  
  
<style>
    body {
          padding-top: 60px;      
          /* 60px to make the container go all the way to the bottom of the topbar */               
    }

    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
        
        margin-left: auto;
        margin-right: auto;
    }

    .modal-body{
        padding: 25px;
    }
    .modal-dialog {
        
        width: 500px;
        margin: 30px auto;
        
    }
    .bootbox {
        background: none;
    }
    
    backdrop.fade.in {
        opacity: 0.6; 
    }    
    
    .navbar .nav .active > a
    {
        /* background-color: skyblue; */
    
    } 
    
    .navbar-fixed-top
    {
        border-bottom-width: 3px;
        border-color: skyblue;
        background-color: #1b1b2b; 
        box-shadow: 0 6px 6px -6px skyblue
    }   
    
    .container {
        width: 1400px;
    }   
    
    .filtered-select {
        width: auto;
    }    
    
    td.actions {
        text-align: center;
    }
    
  </style>


  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
<script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>

$(document).ready(function(){

  $("#addBtn").attr('disabled', 'disabled');
  
  $(".choices").change(function()
  {    
    if($("#categoryId").val() === '0') {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
      var isEmptyPhrase = $('#myPhraseText').val().replace(/^\s+|\s+$/g, "").length === 0;
      if(!isEmptyPhrase){  
        $("#addBtn").removeAttr('disabled');
      }
    }
  });
  
$('#myPhraseText').change(function () {    
    if($(this).val().replace(/^\s+|\s+$/g, "").length === 0)   
    {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
        var isCategoryUnselected = $("#categoryId").val() === '0';        
        if(!isCategoryUnselected){
            $("#addBtn").removeAttr('disabled');
        }
    }    
});  

$("#addBtn").click(function()
{
   $("#addSuccess").hide(); 
});
$("#prevLink").click(function()
{
   $("#addSuccess").hide(); 
});
$("#nextLink").click(function()
{
   $("#addSuccess").hide(); 
});

// hide add  form
$("#myForm").attr('hidden', 'hidden'); 

});

$(document).ready(function() {

$('.close').click(function() {

   $('#successContainer').hide();
   $('#failureContainer').hide();   

});    
        
hide_success_label();
hide_failure_label();
    
// add datatable sorting
$('#profiles-table').DataTable({

        // disable sorting button in last column
        "aoColumnDefs" : [
            { "bSortable": false, "aTargets": [9]},
            {"sWidth": "5%", "aTargets": [0]},
            {"sWidth": "auto", "aTargets": [1]},
            {"sWidth": "auto", "aTargets": [2]},
            {"sWidth": "auto", "aTargets": [3]},
            {"sWidth": "auto", "aTargets": [4]},
            {"sWidth": "auto", "aTargets": [5]},
            {"sWidth": "auto", "aTargets": [6]},
            {"sWidth": "auto", "aTargets": [7]},
            {"sWidth": "13%", "aTargets": [8]},
            {"sWidth": "3%", "aTargets": [9]}            
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) 
        {
          $(nRow).attr("data-profileid",aData[0]);
          return nRow;
        },
        "fnDrawCallback" : function(){
            $('.updateBtn').off('click').on('click', edit);            
        },        
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(9).addClass('actions'); // center actions td content        
            if ( String(data[8]) === 'false') {
                $('td:eq(8)', row).html('<text class="label label-pill label-success">Active</text>');
                data[8] = "Active";
            }else if(String(data[8]) === 'true' )
            {
                $('td:eq(8)', row).html('<text class="label label-pill label-warning">Suspended</text>');
                data[8] = "Suspended";            
            }
        },         
        "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
        "autoWidth": false        
    });
    
//INDIVIDUAL DataTable column filtering - select inputs
var table = $('#profiles-table').DataTable(); 
$("#profiles-table tfoot th").each( function ( i ) 
{    
    //select which headers should include filters
    if(i !== 0 && i !== 1 && i !== 2 && i !== 4 && i !== 6 &&  i !== 7 &&  i !== 9 ){
    var select = $('<select class="filtered-select" style="width: 150px"><option value=""></option></select>')
        .appendTo( $(this).empty() )
        .on( 'change', function () {
            table.column( i )
            .search( $(this).val() )
            .draw();
        });                
                
        table.column( i ).data().sort().unique().each( function ( d, j )
        {                           
            //if(i !== 8) // do not include column 8 yet - status text contains text element- process after this.
            //{
                select.append('<option value="'+d+'">'+d+'</option>');
            //}          
        });
}
});    

//resetColumnFiltersPlainTdOnly(8);

// UNBIND UPDATE click event
$('.updateBtn').off('click');

$('.updateBtn').click(edit);
 

// UPDATE Phrase function    
function edit(event)
{    
    event.preventDefault();    
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var profileId = trElem.attr("data-profileid");    
    var id = $.trim($(trElem).children("td:first").text());      
    var status = $(trElem).find("td:eq(8) text").html();    
    var message = '';   
    var row = trElem;
    
    // delete CONFIRMATION
    bootbox.dialog({
      message: "Are you sure?",
      title: "Change profile status",
      buttons: {
        danger: {
          label: "No",
          className: "btn-danger",
          callback: function() {
            //do something
          }
        },
        main: {
          label: "Yes",
          className: "btn-success",
          callback: function() {

            $.ajax({
                url : 'profiles',
                type: 'POST',
                data : {profileId : profileId, action : 'Update', status : status},
                dataType : 'json',
                beforeSend: function(){               
                },            
                success : function (response)
                {   
                    if(response !== 'ERROR')
                    {
                        var trimData = $.trim(JSON.stringify(response));                   
                        var json = $.parseJSON(trimData);
                        var label = 'success';
                        var status = "Active";
                        var title = 'Set Suspended';
                        var icon = 'fa fa-lock';
                        if(json.status === 'Suspended'){
                            label = 'warning';
                            status = json.status;
                            title = 'Set Active';
                            icon = 'fa fa-unlock';
                        }
                        $('#profiles-table').dataTable().fnUpdate('<text class="label label-pill label-'+label+'">'+status+'</text>', row, 8, false);
                        $('#profiles-table').dataTable().fnUpdate('<a href="#" class="updateBtn" title="'+title+'"><i class="'+icon+'" style="color: black;"></i></a>', row, 9, false);
                        // re-draw datatable
                        $("#profiles-table").dataTable().fnDraw();
                        // reset column filter data in culture column
                        resetColumnFiltersPlainTdOnly(8);
                        message = 'Profile with id <'+ id + '> status updated successfully!';
                        show_success_label(message);
                    }
                    else if(response === 'ERROR'){
                        message = 'Something went wrong profile was not updated';
                        show_failure_label(message);
                    }
                },
                error:function(response) {
                    var trimData = $.trim(JSON.stringify(response));                 
                    var obj = $.parseJSON(trimData);                                 
                    var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                    var json = $.parseJSON(trimResponseJSON);                  
                    var message = json.detail;
                    var code = json.applicationErrorCode;                
                    show_failure_label("Update request failed: "+ message);
                }
            });

          }
        }
      }
    }); //end bootbox
}; 

function populateStatusOptions()
{
    var cells = [];
    var rows = $("#profiles-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find("td:eq(8) text").html();        
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                   
            $("#profiles-table tfoot th:eq(8) select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

function resetColumnFiltersPlainTdOnly(column)
{
    $("#profiles-table tfoot th:eq("+column+")").find('select').children().remove();    
    $("#profiles-table tfoot th:eq("+column+")").find('select').append('<option value=""></option>');    
    var cells = [];
    var rows = $("#profiles-table").dataTable().fnGetNodes();    
    for(var i=0;i<rows.length;i++)
    {
        //var html = $(rows[i]).find('td:eq('+column+')').html();
        var html = $(rows[i]).find('td:eq('+column+') text').text();
        
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                
            $("#profiles-table tfoot th:eq("+column+") select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

function show_success_label(message)
{
    $('#successMessage').html(message);
    $('#successContainer').show();
    $("#successContainer").fadeTo(2000, 500).slideUp(500, function(){
    $("#successContainer").slideUp(1000); });     
}

function show_failure_label(message)
{   
    $('#failureWarningMessage').html(message);                
    $('#failureContainer').show();                    
    $("#failureContainer").fadeTo(3000, 500).slideUp(1000, function(){
    $("#failureContainer").slideUp(2000);});        
}

function hide_success_label()
{
    $('#successContainer').hide();
}

function hide_failure_label()
{
    $('#failureContainer').hide();
}

} );

</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="container" style="width:1400px;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span></a>
          <!-- <a class="brand" href="/complete-it/web/secured/profiles">Home</a> --> 
      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
      
    <ul class="nav navbar-nav" style="margin-top: 5px;font-size: initial;">
    <li><a href="/complete-it/web/secured/dashboard">Dashboard</a></li> 
    <li class="active"><a href="#">Profiles</a></li>        
    <li><a href="/complete-it/web/secured/categories">Categories</a></li> 
    <li><a href="/complete-it/web/secured/phrases">Phrases</a></li>
    <li><a href="/complete-it/web/secured/answers">Answers</a></li> 
    </ul>      
      
    </div>
  </div>
</div>  
        
<div class="container">
<br>
<div class="panel panel-default">
    <div class="panel-heading fa-2x">Profiles</div>
    <div class="panel-body">
        
  <!-- ADD PHRASE -->
  <!-- <h1>Profiles</h1> -->
  <br>
  <form id="myForm" class="movie-input-form form-inline" action="phrases" method="post" name="addForm">
    <p><b>Add Phrase</b></p>
      

    <input id="myPhraseText" type="text" name="text" placeholder="Phrase text" size="70" required="true" 
        pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" 
        title="Use format: word [] [] word etc." style="height:30px; width: 250px"/>     
        
    <select name="category" required="true" id="categoryId" class="choices">
        <!-- <option value="id">Title</option> -->
        <option value="0">-- Select Category --</option>
        <option value="1">Geography</option>
        <option value="2">Transportation</option>
        <option value="3">Leisure</option>
        <option value="4">Calculations</option>
        <option value="5">Traffic</option>
        <option value="6">Prices</option>
        <option value="7">Computer</option>
        <option value="8">Super Market</option>
        <option value="9">Random</option>
        <option value="10">Shopping</option>
        <option value="11">Women</option>
        <option value="12">Money</option>          
        <option value="13">Financial</option>        
        <option value="14">Home</option>                    
    </select>    
    <input id="addBtn" type="submit" name="action" class="btn btn-primary" value="Add" style="padding: 4px 18px;"/>
  </form>

  <!-- success message -->
  <div class="container" id="successContainer" style="padding-right:initial; padding-left:initial; width: inherit" hidden="true">
  <div class="alert alert-success fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
    <strong id="successMessage"></strong>
  </div>
  </div>

  <!-- error message -->
  <div class="container" id="failureContainer" style="width:initial; padding-right:initial; padding-left:initial;" hidden="true">
  <div class="alert alert-danger fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
      <strong id="failureWarningMessage"></strong>
  </div>
  </div> 
  
    <!-- PROFILES DATA TABLE -->    
    <table id="profiles-table" class="table table-striped table-bordered">
    <thead>
    <tr>
      <th>Id</th>
      <th>Nickname</th>
      <th>Email</th>
      <th>Gender</th>
      <th>Image path</th>
      <th>Language</th>      
      <th>Score</th>
      <th>Level</th>
      <th>Status</th>
      <th>&nbsp;</th>
    </tr>
    </thead>
    <tfoot>
        <tr>    
        <th></th>
        <th></th>
        <th></th>
        <th>Gender</th>
        <th></th>
        <th>Language</th>      
        <th></th>
        <th></th>
        <th>Status</th>
        <th></th>
        </tr>
    </tfoot>     
    <tbody>
    <c:forEach items="${profiles}" var="profile">
      <tr>
        <td><c:out value="${profile.id}"/></td>
        <td><c:out value="${profile.nickname}"/></td>
        <td><c:out value="${profile.email}"/></td>
        <td><c:out value="${profile.gender}"/></td>
        <td><c:out value="${profile.pictureFilepath}"/></td>        
        <td><c:out value="${profile.language}"/></td>        
        <td><c:out value="${profile.score}"/></td>
        <td><c:out value="${profile.level}"/></td>      
        <td><c:out value="${profile.deleted}"/></td>
        <c:choose>
            <c:when test="${profile.status == 'Active'}">
                <%-- <td><text class="label label-pill label-success"><c:out value="Active"/></text></td> --%>                
                <td class="actions"><a href="#" class="updateBtn" title="Set Suspended"><i class="fa fa-lock" style='color: black;'></i></a></td> 
            </c:when>    
            <c:otherwise>
                <%-- <td><text class="label label-pill label-warning"><c:out value="Suspended"/></text></td> --%>
                <td class="actions"><a href="#" class="updateBtn" title="Set Active"><i class="fa fa-unlock" style='color: black;'></i></a></td> 
            </c:otherwise>
        </c:choose>
        
        <!-- <td><a href="?action=Remove&id=${phrase.id}" title="Set Active/Inactive"><i class="icon-lock"></i></a></td> -->
      </tr>
    </c:forEach>
    </tbody>
  </table>
    <hr>
    <div id="footer_console">
        Complete-it v1.0 - Admin Console
    </div>         
        
    </div>    
</div>
    
  
    
</div>
<!-- /container -->
</body>
</html>
