<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : ic
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Complete it - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">  
  <link rel="stylesheet" href="http://cdn.css.net/libs/startbootstrap-sb-admin-2/1.0.5/css/sb-admin-2.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/complete-it/assets/css/sb-admin-2.min.css"/>   
  <link rel="stylesheet" href="/complete-it/assets/css/layout.css"/>   
  
  <style>
    body {
      padding-top: 60px;      
      /* 60px to make the container go all the way to the bottom of the topbar */
    }  
  
    
    .row{
        margin-left: -15px;
    }
    
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
        
        margin-left: auto;
        margin-right: auto;
    }

    .modal-body{
        padding: 25px;
    }
    .modal-dialog {
        
        width: 500px;
        margin: 30px auto;
        
    }
    .bootbox {
        background: none;
    }
    
    backdrop.fade.in {
        opacity: 0.6; 
    }    
    
    .navbar .nav .active > a
    {
        /* background-color: skyblue; */

    }    
    
    .navbar-fixed-top
    {
        border-bottom-width: 3px;
        border-color: skyblue;
        background-color: #1b1b2b; 
        box-shadow: 0 6px 6px -6px skyblue
    }  
    
    .container {
        width: 1400px;
    }  
    
    .filtered-select {
        width: auto;
    }
    
    td.actions {
        text-align: center;
    }    
    
  </style>

  
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.js"></script>

<script>
   
$(document).ready(function(){  

hide_updating_message();

// DELETE Phrase function    
function deletePhrase(event){           
    
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var phraseId = trElem.attr("data-phraseid");        
    var id = $.trim($(trElem).children("td:first").text());    
    var message = '';   
    var phraseRow = trElem;
    var approvalStatusText = $(trElem).find("td:eq(4) text").html();    
    
// delete CONFIRMATION
bootbox.dialog({
  message: "Are you sure?",
  title: "Phrase deletion",
  buttons: {
    danger: {
      label: "Cancel",
      className: "btn-danger",
      callback: function() {        
      }
    },
    main: {
      label: "OK",
      className: "btn-success",
      callback: function() {
          
        $.ajax({
            url : 'phrases',
            type: 'GET',
            data : { action : "Delete", id : phraseId},            
            dataType : 'json',
            beforeSend: function(){               
            },            
            success : function (response){
                var trimData = $.trim(JSON.stringify(response));                    
                var json = $.parseJSON(trimData);   
                if(json.detail === 'Deleted' &&  json.httpStatus === 200){
                    
                    $('#phrases-table').dataTable().fnDeleteRow(phraseRow);                    
                    message = 'Phrase with id <'+ id + '> deleted successfully';                    
                    show_success_label(message);                   
                    // re-draw datatable
                    $("#phrases-table").dataTable().fnDraw(); 
                    
                    //update approval status column filters
                    resetApprovalOptions(); // approval
                    resetColumnFiltersPlainTdOnly(1); // text
                    resetColumnFiltersPlainTdOnly(3); // category
                    resetColumnFiltersPlainTdOnly(5); //added by
                    /*
                    var options = $("#phrases-table tfoot th:eq(4)").find('select').children();                    
                    $("#phrases-table tfoot th:eq(4)").find('select option:eq(1)').html();
                    $("#phrases-table tfoot th:eq(4)").find('select').children().eq(1).html()                                        
                    $("#phrases-table tfoot th:eq(4)").find('select').append('<option value=""></option><option value="Pending">Pending</option>');             
                    */
                }                
            },
            error:function(response) {
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);  
                var message = json.detail;                  
                show_failure_label(message);
            }
        });

      }
    }
  }
}); //end bootbox
};

    
$("#prevLink").click(function()
{
   $("#addSuccess").hide(); 
});

$("#nextLink").click(function()
{
   $("#addSuccess").hide(); 
});


//PHRASES DataTable INIT
$('#phrases-table').DataTable(
{
    //"paging":   false,
    // disable sorting button in last column
    'autoWidth': false,
    //"sPaginationType": "full_numbers",
    "aoColumnDefs" : [
        { 
            "bSortable": false, 
            "aTargets": [6]
        },        
        {"sWidth": "auto", "aTargets": [1]},
        {"sClass": "updatableStatus", "aTargets":[4] }
    ],
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).attr("data-phraseid",aData[0]); 
      return nRow;
    },
    "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
    "fnDrawCallback" : function(){
        $('.deleteBtn').off('click').on('click',deletePhrase);
        $('.editBtn').off('click').on('click',editPhrase);        
    },
    "createdRow": function ( row, data, index ) {
        $('td', row).eq(6).addClass('actions'); // center actions td content        
        if ( String(data[4]) === 'false') {
            $('td:eq(4)', row).html('<text class="label label-pill label-success">Approved</text>');
            data[4] = "Approved";
        }else if(String(data[4]) === 'true' )
        {
            $('td:eq(4)', row).html('<text class="label label-pill label-danger">Pending</text>');
            data[4] = "Pending";            
        }
    }   
}); 


//INDIVIDUAL DataTable column filtering - select inputs
var table = $('#phrases-table').DataTable(); 
$("#phrases-table tfoot th").each( function ( i ) 
{    
    //select which headers should include filters
    if(i !== 0 && i !== 1 && i !== 2 && i !== 6  ){
    var select = $('<select class="filtered-select"><option value=""></option></select>')
        .appendTo( $(this).empty() )
        .on( 'change', function () {
            table.column( i )
            .search( $(this).val() )
            .draw();
        });                
                
        table.column( i ).data().sort().unique().each( function ( d, j )
        {                           
            //if(i !== 4) // do not include column 4 yet - approval status text contains text element- process after this.
            //{
                select.append('<option value="'+d+'">'+d+'</option>');
            //}            
        });
}
});

function resetFilters()
{
    $("#phrases-table tfoot th:eq(4)").find('select').children().remove();
    $("#phrases-table tfoot th:eq(4)").find('select').append('<option value=""></option>');
    var cells = [];
    var rows = $("#phrases-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find("td:eq(4) text").html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#phrases-table tfoot th:eq(4) select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

// fill approval options with unique data
//populateApprovalOptions();


// UNBIND DELETE click event
$('.deleteBtn').off('click');

// ADD Phrase FORM SUBMISSION
$('#myForm').on('submit', function(e) {
      
   e.preventDefault();
   var myPhrase = $('#myPhraseText').val();
   var categoryId = $('#categoryId').val();   
   
        $.ajax({
            url : 'phrases',
            type: 'POST',
            data : { action : "Add", category : categoryId, text : myPhrase},            
            dataType : 'json',
            beforeSend: function(){               
            },            
            success : function (response){
                if(response !== ''){ //inserted OK
                    message = 'Phrase created successfully!';
                    var trimData = $.trim(JSON.stringify(response));                    
                    var obj = $.parseJSON(trimData);                     
                    //$('#phrases-table').dataTable().fnUpdate( '<text class="label label-pill label-success">'+json.isApproved+'</text>', trElem, 4, true);
                    $('#phrases-table').dataTable().fnAddData([
                        obj.id,
                        obj.phrase,
                        obj.gaps,
                        obj.categories,
                        //'<text class="label label-pill label-success">'+obj.isApproved+'</text>',
                        obj.status,
                        obj.addingProfileEmail,
                        '<td class="actions" id="actions-td"><a href="#" class="deleteBtn" title="Delete"><span class="fa-stack"><i class="icon-trash"></i></span></a>\n\
                        <a href="#" class="editBtn" title="Edit"><span class="fa-stack"><i class="icon-pencil"></i></span></a></td>'                        
                        ]);
                        
                    /*    
                    //update approval text column filter
                    var option =  $("#phrases-table tfoot th:eq(4)").find('select option');                    
                    var options = [];
                    if(option.length !== 0){
                        option.each(function(i)
                        {                            
                            options.push($(this).text());
                        });                        
                        if(options.indexOf(obj.isApproved) === -1)
                        {
                           $("#phrases-table tfoot th:eq(4)").find('select').append( '<option value="'+obj.isApproved+'">'+obj.isApproved+'</option>' );
                        }
                    }                          
                    */
            
                    //refresh datatable
                    $('#phrases-table').dataTable().fnDraw();                      
                                        
                    // clear input element's value
                    $('#myPhraseText').val("");
                    $('#categoryId').val("");                    
                    
                    show_success_label(message);                      
                }
                else if(response === ''){ // NOT INSERTED
                    message = 'Something went wrong phrase was not created.';
                    show_failure_label(message);
                }
            },
            error:function(response) {                
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);  
                var message = json.detail; 
                show_failure_label(message);
            }
        });    
    
  });


$('#addSuccess').hide();
$('#phraseActionFailure').hide();

$('.close').click(function() {

   $('#phraseActionFailure').hide();
   $('#addSuccess').hide();
   $('#phraseChangedFailure').hide();

});

// DELETE Phrase click EVENT
$('.deleteBtn').click(deletePhrase);  // end delete click
      
// Edit phrase row button
$('.editBtn').click(editPhrase);

// UPDATE Phrase FORM submit
$('#form_phrase').on('submit', function(event)
{
    event.preventDefault();       
    // collect updated information
    var phraseId = $('#form_phrase #id').val();
    var phraseText = $('#form_phrase #phrase').val();
    var categoryId = $('#form_phrase #categories').val();
    var approvalStatus = $('#form_phrase #approvalStatus').val();    
       
    //access tr element 
    var trElem = $("tr[data-phraseid="+phraseId+"]");    
        
    // Send updated phrase to backend db via POST AJAX
    hide_lightbox();
    show_loading_message();
    var id = $('#form_phrase').attr('data-id');    
        
        $.ajax({
            url : 'phrases',
            type: 'POST',
            data : { action : 'Update', id : id, categoryId : categoryId, phraseText : phraseText, approvalStatus : approvalStatus},            
            dataType : 'json',
            beforeSend: function(){
            },            
            success : function (response)
            {
                if(response !== 'ERROR') //updated OK
                {                     
                    json = response;                    
                    /*
                    trElem.children("td:eq(1)").html(json.phrase);
                    trElem.children("td:eq(2)").html(json.gaps);
                    trElem.children("td:eq(3)").html(json.categories);
                    trElem.children("td:eq(4)").html(json.status);
                    */
                                        
                    $('#phrases-table').dataTable().fnUpdate(json.phrase, trElem, 1, false);
                    $('#phrases-table').dataTable().fnUpdate(json.gaps, trElem, 2, false);
                    $('#phrases-table').dataTable().fnUpdate(json.categories, trElem, 3, false);                                        
                    if(json.status === false){
                        
                        $('#phrases-table').dataTable().fnUpdate( '<text class="label label-pill label-success">'+json.isApproved+'</text>', trElem, 4, false);
                        //trElem.children("td:eq(4)").html('<text class="label label-pill label-success">'+json.isApproved+'</text>');
                        $('#disapprovedDiv').show();                        
                    }
                    else if(json.status === true){                        
                        $('#phrases-table').dataTable().fnUpdate( '<text class="label label-pill label-danger">'+json.isApproved+'</text>', trElem, 4, false);
                        //trElem.children("td:eq(4)").html('<text class="label label-pill label-danger">'+json.isApproved+'</text>');
                        $('#approvedDiv').show();                        
                    }                                        
                    resetColumnFiltersPlainTdOnly(3);
                    resetApprovalOptions();
 
                    hide_loading_message();   
                    //show_message("Phrase id '" + id + "' updated successfully.", 'success');   
                    show_success_label("Phrase id '" + id + "' updated successfully");
                }
                else if(response === 'ERROR'){ // NOT updated
                    hide_loading_message();
                    //show_message('Update request failed', 'error');
                    show_failure_label('Update request failed');
                }
            },
            error:function(response) 
            {   
                hide_loading_message();
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);  
                var message = json.detail;
                var code = json.applicationErrorCode;                
                if(code === 100) // PHRASE_TEXT_INVALID EXISTS IN ANSWERS ALREADY, CANNOT BE MODIFIED
                {                    
                    show_lightbox();
                    show_phraseChangedFailure();
                }
                else
                {                                        
                    //show_message('Update request failed: ' + message, 'error');
                    show_failure_label("Update request failed: "+ message);
                }
            }
        });
});

//UPDATE event
function updatePhrase(event)
{
    event.preventDefault();   
    
    // collect updated information
    var phraseId = $('#form_phrase #id').val();
    var phraseText = $('#form_phrase #phrase').val();
    var categoryId = $('#form_phrase #categories').val();
    var approvalStatus = $('#form_phrase #approvalStatus').val();
    var categoryText = $('#form_phrase #categories').text();
    var approvalStatusText = $('#form_phrase #approvalStatus').text();
        
    //access tr element 
    var trElem = $("tr[data-phraseid="+phraseId+"]");
    var firstTd = trElem.children("td:eq(1)").html();    
        
    // Send updated phrase to backend db via POST AJAX
    hide_lightbox();
    show_loading_message();
    var id        = $('#form_phrase').attr('data-id');    
        $.ajax({
            url : 'phrases',
            type: 'POST',
            data : { action : 'Update', id : id, categoryId : categoryId, phraseText : phraseText, approvalStatus : approvalStatus},            
            dataType : 'json',
            beforeSend: function(){
            },            
            success : function (response)
            {
                if(response !== 'ERROR') //updated OK
                {   
                    json = response;
                    //var children = trElem.children("td:nth-child(1)").val(phraseText);                    
                    trElem.children("td:eq(1)").html(json.phrase);
                    trElem.children("td:eq(3)").html(json.categories);
                    trElem.children("td:eq(4)").html(json.isApproved);
                    hide_loading_message();   
                    show_message("Phrase id '" + id + "' edited successfully.", 'success');
                    
                }
                else if(response === 'ERROR'){ // NOT updated                    
                    hide_loading_message();
                    show_message('Edit request failed', 'error'); 
                }
            },
            error:function(responseText) 
            {                
                var trimData = $.trim(JSON.stringify(responseText));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);                
                var message = json.detail;        
                hide_loading_message();
                show_message('Information request failed: ' + message, 'error'); 
            }
        });      
    
}

//EDIT Event
function editPhrase(event)
{
    event.preventDefault();
    show_loading_message();    
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var phraseId = trElem.attr("data-phraseid");    
    // Get phrase info from backend
    var request = $.ajax({
      url:          'phrases',
      cache:        false,
      data:         'action=Fetch&id=' + phraseId,
      dataType:     'json',
      contentType:  'application/json; charset=utf-8',
      type:         'GET'
    });    
        
    request.done(function(output)
    { 
        
      if (output.result === ''){
        hide_loading_message();                
        show_message('Edit request failed: ' + "Phrase not found", 'error');
      } 
      else// success , get json
      {
        hide_loading_message();
        show_lightbox();                
        //fill form with fetched data
        $('#form_phrase').attr('data-id', phraseId);
        $('#form_phrase .field_container label.error').hide();
        $('#form_phrase .field_container').removeClass('valid').removeClass('error');
        $('#form_phrase #id').val(output.id);
        $('#form_phrase #phrase').val(output.phrase);
        $('#form_phrase #gaps').val(output.gaps);                
        $('#form_phrase #addedByProfile').val(output.addingProfileEmail);
        
        // Remove all option entries from select elements
        $('#form_phrase #categories option').each(function() {
            $(this).remove();     
        });        
        $('#form_phrase #approvalStatus option').each(function() {
            $(this).remove();     
        });         
        
        //populate options for categories and aproval status - select right index
        var categories = output.categoryItems;        
        for (i = 0; i < categories.length; i++){
           $('#form_phrase #categories').append($('<option>', {value:categories[i].id, text:categories[i].name}));            
           // select index 
           if(categories[i].name === output.categories)
           {   
               $('#form_phrase #categories').prop('selectedIndex', i);            
           }
        }
        
        //set approval status and options list
        var options = ['Pending','Approved'];
        for(var i = 0; i < 2; i++)
        {
            $('#form_phrase #approvalStatus').append($('<option>', {value:i, text:options[i]})); 
        }         
        if(output.isApproved === "Approved")
        {           
            $('#form_phrase #approvalStatus').prop('selectedIndex', 1);    
        }else{
            $('#form_phrase #approvalStatus').prop('selectedIndex', 0); 
        }
        
        //$('select').append($('<option>', {value:1, text:'One'}));
        //$('select').append('<option val="1">One</option>');
        //var option = new Option(text, value); $('select').append($(option));
      }
    });
    request.fail(function(response)
    {        
        var trimData = $.trim(JSON.stringify(response));                 
        var obj = $.parseJSON(trimData);                                 
        var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
        var json = $.parseJSON(trimResponseJSON);                
        var message = json.detail;        
        hide_loading_message();
        //show_message('Edit request failed: ' + message, 'error');
        show_failure_label('Edit request failed: ' + message);
    });    
}

// Show loading message
function show_loading_message(){
    $('#loading_container4').html('Loading, please wait');
    $('#loading_container').show();
}

// Hide loading message
function hide_loading_message(){
    $('#loading_container').hide();
}

// Show message
function show_message(message_text, message_type){
    $('#message').html('<p>' + message_text + '</p>').attr('class', message_type);
    $('#message_container').show();
    if (typeof timeout_message !== 'undefined'){
      window.clearTimeout(timeout_message);
    }
    timeout_message = setTimeout(function(){
      hide_message();
    }, 8000);
}

// Hide message
function hide_message(){
    $('#message').html('').attr('class', '');
    $('#message_container').hide();
}
  
// Show edit form
function show_lightbox(){
    $('.lightbox_bg').show();
    $('.lightbox_container').show();
}

// Hide edit form
function hide_lightbox(){
    $('.lightbox_bg').hide();
    $('.lightbox_container').hide();
}  


// Show loading message
function show_updating_message(message){
    $('#loading_container4').html('Updating, please wait');
    //$('#loading_container').css({ 'display': "block" });
    $('#loading_container').show();
}

// Hide loading message
function hide_updating_message(){
    $('#loading_container').hide();
}
  
function show_success_label(message)
{
    $('#additionSuccessMsg').html(message);
    $('#addSuccess').show();
    $("#addSuccess").fadeTo(2000, 500).slideUp(500, function(){
    $("#addSuccess").slideUp(1000); });     
}

function show_failure_label(message)
{   
    $('#failureWarningMessage').html(message);                
    $('#phraseActionFailure').show();                    
    $("#phraseActionFailure").fadeTo(3000, 500).slideUp(1000, function(){
    $("#phraseActionFailure").slideUp(2000);});        
}
  
// Lightbox background
$(document).on('click', '.lightbox_bg', function(){
    hide_lightbox();
});

// Lightbox close button
$(document).on('click', '.lightbox_close', function(){
    hide_lightbox();
});  
  

$("#approveAllBtn").click(function(event)
{
    event.preventDefault();
    bootbox.confirm({
    message: "Set approval status to 'Approved' for all phrases, are you sure?",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
        if(result === true)
        {          
        show_updating_message();
        $.ajax({
            url : 'phrases',
            type: 'POST',
            data : { action : 'ApproveAll'},            
            dataType : 'json',            
            beforeSend: function(){
            },            
            success : function (response)
            {   
                if(response.detail === 'Success' && response.httpStatus === 200) //updated OK
                {   
                    var rows = $("#phrases-table").dataTable().fnGetNodes();                    
                    for(var i=0;i<rows.length;i++)
                    {
                        var tr = $(rows[i]);                        
                        var text = tr.find("td:eq(4)").find("text").html();
                        //var phraseId = tr.find("td:eq(0)").html();
                        if(text !== 'Approved')
                        {                            
                            //var trElem = $(tr+"[data-phraseid="+phraseId+"]"); 
                            //trElem.children("td:eq(4)").html('<text class="label label-pill label-success">Approved</text>');
                            //access approval status column and update content
                            //cells.push($(rows[i]).find("td:eq(4)").html('<text class="label label-pill label-primary">Approved</text>'));
                            $('#phrases-table').dataTable().fnUpdate( '<text class="label label-pill label-success">Approved</text>', rows[i], 4);
                        }
                    } 
                    //$('#phrases-table').dataTable().fnDraw();
                    showApprovalStatusButtons(false);
                    hide_updating_message();                       
                    //show_message("Phrases approval status updated successfully.", 'success');
                    show_success_label("Phrases approval status updated successfully");
                                        
                    // clear approval status options list
                    //update approval text column filter                    
                    $("#phrases-table tfoot th:eq(4)").find('select').children().remove();
                    $("#phrases-table tfoot th:eq(4)").find('select').append('<option value=""></option><option value="Approved">Approved</option>');
                    
                    /*
                    var options = [];
                    var optionApproved = 'Approved';
                    if(option.length !== 0){
                        option.each(function(i)
                        {                            
                            options.push($(this).text());
                        });                        
                        if(options.indexOf(optionApproved) === -1)
                        {
                           $("#phrases-table tfoot th:eq(4)").find('select').append( '<option value="'+optionApproved+'">'+optionApproved+'</option>' );
                        }
                    }
                    */
                }
                else if(response.detail === 'Error'){ // NOT updated                    
                    hide_updating_message();                    
                    //show_message('Update request failed', 'error');
                    show_failure_label('Update request failed');
                }
            },
            error:function(response) 
            {   
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);                
                var message = json.detail; 
                hide_updating_message();                
                //show_message('Update request failed: ' + message, 'error');
                show_failure_label('Update request failed: ' + message);
            }
        }); 
        }
        
    }
});

        
        
});    

$('#disapproveAllBtn').click(function(){
    event.preventDefault();
    bootbox.confirm({
    message: "Set approval status to 'Pending' for all phrases, are you sure?",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
        if(result === true){
            
        show_updating_message();            
        $.ajax({
            url : 'phrases',
            type: 'POST',
            data : { action : 'DisapproveAll'},            
            dataType : 'json',
            beforeSend: function(){
            },            
            success : function (response)
            {   
                if(response.detail === 'Success' && response.httpStatus === 200) //updated OK
                {  
                    var rows = $("#phrases-table").dataTable().fnGetNodes();                 
                    for(var i=0;i<rows.length;i++)
                    {
                        var tr = $(rows[i]);                        
                        var text = tr.find("td:eq(4)").find("text").html();
                        if(text !== 'Pending')
                        {                            
                            //access approval status column and update content
                            //cells.push($(rows[i]).find("td:eq(4)").html('<text class="label label-pill label-primary">Approved</text>'));
                            $('#phrases-table').dataTable().fnUpdate( '<text class="label label-pill label-danger">Pending</text>', rows[i], 4);
                        }
                    }
                    showApprovalStatusButtons(true);
                    hide_updating_message();   
                    //show_message("Phrases approval status updated successfully.", 'success');
                    show_success_label("Phrases approval status updated successfully");
                    
                    // clear approval status options list
                    //update approval text column filter                    
                    $("#phrases-table tfoot th:eq(4)").find('select').children().remove();
                    $("#phrases-table tfoot th:eq(4)").find('select').append('<option value=""></option><option value="Pending">Pending</option>');                    
                }
                else if(response.detail === 'Error'){ // NOT updated                    
                    hide_updating_message();
                    //show_message('Update request failed', 'error'); 
                    show_failure_label("Update request failed");
                }
            },
            error:function(response) 
            {              
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);  
                var message = json.detail;        
                hide_loading_message();
                show_message('Update request failed: ' + message, 'error'); 
                show_failure_label("Update request failed: " + message);
            }
        });           
        }
        
    }
});      
});     
   
// show/hide approve-all/disapprove-all buttons   
function showApprovalStatusButtons(isPending)
{    
    if(isPending === false){                      
        $('#approvedDiv').hide();        
        $('#disapprovedDiv').show();
        //$("#disapproveAllBtn").attr("disabled", false);                
    }else if(isPending === true ){
       
       $('#approvedDiv').show();
       $('#disapprovedDiv').hide();
       //$("#approveAllBtn").attr("disabled", false);              
    }
}

// Update approveall/disapprove all visibility
function updateButtons()
{
    <c:if test="${isAllApproved == true}">
        $('#approvedDiv').hide();    
    </c:if>
    <c:if test="${isAllPending == true}">
        $('#disapprovedDiv').hide();
    </c:if>        
}

// show phrase edit failure element
function show_phraseChangedFailure()
{
    $('#phraseChangedFailure').show();
    $("#phraseChangedFailure").fadeTo(3000, 500).slideUp(500, function(){
    $("#phraseChangedFailure").slideUp(500); });     
}

// hide phrase edit failure element
function hide_phraseChangedFailure()
{
    $('#phraseChangedFailure').hide();
}
    
updateButtons();

hide_phraseChangedFailure();

function populateApprovalOptions()
{
    var cells = [];
    var rows = $("#phrases-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find("td:eq(4) text").html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#phrases-table tfoot th:eq(4) select").append('<option value="'+html+'">'+html+'</option>');            
        }
    }
}

function resetApprovalOptions()
{
    $("#phrases-table tfoot th:eq(4)").find('select').children().remove();
    $("#phrases-table tfoot th:eq(4)").find('select').append('<option value=""></option>');
    var cells = [];
    var rows = $("#phrases-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find("td:eq(4) text").html();
        if(cells.indexOf(html) === -1)
        {            
            cells.push(html);                         
            $("#phrases-table tfoot th:eq(4) select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
    console.log(cells);
    
    if(cells.length === 1)
    {
        if(cells[0] === "Approved")
        {
            $('#approvedDiv').hide(); 
            $('#disapprovedDiv').show(); 
        }
        else if(cells[0] === "Pending")
        {
            $('#disapprovedDiv').hide(); 
            $('#approvedDiv').show(); 
        }       
    }
    else if(cells.length === 2)
    {
        
        $('#approvedDiv').show();
        $('#disapprovedDiv').show();
    }

}

function populateFilterColumnOptions(column)
{
    var cells = [];
    var rows = $("#phrases-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find('td:eq('+column+') text').html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#phrases-table tfoot th:eq("+column+") select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

function resetColumnFiltersPlainTdOnly(column)
{
    $("#phrases-table tfoot th:eq("+column+")").find('select').children().remove();
    $("#phrases-table tfoot th:eq("+column+")").find('select').append('<option value=""></option>');
    var cells = [];
    var rows = $("#phrases-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find('td:eq('+column+')').html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#phrases-table tfoot th:eq("+column+") select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

        
  
}); // end document ready  



</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="container" style="width:1400px;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span></a>
          <!-- <a class="brand" href="/complete-it/web/secured/profiles">Complete it</a> -->

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
      
    <ul class="nav navbar-nav" style="margin-top: 5px;font-size: initial;">
    <li><a href="/complete-it/web/secured/dashboard">Dashboard</a></li> 
    <li><a href="/complete-it/web/secured/profiles">Profiles</a></li> 
    <li><a href="/complete-it/web/secured/categories">Categories</a></li> 
    <li class="active"><a href="#">Phrases</a></li>
    <li><a href="/complete-it/web/secured/answers">Answers</a></li> 
    </ul>      
      
    </div>
  </div>
</div>   

<div class="container">
<br>
<div class="panel panel-default">
    <div class="panel-heading fa-2x">Phrases</div>
    <div class="panel-body">    
    
    
<!-- <h1>Phrases</h1> -->
<br>
  <!-- ADD PHRASE -->
    <form id="myForm" class="movie-input-form form-inline" action="phrases" method="post" name="addForm">
    <!-- <div id="addition" class="add-item-box"> -->
    <p><b>New Phrase</b></p>     
    
    <!-- pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" -->
    <input id="myPhraseText" type="text" name="text" placeholder="Phrase text" size="70" required="true"            
        pattern="(^\s*[^\[\]\s]+(\s+[^\[\]\s]+)*(\s+\[\])+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)|(^\s*(\[\])(\s+\[\])*(\s+[^\[\]\s]+)+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)"
        title="Use format: word [] [] word etc." style="height:30px; width: 250px; margin: auto;"/>             
    <select name="category" required="true" id="categoryId" class="choices" style="margin: auto;">
        <!-- <option value="id">Title</option> -->
        <option value="">-- Select Category --</option>
        <c:forEach items="${categories}" var="category">
          <option value="${category.id}">${category.name}</option>
        </c:forEach>
       <!-- 
        <option value="0">Geography</option>
        <option value="2">Transportation</option>
        <option value="3">Leisure</option>
        <option value="4">Calculations</option>
        <option value="5">Traffic</option>
        <option value="6">Prices</option>
        <option value="7">Computer</option>
        <option value="8">Super Market</option>
        <option value="9">Random</option>
        <option value="10">Shopping</option>
        <option value="11">Women</option>
        <option value="12">Money</option>          
        <option value="13">Financial</option>        
        <option value="14">Home</option>
       -->                   
    </select>    
    <input id="addBtn" type="submit" name="action" class="btn btn-primary" value="Add" style="padding: 4px 18px; position: absolute;
    margin-left: 5px;"/>
  </form>
<!-- </div> -->
  
  <!-- phrase insertion success message -->
  <div class="container" id="addSuccess" style="width:initial; padding-right:initial; padding-left:initial;" hidden="true">
  <div class="alert alert-success fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
    <strong id="additionSuccessMsg"> Phrase inserted successfully!</strong>
  </div>
  </div>

  <!-- phrase deletion error message -->
  <div class="container" id="phraseActionFailure" style="width:initial; padding-right:initial; padding-left:initial;" hidden="true">
  <div class="alert alert-danger fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
      <strong id="failureWarningMessage"> Phrase cannot be deleted because it is referenced in records.</strong>
  </div>
  </div>  

    <!-- hr -->
    <hr>  
  
    <!-- Approve status container -->    
    <div id="ApprovalStatusBtnContainer">
    <div id="approvedDiv">
        <input id="approveAllBtn" type="submit" name="action" class="btn btn-success" value="Approve All" style="margin-bottom: 15px; float:left"/>            
    </div>
    <div id="disapprovedDiv">
        <input id="disapproveAllBtn" type="submit" name="action" class="btn btn-danger" value="Disapprove All" style="margin-left:10px; margin-bottom: 15px;"/>            
    </div>
    </div>
        
    <!-- PHRASES DATA TABLE -->      
    <table id="phrases-table" class="table table-striped table-bordered">
    <thead>
    <tr>
      <th>Id</th>
      <th>Text</th>
      <th>Gaps</th>
      <th>Category</th>
      <th>Approval Status</th>
      <th>Added by</th>
      <th>&nbsp;</th>      
    </tr>
    </thead>
    <tfoot>
        <tr>    
        <th></th>
        <th></th>        
        <th></th>
        <th>Category</th>
        <th>Approval Status</th>
        <th>Added by</th>
        <th></th>
        </tr>
    </tfoot>    
    
    <tbody>
    <c:forEach items="${phrases}" var="phrase">
      <tr>
        <td><c:out value="${phrase.id}"/></td>
        <td><c:out value="${phrase.phrase}"/></td>      
        <td><c:out value="${phrase.gaps}"/></td>        
        <td><c:out value="${phrase.categories}"/></td>
        <td><c:out value="${phrase.status}"/></td>
        <%--
        <c:if test="${phrase.isApproved == 'Approved'}">
            <td><text class="label label-pill label-success"><c:out value="${phrase.isApproved}"/></text></td>       
        </c:if> 
        <c:if test="${phrase.isApproved =='Pending'}">
            <td><text class="label label-pill label-danger"><c:out value="${phrase.isApproved}"/></text></td>       
        </c:if>
        --%>    
        <%-- <td><text class="label label-pill label-primary"><c:out value="${phrase.isApproved}"/></text></td> --%> <!-- label label-pill label-warning -->
        <td><c:out value="${phrase.addingProfileEmail}"/></td>
        
        
        <!-- <td><a href="?action=Delete&id=${phrase.id}" class="deleteBtn"><i class="icon-trash"></i></a></td>-->
        <td class="actions"><a href="#" class="deleteBtn" title="Delete"><span class="fa-stack"><i class="icon-trash"></i></span></a>
            <a href="#" class="editBtn" title="Edit"><span class="fa-stack"><i class="icon-pencil"></i></span></a></td>        
               
      </tr>
    </c:forEach>
    </tbody>
  </table>
    <hr>
    <div id="footer_console">
        Complete-it v1.0 - Admin Console
    </div>    
    
  
      
</div>    
</div>


<!-- edit lightbox container-->
    <div class="lightbox_bg"></div>

    
    <div class="lightbox_container">
      <div class="lightbox_close"></div>
      <div class="lightbox_content">
        
        <h2>Edit phrase</h2>
        <form class="form add" id="form_phrase" data-id="" method="post">
          <div class="input_container">
            <label for="rank">Id: <span class="required"></span></label>
            <div class="field_container">
              <input type="number" step="1" min="0" class="text" name="id" id="id" value="" readonly>              
            </div>
          </div>
          <div class="input_container">
            <label for="company_name">Text: <span class="required">*</span></label>
            <div class="field_container">
              <input type="text" class="text" name="phrase" id="phrase" value="" required="true" 
                pattern="(^\s*[^\[\]\s]+(\s+[^\[\]\s]+)*(\s+\[\])+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)|(^\s*(\[\])(\s+\[\])*(\s+[^\[\]\s]+)+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)" title="Use format: word [] [] word etc.">
            </div>
          </div>
          <div class="input_container">
            <label for="industries">Gaps: <span class="required"></span></label>
            <div class="field_container">
              <input type="text" class="text" name="gaps" id="gaps" value="" readonly>
            </div>
          </div>
          <div class="input_container">
            <label for="revenue">Category: <span class="required">*</span></label>
            <div class="field_container">              
                <select name="categories" required="true" id="categories" class="choices" style="margin: auto;">                    
                    <!-- <option value="" id="category" name="category">-- Select Category --</option> -->
                </select>
            </div>
          </div>
          <div class="input_container">
            <label for="fiscal_year">Approval status: <span class="required"></span></label>
            <div class="field_container">              
                <select name="approvalStatus" required="true" id="approvalStatus" class="choices" style="margin: auto;">                    
                    <!-- <option value=1>Pending</option>
                    <option value=0>Approved</option>
                    -->
                </select>              
            </div>
          </div>
          <div class="input_container">
            <label for="employees">Added by Profile: <span class="required"></span></label>
            <div class="field_container">
              <input type="text" class="text" name="addedByProfile" id="addedByProfile" value="" readonly>
            </div>
          </div>
         
         <!-- error area -->   
         <div class="input_container">            
            <!-- phrase text changed error message -->
            <div class="container" id="phraseChangedFailure" style="width:initial; padding-right:initial; padding-left:initial;">
            <div class="alert alert-danger fade in">
                <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
                <strong id="phraseChangedMessage"> Phrase gaps cannot be modified because it is referenced in answer records.</strong>
            </div>
            </div>  
          </div>            
            
          <div class="button_container">
            <button type="submit" class="updateBtn">Update</button>
          </div>
        </form>
        
      </div>
    </div>


<!-- MESSAGE container-->
<div id="message_container">
      <div id="message" class="success">
        <p>This is a success message.</p>
      </div>
</div>

<!-- EDIT loading container-->
<div id="loading_container">
      <div id="loading_container2">
        <div id="loading_container3">
          <div id="loading_container4">
            Loading, please wait...
          </div>
        </div>
      </div>
</div>


</div>
<!-- /container -->




</body>
</html>
