<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : AdminAccount
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Complete it - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="/complete-it/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <style>
    body {
      padding-top: 60px;      
      /* 60px to make the container go all the way to the bottom of the topbar */
    }
  </style>


  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>

$(document).ready(function(){

  $("#addBtn").attr('disabled', 'disabled');
  
  $(".choices").change(function()
  {    
    if($("#categoryId").val() === '0') {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
      var isEmptyPhrase = $('#myPhraseText').val().replace(/^\s+|\s+$/g, "").length === 0;
      if(!isEmptyPhrase){  
        $("#addBtn").removeAttr('disabled');
      }
    }
  });
  
$('#myPhraseText').change(function () {    
    if($(this).val().replace(/^\s+|\s+$/g, "").length === 0)   
    {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
        var isCategoryUnselected = $("#categoryId").val() === '0';        
        if(!isCategoryUnselected){
            $("#addBtn").removeAttr('disabled');
        }
    }    
});  

$("#addBtn").click(function()
{
   $("#addSuccess").hide(); 
});
$("#prevLink").click(function()
{
   $("#addSuccess").hide(); 
});
$("#nextLink").click(function()
{
   $("#addSuccess").hide(); 
});

});

$(document).ready(function() {
    $('#example').DataTable({
      
      // disable sorting button in last column
      "aoColumns" : [
      null,
      null,
      null,
      null,      
      {'bSortable' : false}        
      ]  
    });
});



</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse"
         data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span>
      </a> <a class="brand" href="#">Complete it</a>

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
    </div>
  </div>
</div>

<div class="container">

  <!-- ADD PHRASE -->
  <h1>Phrases - Console</h1>
  <form id="myForm" class="movie-input-form form-inline" action="phrases" method="post" name="addForm">
    <p><b>Add Phrase</b></p>
    
    <input id="myPhraseText" type="text" name="text" placeholder="Phrase text" size="70" required="true" 
           pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" 
           title="Use format: word [] [] word etc." style="height:30px; width: 250px"/>    
     
    <select name="category" required="true" id="categoryId" class="choices">
        <!-- <option value="id">Title</option> -->
        <option value="0">-- Select Category --</option>
        <option value="1">Geography</option>
        <option value="2">Transportation</option>
        <option value="3">Leisure</option>
        <option value="4">Calculations</option>
        <option value="5">Traffic</option>
        <option value="6">Prices</option>
        <option value="7">Computer</option>
        <option value="8">Super Market</option>
        <option value="9">Random</option>
        <option value="10">Shopping</option>
        <option value="11">Women</option>
        <option value="12">Money</option>          
        <option value="13">Financial</option>        
        <option value="14">Home</option>                    
    </select>    
    <input id="addBtn" type="submit" name="action" class="btn btn-primary" value="Add" style="padding: 4px 18px;"/>
  </form>

  <!-- insertion success message -->
 
  <c:if test="${isInserted == 1}">
  <div class="alert alert-success fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right:0px">&times;</a>
    <strong> Phrase inserted successfully!</strong>
  </div>
</c:if>
  
    <!-- PHRASES DATA TABLE -->
    
    <c:if test="${count < -1}">
    <c:if test="${page < 0}">
        <a id="prevLink" href="<c:url value="phrases"><c:param name="page" value="${page - 1}"/><c:param name="field" value="${field}"/>
        <c:param name="key" value="${key}"/></c:url>">&lt; Prev</a>&nbsp;
    </c:if>
    <!-- Showing records ${start} to ${end} of ${count} -->
    <c:if test="${page < -1}">
      &nbsp;<a id="nextLink" href="<c:url value="phrases"><c:param name="page" value="${page + 1}"/><c:param name="field" value="${field}"/><c:param name="key"                                                                                                                                        value="${key}"/></c:url>">Next &gt;</a>    </c:if>
  </c:if>
      
  <table id="phrases-table" class="table table-striped table-bordered">
    <thead>
    <tr>
      <th>Id</th>
      <th>Text</th>
      <th>Gaps</th>
      <th>Category</th>
      <th>Approval Status</th>
      <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${phrases}" var="phrase">
      <tr>
        <td><c:out value="${phrase.id}"/></td>
        <td><c:out value="${phrase.phrase}"/></td>
        <td><c:out value="${phrase.gaps}"/></td>        
        <td><c:out value="${phrase.categories}"/></td>
        <td><c:out value="${phrase.isApproved}"/></td>
        <td><a href="?action=Remove&id=${phrase.id}"><i class="icon-trash"></i></a></td>
      </tr>
    </c:forEach>
    </tbody>
  </table>

      
</div>
<!-- /container -->
</body>
</html>
