<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : ic
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Guess-my-answer - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css"/>
  <link href="/complete-it/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>  
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>  
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>          
  <link rel="stylesheet" href="http://cdn.css.net/libs/startbootstrap-sb-admin-2/1.0.5/css/sb-admin-2.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/complete-it/assets/css/sb-admin-2.min.css"/>
<style>
    body {
          padding-top: 60px;      
          /* 60px to make the container go all the way to the bottom of the topbar */               
    }

    .navbar .nav .active > a
    {
        /* background-color: skyblue; */
    
    } 
    
    .navbar-fixed-top
    {
        border-bottom-width: 3px;
        border-color: skyblue;
        background-color: #1b1b2b; 
        box-shadow: 0 6px 6px -6px skyblue
    }   
    
    .container {
        width: 1400px;
    }   

    .list-group-item>.badge 
    {
        float: none; margin-left: 5px;
    }
    
  </style>


  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
<script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="/complete-it/assets/js/jquery.canvasjs.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">



$(document).ready(function(){

    
  $("#addBtn").attr('disabled', 'disabled');
  
  $(".choices").change(function()
  {    
    if($("#categoryId").val() === '0') {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
      var isEmptyPhrase = $('#myPhraseText').val().replace(/^\s+|\s+$/g, "").length === 0;
      if(!isEmptyPhrase){  
        $("#addBtn").removeAttr('disabled');
      }
    }
  });
  
$('#myPhraseText').change(function () {    
    if($(this).val().replace(/^\s+|\s+$/g, "").length === 0)   
    {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
        var isCategoryUnselected = $("#categoryId").val() === '0';        
        if(!isCategoryUnselected){
            $("#addBtn").removeAttr('disabled');
        }
    }    
});  

$("#addBtn").click(function()
{
   $("#addSuccess").hide(); 
});
$("#prevLink").click(function()
{
   $("#addSuccess").hide(); 
});
$("#nextLink").click(function()
{
   $("#addSuccess").hide(); 
});

    // hide add  form
    $("#myForm").attr('hidden', 'hidden'); 

});

google.charts.load('current', {packages: ['corechart','bar']});
google.charts.load('current', {packages: ['bar']});
doPostAjax();

function doPostAjax() {
  $.ajax({
    url: '/complete-it/web/secured/dashboard',
    type: 'POST',
    dataType : 'json',    
    context: document.body,
    success: function(response) {
      var trimData = $.trim(JSON.stringify(response));                          
      var obj = $.parseJSON(trimData);       
      $('#invites').text(" "+ obj.invites + " Invites");
      $('#shares').text(" "+ obj.shares + " Shares");
      $('#reported').text(" "+ obj.reported + " Reports");
      $('#liked').text(" "+ obj.liked + " Likes");
      $('#answered').text(" "+ obj.answered + " Phrases answered");      
      $('#profiles').html(obj.profiles);      
      $('#categories').html(obj.categories);      
      $('#phrases').html(obj.phrases);      
      $('#answers').html(obj.answers);  
      
      //max vAxisMax
      var maxReported = obj.maxReported;
      var maxUnreported = obj.maxUnreported;
      var maxAnswered = obj.maxAnswered;
      var maxUnanswered = obj.maxUnanswered;      
      var maxVaxisAnswers = maxReported >= maxUnreported ? maxReported : maxUnreported;
      var maxVaxisPhrases = maxAnswered >= maxUnanswered ? maxAnswered : maxUnanswered;
      
      // Populate phrases in categories series
      var categoriesSeries = [];        
        for (i = 0; i < obj.categoryPhrases.length; i++){
            categoriesSeries[i] = {                
                label: obj.categoryPhrases[i].categoryName,
                y: obj.categoryPhrases[i].percentage,
                legendText: obj.categoryPhrases[i].categoryName,
                exploded: obj.categoryPhrases[i].highest
            };
        }      
      
      // populate answers in categories series
      //['Geography', 6, 2],
      //{ y: 166, label: "Geography"},
      var categoryAnswersSeries = [];
      var reportedSeries = [];
      var unreportedSeries = [];      
      for (i = 0; i < obj.categoryAnswers.length; i++){
            categoryAnswersSeries[i] = [                
                obj.categoryAnswers[i].name,
                obj.categoryAnswers[i].reported,
                obj.categoryAnswers[i].unreported
      ];
                        
        reportedSeries[i] = { y: obj.categoryAnswers[i].reported, label: obj.categoryAnswers[i].name };            
        unreportedSeries[i] = { y: obj.categoryAnswers[i].unreported, label: obj.categoryAnswers[i].name };
      }      
        
      // populate phrases in categories series
      //['Geography', 6, 2],
      //{ y: 166, label: "Geography"},
      var categoryPhrasesSeries = [];
      var answeredSeries = [];
      var unansweredSeries = [];
      
        for (i = 0; i < obj.categoryPhraseStats.length; i++){
            categoryPhrasesSeries[i] = [                
                obj.categoryPhraseStats[i].name,
                obj.categoryPhraseStats[i].answered,
                obj.categoryPhraseStats[i].unanswered
            ];
                        
            answeredSeries[i] = { y: obj.categoryPhraseStats[i].answered, label: obj.categoryPhraseStats[i].name };            
            unansweredSeries[i] = { y: obj.categoryPhraseStats[i].unanswered, label: obj.categoryPhraseStats[i].name };
        }         
           
google.charts.setOnLoadCallback(onLoadChartsCallBack(categoryPhrasesSeries,maxVaxisPhrases,categoryAnswersSeries,maxVaxisAnswers)); 
    
// create Reported Answers Chart
$("#ReportedChart").CanvasJSChart({ 
                        title: { 
                                //text: "Reported Answers",
                                //fontSize: 24
                        }, 
                        axisY: { 
                                title: "Reported in %" 
                        }, 
                        legend :{ 
                                verticalAlign: "center", 
                                horizontalAlign: "right" 
                        }, 
                        data: [ 
                        { 
                                type: "doughnut", 
                                showInLegend: true, 
                                toolTipContent: "{label} <br/> {y} %", 
                                indexLabel: "{y} %", 
                                dataPoints: [ 
                                        { label: "Reported",  y: obj.reportedAnswers.reported, legendText: "Reported", color: "#d95f02"}, //"#b52b27"
                                        { label: "Unreported",    y: obj.reportedAnswers.unreported, legendText: "Unreported" , color: "#12bb6b" } //"#57a957"

                                ] 
                        } 
                        ] 
        });
        
// create Phrases answered chart
$("#AnsweredPhrasesChart").CanvasJSChart({ 
		title: { 
			//text: "Phrases answered",
			//fontSize: 24
		}, 
		axisY: { 
			title: "Reported in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right" 
		}, 
		data: [ 
		{ 
			type: "doughnut", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Answered",  y: obj.phrasesAnswered.answered, legendText: "Answered", color: "#33aaf3"}, 
				{ label: "Unanswered",    y: obj.phrasesAnswered.unanswered, legendText: "Unanswered" , color: "#ae62c4" }

			] 
		} 
		] 
});

//CategoryPhrasesChart
$("#CategoryPhrasesChart").CanvasJSChart({ 
		title: { 
			//text: "Phrases in Categories",
			//fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: categoriesSeries
		} 
		] 
});    
    },
    complete: function()
    {
        setTimeout(doPostAjax, 30000); // refresh content every 30 minutes.         
    },              
    error:function(responseText) {
       
    }        
  });
  
}


function onLoadChartsCallBack(phrasesMultiSeries,v1AxisMax, answersMultiSeries,v2AxisMax)
{
    drawPhrasesMultiSeries(phrasesMultiSeries,v1AxisMax);
    drawAnswersMultiSeries(answersMultiSeries,v2AxisMax);
}

// Draw MultiSeries Column Chart
function drawAnswersMultiSeries(rows,vAxisMax)
{
    
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Category');
      data.addColumn('number', 'Reported');
      data.addColumn('number', 'Unreported');

      for (i = 0; i < rows.length; i++){
            data.addRows([                
                rows[i]                
            ]);            
        } 
      
        var options = {
          chart: {
            title: 'Answers Per Category',
            subtitle: 'Reported & Unreported'
          },
          bars: 'vertical',
          vAxis: {
              format: 'decimal', title: 'Answers', max: vAxisMax 
          },
          hAxis: {
            textStyle : {fontSize: 10 }
          },
          legend: {position : "right"},
          bar: { groupWidth: '50%' },
          height: 450,
          //width: 1300,
          colors: ['#d95f02','#12bb6b']
        };      
        var chart = new google.charts.Bar(document.getElementById('AnswersMultiColumnChart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));      
    }
    
// Draw Phrases MultiSeries Column Chart
function drawPhrasesMultiSeries(rows,vAxisMax)
{
    
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Category');
      data.addColumn('number', 'Answered');
      data.addColumn('number', 'Unanswered');

      for (i = 0; i < rows.length; i++){
            data.addRows([                
                rows[i]                
            ]);            
        } 
      
        var options = {
          chart: {
            title: 'Phrases Per Category',
            subtitle: 'Answered & Unanswered'
          },
          bars: 'vertical',
          vAxis: {
              format: 'decimal', title: 'Phrases', max: vAxisMax 
          },
          hAxis: {
            textStyle : {fontSize: 10 }
          },
          legend: {position : "right"},
          bar: { groupWidth: '50%' },
          height: 450,
          //width: 1300,
          colors: ['#33aaf3','#ae62c4']
        };      
        var chart = new google.charts.Bar(document.getElementById('PhrasesMultiColumnChart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));      
}

</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="container" style="width:1400px;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span></a>
          <!-- <a class="brand" href="/complete-it/web/secured/profiles">Home</a> -->

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
      
    <ul class="nav navbar-nav" style="margin-top: 5px;font-size: initial;">
    <li class="active"><a href="#">Dashboard</a></li>        
    <li><a href="/complete-it/web/secured/profiles">Profiles</a></li> 
    <li><a href="/complete-it/web/secured/categories">Categories</a></li> 
    <li><a href="/complete-it/web/secured/phrases">Phrases</a></li>
    <li><a href="/complete-it/web/secured/answers">Answers</a></li>     
    </ul>      
      
    </div>
  </div>
</div>
    
    
<div class="container">

  <!-- ADD PHRASE -->
  <!-- <h1>Profiles</h1> -->
  <br>
  <form id="myForm" class="movie-input-form form-inline" action="phrases" method="post" name="addForm">
    <p><b>Add Phrase</b></p>
      

    <input id="myPhraseText" type="text" name="text" placeholder="Phrase text" size="70" required="true" 
        pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" 
        title="Use format: word [] [] word etc." style="height:30px; width: 250px"/>     
        
    <select name="category" required="true" id="categoryId" class="choices">
        <!-- <option value="id">Title</option> -->
        <option value="0">-- Select Category --</option>
        <option value="1">Geography</option>
        <option value="2">Transportation</option>
        <option value="3">Leisure</option>
        <option value="4">Calculations</option>
        <option value="5">Traffic</option>
        <option value="6">Prices</option>
        <option value="7">Computer</option>
        <option value="8">Super Market</option>
        <option value="9">Random</option>
        <option value="10">Shopping</option>
        <option value="11">Women</option>
        <option value="12">Money</option>          
        <option value="13">Financial</option>        
        <option value="14">Home</option>                    
    </select>    
    <input id="addBtn" type="submit" name="action" class="btn btn-primary" value="Add" style="padding: 4px 18px;"/>
  </form>

  <!-- insertion success message -->
  <div class="container" id="addSuccess" hidden="true">
  <div class="alert alert-success fade in">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> Phrase inserted successfully!</strong>
  </div>
  </div>
  
<!-- Dashboard Panels -->
<div class="panel panel-default">
    <div class="panel-heading fa-lg">Statistics</div>
<div class="panel-body">
    <br>
<div class="row">
    
<div class="col-lg-6">    
    
                <div class="row">   
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <%-- <div class="huge"><c:out value="${profiles}"/></div> --%>
                                    <div id="profiles" class="huge"></div>
                                    <div>Profiles</div>
                                </div>
                            </div>
                        </div>
                        <a href="/complete-it/web/secured/profiles">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>    
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bars fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <%-- <div class="huge"><c:out value="${categories}"/></div> --%>
                                    <div id="categories" class="huge"></div>
                                    <div>Categories</div>
                                </div>
                            </div>
                        </div>
                        <a href="/complete-it/web/secured/categories">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                </div>
                                    
                <div class="row">                                    
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-question-circle-o fa-5x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <%-- <div class="huge"><c:out value="${phrases}"/></div> --%>
                                    <div id="phrases" class="huge"></div>
                                    <div>Phrases</div>
                                </div>
                            </div>
                        </div>
                        <a href="/complete-it/web/secured/phrases">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-pencil-square fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <%-- <div class="huge"><c:out value="${answers}"/></div> --%>
                                    <div id="answers" class="huge"></div>
                                    <div>Answers</div>
                                </div>
                            </div>
                        </div>
                        <a href="/complete-it/web/secured/answers">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                </div>
</div>
    
<!-- TOTALS Section -->                                    
<div class="col-lg-6"> 
<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-info-circle"></i>Totals</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <!-- <span class="badge">my invites</badge> -->
                                            <i class="fa fa-fw fa-envelope" ></i><text id="invites" class="badge">Invites</text>                                            
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <!-- <span class="badge">my invites</badge> -->
                                        <i class="fa fa-fw fa-share-alt"></i> <text id="shares" class="badge">Shared</text>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <!-- <span class="badge">my invites</badge> -->
                                        <i class="fa fa-fw fa-ban"></i> <text id="reported" class="badge">Reports</text>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <!-- <span class="badge">my invites</badge> -->
                                        <i class="fa fa-fw fa-thumbs-up"></i> <text id="liked" class="badge">Likes</text>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <!-- <span class="badge">my invites</badge> -->
                                        <i class="fa fa-fw fa-pencil-square"></i> <text id="answered" class="badge">Phrases answered</text>
                                    </a>
                                </div>
                            </div>
                        </div>    
</div>
                                    
                                    
</div>    
</div>
</div>  <!-- end panel info --> 

  
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
        <div class="panel-heading fa-lg">Phrases in Categories</div>
        <div class="panel-body">         
        <div id="CategoryPhrasesChart" style="height: 300px; width: 100%;"></div>
        </div>
        <a href="/complete-it/web/secured/phrases">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
            </div>
        </a>          
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
        <div class="panel-heading fa-lg">Reported Answers</div>
        <div class="panel-body">         
        <div id="ReportedChart" style="height: 300px; width: 100%;"></div>
        </div>
        <a href="/complete-it/web/secured/answers">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
            </div>
        </a>        
        </div>
    </div>        
    <div class="col-lg-4">
        <div class="panel panel-default">
        <div class="panel-heading fa-lg">Phrases answered</div>
        <div class="panel-body">         
        <div id="AnsweredPhrasesChart" style="height: 300px; width: 100%;"></div>
        </div>
        <a href="/complete-it/web/secured/phrases">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
            </div>
        </a>          
        </div>
    </div>         

</div>
        
<div class="panel panel-default">
<div class="panel-heading fa-lg">Multiseries in Categories</div>    
<div class="panel-body">  

<!-- 
<div class="col-lg-12">   
<div id="AnswersRangeChart" style="height:350px; width: 100%;"></div> 
</div>    
    
<div class="col-lg-12">   
<div id="PhrasesDualColumnChart" style="height:450px; width: 100%;"></div> 
</div>     
-->
 
<div class="col-lg-12">   
<div id="AnswersMultiColumnChart"></div> 
</div> 
  
<div class="col-lg-12">   
<hr>      
<div id="PhrasesMultiColumnChart"></div> 
</div>     


</div>
</div>  





    <hr>
    <div id="footer_console">
        Guess-my-Answer v1.0 - Admin Console
    </div>   
    
</div>
<!-- /container -->
</body>
</html>
