<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : AdminAccount
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Complete it - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->  
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">
  <link href="/complete-it/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">  
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">  
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">  
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/complete-it/assets/css/layout.css"/> 

  <style>
    body {
      padding-top: 60px;      
      /* 60px to make the container go all the way to the bottom of the topbar */
    }
    
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
        
        margin-left: auto;
        margin-right: auto;
    }

    .modal-body{
        padding: 25px;
    }
    .modal-dialog {
        
        width: 500px;
        margin: 30px auto;
        
    }
    .bootbox {
        background: none;
    }
    
    backdrop.fade.in {
        opacity: 0.6; 
    }   
    
    #categoryId{
        margin-top: 0px;
    }
    
    .navbar .nav .active > a
    {
        /* background-color: skyblue; */

    }  
    
    .navbar-fixed-top
    {
        border-bottom-width: 3px;
        border-color: skyblue;
        background-color: #1b1b2b; 
        box-shadow: 0 6px 6px -6px skyblue
    }
        
    .container {
        width: 1400px;
    } 
    
    td.actions {
        text-align: center;
    }    
        
  </style>
  
<script   src="https://code.jquery.com/jquery-3.1.1.js"
    integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" 
    crossorigin="anonymous"></script>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="/complete-it/assets/js/bootstrap-filestyle.js"></script>
<script src="/complete-it/assets/js/bootstrap-filestyle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.js"></script>

<script>

$(document).ready(function(){

// Hide loadind message at startup
hide_loading_message();


$(".choices").change(function()
  {    
    if($("#categoryId").val() === '0') {
        $("#addBtn").attr('disabled', 'disabled');
    }
    else
    {    
      var isEmptyPhrase = $('#myCategoryName').val().replace(/^\s+|\s+$/g, "").length === 0;
      if(!isEmptyPhrase){  
        $("#addBtn").removeAttr('disabled');
      }
    }
});


$("#addBtn").click(function()
{
   $("#addSuccess").hide(); 
});
$("#prevLink").click(function()
{
   $("#addSuccess").hide(); 
});
$("#nextLink").click(function()
{
   $("#addSuccess").hide(); 
});

$('#categories-table').DataTable(
{
    // disable sorting button in last column
    "aoColumnDefs" : [
        { "bSortable": false, "aTargets": [5]},
        {"sWidth": "5%", "aTargets": [0]},
        {"sWidth": "12%", "aTargets": [1]},
        {"sWidth": "20%", "aTargets": [2]},
        {"sWidth": "25%", "aTargets": [3]},
        {"sWidth": "auto", "aTargets": [4]},
        {"sWidth": "6%", "aTargets": [5]}        
    ],
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).attr("data-categoryid",aData[0]);          
        return nRow;
    },
    "fnDrawCallback" : function(){
        $('.deleteBtn').off('click').on('click',deleteCategory);
        $('.editBtn').off('click').on('click',editCategory);
    },
    "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
    "autoWidth": false,
    "createdRow": function ( row, data, index ) {
        $('td', row).eq(5).addClass('actions');
    }    
});

//INDIVIDUAL DataTable column filtering - select inputs
var table = $('#categories-table').DataTable(); 
$("#categories-table tfoot th").each( function ( i ) 
{    
    //select which headers should include filters
    if(i !== 0 && i !== 1 && i !== 3 && i !== 4 && i !== 5  ){
    var select = $('<select class="filtered-select"><option value=""></option></select>')
        .appendTo( $(this).empty() )
        .on( 'change', function () {
            table.column( i )
            .search( $(this).val() )
            .draw();
        });                
                
        table.column( i ).data().sort().unique().each( function ( d, j )
        {                           
            //if(i !== 4) // do not include column 4 yet - approval status text contains text element- process after this.
            //{
                select.append('<option value="'+d+'">'+d+'</option>');
            //}            
        });
}
});

// UNBIND DELETE click event
$('.deleteBtn').off('click');

//$('#categories-select').hide();
$('#addSuccess').hide();
$('#categoryActionFailure').hide();

$('.close').click(function() {
    $('#categoryActionFailure').hide();
    $('#addSuccess').hide();
    $('#categoryChangedFailure').hide();    
}); 
    
    
// Variable to store your files
var files;

// Add events
$("input[type='file']").on('change', prepareUpload);

// Grab the files and set them to our variable
function prepareUpload(event)
{
    files = event.target.files;
    $("#addBtn").prop({"disabled":false});
} 
   
 
// DELETE Phrase click EVENT
$('.deleteBtn').click(deleteCategory);  // end delete click
    
// DELETE Phrase function    
function deleteCategory(event){    
        
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var categoryId = trElem.attr("data-categoryid");    
    var id = $.trim($(trElem).children("td:first").text());      
    var message = '';   
    var phraseRow = trElem;
    
    // delete CONFIRMATION
    bootbox.dialog({
      message: "Are you sure?",
      title: "Category deletion",
      buttons: {
        danger: {
          label: "No",
          className: "btn-danger",
          callback: function() {
            //do something
          }
        },
        main: {
          label: "Yes",
          className: "btn-success",
          callback: function() {

            $.ajax({
                url : 'categories?id='+categoryId,
                type: 'DELETE',                
                dataType : 'html',
                beforeSend: function(){               
                },            
                success : function (responseText){
                    if(responseText === 'deleted'){

                        $('#categories-table').dataTable().fnDeleteRow(phraseRow);                    
                        message = 'Category with id <'+ id + '> deleted successfully!';
                        $('#additionSuccessMsg').html(message);
                        $('#addSuccess').show();                                         
                        $("#addSuccess").fadeTo(2000, 500).slideUp(500, function(){
                        $("#addSuccess").slideUp(1000); });

                        // re-draw datatable
                        $("#categories-table").dataTable().fnDraw();
                        
                        // reset column filter data in culture column
                        resetColumnFiltersPlainTdOnly(2);

                    }
                    else if(responseText === 'denied'){
                        message = 'Cannot delete category with id <'+ id + '> because it is referenced in records.';
                        $('#failureWarningMessage').html(message);                
                        $('#categoryActionFailure').show();                    
                        $("#categoryActionFailure").fadeTo(2000, 500).slideUp(500, function(){
                            $("#categoryActionFailure").slideUp(1000);                    
                        });
                    }
                },
                error:function(responseText) {
                    $('#failureWarningMessage').html(responseText);
                    $('#categoryActionFailure').show();
                }
            });

          }
        }
      }
    }); //end bootbox
};    

// ADD CATEGORY Submit event
$('#myForm').on('submit', function(e) {
      
    e.preventDefault();
    var myCategory = $('#myCategoryName').val();
    var culture = $('#categories-select').val();    
    
    // Create a formdata object and add the files
    var data = new FormData();    
    $.each(files, function(key, value)
    {
        data.append(key, value);        
    });
    data.append("category",myCategory);
    data.append("action", "Add");
    data.append("culture", culture);
           
        // AJAX POST request
        $.ajax({
            url : 'categories',
            type: 'POST',                        
            data : data,           
            dataType : 'json',
            contentType: false, 
            cache: false,
            processData: false,
            beforeSend: function()
            { 
            },            
            success : function (response){
                if(response !== ''){ //inserted OK
                    message = 'Category created successfully!';                    
                    var trimData = $.trim(JSON.stringify(response));                    
                    var obj = $.parseJSON(trimData);                        
                    $('#categories-table').dataTable().fnAddData([
                        
                        /*
                        obj.id,
                        obj.name,
                        obj.culture.displayName,
                        obj.imageRelativePath,
                        obj.description,      
                        */
                        obj.id,
                        obj.category,
                        obj.cultureDisplayName,
                        obj.image,
                        obj.description,                        
                        '<td class="actions" id="actions-td"><a href="#" class="deleteBtn" title="Delete"><span class="fa-stack"><i class="icon-trash"></i></span></a>\n\
                        <a href="#" class="editBtn" title="Edit"><span class="fa-stack"><i class="icon-pencil"></i></span></a></td>'                        
                        ]);
                    $('#categories-table').dataTable().fnDraw();
                                                            
                    resetColumnFiltersPlainTdOnly(2);
                    
                    // clear input element's value
                    $('#myCategoryName').val("");
                    $('#upload').val("");
                    $('#categories-select').val("");
                    
                    //$('.deleteBtn').off('click').on('click',deleteCategory);
                    
                    $('#additionSuccessMsg').html(message);
                    $('#addSuccess').show();                                         
                    $("#addSuccess").fadeTo(2000, 500).slideUp(500, function(){
                    $("#addSuccess").slideUp(1000); });     
                }
                else if(response === ''){ // NOT INSERTED
                    message = 'Something went wrong. Phrase was not created.';
                    $('#failureWarningMessage').html(message);                
                    $('#categoryActionFailure').show();                    
                    $("#categoryActionFailure").fadeTo(2000, 500).slideUp(500, function(){
                    $("#categoryActionFailure").slideUp(1000);                    
                    });
                }

            },
            error:function(responseText) {
                                
                var trimData = $.trim(JSON.stringify(responseText));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);                
                var message = json.detail;                 
                $('#failureWarningMessage').html(message);
                $('#categoryActionFailure').show();
                $("#categoryActionFailure").fadeTo(5000, 500).slideUp(500, function(){
                    $("#categoryActionFailure").slideUp(1000);                    
                });
            }
        });      
      
  });

// EDIT Category row button click event
$('.editBtn').click(editCategory);

//EDIT Category Event
function editCategory(event)
{
    event.preventDefault();
    show_loading_message();    
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var categoryId = trElem.attr("data-categoryid");    
    // Get category info from backend
    var request = $.ajax({
      url:          'categories',
      cache:        false,
      data:         'action=Fetch&id=' + categoryId,
      dataType:     'json',
      contentType:  'application/json; charset=utf-8',
      type:         'GET'
    });    
        
    request.done(function(output)
    { 
        
      if (output.result === ''){
        hide_loading_message();                
        show_message('Edit request failed: ' + "Category not found", 'error');
      } 
      else// success , get json
      {
        hide_loading_message();
        show_lightbox();                
        //fill form with fetched data
        $('#edit_form').attr('data-id', categoryId);
        $('#edit_form .field_container label.error').hide();
        $('#edit_form .field_container').removeClass('valid').removeClass('error');
        $('#edit_form #id').val(output.id);
        $('#edit_form #category').val(output.category);
        //$('#edit_form #culture').val(output.category.culture.languageName);
        $('#edit_form #imagePath').val(output.image);                
        $('#edit_form #description').val(output.description);
        
        // Remove all option entries from select elements
        $('#edit_form #culture option').each(function() {
            $(this).remove();     
        });                
        
        
        //populate options for culture language names - select right index
        var cultures = output.cultureItems;        
        for (i = 0; i < cultures.length; i++){
           $('#edit_form #culture').append($('<option>', {value:cultures[i].id, text:cultures[i].language}));            
           // select index 
           if(cultures[i].id === output.cultureId)
           {   
               $('#edit_form #culture').prop('selectedIndex', i);                               
           }
       }
     }
       
    });
    request.fail(function(response)
    {        
        var trimData = $.trim(JSON.stringify(response));                 
        var obj = $.parseJSON(trimData);                                 
        var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
        var json = $.parseJSON(trimResponseJSON);                
        var message = json.detail;        
        hide_loading_message();        
        show_failure_label('Edit request failed: ' + message);
    });    
}

// UPDATE Category FORM submit
$('#edit_form').on('submit', function(event)
{
    event.preventDefault();       
    // collect updated information
    var categoryId = $('#edit_form #id').val();
    var name = $('#edit_form #category').val();
    var image = $('#edit_form #image').val();
    var description = $('#edit_form #description').val();    
    var culture = $('#edit_form #culture').val(); 
    var myCategory = $('#myCategoryName').val();   
    // Create a formdata object and add the files
    var data = new FormData();    
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });
    data.append("action", "Update");  
    data.append("id",categoryId);
    data.append("updatedName",name);        
    data.append("description", description);  
    data.append("culture", culture);
    
    //access tr element 
    var trElem = $("tr[data-categoryid="+categoryId+"]");
        
    // Send updated category to backend db via POST AJAX
    hide_lightbox();
    show_loading_message();
    var id = $('#edit_form').attr('data-id');    
        
        $.ajax({
            url : 'categories',
            type: 'POST',
            //data : { action : 'Update', id : categoryId, name : name, image : image, description : description},            
            data : data,
            dataType : 'json',
            contentType: false, 
            cache: false,
            processData: false,            
            beforeSend: function(){
            },            
            success : function (response)
            {
                if(response !== 'ERROR') //updated OK
                {   
                    var trimData = $.trim(JSON.stringify(response));                   
                    var json = $.parseJSON(trimData);                    
                    //trElem.children("td:eq(1)").html(json.category);
                    //trElem.children("td:eq(2)").html(json.cultureDisplayName);
                    //trElem.children("td:eq(3)").html(json.image);
                    //trElem.children("td:eq(4)").html(json.description);
                    
                    $('#categories-table').dataTable().fnUpdate(json.category, trElem, 1, false);
                    $('#categories-table').dataTable().fnUpdate(json.cultureDisplayName, trElem, 2, false);
                    $('#categories-table').dataTable().fnUpdate(json.image, trElem, 3, false);
                    $('#categories-table').dataTable().fnUpdate(json.description, trElem, 4, false);
                    $('#categories-table').dataTable().fnDraw();
                    
                    //update culture names column filter
                    resetColumnFiltersPlainTdOnly(2);
                   
                    hide_loading_message();                       
                    show_success_label("Category id '" + categoryId + "' updated successfully");
                }
                else if(response === 'ERROR'){ // NOT updated                    
                    hide_loading_message();                    
                    show_failure_label('Update request failed');
                }
            },
            error:function(response) 
            {   
                hide_loading_message();
                var trimData = $.trim(JSON.stringify(response));                 
                var obj = $.parseJSON(trimData);                                 
                var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                var json = $.parseJSON(trimResponseJSON);                  
                var message = json.detail;
                var code = json.applicationErrorCode;                
                if(code === 300) // category name exists exception error (entity exists error code 300)
                {                    
                    show_lightbox();
                    show_CategoryChangedFailure();
                }
                else
                {
                show_failure_label("Update request failed: "+ message);
                }
            }
        });
});

// Show loading message
function show_loading_message(){
    $('#loading_container4').html('Loading, please wait');
    $('#loading_container').show();
}

// Hide loading message
function hide_loading_message(){
    $('#loading_container').hide();
}

// Show message
function show_message(message_text, message_type){
    $('#message').html('<p>' + message_text + '</p>').attr('class', message_type);
    $('#message_container').show();
    if (typeof timeout_message !== 'undefined'){
      window.clearTimeout(timeout_message);
    }
    timeout_message = setTimeout(function(){
      hide_message();
    }, 8000);
}

// Hide message
function hide_message(){
    $('#message').html('').attr('class', '');
    $('#message_container').hide();
}
  
// Show edit form
function show_lightbox(){
    $('.lightbox_bg').show();
    $('.lightbox_container').show();
}

// Hide edit form
function hide_lightbox(){
    $('.lightbox_bg').hide();
    $('.lightbox_container').hide();
}  


// Show loading message
function show_updating_message(message){
    $('#loading_container4').html('Updating, please wait');
    //$('#loading_container').css({ 'display': "block" });
    $('#loading_container').show();
}

// Hide loading message
function hide_updating_message(){
    $('#loading_container').hide();
}
  
function show_success_label(message)
{
    $('#additionSuccessMsg').html(message);
    $('#addSuccess').show();
    $("#addSuccess").fadeTo(2000, 500).slideUp(500, function(){
    $("#addSuccess").slideUp(1000); });     
}

function show_failure_label(message)
{   
    $('#failureWarningMessage').html(message);                
    $('#categoryActionFailure').show();                    
    $("#categoryActionFailure").fadeTo(3000, 500).slideUp(1000, function(){
    $("#categoryActionFailure").slideUp(2000);});        
}
  
// show category edit failure element
function show_CategoryChangedFailure()
{
    $('#categoryChangedFailure').show();
    $("#categoryChangedFailure").fadeTo(3000, 500).slideUp(500, function(){
    $("#categoryChangedFailure").slideUp(500); });     
}

// hide category edit failure element
function hide_CategoryChangedFailure()
{
    $('#categoryChangedFailure').hide();
}  
  
// Lightbox background
$(document).on('click', '.lightbox_bg', function(){
    hide_lightbox();
});

// Lightbox close button
$(document).on('click', '.lightbox_close', function(){
    hide_lightbox();
});

// Hide category failure div element on load!
hide_CategoryChangedFailure();

/*
function resetApprovalOptions()
{
    $("#categories-table tfoot th:eq(4)").find('select').children().remove();
    $("#categories-table tfoot th:eq(4)").find('select').append('<option value=""></option>');
    var cells = [];
    var rows = $("#categories-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find("td:eq(4) text").html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#categories-table tfoot th:eq(4) select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}
*/

function resetColumnFiltersPlainTdOnly(column)
{
    $("#categories-table tfoot th:eq("+column+")").find('select').children().remove();
    $("#categories-table tfoot th:eq("+column+")").find('select').append('<option value=""></option>');
    var cells = [];
    var rows = $("#categories-table").dataTable().fnGetNodes();
    for(var i=0;i<rows.length;i++)
    {
        var html = $(rows[i]).find('td:eq('+column+')').html();
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                         
            $("#categories-table tfoot th:eq("+column+") select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}


}); // end document ready


</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="container" style="width:1400px;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span></a>
          <!-- <a class="brand" href="/complete-it/web/secured/profiles">Complete it</a> -->

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
      
    <ul class="nav navbar-nav" style="margin-top: 5px;font-size: initial;">
    <li><a href="/complete-it/web/secured/dashboard">Dashboard</a></li>         
    <li><a href="/complete-it/web/secured/profiles">Profiles</a></li>     
    <li class="active" style="background: white;"><a href="#">Categories</a></li>  
    <li><a href="/complete-it/web/secured/phrases">Phrases</a></li>
    <li><a href="/complete-it/web/secured/answers">Answers</a></li> 
    </ul>      
      
    </div>
  </div>
</div>    

<div class="container">
<br>
<div class="panel panel-default">
    <div class="panel-heading fa-2x">Categories</div>
    <div class="panel-body"> 
    
  <!-- ADD PHRASE -->
  <!-- <h1>Categories</h1> -->
  <br>
  <form id="myForm" class="movie-input-form form-inline" method="post" action="categories" name="addForm" enctype="multipart/form-data">
    <p><b>New Category</b></p>      

    <input id="myCategoryName" type="text" name="category" placeholder="Category name" size="70" required="true" 
        pattern="^(([a-zA-Z0-9])+([-_])*[\s]?)+$" 
        title="A-Z, a-z, 0-9, or special characters '-' or '_' allowed." style="height:30px; width: 250px"/>     
        
    <select name="culturesSelect" required="true" id="categories-select" class="choices" style="margin: auto;">
        <!-- <option value="id">Title</option> -->
        <option value="">-- Select Language --</option>
        <c:forEach items="${cultures}" var="culture">
          <option value="${culture.id}">${culture.displayName}</option>
        </c:forEach>
    </select>   
    
    <input id="upload" type="file" name="file" class="btn btn-primary" value="Upload" style="padding: 4px 18px;
    background: lightsteelblue;display: inline-block; height: auto;line-height: normal;"/>
    
    <input id="addBtn" type="submit" name="create" class="btn btn-primary" value="Add" style="display: table;
    margin-top: 10px;"/>

  </form>
 
  <!-- category insertion success message -->
  <div class="container" id="addSuccess" style="padding-right:initial; padding-left:initial; width: inherit">
  <div class="alert alert-success fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
    <strong id="additionSuccessMsg"> Category created successfully!</strong>
  </div>
  </div>

  <!-- category deletion error message -->
  <div class="container" id="categoryActionFailure" style="width:initial; padding-right:initial; padding-left:initial;">
  <div class="alert alert-danger fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
      <strong id="failureWarningMessage"> Category cannot be deleted because it is referenced in records.</strong>
  </div>
  </div> 
  

    <!-- hr -->
    <hr>
    
    <!-- CATEGORIES DATA TABLE -->
    <table id="categories-table" class="table table-striped table-bordered">
    <thead>
    <tr>
      <th>Id</th>
      <th>Name</th>
      <th>Culture</th>
      <th>Image path</th>
      <th>Description</th>      
      <th>&nbsp;</th>
    </tr>
    </thead>
    <tfoot>
        <tr>    
        <th></th>
        <th></th>
        <th>Culture</th>
        <th></th>
        <th></th>      
        <th></th>
        </tr>
    </tfoot>    
    <tbody>
    <c:forEach items="${categories}" var="category">
      <tr>
        <td><c:out value="${category.id}"/></td>
        <td><c:out value="${category.name}"/></td>
        <td><c:out value="${category.culture.displayName}"/></td>
        <td><c:out value="${category.imageRelativePath}"/></td>        
        <td><c:out value="${category.description}"/></td>        
        <!-- <td><a href="?action=Remove&id=${category.id}"><i class="icon-trash"></i></a></td> -->
        <!-- <td><a href="#" class="deleteBtn"><i class="icon-trash"></i></a></td> -->
        
        <td class="actions" id="actions-td">
            <a href="#" class="deleteBtn" title="Delete"><span class="fa-stack"><i class="icon-trash"></i></span></a>
            <a href="#" class="editBtn" title="Edit"><span class="fa-stack"><i class="icon-pencil"></i></span></a>
        </td>        
        
      </tr>
    </c:forEach>
    </tbody>
  </table>
    <hr>
    <div id="footer_console">
        Guess-my-answer v1.0 - Admin Console
    </div>

</div>
</div>

<!-- edit lightbox container-->
    <div class="lightbox_bg"></div>
    
    <div class="lightbox_container">
      <div class="lightbox_close"></div>
      <div class="lightbox_content">
        
        <h2>Edit category</h2>
        <form class="form add" id="edit_form" data-id="" method="post" enctype="multipart/form-data">
          <div class="input_container">
            <label for="id">Id: <span class="required"></span></label>
            <div class="field_container">
              <input type="number" step="1" min="0" class="text" name="id" id="id" value="" readonly>              
            </div>
          </div>
          <div class="input_container">
            <label for="category_name">Name <span class="required">*</span></label>
            <div class="field_container">
              <input type="text" class="text" name="category" id="category" value="" required="true" 
                pattern="^(([a-zA-Z0-9])+([-_])*[\s]?)+$" 
                title="A-Z, a-z, 0-9, or special characters '-' or '_' allowed.">
            </div>
          </div>
            
          <div class="input_container">
            <label for="culture">Language <span class="required">*</span></label>
            <div class="field_container">
                <select name="culture" required="true" id="culture" class="choices" style="margin: auto;">                    
                </select>
            </div>
          </div>
            
          <div class="input_container">
            <label for="image">Image: <span class="required"></span></label>
            <div class="field_container">
                <input type="text" class="text" name="image" id="imagePath" value="" readonly="">
              <input id="uploadEdited" type="file" name="editedFile" class="btn btn-primary" value="Upload" style="padding: 4px 18px;
              background: lightsteelblue;display: inline-block; height: auto;line-height: normal;"/>              
            </div>
          </div>
          <div class="input_container">
            <label for="description">Description: <span></span></label>
            <div class="field_container">
              <input type="text" class="text" name="description" id="description" value="">
            </div>
          </div>
         
         <!-- error area -->   
         <div class="input_container">            
            <!-- category name changed error message -->
            <div class="container" id="categoryChangedFailure" style="width:initial; padding-right:initial; padding-left:initial;">
            <div class="alert alert-danger fade in">
                <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
                <strong id="categoryNameChangedMessage"> Category name exists</strong>
            </div>
            </div>  
          </div>            
            
          <div class="button_container">
            <button type="submit" class="updateBtn">Update</button>
          </div>
        </form>
        
      </div>
    </div>


<!-- MESSAGE container-->
<div id="message_container">
      <div id="message" class="success">
        <p>This is a success message.</p>
      </div>
</div>

<!-- EDIT loading container-->
<div id="loading_container">
      <div id="loading_container2">
        <div id="loading_container3">
          <div id="loading_container4">
            Loading, please wait...
          </div>
        </div>
      </div>
</div>

</div>
</body>
</html>
