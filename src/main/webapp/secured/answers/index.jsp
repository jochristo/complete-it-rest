<%-- 
    Document   : view
    Created on : Sep 19, 2016, 3:26:35 PM
    Author     : ic
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${pageContext.request.locale}"/>
<fmt:setLocale value="${language}"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <title>Complete it - Administration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="/complete-it/assets/css/completeit.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 
  <link href="/complete-it/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">  
  <link rel="stylesheet" href="http://cdn.css.net/libs/startbootstrap-sb-admin-2/1.0.5/css/sb-admin-2.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="/complete-it/assets/css/sb-admin-2.min.css"/>   
  <link rel="stylesheet" href="/complete-it/assets/css/layout.css"/> 
  
  <style>
    body {
      padding-top: 60px;      
      /* 60px to make the container go all the way to the bottom of the topbar */
    }  
  
    
    .row{
        margin-left: -15px;
    }
    
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
        
        margin-left: auto;
        margin-right: auto;
    }

    .modal-body{
        padding: 25px;
    }
    .modal-dialog {
        
        width: 500px;
        margin: 30px auto;
        
    }
    .bootbox {
        background: none;
    }
    
    backdrop.fade.in {
        opacity: 0.6; 
    }    
    
    .navbar .nav .active > a
    {
        /* background-color: skyblue; */

    }    
    
    .navbar-fixed-top
    {
        border-bottom-width: 3px;
        border-color: skyblue;
        background-color: #1b1b2b; 
        box-shadow: 0 6px 6px -6px skyblue
    }  
    
    .container {
        width: 1400px;
    }  
    
    td.actions {
        text-align: center;        
    }    
    
    text.gap {
        font-weight: bold;
    }
    
  </style>


  <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(document).ready(function(){

hide_success_label();
hide_failure_label();


// ANSWERS DataTable Init
$('#answers-table').DataTable(
{

    // disable sorting button in last column
    "aoColumnDefs" : 
    [
        { "bSortable": false, "aTargets": [1] }, // text
        { "bSortable": false, "aTargets": [7] }, // actions column   
        {"sWidth": "3%", "aTargets": [7]}, 
        {"sWidth": "8%", "aTargets": [2]} 
    ],
    "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
    "autoWidth": false,      
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).attr("data-answerid",aData[0]);
      return nRow;
    },
    "fnDrawCallback" : function(){
            $('.updateBtn').off('click').on('click', edit);            
    },
    "createdRow": function ( row, data, index ) {
            $('td', row).eq(7).addClass('actions'); // center actions td content        
            if ( String(data[6]) === 'false') {
                $('td:eq(6)', row).html('<text class="label label-pill label-success">Active</text>');
                data[6] = "Active";
            }else if(String(data[6]) === 'true' )
            {
                $('td:eq(6)', row).html('<text class="label label-pill label-warning">Suspended</text>');
                data[6] = "Suspended";            
            }
    }
});

//INDIVIDUAL DataTable column filtering - select inputs
var table = $('#answers-table').DataTable(); 
$("#answers-table tfoot th").each( function ( i ) 
{    
    //select which headers should include filters
    if(i !== 0 && i !== 1 && i !== 7 )
    {
        var select = $('<select class="filtered-select" style="width:auto"><option value=""></option></select>')
        .appendTo( $(this).empty() )
        .on( 'change', function () {
            table.column( i )
            .search( $(this).val() )
            .draw();
        });               
                
        table.column( i ).data().sort().unique().each( function ( d, j )
        {
            select.append('<option value="'+d+'">'+d+'</option>');            
        });
    }
});

//resetColumnFiltersPlainTdOnly(6);

// UNBIND UPDATE click event
$('.updateBtn').off('click');

$('.updateBtn').click(edit);

// UPDATE Answer function    
function edit(event)
{    
    event.preventDefault();    
    var trElem = $( event.target ).closest("tr");// grabs the button's parent tr element
    var answerId = trElem.attr("data-answerid");    
    var id = $.trim($(trElem).children("td:first").text());      
    var status = $(trElem).find("td:eq(6) text").html();    
    var message = '';   
    var row = trElem;
    
    // delete CONFIRMATION
    bootbox.dialog({
      message: "Are you sure?",
      title: "Change answer status",
      buttons: {
        danger: {
          label: "No",
          className: "btn-danger",
          callback: function() {
            //do something
          }
        },
        main: {
          label: "Yes",
          className: "btn-success",
          callback: function() {

            $.ajax({
                url : 'answers',
                type: 'POST',
                data : { answerId : answerId, action : 'Update', status : status },
                dataType : 'json',
                beforeSend: function(){               
                },            
                success : function (response)
                {   
                    if(response !== 'ERROR')
                    {
                        var trimData = $.trim(JSON.stringify(response));                   
                        var json = $.parseJSON(trimData);
                        var label = 'success';
                        var status = "Active";
                        var title = 'Set Suspended';
                        var icon = 'fa fa-lock';
                        if(json.status === 'Suspended'){
                            label = 'warning';
                            status = json.status;
                            title = 'Set Active';
                            icon = 'fa fa-unlock';
                        }
                        $('#answers-table').dataTable().fnUpdate('<text class="label label-pill label-'+label+'">'+status+'</text>', row, 6, false);
                        $('#answers-table').dataTable().fnUpdate('<a href="#" class="updateBtn" title="'+title+'"><i class="'+icon+'" style="color: black;"></i></a>', row, 7, false);
                        // re-draw datatable
                        $("#answers-table").dataTable().fnDraw();
                        // reset column filter data in culture column
                        resetColumnFiltersPlainTdOnly(6);
                        message = 'Answer with id <'+ id + '> status updated successfully!';
                        show_success_label(message);
                    }
                    else if(response === 'ERROR'){
                        message = 'Something went wrong answer was not updated';
                        show_failure_label(message);
                    }
                },
                error:function(response) {
                    var trimData = $.trim(JSON.stringify(response));                 
                    var obj = $.parseJSON(trimData);                                 
                    var trimResponseJSON = $.trim(JSON.stringify(obj.responseJSON));                
                    var json = $.parseJSON(trimResponseJSON);                  
                    var message = json.detail;                    
                    show_failure_label("Update request failed: "+ message);
                }
            });

          }
        }
      }
    }); //end bootbox
}; 


function resetColumnFiltersPlainTdOnly(column)
{
    $("#answers-table tfoot th:eq("+column+")").find('select').children().remove();    
    $("#answers-table tfoot th:eq("+column+")").find('select').append('<option value=""></option>');    
    var cells = [];
    var rows = $("#answers-table").dataTable().fnGetNodes();    
    for(var i=0;i<rows.length;i++)
    {
        //var html = $(rows[i]).find('td:eq('+column+')').html();
        var html = $(rows[i]).find('td:eq('+column+') text').text();
        
        if(cells.indexOf(html) === -1)
        {
            cells.push(html);                
            $("#answers-table tfoot th:eq("+column+") select").append('<option value="'+html+'">'+html+'</option>');
        }
    }
}

function show_success_label(message)
{
    $('#successMessage').html(message);
    $('#successContainer').show();
    $("#successContainer").fadeTo(2000, 500).slideUp(500, function(){
    $("#successContainer").slideUp(1000); });     
}

function show_failure_label(message)
{   
    $('#failureWarningMessage').html(message);                
    $('#failureContainer').show();                    
    $("#failureContainer").fadeTo(3000, 500).slideUp(1000, function(){
    $("#failureContainer").slideUp(2000);});        
}

function hide_success_label()
{
    $('#successContainer').hide();
}

function hide_failure_label()
{
    $('#failureContainer').hide();
}

// Lightbox background
$(document).on('click', '.lightbox_bg', function(){
    hide_lightbox();
});

// Lightbox close button
$(document).on('click', '.lightbox_close', function(){
    hide_lightbox();
});  

// Hide edit form
function hide_lightbox(){
    $('.lightbox_bg').hide();
    $('.lightbox_container').hide();
} 

// Hide loading message
function hide_updating_message(){
    $('#loading_container').hide();
}


hide_updating_message();
}); // end document ready


</script>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="container" style="width:1400px;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span
          class="icon-bar"></span> <span class="icon-bar"></span></a>
          <!-- <a class="brand" href="/complete-it/web/secured/profiles">Complete it</a> -->

      <form class="navbar-form pull-right" hidden="true">
        <select name="field" >
          <!-- <option value="id">Title</option> -->
          <option id="phraseText" value="text">Phrase</option>
          <option value="gaps">Gaps</option>
          <option value="category">Category</option>
          <option value="approval">Approval status</option>
        </select>
          <input type="text" name="key" size="20">
        <button type="submit" class="btn">Search</button>
      </form>

      <!--/.nav-collapse -->
      
    <ul class="nav navbar-nav" style="margin-top: 5px;font-size: initial;">
    <li><a href="/complete-it/web/secured/dashboard">Dashboard</a></li> 
    <li><a href="/complete-it/web/secured/profiles">Profiles</a></li> 
    <li><a href="/complete-it/web/secured/categories">Categories</a></li> 
    <li><a href="/complete-it/web/secured/phrases">Phrases</a></li> 
    <li class="active"><a href="#">Answers</a></li> 
    </ul>      
      
    </div>
  </div>
</div>
  

<div class="container">
<br>
<div class="panel panel-default">
    <div class="panel-heading fa-2x">Answers</div>
    <div class="panel-body">    

  <!-- ADD ANSWER -->
  <form id="myForm" class="movie-input-form form-inline" action="phrases" method="post" name="addForm" hidden="true">
    <!-- <div id="addition" class="add-item-box"> -->
    <p><b>Add Answer</b></p>     
    
    <input id="myPhraseText" type="text" name="text" placeholder="Phrase text" size="70" required="true" 
        pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" 
        title="Use format: word [] [] word etc." style="height:30px; width: 250px; margin: auto;"/>     
        
    <select name="category" required="true" id="categoryId" class="choices" style="margin: auto;">
        <!-- <option value="id">Title</option> -->
        <option value="">-- Select Category --</option>
        <c:forEach items="${categories}" var="category">
          <option value="${category.id}">${category.name}</option>
        </c:forEach>                  
    </select>    
    <input id="addBtn" type="submit" name="action" class="btn btn-primary" value="Add" style="padding: 4px 18px; position: absolute;
    margin-left: 5px;"/>
  </form>
<!-- </div> -->
  
  <!-- success message -->
  <div class="container" id="successContainer" style="padding-right:initial; padding-left:initial; width: inherit" hidden="true">
  <div class="alert alert-success fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
    <strong id="successMessage"></strong>
  </div>
  </div>

  <!-- error message -->
  <div class="container" id="failureContainer" style="width:initial; padding-right:initial; padding-left:initial;" hidden="true">
  <div class="alert alert-danger fade in">
      <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
      <strong id="failureWarningMessage"></strong>
  </div>
  </div> 

    <!-- hr -->
    <hr>  
  
    <!-- PHRASES DATA TABLE -->      
    <table id="answers-table" class="table table-striped table-bordered">
    <thead>
    <tr>
      <th>Id</th>
      <th>Text</th>
      <th>Phrase id</th>
      <th>User</th>
      <th>Likes</th>
      <th>Reports</th>
      <th>Status</th>
      <th>&nbsp;</th>
    </tr>
    </thead>
    <tfoot>
        <tr>    
        <th></th>
        <th></th>
        <th>Phrase id</th>
        <th>User</th>
        <th>Likes</th>        
        <th>Reports</th>      
        <th>Status</th>
        <th></th>        
        </tr>
    </tfoot>     
    <tbody>
    <c:forEach items="${answers}" var="answer">
      <tr>
        <td><c:out value="${answer.id}"/></td>
        <td><c:out value="${answer.answers}" escapeXml="false"/></td>
        <td><c:out value="${answer.phrase}"/></td>        
        <td><c:out value="${answer.email}"/></td>        
        <td><c:out value="${answer.likes}"/></td>
        <td><c:out value="${answer.reports}"/></td>                
        <td><c:out value="${answer.deleted}"/></td>                
        <c:choose>
            <c:when test="${answer.status == 'Active'}">
                <%-- <td><text class="label label-pill label-success"><c:out value="Active"/></text></td> --%>
                <td class="actions"><a href="#" class="updateBtn" title="Set Suspended"><i class="fa fa-lock" style='color: black;'></i></a></td> 
            </c:when>    
            <c:otherwise>
                <%-- <td><text class="label label-pill label-warning"><c:out value="Suspended"/></text></td> --%>
                <td class="actions"><a href="#" class="updateBtn" title="Set Active"><i class="fa fa-unlock" style='color: black;'></i></a></td> 
            </c:otherwise>
        </c:choose>         
      </tr>
    </c:forEach>
    </tbody>
  </table>
    <hr>
    <div id="footer_console">
        Complete-it v1.0 - Admin Console
    </div>    
      
    </div>
    </div>

<!-- edit lightbox container-->
    <div class="lightbox_bg"></div>

    
    <div class="lightbox_container">
      <div class="lightbox_close"></div>
      <div class="lightbox_content">
        
        <h2>Edit phrase</h2>
        <form class="form add" id="form_phrase" data-id="" method="post">
          <div class="input_container">
            <label for="rank">Id: <span class="required"></span></label>
            <div class="field_container">
              <input type="number" step="1" min="0" class="text" name="id" id="id" value="" readonly>              
            </div>
          </div>
          <div class="input_container">
            <label for="company_name">Text: <span class="required">*</span></label>
            <div class="field_container">
              <input type="text" class="text" name="phrase" id="phrase" value="" required="true" 
                pattern="^([^\[\]]+|(\[\]))([\s]{1,}([^\[\]]+|(\[\])))*$" title="Use format: word [] [] word etc.">
            </div>
          </div>
          <div class="input_container">
            <label for="industries">Gaps: <span class="required"></span></label>
            <div class="field_container">
              <input type="text" class="text" name="gaps" id="gaps" value="" readonly>
            </div>
          </div>
          <div class="input_container">
            <label for="revenue">Category: <span class="required">*</span></label>
            <div class="field_container">              
                <select name="categories" required="true" id="categories" class="choices" style="margin: auto;">                    
                    <!-- <option value="" id="category" name="category">-- Select Category --</option> -->
                </select>
            </div>
          </div>
          <div class="input_container">
            <label for="fiscal_year">Approval status: <span class="required"></span></label>
            <div class="field_container">              
                <select name="approvalStatus" required="true" id="approvalStatus" class="choices" style="margin: auto;">                    
                    <!-- <option value=1>Pending</option>
                    <option value=0>Approved</option>
                    -->
                </select>              
            </div>
          </div>
          <div class="input_container">
            <label for="employees">Added by Profile: <span class="required"></span></label>
            <div class="field_container">
              <input type="text" class="text" name="addedByProfile" id="addedByProfile" value="" readonly>
            </div>
          </div>
         
         <!-- error area -->   
         <div class="input_container">            
            <!-- phrase text changed error message -->
            <div class="container" id="phraseChangedFailure" style="width:initial; padding-right:initial; padding-left:initial;">
            <div class="alert alert-danger fade in">
                <a href="#" class="close" aria-label="close" style="right:0px">&times;</a>
                <strong id="phraseChangedMessage"> Phrase gaps cannot be modified because it is referenced in answer records.</strong>
            </div>
            </div>  
          </div>            
            
          <div class="button_container">
            <button type="submit" class="updateBtn">Update</button>
          </div>
        </form>
        
      </div>
    </div>

<!-- EDIT loading container-->
<div id="loading_container">
      <div id="loading_container2">
        <div id="loading_container3">
          <div id="loading_container4">
            Loading, please wait...
          </div>
        </div>
      </div>
</div>

</div>
<!-- /container -->
</body>
</html>
