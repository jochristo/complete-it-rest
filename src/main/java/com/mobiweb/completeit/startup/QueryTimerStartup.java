/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.startup;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author ic
 */
@DependsOn({"SelectQueryTimerTask"})
@Startup
@Singleton
public class QueryTimerStartup{

    
    private @EJB SelectQueryTimerTask timer;
    
    @PostConstruct
    private void start()
    {
        timer.start();
    }

    
    
}
