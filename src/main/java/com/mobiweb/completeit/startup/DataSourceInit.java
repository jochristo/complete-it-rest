/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.startup;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Startup;
import com.mobiweb.completeit.common.Global;
import com.mobiweb.completeit.domain.Culture;
import com.mobiweb.completeit.domain.EntityCounter;
import com.mobiweb.completeit.service.AnswerService;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.CultureService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.PhraseService;
import com.mobiweb.completeit.service.ProfileService;
import javax.ejb.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ic
 */
@DependsOn({"CategoryService", "ProfileService","PhraseService","EntityCounterService","AnswerService", "CultureService"})
@Startup
@Singleton
/*
@DataSourceDefinition(
    name="java:global/jdbc/completeit_ds", 
    user="compluser",
    password="D@nesiC0Ffee",
    className="com.mysql.cj.jdbc.Driver",    
    url="jdbc:mysql://localhost:3306/db_completeit?useSSL=false"
        + "&nullNamePatternMatchesAll=true&createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=utf-8"
            + "&useLegacyDatetimeCode=false&serverTimezone=Europe/Athens")
*/
@DataSourceDefinition(
    name="java:global/jdbc/guessmyanswer_ds", 
    user="guessadmin",
    password="guessadmin",
    className="com.mysql.cj.jdbc.Driver",    
    url="jdbc:mysql://localhost:3306/db_guessmyanswer?useSSL=false"
        + "&nullNamePatternMatchesAll=true&createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=utf-8"
            + "&useLegacyDatetimeCode=false&serverTimezone=Europe/Athens")
public class DataSourceInit {   
    
    //&connectionCollation=utf8_general_ci&characterSetResults=utf8
    // inject service
    @EJB CategoryService cs;  
    @EJB PhraseService ps;
    @EJB ProfileService profileService;
    @EJB AnswerService as;
    @EJB EntityCounterService entityCounterService;
    @EJB CultureService cultureService;
    
    private Logger logger = LoggerFactory.getLogger(DataSourceInit.class);
    
    @PostConstruct
    @SuppressWarnings("unused")
    private void init()
    {        
        //populate cultures data into database - first time only
        List<Culture> cultures = cultureService.findAll();
        if(cultures.isEmpty())
        {
            logger.info("Populating cultures list...");
            cultureService.populate();
            logger.info("Done");
        }
        
        /*
        // populate categories data into database - first time only
        List<Category> list = cs.findAll();
        if(list != null && list.isEmpty())
        {            
            //default culture is US english
            Culture us = cultureService.findByIso("ENU");
            Map<Integer,Map.Entry<String,String>> categories = Global.Categories.getCategories();            
            for (Map.Entry<Integer, Map.Entry<String, String>> entry : categories.entrySet()) {                
                Map.Entry<String,String> value = entry.getValue(); 
                String name = value.getKey();
                String path = value.getValue();
                Category c = new Category();
                c.setName(name);
                c.setImageRelativePath(path);
                c.setDescription(null);
                c.setCulture(us);
                cs.create(c);
            }            
        }
        */
        
        //create entity counter data
        boolean isEntityCounterCreated = entityCounterService.findAll() != null && entityCounterService.findAll().isEmpty();
        List<String> names = new ArrayList();
        if(isEntityCounterCreated)
        {
            names = new ArrayList();
            names.add("Profile");
            names.add("Category");
            names.add("Phrase");
            names.add("Answer");            
            for(int i = 0; i < 4; i++)
            {
                EntityCounter ec = new EntityCounter();
                ec.setEntity_type(names.get(i));
                entityCounterService.create(ec);
            }
        }
        else if(entityCounterService.findAll() != null && !entityCounterService.findAll().isEmpty())
        {
            //update entity record counter in db store

            // PROFILES                
            int count = profileService.count();
            EntityCounter entityCounter = entityCounterService.findByEntityType("Profile");
            entityCounter.setCounter(count);
            entityCounterService.update(entityCounter);            

            // CATEGORIES                
            count = cs.count();
            entityCounter = entityCounterService.findByEntityType("Category");
            entityCounter.setCounter(count);
            entityCounterService.update(entityCounter);
                
            // PHRASES            
            count = ps.count();
            entityCounter = entityCounterService.findByEntityType("Phrase");
            entityCounter.setCounter(count);
            entityCounterService.update(entityCounter);
                
            // ANSWERS
            count = as.count();
            entityCounter = entityCounterService.findByEntityType("Answer");
            entityCounter.setCounter(count);                
            entityCounterService.update(entityCounter);
        }

        
        cultures = cultureService.findAllDistinct();
        if(cultures.size() > 0)
        {
            for(Culture c : cultures)
            {
                c = cultureService.find(c.getId());
                c.setLanguageName(Global.Cultures.extractLanguage(c.getDisplayName()));
                cultureService.update(c);
            }
        }
    }
    
}
