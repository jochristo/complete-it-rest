/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.startup;

import com.mobiweb.completeit.service.ProfileService;
import java.util.Timer;
import java.util.TimerTask;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author ic
 */
@DependsOn({"ProfileService"})
@Singleton
public class SelectQueryTimerTask{
 
    private @EJB ProfileService ps;
    
    public void start()
    {               
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(new QueryTimerTask(), 1000, 18000000);  //run every 5 hours
    }
    
    private void complete()
    {
        boolean exists = ps.exists(0);
    }
    
    public class QueryTimerTask extends TimerTask
    {

    @Override
    public void run() {
        complete();        
    }
                
    }
}
