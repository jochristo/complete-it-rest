/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.oauth;

/**
 * Represents an access token returned after authorization is granted.
 * @author ic
 */

public class AccessTokenJson
{
    private String access_token;   
    private String token_type;
    private String expires_in;  
    private String refresh_token;     

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    
    // JSON FORMAT
    /*
    {
    "access_token": { access access_token for this user },
    "token_type": { token_type of access_token },
    "expires_in": { Time in seconds that the access_token remains valid },
    "refresh_token": { Refresh access_token to use when the access_token has timed out }
    }    
    */
   
    
    public enum GrantType{    
    BEARER, REFRESH_TOKEN
    }        
    
}
