/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.oauth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.mobiweb.completeit.common.MemCached;
import com.mobiweb.completeit.common.SHA256Salted;
import com.mobiweb.completeit.common.PBKDF2SHA1;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.json.ErrorUtils.ErrorMessage;
import com.mobiweb.completeit.service.ProfileService;
import javax.ejb.Stateless;
import javax.servlet.ServletContext;
import net.spy.memcached.MemcachedClient;

/**
 * Provides OAuth2 token services to clients.
 * @author ic
 */

@Path("/oAuth")
@Stateless
public class OAuthResource
{   
    private final MemcachedClient cache;  
    private int AccessTokenExpiry = 3600;
    private int RefreshTokenExpiry = 3600;
    private @EJB ProfileService service;
    private @Context ServletContext context;

    public OAuthResource() throws IOException {
        this.cache = MemCached.getClient();
        //this.AccessTokenExpiry = this.getTokenExpiry();
        //this.RefreshTokenExpiry = getRefreshTokenExpiry();
    }    
    
    @POST    
    @Produces("application/vnd.api+json")     
    @Path("/token")    
    public Response token(@Context HttpServletRequest request,
            @FormParam("grant_type") String grantType, 
            @FormParam("email") String email, 
            @FormParam("password") String password,
            @FormParam("refresh_token") String refresh_Token) 
            throws IOException, NoSuchProviderException
    {
        this.AccessTokenExpiry = getTokenExpiry();
        this.RefreshTokenExpiry = getRefreshTokenExpiry();        
        
        // form params empty, return bad request code
        if(grantType == null || grantType.isEmpty()){              
            ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)                    
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                     
        }
        else if(!"password".equals(grantType) && !"refresh_token".equals(grantType)){
            ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }            
        
        if(grantType.equals("password")){
            if(Utilities.isEmpty(email) || Utilities.isEmpty(password)){                
                ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)
                .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                
            }
        }
        else if(grantType.equals("refresh_token")){
            
            if(Utilities.isEmpty(refresh_Token)){
                ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)
                .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                     
            }
        }        
        
        //check if grant_type is refresh_token or password
        boolean  isRefreshTokenMode = ("refresh_token".equals(grantType)) ? true: false;
        try{
            
            UUID randomToken = UUID.randomUUID();
            UUID randomRefreshToken = UUID.randomUUID();
            String hashedToken = SHA256Salted.hash(String.valueOf(randomToken), SHA256Salted.getSalt());
            String hashedRefreshToken = SHA256Salted.hash(String.valueOf(randomRefreshToken), SHA256Salted.getSalt());             
            Gson gson = new Gson();
            TokenInfo tokenInfo = new TokenInfo();  
            Object cacheValue = null;
            AccessTokenJson token = new AccessTokenJson();
            
            // PASSWORD grant_type
            if(!isRefreshTokenMode){
                
                Profile profile = service.findSingleByEmail(email);        
                if(profile== null)
                {
                    ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                 
                }            
                                
                boolean isPassValid = PBKDF2SHA1.isPasswordValid(password, profile.getPassword());
                if(!isPassValid)
                {
                    ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                
                }
            
                tokenInfo.setId(String.valueOf(profile.getId()));           
                tokenInfo.setEmail(profile.getEmail());
                tokenInfo.setNickname(profile.getNickname());  
            
                //first key store in cache          
                if(cache.get(String.valueOf(profile.getId())) == null)
                {
                    tokenInfo.setAccess_token(hashedToken);
                    tokenInfo.setRefresh_token(hashedRefreshToken); 
                    token.setAccess_token(hashedToken);
                    token.setRefresh_token(hashedRefreshToken);     
                    String jsonTokenInfo = gson.toJson(tokenInfo);
                    cache.set(String.valueOf(profile.getId()),AccessTokenExpiry, jsonTokenInfo);
                    cache.set(tokenInfo.getRefresh_token(), RefreshTokenExpiry, jsonTokenInfo);                
                    cache.set(hashedToken, AccessTokenExpiry, jsonTokenInfo);
                }
                else //key found, update tokens
                {                    
                    tokenInfo = gson.fromJson(cache.get(String.valueOf(profile.getId())).toString(), TokenInfo.class);
                    token.setAccess_token(tokenInfo.getAccess_token());
                    token.setRefresh_token(tokenInfo.getRefresh_token());                 
                }
            }

            //REFRESH_TOKEN grant_type       
            else{                
                                
                //find key-value for given refresh token                
                cacheValue = cache.get(refresh_Token);
                if(cacheValue == null)
                {
                    ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
                    .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
                    .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
                    return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                    
                }                
                tokenInfo = gson.fromJson(cacheValue.toString(), TokenInfo.class);
                
                // cache entered access_token as refresh_token: display error response
                if(!Utilities.isEqual(tokenInfo.getRefresh_token(),refresh_Token)){
                    ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
                    .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
                    .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
                    return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                        
                }                
                
                //update cache entries and json tokens
                String oldAccessToken = tokenInfo.access_token;
                randomToken = UUID.randomUUID();
                hashedToken = SHA256Salted.hash(String.valueOf(randomToken), SHA256Salted.getSalt());
                randomRefreshToken = UUID.randomUUID();
                hashedRefreshToken = SHA256Salted.hash(String.valueOf(randomRefreshToken), SHA256Salted.getSalt());                
                tokenInfo.setAccess_token(hashedToken);                
                tokenInfo.setRefresh_token(hashedRefreshToken);                
                token.setAccess_token(hashedToken);
                token.setRefresh_token(hashedRefreshToken);                   
                String jsonTokenInfo = gson.toJson(tokenInfo); 
                cache.set(tokenInfo.getId(),AccessTokenExpiry,jsonTokenInfo);
                cache.set(hashedRefreshToken, RefreshTokenExpiry, jsonTokenInfo);
                cache.set(refresh_Token, RefreshTokenExpiry, jsonTokenInfo);
                //store token-jsonTokenInfo pair too - delete old refresh token entry
                cache.set(hashedToken, AccessTokenExpiry, jsonTokenInfo);                
                cache.delete(refresh_Token);
                cache.delete(oldAccessToken);
            }                     

            token.setToken_type("Bearer");   
            token.setExpires_in(Integer.toString(AccessTokenExpiry));                          
                        
            //return json response with token details
            ObjectMapper mapper = new ObjectMapper();
            StringWriter writer = new StringWriter();
            mapper.writeValue(writer, token);
            CacheControl cc = new CacheControl();
            cc.setNoCache(true);
            cc.setNoStore(true);
            cc.setNoTransform(false);
            return Response.ok(writer.toString(), "application/vnd.api+json;charset=UTF-8").cacheControl(cc).build();
            
        } catch(NoSuchAlgorithmException | InvalidKeySpecException | WebApplicationException ex)
        {
            ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
        }
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
    }   

    public Response checkAuthorization(@Context HttpServletRequest httpServletRequest)
    {
        String authorization = httpServletRequest.getHeader("Authorization");
        ErrorUtils.ErrorMessage errorMessage = null;
        if(authorization == null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.AUTHORIZATION_HEADER_MISSING)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        
        boolean isAuthorizationHeaderValid = authorization.matches("^Bearer \\w+$");
        if(!isAuthorizationHeaderValid)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_AUTHORIZATION_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        String parsed[] = authorization.split("Bearer ");
        authorization = parsed[1];
        
        // look it up in memcache combined with user email
        // check against memcached
        Gson gson = new Gson();                  
        Object value = null;
        try{
            value = cache.get(authorization);
        }
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }            
        // TOKEN NOT FOUND
        if(value==null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        
        // INVALID TOKEN
	TokenInfo tokenInfo = gson.fromJson(value.toString(), TokenInfo.class); 
        value = tokenInfo.getAccess_token();      
        if(value == null || !value.equals(authorization))
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        
        return null;
    }
     
    
    protected int getTokenExpiry()
    {   
        String _tokenExpiry = (String)context.getInitParameter("Token.Expiry");
        if(!Strings.isNullOrEmpty(_tokenExpiry)){
            return Integer.parseUnsignedInt(_tokenExpiry);
        }
        return 3600;
    }
        
    protected int getRefreshTokenExpiry()
    {
        String _refhreshTokenExpiry = (String)context.getInitParameter("RefreshToken.Expiry");
        if(!Strings.isNullOrEmpty(_refhreshTokenExpiry)){
            return Integer.parseUnsignedInt(_refhreshTokenExpiry);
        }
        return 3600;
    }    
    
}
