/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.common.ClassUtils;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.mobiweb.completeit.common.Utilities;
import static com.mobiweb.completeit.common.Utilities.getInputStreamAsString;
import com.mobiweb.completeit.common.exception.InvalidJsonApiClassException;
import com.mobiweb.completeit.common.exception.InvalidJsonFormatException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;


/**
 *
 * @author ic
 */
public class JsonParser {
    
    public static <T extends Object> T deserialized(InputStream is, Class<T> type) throws InvalidJsonFormatException           
    {
        try
        {   
            String jsonString = getInputStreamAsString(is);           
            ResourceConverter converter = new ResourceConverter(type);
            //T t = converter.readObject(jsonString.getBytes(), type);  
            JSONAPIDocument<T> doc = converter.readDocument(jsonString.getBytes(), type);
            T t = doc.get();            
            return t; 
        }
        catch (Exception ex)
        {
            Throwable inner = ex.getCause();
            if(inner instanceof JsonParseException)
            {
                String pattern = "com.fasterxml.jackson.core.JsonParseException: ";
                String[] split = ex.getMessage().split(pattern);                
                throw new InvalidJsonFormatException(split[1], ex.getCause());                
            }
            else if(inner instanceof InvalidFormatException)
            {
                String pattern = ":";
                String[] split = ex.getMessage().split(pattern);                
                throw new InvalidJsonFormatException(split[1], ex.getCause());                
            }
            if(ex instanceof InvalidJsonFormatException)
            {
                throw new InvalidJsonFormatException(ex.getMessage(), ex.getCause());
            }
            throw new InvalidJsonFormatException(ex.getMessage(), ex.getCause());
        }
    } 
    
    public static <T,R extends Object> T deserializedWithRelationship(InputStream is, Class<T> type, Class<R> relationType) throws InvalidJsonFormatException           
    {
        try
        {   
            String jsonString = getInputStreamAsString(is);
            ResourceConverter converter = new ResourceConverter(type, relationType);             
            //T t = converter.readObject(jsonString.getBytes(), type); //deprecated     
            JSONAPIDocument<T> doc = converter.readDocument(jsonString.getBytes(), type);
            T t = doc.get();
            return t; 
        }
        catch (Exception ex)
        {
            Throwable inner = ex.getCause();
            if(inner instanceof JsonParseException)
            {
                String pattern = "com.fasterxml.jackson.core.JsonParseException: ";
                String[] split = ex.getMessage().split(pattern);                
                throw new InvalidJsonFormatException(split[1], ex.getCause());                
            }
            if(ex instanceof InvalidJsonFormatException)
            {
                throw new InvalidJsonFormatException(ex.getMessage(), ex.getCause());
            }
            throw new InvalidJsonFormatException(ex.getMessage(), ex.getCause());
        }
    }    

        /**
     * Converts given instance of given type into JSON API 1.0 string format.
     * @param <T> 
     * @param t Given object
     * @param type 
     * @return String The JSON API formatted string.
     * @throws java.lang.IllegalAccessException
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    public static <T extends Object> String serialized(T t, Class<T> type) throws IllegalAccessException, JsonProcessingException             
    {
        ResourceConverter converter = new ResourceConverter(type);
        return  new String(converter.writeObject(t));        
    }
    

    /**
     * Converts given instance of given type into JSON API 1.0 string format.
     * @param <T> 
     * @param <R> 
     * @param t Given object
     * @param type 
     * @param relationType 
     * @return String The JSON API formatted string.
     * @throws java.lang.IllegalAccessException
     * @throws com.fasterxml.jackson.core.JsonProcessingException
     */
    public static <T,R extends Object> String serializedWithRelatioship(T t, Class<T> type, Class<R> relationType) throws IllegalAccessException, JsonProcessingException             
    {
        ObjectMapper mapper = new ObjectMapper();
        ResourceConverter converter = new ResourceConverter(mapper, type, relationType);
        return new String(converter.writeObject(t));        
    }  
    
    /**
     * Creates custom json-based category response body message from given jsonapi errors.
     * @param entities
     * @return 
     */
    public static String getAllCategoriesAsJson(List<OutboundCategory> entities)
    {
        //String name = type.getCanonicalName().toLowerCase();
                
        String body = "{\r\"data\" : ";
        if(entities != null)
        {
            int lastItemPos = entities.size()-1;
            body += "[\r"; 
            for (OutboundCategory category : entities) {
                body += "{";
                body += "\r\"type\"" + " : \"" + "categories" + "\",\r";
                body += "\r\"id\"" + " : \"" + category.getId() + "\",\r";
                body += "\r\"attributes\"" + " : " + "{\r";                
                
                body += "\"name\"" + " : " + "\"" + category.getName() +"\"" + ",\r";
                body += "\"imagePath\"" + " : \"" + category.getImagePath() + "\",\r";                
                String description = category.getDescription();
                if(description != null){
                   body += "\"description\"" + " : \"" + description + "\"\r}"; 
                }else{
                    body += "\"description\"" + " : " + description + "\r}";
                }
                         
                body += "}";

                if(entities.size() > 1 && entities.get(lastItemPos) != category ){
                    body += ",\r";
                }
            }
            
            body += "]";
            body += "\r}";
            return body;
        }    
        return null;
    }    
    
    public static <T extends Object> String toJsonAPI(List<T> entries , Class<T> type) 
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {        
        Type annotation = type.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);        
        String typeName = annotation.value();
        Field[] fields = type.getClass().getDeclaredFields();
        String body = "{\r\"data\" : ";         
        if(entries != null)
        {
            int lastItemPos = entries.size()-1;
            body += "[\r"; 
            for (T t : entries)
            {         
                fields = t.getClass().getDeclaredFields();
                body += "{";
                body += "\r\"type\"" + " : \"" + typeName + "\",\r";
                Map<String,Object> id = ClassUtils.getFieldNameValuePair(t, "id");                  
                body += "\r\"id\"" + " : \"" + id.get("id") + "\",\r";  
                body += "\r\"attributes\"" + " : " + "{\r";
                int length = fields.length;
                int index = 0;
                for(Field field : fields)
                {                    
                    String name = field.getName();
                    Object value = ClassUtils.getFieldValue(field, t);
                    if(name != null && !name.equals("id"))
                    {
                        if(value == null)
                        {
                           body += "\"" + name + "\"" + " : " + value;  
                        }
                        else
                        {
                            body += "\"" + name + "\"" + " : \"" + value + "\""; 
                        }
                       
                       if(index < length-1){
                        body += ",\r"; 
                       }                       
                    }                    
                    if(index == length-1) // last attribute reached, close json entry
                    {
                       body += "\r}";
                    }                   
                    index++;                                       
                }
                body += "}";
                if(entries.size() > 1 && entries.get(lastItemPos) != t )
                {
                    body += ",\r";
                } 
            }            
            body += "]";
            body += "\r}";
            return body;
        }
        return null;
    }    
    
    public static <T extends Object> String toJsonAPIWithRelationships(List<T> entries , Class<T> type) 
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {        
        Type annotation = type.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);        
        String typeName = annotation.value();
        Field[] fields = type.getClass().getDeclaredFields();
        String body = "{\r\"data\" : ";         
        if(entries != null)
        {
            int lastItemPos = entries.size()-1;
            body += "[\r"; 
            for (T t : entries)
            {         
                fields = t.getClass().getDeclaredFields();
                body += "{";
                body += "\r\"type\"" + " : \"" + typeName + "\",\r";
                Map<String,Object> id = ClassUtils.getFieldNameValuePair(t, "id");                  
                body += "\r\"id\"" + " : \"" + id.get("id") + "\",\r";  
                body += "\r\"attributes\"" + " : " + "{\r";
                int length = fields.length;
                int index = 0;
                for(Field field : fields)
                {                    
                    String name = field.getName();
                    Object value = ClassUtils.getFieldValue(field, t);
                    if(name != null && !name.equals("id") )//&& value.getClass()!= ArrayList)
                    {
                        if(value == null)
                        {
                           body += "\"" + name + "\"" + " : " + value;  
                        }
                        else
                        {
                            body += "\"" + name + "\"" + " : \"" + value + "\""; 
                        }
                       
                       if(index < length-1){
                        body += ",\r"; 
                       }                       
                    }                    
                    if(index == length-1) // last attribute reached, close json entry
                    {
                       body += "\r}";
                    }                   
                    index++;                                       
                }
                body += "}";
                if(entries.size() > 1 && entries.get(lastItemPos) != t )
                {
                    body += ",\r";
                } 
            }            
            body += "]";
            body += "\r}";
            return body;
        }
        return null;
    }      
    
    
    public static <T,R extends Object> String serializedWithIncludedRelatioship(T t, Class<T> type, Class<R> relationType) throws IllegalAccessException, JsonProcessingException             
    {      
        return t.toString();
    }    
    
    public static <T,R extends Object> String serializedWithIncludedRelatioship(T t) throws IllegalAccessException, JsonProcessingException             
    {      
        return t.toString();
    }      
    
    public static <T extends Object> String withIncludedRelationshipsOLD(List<T> entries , Class<T> type) 
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {        
        Type annotation = type.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);        
        String typeName = annotation.value();
        Field[] fields = type.getClass().getDeclaredFields();
        List<Object> included = new ArrayList();
        String relationshipName = "";
        
        String body = "{\"data\" : ";         
        if(entries != null)
        {
            int lastItemPos = entries.size()-1;
            body += "["; 
            int idCounter = 1;
            for (T t : entries)
            {         
                List<Object> relationships = new ArrayList();
                fields = t.getClass().getDeclaredFields();
                body += "{";
                body += "\"type\"" + " : \"" + typeName + "\",";
                Map<String,Object> id = ClassUtils.getFieldNameValuePair(t, "id");                  
                body += "\"id\"" + " : \"" + id.get("id") + "\",";  
                body += "\"attributes\"" + " : " + "{";
                int currentPos = 1;
                for(Field field : fields)
                {   
                    Relationship relationshipAnnotation = field.getAnnotation(Relationship.class);
                    String name = field.getName();
                    Object value = ClassUtils.getFieldValue(field, t);
                    if(name != null && !name.equals("id") && relationshipAnnotation == null) // do not include id and relationship fields
                    {
                        if(value == null)
                        {
                           body += "\"" + name + "\"" + " : " + value;  
                        }
                        else
                        {
                            body += "\"" + name + "\"" + " : \"" + value + "\""; 
                        } 
                        if(currentPos < fields.length - 2) // -2 : because id and Relationship annotated fields are not needed here!
                        {
                            body += ","; 
                        }                         
                        currentPos++; 
                    }
                    else if(relationshipAnnotation != null)
                    {                        
                        Field[] relationFields = relationshipAnnotation.getClass().getDeclaredFields();
                        for(Field f : relationFields)
                        {
                            if(f.getName().equals("value")){
                                relationshipName = f.getName(); //(String)getFieldValue(f, value); // get @Relationship value
                            }
                        }
                        relationshipName = relationshipAnnotation.value().toString();
                        relationships.add(value); // add relationship value-list to cache
                        included.add(value); // add included item to parse after relationships field is done                       
                    }
                }
                
                body += "}";                
                
                //include relationships if any
                if(relationships.size() > 0)
                {
                   body += ",";
                   body += "\"relationships\":{";
                   body += "\"" + relationshipName + "\":{";
                   body += "\"data\":[";
                   if(relationships.size() > 0) // write data to json body
                   {
                       //String RelationshipsData = "";
                       String idData = "";
                       String typeData = "";                       
                       for(Object obj : relationships)
                       {
                           // Type field
                           if(!(obj instanceof List<?>))
                           {
                               //specific type found, process fields
                               Field[] objFields = obj.getClass().getDeclaredFields();
                               for(Field f : objFields)
                               {                                   
                                   if(f.getName() == "id")
                                   {
                                       idData += "\"id\":" + "\"" + idCounter + "\"";
                                   }  
                               }
                               typeData += "\"type\":\"" + relationshipName + "\"";                               
                               
                               if(!Utilities.isEmpty(idData) && !Utilities.isEmpty(typeData))
                               {
                                   body += "{" + typeData + "," + idData + "}";
                               }
                           }
                           /*
                           // List holding relationships values
                           else if(obj instanceof List<?>)
                           {
                               List<?> objects = (List<?>)obj;
                               int relationIdCounter = 1;
                               int index = 0;
                               int size = objects.size();
                               for(Object relObject : objects)
                               {
                                   Type ObjectType = relObject.getClass().getAnnotation(Type.class);
                                   Field[] relObjectFields = relObject.getClass().getDeclaredFields();
                                    for(Field f : relObjectFields)
                                    {                                   
                                        if(f.getName() == "id")
                                        {
                                            idData = "\"id\":" + "\"" + relationIdCounter + "\"";
                                        }  
                                    }
                                    typeData = "\"type\":\"" + ObjectType.value().toString() + "\"";                               

                                    if(!Utilities.isEmpty(idData) && !Utilities.isEmpty(typeData))
                                    {
                                        body += "{" + typeData + "," + idData + "}";
                                    }      
                                    if(index  < size -1)
                                    {
                                        body += ",";
                                    }
                                    index++;                                    
                                    relationIdCounter++;
                                }
                            }
                           */
                   }    

                   body += "]"; // end of relationship data
                   body += "}}";     
                }                
                            
                
                body += "}";
                /*
                if(entries.size() > 1 && entries.get(lastItemPos) != t )
                {
                    body += ",\r";
                }
                */
                idCounter++;
            }
            
            body += "]"; // end all data
            
            //process included items
            if(included.size() > 0)
            {
               body += ","; 
               body += "\"included\":[";
               idCounter = 1;
               Map<String, Object> attributes = new LinkedHashMap();
               int includedSize = included.size();
               for(Object inc : included)
               {
                   Type typeAnnotation = inc.getClass().getAnnotation(Type.class);
                   if(!(inc instanceof List<?>))
                   {
                        String idData = "";
                        String typeData = "";                       
                        Field[] objFields = inc.getClass().getDeclaredFields();
                        for(Field f : objFields)
                        {
                            if(f.getName() == "id")
                            {
                                idData += "\"id\":" + "\"" + idCounter + "\"";
                            }
                            else //attributes
                            {                                    
                                 attributes.put(f.getName(), ClassUtils.getFieldValue(f, inc));
                            }
                            
                        }
                        typeData += "\"type\":\"" + typeAnnotation.value().toString() + "\"";
                        body += "{" + typeData + "," + idData + ",";
                        body += "\"attributes\":{";
                        int entrySetSize = attributes.size();
                        int entryCounter = 1;
                        for(Entry entry : attributes.entrySet())
                        {
                             body += "\"" +entry.getKey() + "\":\"" + entry.getValue() + "\"";
                             if(entrySetSize > 1 && entryCounter != entrySetSize  )
                            {
                                body += ",\r";
                            }
                             entryCounter++;
                        }
                        body += "}"; // end attributes
                        body += "}"; // end included  data
                        
                        if(includedSize > 1 && included.get(includedSize - 1) != inc )
                        {
                            body += ",\r";
                        }                        
                    }                  
                                      
                   idCounter++;
               }               
               body += "]";//end included array
            }            
            body += "}"; // end body
            return body;
        }
        return null;
    }
    
        return body;
    }
    
    /**
     * Returns list of T entries with relationships and included data as JSON API string
     * @param <T>
     * @param entries
     * @param type
     * @return String the JSON API body
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException 
     * @throws com.mobiweb.completeit.common.exception.InvalidJsonApiClassException 
     */
    public static <T extends Object> String toJsonApiWithIncludedRelationships(List<T> entries , Class<T> type) 
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InvalidJsonApiClassException
    {        
        Type annotation = type.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);
        if(annotation == null){
            throw new InvalidJsonApiClassException("@Type annotation is not declared in class type <" + Type.class.getName()+">");
        }
        
        String typeName = annotation.value();
        Field[] fields = type.getClass().getDeclaredFields();
        List<Object> included = new ArrayList();
        String relationshipName = "";
        
        String body = "{\"data\" : ";
        body += "["; 
        if(entries != null)
        {
            int lastItemPos = entries.size()-1;
            //body += "["; 
            int idCounter = 1;
            for (T t : entries)
            {         
                List<Object> relationships = new ArrayList();
                fields = t.getClass().getDeclaredFields();
                body += "{";
                body += "\"type\"" + " : \"" + typeName + "\",";
                Map<String,Object> id = ClassUtils.getFieldNameValuePair(t, "id");                  
                body += "\"id\"" + " : \"" + id.get("id") + "\",";  
                body += "\"attributes\"" + " : " + "{";
                int currentPos = 1;
                for(Field field : fields)
                {   
                    Relationship relationshipAnnotation = field.getAnnotation(Relationship.class);
                    String name = field.getName();
                    Object value = ClassUtils.getFieldValue(field, t);
                    if(name != null && !name.equals("id") && relationshipAnnotation == null) // do not include id and relationship fields
                    {
                        if(value == null)
                        {
                           body += "\"" + name + "\"" + " : " + value;  
                        }
                        else
                        {
                            body += "\"" + name + "\"" + " : \"" + value + "\""; 
                        } 
                        if(currentPos < fields.length - 2) // -2 : because id and Relationship annotated fields are not needed here!
                        {
                            body += ","; 
                        }                         
                        currentPos++; 
                    }
                    else if(relationshipAnnotation != null)
                    {                        
                        Field[] relationFields = relationshipAnnotation.getClass().getDeclaredFields();
                        for(Field f : relationFields)
                        {
                            if(f.getName().equals("value")){
                                relationshipName = f.getName();
                            }
                        }
                        relationshipName = relationshipAnnotation.value().toString();
                        relationships.add(value); // add relationship value-list to cache
                        included.add(value); // add included item to parse after relationships field is done                       
                    }
                }
                
                body += "}";                
                
                //include relationships if any
                if(relationships.size() > 0)
                {
                   body += ",";
                   body += "\"relationships\":{";
                   body += "\"" + relationshipName + "\":{";
                   body += "\"data\":[";
                   if(relationships.size() > 0) // write data to json body
                   {                       
                       String idData = "";
                       String typeData = "";                       
                       for(Object relationship : relationships)
                       {
                           if(!(relationship instanceof List<?>))
                           {
                               //specific type found, process fields
                               Field[] relationshipFields = relationship.getClass().getDeclaredFields();
                               for(Field f : relationshipFields)
                               {                                   
                                   if(f.getName() == "id")
                                   {
                                       idData += "\"id\":" + "\"" + idCounter + "\"";
                                   }  
                               }
                               typeData += "\"type\":\"" + relationshipName + "\"";                               
                               
                               if(!Utilities.isEmpty(idData) && !Utilities.isEmpty(typeData))
                               {
                                   body += "{" + typeData + "," + idData + "}";
                               }
                           }
                           else if(relationship instanceof List<?>)
                           {
                               List<?> objects = (List<?>)relationship;
                               int relationshipIdCounter = 1;
                               int index = 0;
                               int size = objects.size();
                               for(Object relationshipItem : objects)
                               {
                                   Type itemType = relationshipItem.getClass().getAnnotation(Type.class);
                                   Field[] relationshipItemFields = relationshipItem.getClass().getDeclaredFields();
                                    for(Field f : relationshipItemFields)
                                    {                                   
                                        if(f.getName() == "id")
                                        {
                                            idData = "\"id\":" + "\"" + relationshipIdCounter + "\"";
                                        }  
                                    }
                                    typeData = "\"type\":\"" + itemType.value().toString() + "\"";                               

                                    if(!Utilities.isEmpty(idData) && !Utilities.isEmpty(typeData))
                                    {
                                        body += "{" + typeData + "," + idData + "}";
                                    }      
                                    if(index  < size -1)
                                    {
                                        body += ",";
                                    }
                                    index++;                                    
                                    relationshipIdCounter++;
                                }
                            }                           
                       }
                   }

                   body += "]"; // end of relationship data
                   body += "}}";     
                }                
                                
                body += "}";
                if(entries.size() > 1 && entries.get(lastItemPos) != t )
                {
                    body += ",\r";
                } 
                
                idCounter++;
            }
            
            body += "]"; // end all data
            
            //process included items
            if(included.size() > 0)
            {
               body += ","; 
               body += "\"included\":[";
               idCounter = 1;
               Map<String, Object> attributes = new LinkedHashMap();
               int includedSize = included.size();
               for(Object inc : included)
               {
                   Type typeAnnotation = inc.getClass().getAnnotation(Type.class);
                   if(!(inc instanceof List<?>))
                   {
                        String idData = "";
                        String typeData = "";                       
                        Field[] objFields = inc.getClass().getDeclaredFields();
                        for(Field f : objFields)
                        {
                            if(f.getName() == "id")
                            {
                                idData += "\"id\":" + "\"" + idCounter + "\"";
                            }
                            else //attributes
                            {                                    
                                 attributes.put(f.getName(), ClassUtils.getFieldValue(f, inc));
                            }
                            
                        }
                        typeData += "\"type\":\"" + typeAnnotation.value().toString() + "\"";
                        body += "{" + typeData + "," + idData + ",";
                        body += "\"attributes\":{";
                        int entrySetSize = attributes.size();
                        int entryCounter = 1;
                        for(Entry entry : attributes.entrySet())
                        {
                             body += "\"" +entry.getKey() + "\":\"" + entry.getValue() + "\"";
                             if(entrySetSize > 1 && entryCounter != entrySetSize  )
                            {
                                body += ",\r";
                            }
                             entryCounter++;
                        }
                        body += "}"; // end attributes
                        body += "}"; // end included  data
                        
                        if(includedSize > 1 && included.get(includedSize - 1) != inc )
                        {
                            body += ",\r";
                        }                        
                    }                  
                                      
                   idCounter++;
               }               
               body += "]";//end included array
            }            
            body += "}"; // end body
            return body;
        }
        
        // null data 
        body += "]}"; 
        return body;
    }

    public static <T extends Object> boolean hasDuplicatesJsonTypeId(List<T> collection) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        HashSet set = new HashSet();
        for (int i = 0; i < collection.size(); i++) {
            T t = collection.get(i);
            Field field = t.getClass().getDeclaredField("id");
            Id idAnnotation = field.getAnnotation(Id.class);
            Object id = ClassUtils.getFieldValue(field, t);
            boolean val = set.add(id);
            if (val == false && idAnnotation != null) {
                return !val;
            }
        }
        return false;
    }
}
    
