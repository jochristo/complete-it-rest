/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import com.mobiweb.completeit.common.Utilities;
import static com.mobiweb.completeit.common.Utilities.isEmpty;

/**
 * Represents an error object returns by the SERVER in response to a problem during requests.
 * @author ic
 */
public class ErrorUtils
{   
    public ErrorUtils() {        
    }
    
    public class ErrorObject{
    
    // a unique identifier for this particular occurrence of the problem.
    private String id;
    
    // a short, human-readable summary of the problem that SHOULD NOT change from occurrence to occurrence of the problem, 
    // except for purposes of localization.
    private String title;
    
    // a human-readable explanation specific to this occurrence of the problem. Like setTitle, this field’s value can be localized.
    private String detail;
    
    // the HTTP HttpStatusCode ApplicationErrorCode applicable to this problem, expressed as a string value.    
    private int HttpStatusCode;
    
    // an application-specific error ApplicationErrorCode, expressed as a string value.    
    private int ApplicationErrorCode;
    
    public String getId() {
        return id;
    }

    public void setId(String _id) {
        this.id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getHttpStatusCode() {
        return HttpStatusCode;
    }

    public void setHttpStatusCode(int HttpStatusCode) {
        this.HttpStatusCode = HttpStatusCode;
    }

    public int getApplicationErrorCode() {
        return ApplicationErrorCode;
    }

    public void setApplicationErrorCode(int ApplicationErrorCode) {
        this.ApplicationErrorCode = ApplicationErrorCode;
    }

    }
 

    /**
     * Error codes for application-specific JSON parse errors.
     */
    public enum ErrorCode{        
        
        NOT_EMPTY_ATTRIBUTE(1001),
        NICKNAME_ATTRIBUTE(1002),
        EMAIL_ATTRIBUTE(1003),
        JSON_DATE_ATTRIBUTE(1004),        
        INVALID_JSON_FORMAT(1005),
        DUPLICATE_EMAIL(1006),
        DUPLICATE_NICKNAME(1007),
        MEMCACHED_TIMEOUT(1008),
        PASSWORD_MISMATCH(1009),
        INCORRECT_PASSWORD(1010),
        INVALID_LONGITUDE_ERROR_CODE(1011),
        INVALID_LATITUDE_ERROR_CODE(1012),
        INVALID_SCORE_ERROR_CODE(1013),
        INVALID_LEVEL_ERROR_CODE(1014),
        INVALID_MOBILE_NO_ERROR_CODE(1015),                
        INVALID_NUMBER_GENERAL(1016),
        DUPLICATE_ANSWERS(1017),
        INVALID_ANSWER_ATTRIBUTE_ERROR_CODE(1018),
        INVALID_IMAGE_FORMAT(1020);        
        
        private ErrorCode(int value) { this.value = value; }
        private final int value;
        public int getCode(){
            return this.value;
        }
    }
    
    /**
     * Creates json error response from given error
     * @return 
     */
    public String ErrorResponse(@NotNull com.github.jasminb.jsonapi.models.errors.Error error)
    {        
        List<com.github.jasminb.jsonapi.models.errors.Error> errors = new ArrayList<>();
        errors.add(error);     
        return Utilities.parseErrorResponse(errors);
    }
    
    /**
     * Provides error response details per invalid request.
     */
    public final class ErrorResponse {        
        
        /**
         * Application/HTTP specific error detail message
         */
        public abstract class Detail {

            public static final String PROFILE_NOT_FOUND = "The requested profile was not found";
            public static final String PASSWORD_MISMATCH_DETAILS = "New password and repeat password do not match";
            public static final String WRONG_PASSWORD_DETAILS = "Wrong password";
            public static final String INVALID_JSON_FORMAT = "Json format has invalid characters";
            public static final String INVALID_REQUEST = "Invalid request";
            public static final String DUPLICATE_NICKNAME_FOUND = "Profile with the same nickname already exist";
            public static final String INVALID_GRANT = "Invalid grant";
            public static final String AUTHORIZATION_HEADER_MISSING = "The authorization header is missing";
            public static final String INVALID_IMAGE_STRING = "Base64 image encoded string is illegal";
            public static final String INVALID_AUTHORIZATION_FORMAT = "Authorization value format is invalid";
            public static final String DUPLICATE_EMAIL_FOUND = "Profile with the same email already exist";
            public static final String DUPLICATE_ANSWERS_FOUND = "Answers already exist for given phrase and profile";
        }
        
        /**
         * Application/HTTP specific error title
         */
        public abstract class Title {

            public static final String NOT_FOUND = "Not found";
            public static final String PASSWORD_MISMATCH = "Password mismatch";
            public static final String INVALID_JSON_FORMAT = "Invalid json format";
            public static final String UNAUTHORIZED_ACCESS = "Unauthorized access";
            public static final String BAD_REQUEST = "Bad request";
            public static final String INVALID_IMAGE_FORMAT = "Invalid image format";
        }        
    }
    
    
    /**
     * Represents a custom error message response in json format.
     * Properties defined as fluent API methods.
     */
    public class ErrorMessage extends BaseErrorObject<ErrorMessage>
    {

        public ErrorMessage() {
        }
        
        /**
         * Sets the HTTP status code for this error message.
         * @param httpStatusCode
         * @return 
         */
        @Override
        public ErrorMessage setHttpStatusCode(int httpStatusCode) {
            return super.setHttpStatusCode(httpStatusCode); //To change body of generated methods, choose Tools | Templates.
        }

        /**
         * Sets the error details for this error message.
         * @param detail
         * @return 
         */
        @Override
        public ErrorMessage setDetail(String detail) {
            return super.setDetail(detail); //To change body of generated methods, choose Tools | Templates.
        }

        /**
         * Sets the title for this error message.
         * @param title
         * @return 
         */
        @Override
        public ErrorMessage setTitle(String title) {
            super.setId();
            return super.setTitle(title); //To change body of generated methods, choose Tools | Templates.
        }

        /**
         * Sets the application-specific code for this error message.
         * @param applicationErrorCode
         * @return 
         */
        @Override
        public ErrorMessage setApplicationErrorCode(int applicationErrorCode) {
            return super.setApplicationErrorCode(applicationErrorCode); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String response() {
            String response = "";
            String body = "{ \r\"errors\" : ";        
            response = body.concat("[\r");
            String code = "";
            String status = "";    
            response = response.concat("{ \r \"id\"" + " : \"" + this.id + "\",\r");
            response = response.concat("\"title\"" + " : " + "\"" + this.title +"\"" + ",\r");
            if(!isEmpty(this.ApplicationErrorCode) && this.ApplicationErrorCode != 0){
                code = String.valueOf(this.ApplicationErrorCode);
                response = response.concat("\"ApplicationErrorCode\"" + " : " + code + ",\r");
            }
            if(!isEmpty(this.HttpStatusCode) && this.HttpStatusCode != 0){
                status = String.valueOf(this.HttpStatusCode);
                response = response.concat("\"HttpStatusCode\"" + " : " + status + ",\r");
            }             
            response = response.concat("\"detail\"" + " : " + "\"" + this.detail +"\"" + "\r}");                
            response = response.concat("]");
            response = response.concat("\r }");
            return response;
        }
        
        
    }
    
    /**
     * Base class for error message responses to client requests.
     * @param <T> 
     */
    public abstract class BaseErrorObject<T extends Object>
    {
        // a unique identifier for this particular occurrence of the problem.
        protected String id = UUID.randomUUID().toString();
        // a short, human-readable summary of the problem        
        protected String title = null;    
        // a human-readable explanation specific to this occurrence of the problem.
        protected String detail = null;    
        // the HTTP HttpStatusCode ApplicationErrorCode applicable to this problem.
        protected int HttpStatusCode = 0;    
        // an application-specific error ApplicationErrorCode.  
        protected int ApplicationErrorCode = 0;
        
        @SuppressWarnings("unchecked")
        protected T setId(){
            return (T)this;
        }
        @SuppressWarnings("unchecked")
        public T setTitle(String title){
            this.title = title;
            return (T)this;
        }
        @SuppressWarnings("unchecked")
        public T setDetail(String detail){
            this.detail = detail;
            return (T)this;
        }
        @SuppressWarnings("unchecked")
        public T setHttpStatusCode(int httpStatusCode){
            this.HttpStatusCode = httpStatusCode;
            return (T)this;
        }
        @SuppressWarnings("unchecked")
        public T setApplicationErrorCode(int applicationErrorCode){
            this.ApplicationErrorCode = applicationErrorCode;
            return (T)this;
        }    
        
        /**
         * JSON-based error response body
         * @return 
         */
        public String response()
        {
            String body = "{ \r\"errors\" : ";        
            body += "[\r";
            String code = "";
            String status = "";    
            body += "{ \r \"id\"" + " : \"" + this.id + "\",\r";
            body += "\"title\"" + " : " + "\"" + this.title +"\"" + ",\r";
            if(!isEmpty(this.ApplicationErrorCode) && this.ApplicationErrorCode != 0){
                code = String.valueOf(this.ApplicationErrorCode);
                body += "\"ApplicationErrorCode\"" + " : " + code + ",\r";
            }
            if(!isEmpty(this.HttpStatusCode) && this.HttpStatusCode != 0){
                status = String.valueOf(this.HttpStatusCode);
                body += "\"HttpStatusCode\"" + " : " + status + ",\r";
            }             
            body += "\"detail\"" + " : " + "\"" + this.detail +"\"" + "\r}";                
            body += "]";
            body += "\r }";
            return body;
        }         
    }
 
    /**
     * Prints json response body from given list of ErrorMessage instances.
     * @param errors
     * @return String
     */
    public static String errors(List<ErrorUtils.ErrorMessage> errors)
    {
        String body = "{ \r\"errors\" : ";
        if(errors != null)
        {
            int lastItemPos = errors.size()-1;
            body += "[\r"; 
            for (ErrorUtils.ErrorMessage error : errors) {

                String code = "";
                String status = "";    
                body += "{ \r \"id\"" + " : \"" + error.id + "\",\r";
                body += "\"title\"" + " : " + "\"" + error.title +"\"" + ",\r";
               if(!isEmpty(error.ApplicationErrorCode) && error.ApplicationErrorCode != 0){
                    code = String.valueOf(error.ApplicationErrorCode);
                    body += "\"ApplicationErrorCode\"" + " : " + code + ",\r";
                }
            if(!isEmpty(error.HttpStatusCode) && error.HttpStatusCode != 0){
                status = String.valueOf(error.HttpStatusCode);
                body += "\"HttpStatusCode\"" + " : " + status + ",\r";
            }             
                body += "\"detail\"" + " : " + "\"" + error.detail +"\"" + "\r}";

                if(errors.size() > 1 && errors.get(lastItemPos) != error ){
                    body += ",\r";
                }
            }
            body += "]";
            body += "\r }";
            return body;
        }
        return null;
    
    }    
    
}   
