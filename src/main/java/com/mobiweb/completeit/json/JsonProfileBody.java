/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import java.text.SimpleDateFormat;
import com.mobiweb.completeit.annotations.NotEmptyAttribute;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.annotations.NicknameAttribute;
import com.mobiweb.completeit.annotations.JsonDateAttribute;

/**
 *
 * @author ic
 */
@Type("profiles")
public class JsonProfileBody {
    
    public JsonProfileBody() {
        this.id = "";
    }
        
    public JsonProfileBody(String id, String nickname, String picture, String dob, 
            String location, String language, String email, String password) {
        this.id = "";
        

    }    
    
    public JsonProfileBody(Profile profile) {
        
        final SimpleDateFormat sdfSlash = new SimpleDateFormat("dd/MM/yyyy");  
        final SimpleDateFormat sdfDash = new SimpleDateFormat("dd-MM-yyyy");  
        
        this.id = "";        
        this.id = Long.toString(profile.getId());
        this.nickname = profile.getNickname();
        this.pictureEncodedString = profile.getPictureFilepath();
        this.dob = sdfDash.format(profile.getDob());
        this.language = profile.getLanguage();
        this.email = profile.getEmail();        
        this.longitude = Double.toString(profile.getLocation().getX());
        this.latitude = Double.toString(profile.getLocation().getY());
        this.score = Long.toString(profile.getScore());
        this.level = Integer.toString(profile.getLevel());
        this.mobileNo = profile.getMobileNo();
        this.isMobileNoVerified = Boolean.toString(profile.isMobileNoVerified());
    }    
        
    @Id
    private String id;  
    
    @NotEmptyAttribute(message="Nickname is required")
    @NicknameAttribute(pattern = NicknameAttribute.AttributeType.NICKNAME)
    private String nickname; 
    
    //@NotEmptyAttribute(message="Picture string is required")
    private String pictureEncodedString = null; 
    
    @NotEmptyAttribute(message="Date of birth is required")
    @JsonDateAttribute
    private String dob;
    
    //@NotEmptyAttribute(message="Longitude is required")
    private String longitude = "";
    
    //@NotEmptyAttribute(message="Latitude is required")
    private String latitude = "";
    
    @NotEmptyAttribute(message="Language is required")
    private String language;
    
    @NotEmptyAttribute(message="Email is required")
    private String email;
            
    //@NotEmptyAttribute(message="Score is required")
    private String score = "";
    
    //@NotEmptyAttribute(message="Player is required")
    private String level = "";
    
    @NotEmptyAttribute(message="Mobile is required")
    private String mobileNo;
    
    private String isMobileNoVerified;

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {        
        this.dob = dob;
    }

    public String getPictureEncodedString() {
        return pictureEncodedString;
    }

    public void setPictureEncodedString(String pictureEncodedString) {
        this.pictureEncodedString = pictureEncodedString;
    }    
    
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }    
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsMobileNoVerified() {
        return isMobileNoVerified;
    }

    public void setIsMobileNoVerified(String isMobileNoVerified) {
        this.isMobileNoVerified = isMobileNoVerified;
    }      
    
}
