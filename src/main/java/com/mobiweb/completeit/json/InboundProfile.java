/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import java.text.SimpleDateFormat;
import com.mobiweb.completeit.annotations.NotEmptyAttribute;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.annotations.NicknameAttribute.AttributeType;
import com.mobiweb.completeit.annotations.EmailAttribute;
import com.mobiweb.completeit.annotations.NicknameAttribute;
import com.mobiweb.completeit.annotations.JsonDateAttribute;
import com.mobiweb.completeit.annotations.NumericAttribute;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.LATITUDE;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.LONGITUDE;
import static com.mobiweb.completeit.json.validators.NumericValidator.*;

/**
 * Represents an raw inbound json-based profile data from client request.
 * @author ic
 */
@Type("profiles")
public class InboundProfile {
    
    public InboundProfile() {
        this.id = "";
    }
        
    public InboundProfile(String id, String nickname, String picture, String dob, 
            String location, String language, String email, String password) {
        this.id = "";
    }    
    
    public InboundProfile(Profile profile) {
        
        final SimpleDateFormat sdfSlash = new SimpleDateFormat("dd/MM/yyyy");  
        final SimpleDateFormat sdfDash = new SimpleDateFormat("dd-MM-yyyy");  
        
        this.id = "";        
        this.id = Long.toString(profile.getId());
        this.nickname = profile.getNickname();
        this.pictureEncodedString = profile.getPictureFilepath();
        this.dob = sdfSlash.format(profile.getDob());
        this.language = profile.getLanguage();
        this.email = profile.getEmail();
        this.password = profile.getPassword();    
        this.longitude = Double.toString(profile.getLocation().getX());
        this.latitude = Double.toString(profile.getLocation().getY());
        this.score = Long.toString(profile.getScore());
        this.level = Integer.toString(profile.getLevel());
        this.mobileNo = profile.getMobileNo();
        this.isMobileNoVerified = Boolean.toString(profile.isMobileNoVerified());
        
        this.gender = profile.getGender();
    }    
        
    @Id
    private String id;  
    
    @NotEmptyAttribute(message="Nickname is required")
    @NicknameAttribute(pattern = AttributeType.NICKNAME)
    private String nickname;     
    
    private String pictureEncodedString = null; 
    
    @NotEmptyAttribute(message="Date of birth is required")
    @JsonDateAttribute
    private String dob;
        
    @NumericAttribute(pattern=LONGITUDE, title=INVALID_LONGITUDE_TITLE, message=INVALID_LONGITUDE_MESSAGE, code=INVALID_LONGITUDE_ERROR_CODE)
    private String longitude = "";    
    
    @NumericAttribute(pattern=LATITUDE, title=INVALID_LATITUDE_TITLE, message=INVALID_LATITUDE_MESSAGE, code=INVALID_LATITUDE_ERROR_CODE)
    private String latitude = "";
    
    @NotEmptyAttribute(message="Language is required")
    private String language;
    
    @NotEmptyAttribute(message="Email is required")
    @EmailAttribute
    private String email;
    
    @NotEmptyAttribute(message="Password is required")
    private String password;      
        
    private String score = "";    
        
    private String level = "";
    
    @NotEmptyAttribute(message="Mobile is required")
    private String mobileNo;
    
    private String isMobileNoVerified = "false";

    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {        
        this.dob = dob;
    }

    public String getPictureEncodedString() {
        return pictureEncodedString;
    }

    public void setPictureEncodedString(String pictureEncodedString) {
        this.pictureEncodedString = pictureEncodedString;
    }    
    
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }    
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsMobileNoVerified() {
        return isMobileNoVerified;
    }

    public void setIsMobileNoVerified(String isMobileNoVerified) {
        this.isMobileNoVerified = isMobileNoVerified;
    }    
    
}
