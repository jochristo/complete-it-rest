/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.domain.Phrase;

/**
 * Represents a JSON API phrase with statistics HTTP response object.
 * @author ic
 */
@Type("phrases")
public class OutboundStatisticsPhrase {
    
    public OutboundStatisticsPhrase() {
        super();
        this.statistics = new StatisticsItem();
    }
    
    public OutboundStatisticsPhrase(Phrase phrase)
    {
        
        this.id = String.valueOf(phrase.getId());
        this.phrase = phrase.getPhrase();
        this.gaps = String.valueOf(phrase.getGaps());        
        this.statistics = new StatisticsItem();
    }
        
    public OutboundStatisticsPhrase(Phrase phrase, StatisticsItem stats)
    {
        this.id = String.valueOf(phrase.getId());
        this.phrase = phrase.getPhrase();
        this.gaps = String.valueOf(phrase.getGaps());        
        this.statistics = new StatisticsItem();      
        this.statistics = stats;
    }    
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    //@TransientId
    private String id;  
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String phrase; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String gaps;     
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @Relationship(value="statistics",resolve = true )
    private StatisticsItem statistics;    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatisticsItem getStatistics() {
        return statistics;
    }

    public void setStatistics(StatisticsItem statistics) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        this.statistics = statistics;    
        /*
        if(Utilities.hasDuplicatesJsonTypeId(statistics))
        {
            int idCounter = 1;
            for(StatisticsItem item : statistics)
            {
                item.setId(String.valueOf(idCounter));            
                idCounter++;
            }
        } 
        */
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getGaps() {
        return gaps;
    }

    public void setGaps(String gaps) {
        this.gaps = gaps;
    }
    
    
    
    @Override
    public String toString() {
        
        String json = "{\n\"data\":[{";
        json += "\"type\":\"answers\",";
        json += "\"id\":\"" + this.id + "\",";
        json += "\"attributes\":{\"phrase\":" + this.getPhrase() + "},";        
        json += "\"relationships\":{\"statistics\":{\"data\":[";
        json+= "{\"type\":\"statistics\",\"id\":" + statistics.getId() + "}";
        json += "]"; // end of data in relationships
        json += "}"; // end of gap
        json += "}"; // end of relationships
        json += "}],"; // end data       
        
        // add included data
        json += "\n\"included\":[";        
        json+= "{\"type\":\"statistics\",\"id\":" + statistics.getId() + ",";
        json+= "\"attributes\":{\"males\":\"" + statistics.getMales() + "\",";
        json+= "\"females\":\"" + statistics.getFemales() + "\",";
        json+= "\"averageAge\":\"" + statistics.getAverageAge() + "\"}";        
        json += "]}"; // end of all
        return json;
    }    
    
}
