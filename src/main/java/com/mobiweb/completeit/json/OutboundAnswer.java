/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.mobiweb.completeit.domain.Answer;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a JSON API outbound answer HTTP response object.
 * @author ic
 */
@Type("answers")
public class OutboundAnswer {
    
    public OutboundAnswer()
    {        
        this.words = new ArrayList();
    }
    
    public OutboundAnswer(Answer answer)
    {        
        this.words = new ArrayList();
        this.id = String.valueOf(answer.getId());
        this.phraseId = String.valueOf(answer.getPhrase().getId());
        this.profileId = String.valueOf(answer.getProfile().getId());
        Gson gson = new Gson();
        AnsweredWord[] answeredWords = gson.fromJson(answer.getAnswers(), AnsweredWord[].class);
        List<AnsweredWord> wordsList = Lists.newArrayList(answeredWords);
        setWords(wordsList);
    }    
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String phraseId; 
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @Relationship(value="word",resolve = true )
    private List<AnsweredWord> words;
             
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String profileId;   

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhraseId() {
        return phraseId;
    }

    public void setPhraseId(String phraseId) {
        this.phraseId = phraseId;
    }

    public List<AnsweredWord> getWords() {
        return words;
    }

    public void setWords(List<AnsweredWord> words) {
        this.words = words;        
        int idCounter = 1;
        for(AnsweredWord word : words)
        {
            word.setId(String.valueOf(idCounter));            
            idCounter++;
        }
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
    
    
}
