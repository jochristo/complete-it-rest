/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.TransientId;

/**
 * Represents a JSON API gap relationship item of a particular InboundAnswer object.
 * @author ic
 */
@Type("gaps")
public class RelationGaps {
    
    public RelationGaps() {
    }
        
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @TransientId
    private String id; 
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String word;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }


    
    
}
