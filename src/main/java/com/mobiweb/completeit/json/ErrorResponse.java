/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

/**
 *
 * @author ic
 */
public class ErrorResponse extends com.github.jasminb.jsonapi.models.errors.ErrorResponse{

    @Override
    public String toString() {
        return "{" + "errors=" + (this.getErrors() != null ? this.getErrors() : "Undefined") + 
 				'}'; 
        
    }
    
    
    
}
