/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.RelType;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.mobiweb.completeit.common.Utilities;
import javax.validation.constraints.NotNull;
import com.mobiweb.completeit.domain.Answer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a JSON API inbound answer HTTP request object.
 * @author ic
 */
@Type("answers")
public class InboundAnswer {
 
    public InboundAnswer() {
        gaps = new ArrayList();        
    }
    
    public InboundAnswer(@NotNull Answer answer)
    {
        this.id = String.valueOf(answer.getId());        
        this.phraseId = String.valueOf(answer.getPhrase().getId());        
        Gson gson = new Gson();
        AnsweredWord[] answeredWords = gson.fromJson(answer.getAnswers(), AnsweredWord[].class);
        List<AnsweredWord> words = Lists.newArrayList(answeredWords);        
        gaps = new ArrayList(); 
        int index = 1;
        for(AnsweredWord word : words)
        {
            RelationGaps gap = new RelationGaps();
            gap.setId(String.valueOf(index));
            gap.setWord(word.getWord());
            this.gaps.add(gap);
            index++;
        }
    }
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String phraseId;
    
    @Relationship(value = "gap", resolve = true,relType = RelType.SELF)
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private List<RelationGaps> gaps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhraseId() {
        return phraseId;
    }

    public void setPhraseId(String phraseId) {
        this.phraseId = phraseId;
    }

    public List<RelationGaps> getGaps() {
        return gaps;
    }

    public void setGaps(List<RelationGaps> gaps) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        this.gaps = gaps;
        if(JsonParser.hasDuplicatesJsonTypeId(gaps))
        {
            int idCounter = 1;
            for(RelationGaps gap : gaps)
            {
                gap.setId(String.valueOf(idCounter));            
                idCounter++;
            }
        }
    }

    @Override
    public String toString() {
        
        String json = "{\n\"data\":[{";
        json += "\"type\":\"answers\",";
        json += "\"id\":\"" + this.id + "\",";
        json += "\"attributes\":{\"phraseId\":" + this.getPhraseId() + "},";        
        json += "\"relationships\":{\"gap\":{\"data\":[";
        if(!Utilities.isEmpty(gaps))
        {
            int index = 0;
            int size = gaps.size();
            for(RelationGaps gap : gaps)
            {
                json+= "{\"type\":\"gaps\",\"id\":" + gap.getId() + "}";
                if(index  < size -1)
                {
                    json += ",";
                }
                index++;
            }
        }
        json += "]"; // end of data in relationships
        json += "}"; // end of gap
        json += "}"; // end of relationships
        json += "}],"; // end data       
        
        // add included data
        json += "\n\"included\":[";
        int index = 0;
        int size = gaps.size();
        for(RelationGaps gap : gaps)
        {
            json+= "{\"type\":\"gaps\",\"id\":" + gap.getId() + ",";
            json+= "\"attributes\":{\"word\":\"" + gap.getWord() + "\"}}";
            if(index  < size -1)
            {
                json += ",";
            }
            index++;
        }        
        json += "]}"; // end of all
        return json;
    }
    
    
    
  
    
}
