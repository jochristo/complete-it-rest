/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.NumericAttribute;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.SCORE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_ERROR_CODE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_MESSAGE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_TITLE;
import java.util.UUID;

/**
 * Represents a JSON API request body to update a profile score.
 * Score long value is validated.
 * @author ic
 */
@Type("scores")
public class InboundScore {
    
    @Id    
    private String id = "";
    
    @NumericAttribute(pattern=SCORE, title=INVALID_SCORE_TITLE, message=INVALID_SCORE_MESSAGE, code=INVALID_SCORE_ERROR_CODE)
    private String score = "";

    public InboundScore() {
        this.id = UUID.randomUUID().toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
