/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.TransientId;
import static com.mobiweb.completeit.common.ClassUtils.*;
import com.mobiweb.completeit.common.Utilities;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.validation.constraints.NotNull;

/**
 * JSON API stream generator to serialize given source object and its single relationship property object to 
 * JSON API format to include "relationships" and "include" sections.
 * Provides method to return a JSON API formatted string.
 * @author ic
 * @param <S> the source object to decode and write to output stream as JSON API format string.
 * @param <R> the relationship annotated field or property (@Relationship) of the source object to include in the 
 * relationships and include sections of JSON API document.
 */
public class JsonApiStreamGenerator<S,R extends Object> {

    private Class<S> sourceType = null;
    private Class<R> relationshipType = null;
    private String typeName = null;
    private String relationshipTypeName = null;
    private final StringWriter writer = new StringWriter();
    protected JsonGenerator generator = null;
    private boolean includeRelationships = false;
    private boolean hasRelationships = false;
    private Object source = null;
    private Object relationships = null;    
    
    //meta & links fiels
    private boolean isPaginated = false;
    private String meta = null;
    public String linksBaseUrl = null;
    private String links = null;
    private long count = 0;
    public long page = 0;
    public long size = 0;
        
    public JsonApiStreamGenerator(S source, Class<R> relationshipType) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        
        this.relationshipType = relationshipType;
        this.generator = (JsonGenerator) Json.createGenerator(writer);
        this.source = source;
        Class clazz = source.getClass();
        this.sourceType = clazz;
        this.relationships = getRelationshipsValue(this.source);
        this.hasRelationships = isRelationshipsAvailable(source);
    }

    public JsonApiStreamGenerator(List<S> source, Class<R> relationshipType) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
        
        this.relationshipType = relationshipType;
        this.generator = (JsonGenerator) Json.createGenerator(writer);
        this.source = source;
        Class clazz = source.getClass();
        this.sourceType = clazz;        
        this.relationships = getRelationshipsValue(this.source);
        this.hasRelationships = isRelationshipsAvailable(source);
    }    

    public JsonApiStreamGenerator(S source, Class<R> relationshipType, long count) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        
        this.relationshipType = relationshipType;
        this.generator = (JsonGenerator) Json.createGenerator(writer);
        this.source = source;
        Class clazz = source.getClass();
        this.sourceType = clazz;
        this.relationships = getRelationshipsValue(this.source);
        this.hasRelationships = isRelationshipsAvailable(source);
        
        //set meta & links
        isPaginated = true;
        this.count = count;
        this.meta  = ": {\n\"count\":" + count + "\n}";
    }

    public JsonApiStreamGenerator(List<S> source, Class<R> relationshipType, long count) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
        
        this.relationshipType = relationshipType;
        this.generator = (JsonGenerator) Json.createGenerator(writer);
        this.source = source;
        Class clazz = source.getClass();
        this.sourceType = clazz;        
        this.relationships = getRelationshipsValue(this.source);
        this.hasRelationships = isRelationshipsAvailable(source);
        
        //set meta & links
        isPaginated = true;
        this.count = count;
        this.meta  = ": {\n\"total\":" + count + "\n}";        
    }    
    
    
    private void writeLinks()
    {
        String self = null;
        String first = null;
        String prev = null;
        String next = null;
        String last = null;
        long lastPage = 0;
        
        if(!Utilities.isEmpty(this.meta) && !Utilities.isEmpty(this.linksBaseUrl) && this.page > 0 && (this.size > 0 && this.count > 0))
        {            
            lastPage = Utilities.roundUp(count, size);
            self = this.linksBaseUrl + "?page=" + this.page + "&size=" + this.size;
            first = this.linksBaseUrl + "?page=1&size=" + this.size;
            if(page > 1)
            {
                long previous = page - 1;
                prev = this.linksBaseUrl + "?page=" + previous + "&size=" + this.size;
            }
            
            if(this.page < lastPage)
            {
               long nextPage = this.page + 1;
               next =  this.linksBaseUrl + "?page=" + nextPage + "&size=" + this.size;
            }
            
            if(lastPage > 0 && lastPage != page)
            {
                last =  this.linksBaseUrl + "?page=" + lastPage + "&size=" + this.size;
            }
            
            generator.writeStartObject("links");
            generator.write("self", self);
            generator.write("first", first);
            if(prev != null)
            {
                generator.write("prev", String.valueOf(prev));
            }
            if(next != null)
            {
               generator.write("next", String.valueOf(next)); 
            }
            if(last != null)
            {
               generator.write("last", String.valueOf(last)); 
            }            
            generator.writeEnd();            
        }
    }
    
    private void writeMeta()
    {
        if(!Utilities.isEmpty(this.meta) && this.count > 0)
        {            
            generator.writeStartObject("meta");
            generator.write("total", String.valueOf(this.count));            
            long lastPage = Utilities.roundUp(count, size);
            generator.write("totalPages", String.valueOf(lastPage));
            generator.write("currentPage", String.valueOf(this.page));
            generator.write("pageSize", String.valueOf(this.size));
            generator.writeEnd();
        }        
    }
    
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRelationshipTypeName() {
        return relationshipTypeName;
    }

    public void setRelationshipTypeName(String relationshipTypeName) {
        this.relationshipTypeName = relationshipTypeName;
    }    
    
    public Object getObject() {
        return source;
    }    
    
    /**
     * Checks if Object to serialize has the @Type annotation
     * @return 
     */
    protected boolean isObjectTypeValid()
    {
        Type type = this.sourceType.getDeclaredAnnotation(Type.class);
        return type != null;
    }
    
    protected String typeValue() throws NoSuchFieldException
    {
        /*          
        //java.lang.reflect.Type type = this.source.getGenericType();
        Class cl = this.sourceType;
        java.lang.reflect.Type componentType = null;
        //this.sourceType.getClass() instanceof List.class
        if (this.source instanceof List)
        {
            List list = (List)this.source;
            Type type  = list.get(0).getClass().getAnnotation(Type.class);            
            if(type != null)
            {
                this.typeValue = type.value();
            }
            //Field field = this.source.getClass().getDeclaredField("sourceType");
            //ParameterizedType type = (ParameterizedType) field.getGenericType();
            //componentType = (Class<?>) type.getActualTypeArguments()[0];            
            //this.typeValue = componentType.getClass().getDeclaredAnnotation(Type.class).value();
        } else if (!(this.source instanceof List)) {
            this.typeValue = this.sourceType.getDeclaredAnnotation(Type.class).value();
        }
        return this.typeValue;
        */
        return null;
    }
    
    protected Object getTypeValue(Object object)
    {
        if(!(object instanceof List))
        {
            Type type  = object.getClass().getAnnotation(Type.class);            
            if(type != null)
            {
                return type.value();
            }
        }
        return null;
    }
    
    protected Object getIdValue(Object object) throws InvocationTargetException
    {
        if(!(object instanceof List))
        {
            Field[] fields = object.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Id id = f.getDeclaredAnnotation(Id.class);
                if(id != null)
                {
                    return getFieldValue(f, object);
                }
            }
        }
        return null;        
    }
    
    protected boolean isRelationshipTypeValid()
    {
        Relationship type = this.relationshipType.getDeclaredAnnotation(Relationship.class);
        return type != null;
    }    
    
    protected String relationshipName() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        if(isRelationshipsAvailable())
        {
            Field[] fields = this.source.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if(r != null)
                {
                    return r.value();
                }
            }            
        }
        return null;
    }
    
    protected String relationshipName(Object object) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        //if(isRelationshipsAvailable())
        //{
            Field[] fields = object.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if(r != null)
                {
                    return r.value();
                }
            }            
        //}
        return null;
    }    

    protected String idValue() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        if(this.source != null)
        {
                Field[] fields = this.source.getClass().getDeclaredFields();
                for(Field f : fields)
                {
                    Id r = f.getDeclaredAnnotation(Id.class);
                    if(r != null)
                    {
                        Object value = getFieldValue(f, source);
                        return String.valueOf(value);
                    }
                }
        }
        return null;
    }
    
    protected Class<?> getRelationshipType()
    {
        if(isRelationshipTypeValid())
        {
            return this.relationshipType.getClass();
        }
        return null;
    }

    public boolean isIncludeRelationships() {
        return includeRelationships;
    }

    public void setIncludeRelationships(boolean includeRelationships) {
        this.includeRelationships = includeRelationships;
    }

    private Object getRelationshipsValue() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        if(isRelationshipsAvailable())
        {
            Field[] fields = this.source.getClass().getDeclaredFields();
            for (Field f : fields) {
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if (r != null) {
                    Object value = getFieldValue(f, source);
                    if (value != null) {
                        return value;
                    }
                }
            }
        }
        return null;
    }
    
    private Object getRelationshipsValue(Object object) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field f : fields) {
            Relationship r = f.getDeclaredAnnotation(Relationship.class);
            if (r != null) {

                Object value = getFieldValue(f, object);
                if (value != null) {
                    return value;
                }
            }
        }
        return null;
    }
    
    private boolean isRelationshipsAvailable() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {       
        Field[] fields = this.source.getClass().getDeclaredFields();
        for (Field f : fields) {
            Relationship r = f.getDeclaredAnnotation(Relationship.class);
            if (r != null) {
                java.lang.reflect.Type type = f.getGenericType();
                java.lang.reflect.Type componentType = null;
                if (type instanceof ParameterizedType) {
                    java.lang.reflect.Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                    componentType = actualTypeArguments[0];                    
                } else if (type instanceof GenericArrayType) {
                    java.lang.reflect.Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                    componentType = actualTypeArguments[0];                    
                }                
                if (componentType != null && componentType.getTypeName().equals(relationshipType.getTypeName())) {
                    Object value = getFieldValue(f, source);
                    if (value != null) {                        
                        return true;
                    }
                }
            }
        }
        return this.hasRelationships;
    }
    
    private boolean isRelationshipsAvailable(Object object) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        if(object instanceof List && object != null)
        {            
            List list = (List)object;
            if(list.size() >0){
            object = list.get(0);
            }
        }
        
            Field[] fields = object.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if(r != null)
                {
                    java.lang.reflect.Type type = f.getGenericType();
                    java.lang.reflect.Type componentType = null;                    
                    if(type instanceof ParameterizedType)
                    {
                        java.lang.reflect.Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                        componentType = actualTypeArguments[0];                        
                    }
                    else if(type instanceof GenericArrayType)
                    {
                        java.lang.reflect.Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                        componentType = actualTypeArguments[0];                                                
                    }
                    else if(type.getTypeName().equals(this.relationshipType.getTypeName()))
                    {
                        Object value = getFieldValue(f, object);
                        if(value != null)
                        {                            
                            return true;
                        }
                    }
                    java.lang.reflect.Type relType = relationshipType.getGenericSuperclass();
                    if(componentType != null && componentType.getTypeName().equals(relationshipType.getTypeName()))
                    {
                        Object value = getFieldValue(f, object);
                        if(value != null)
                        {
                            //this.hasRelationships = true;
                            return true;
                        }
                    }
                }
            }      
        return this.hasRelationships;
    }    
    
    private Map<String,Object> attributes() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        Map<String,Object> attributes = new LinkedHashMap();        
        if(this.source != null && this.isObjectTypeValid())
        {            
            Field[] fields = this.source.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Id id = f.getDeclaredAnnotation(Id.class);
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if(r == null && id == null)
                {
                    Object value = getFieldValue(f, source);
                    if(value != null && f.getName() != null)
                    {
                       attributes.put(f.getName(), value);
                    }
                }
            }            
        }
        return attributes;
    }
    
    private Map<String,Object> getRelationshipsAttributes(Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        Map<String,Object> attributes = new LinkedHashMap();        
        if(obj != null && !(obj instanceof List))
        {            
            Field[] fields = obj.getClass().getDeclaredFields();
            for(Field f : fields)
            {
                Id id = f.getDeclaredAnnotation(Id.class);
                Relationship r = f.getDeclaredAnnotation(Relationship.class);
                if(r == null && id == null)
                {
                    Object value = getFieldValue(f, obj);
                    if(value != null && f.getName() != null)
                    {
                       attributes.put(f.getName(), value);
                    }
                }
            }            
        }
        return attributes;
    }    
    
    @Deprecated
    public String write() throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {              
        generator.writeStartObject();        
        generator.writeStartArray("data");        
        if(!(this.source instanceof List) && this.source != null)// single source to print, not list
        {
            generator.writeStartObject();
            generator.write("type",this.typeValue());            
            generator.write("id",this.idValue());
            generator.writeStartArray("attributes");
            int index = 0;            
            int attributesSize = attributes().size();            
            if(attributesSize > 0)
            {
                generator.writeStartObject();
            }
            for(Entry entry : attributes().entrySet())
            {
                if(index < attributesSize){
                    generator.write((String)entry.getKey(),String.valueOf(entry.getValue()));
                }                
                index++;
            }
            
            if(attributesSize > 0 && index == attributesSize)
            {
                generator.writeEnd(); // close attributes fields
                generator.writeEnd(); // close attributes array
            }
            
            Object relations = getRelationshipsValue();
            if(relations != null)
            {                
                generator.writeStartObject("relationships");
                if(!(relations instanceof List)) // relationships single pojo instance
                {
                    generator.writeStartObject(this.relationshipName()); //write relationships name
                    generator.writeStartArray("data"); //write relationships data array START
                    index = 0;
                    Field[] fields = relations.getClass().getDeclaredFields();
                    Type relationsObjectType = relations.getClass().getDeclaredAnnotation(Type.class);
                    for(Field f : fields)
                    {
                        TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                        Id id = f.getDeclaredAnnotation(Id.class);
                        if(tid != null && id != null)
                        {
                            index++;
                            generator.writeStartObject("id").write(String.valueOf(index));
                        }
                        else if(tid == null && id != null) // non transient id field
                        {
                            generator.writeStartObject("id").write(String.valueOf(getFieldValue(f,relations))); 
                        }
                        else if(id == null) // other field
                        {                           
                           generator.writeStartObject("type").write(String.valueOf(relationsObjectType.value())); 
                        }
                    }                    
                    generator.writeEnd();//end data array //write relationships data array END
                }
                else // list of objects found
                {
                    generator.writeStartObject(this.relationshipName()); //write relationships name
                    generator.writeStartArray("data"); //write relationships data array START                    
                    index = 0;
                    for(Object relationObject: (List)relations)
                    {
                        generator.writeStartObject();
                        Field[] fields = relationObject.getClass().getDeclaredFields();
                        Type relationsObjectType = relationObject.getClass().getDeclaredAnnotation(Type.class);
                        for(Field f : fields)
                        {
                            TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                            Id id = f.getDeclaredAnnotation(Id.class);
                            if(tid != null && id != null)
                            {
                                index++;                                
                                generator.write("id",index);                                
                            }
                            else if(tid == null && id != null) // non transient id field
                            {                                
                                generator.write("id",String.valueOf(getFieldValue(f, relationObject)));                             
                            }
                            else if(id == null) // other field
                            {  
                               generator.write("type",String.valueOf(relationsObjectType.value()));                               
                            }
                        }                    
                        generator.writeEnd();//end data array //write relationships data array END                        
                    }
                  
                    generator.writeEnd(); // end relationships data
                    generator.writeEnd(); // end relationships name
                }                
                generator.writeEnd(); // end relationships
            }
            generator.writeEnd();//end start source            
        } 
        
        generator.writeEnd();//end data array        
        if(hasRelationships)
        {
            this.writeIncluded(relationships);
        }
        
        generator.writeEnd();//end start source        
        generator.close();
        generator.flush();
        String data = writer.toString();
        return data;        
    }
    
    /**
     * Writes a JSON API string using an output source.
     * @return JSON API formatted string including relationships and included fields if present on the source object.
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchFieldException 
     */
    public String writeStream() throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {    
        generator.writeStartObject();        

        //write links if paginated requested
        if(this.isPaginated){            
            writeMeta();
        }
        
        generator.writeStartArray("data");               
        List relations = new ArrayList();
        List relationsCache = new ArrayList();        
        if(this.source instanceof List)
        {               
            int idCounter = 1;
            for(Object obj : (List)source)
            {                
                Object relationship = getRelationshipsValue(obj);
                if(relationship != null){
                    relationsCache = (setRelationshipsTransientId(relationship, idCounter));
                    relations.addAll(relationsCache);
                }                
                idCounter += relationsCache.size();
            }            
            writeCollection(source);
        }
        else
        {
            relationships = getRelationshipsValue(source);
            if(relationships != null){
                relations = setRelationshipsTransientId(relationships, 1);
            }
            writeSingle(source);            
        }
        generator.writeEnd();
        if(hasRelationships)
        {
            this.writeIncluded(relations);
        }
        
        //write links if paginated requested
        if(this.isPaginated){            
            writeLinks();
        }
        
        generator.writeEnd();
        generator.close();
        generator.flush();
        String data = writer.toString();
        return data; 
    }
    
    protected void writeCollection(Object collection) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {
        if(collection instanceof List && collection != null)// single source to print, not list
        {
            for(Object obj : (List)collection)
            {
                writeSingle(obj);
            }
        }
    }
    
    protected void writeSingle(Object single) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {
        if(!(single instanceof List) && single != null)// single source to print, not list
        {
            generator.writeStartObject();            
            generator.write("type",String.valueOf(this.getTypeValue(single)));            
            generator.write("id",String.valueOf(this.getIdValue(single)));                        
            generator.writeStartObject("attributes");
            int index = 0;                        
            Map<String,Object> attributesMap = getRelationshipsAttributes(single);
            int attributesSize = attributesMap.size();
            for(Entry entry : attributesMap.entrySet())//attributes().entrySet())
            {
                if(index < attributesSize){
                    generator.write((String)entry.getKey(),String.valueOf(entry.getValue()));
                }                
                index++;
            }
            
            if(attributesSize > 0 && index == attributesSize)
            {
                generator.writeEnd();
            }

            Object relations = getRelationshipsValue(single);
            if(relations != null)
            {                
                int relationshipsDataFieldCounter = 0;
                generator.writeStartObject("relationships");
                if(!(relations instanceof List)) // relationships single pojo instance
                {
                    generator.writeStartObject(this.relationshipName(single));
                    generator.writeStartArray("data");
                    index = 0;
                    Field[] fields = relations.getClass().getDeclaredFields();
                    Type relationsObjectType = relations.getClass().getDeclaredAnnotation(Type.class);
                    generator.writeStartObject();                    
                    for(Field f : fields)
                    {                        
                        TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                        Id id = f.getDeclaredAnnotation(Id.class);                        
                        if(relationshipsDataFieldCounter < 2){ // do not write more than two same data field pairs
                            if(id == null) // other field
                            {  
                               generator.write("type",String.valueOf(relationsObjectType.value()));
                               relationshipsDataFieldCounter++;
                            }                        
                            else if(tid != null && id != null)
                            {
                                index++;
                                generator.write("id",String.valueOf(getFieldValue(f, relations))); 
                                relationshipsDataFieldCounter++;
                            }
                            else if(tid == null && id != null) // non transient id field
                            {                            
                                generator.write("id",String.valueOf(getFieldValue(f, relations)));
                                relationshipsDataFieldCounter++;
                            }
                        }// end relationshipsDataFieldCounter
                    }   
                    generator.writeEnd();
                    generator.writeEnd();
                    generator.writeEnd();
                }
                else // list of objects found
                {                
                    relationshipsDataFieldCounter = 0;
                    generator.writeStartObject(this.relationshipName(single));
                    generator.writeStartArray("data");
                    index = 0;
                    for(Object relationObject: (List)relations)
                    {
                        relationshipsDataFieldCounter = 0; // reset counter for  current data fields pair
                        generator.writeStartObject();
                        Field[] fields = relationObject.getClass().getDeclaredFields();
                        Type relationsObjectType = relationObject.getClass().getDeclaredAnnotation(Type.class);
                        for(Field f : fields)
                        {
                            TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                            Id id = f.getDeclaredAnnotation(Id.class);
                            if(relationshipsDataFieldCounter < 2){ // do not write more than two data field pairs
                                if(id == null) // other field
                                {  
                                   generator.write("type",String.valueOf(relationsObjectType.value()));  
                                   relationshipsDataFieldCounter++;
                                }                            
                                else if(tid != null && id != null)
                                {
                                    index++;                                                                
                                    generator.write("id",String.valueOf(getFieldValue(f, relationObject))); 
                                    relationshipsDataFieldCounter++;
                                }
                                else if(tid == null && id != null) // non transient id field
                                {                                
                                    generator.write("id",String.valueOf(getFieldValue(f, relationObject)));     
                                    relationshipsDataFieldCounter++;
                                }
                            }

                        }                    
                        generator.writeEnd();
                    }                  
                    generator.writeEnd();
                    generator.writeEnd();
                }                
                generator.writeEnd();
            }
            generator.writeEnd();
        }        
    }
    
    protected void writeIncluded(@NotNull Object relations) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {        
        if(relations != null)
        {
            generator.writeStartArray("included");
            if(!(relations instanceof List)) // relationships single pojo instance
            {
                int index = 0;
                generator.writeStartObject();
                Field[] fields = relations.getClass().getDeclaredFields();
                Type relationsObjectType = relations.getClass().getDeclaredAnnotation(Type.class);
                generator.write("type", String.valueOf(relationsObjectType.value()));
                for (Field f : fields)
                {
                    TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                    Id id = f.getDeclaredAnnotation(Id.class);
                    if (tid != null && id != null)
                    {
                        index++;                        
                        generator.write("id", String.valueOf(getFieldValue(f, relations)));
                    }
                    else if (tid == null && id != null) // non transient id field
                    {
                        generator.write("id", String.valueOf(getFieldValue(f, relations)));
                    }
                }            
                generator.writeStartObject("attributes");
                Map<String, Object> attributes = getRelationshipsAttributes(relations);
                for (Entry entry : attributes.entrySet())
                {
                    generator.write((String) entry.getKey(), String.valueOf(entry.getValue()));
                }
                generator.writeEnd();
                generator.writeEnd();

            }
            else // many parameterized objects
            {            
                int index = 0;
                for (Object relationObject : (List) relations)
                {
                    generator.writeStartObject();
                    Field[] fields = relationObject.getClass().getDeclaredFields();
                    Type relationsObjectType = relationObject.getClass().getDeclaredAnnotation(Type.class);
                    generator.write("type", String.valueOf(relationsObjectType.value()));                                
                    for (Field f : fields)
                    {
                        TransientId tid = f.getDeclaredAnnotation(TransientId.class);
                        Id id = f.getDeclaredAnnotation(Id.class);
                        if (tid != null && id != null)
                        {
                            index++;                            
                            generator.write("id", String.valueOf(getFieldValue(f, relationObject)));
                        } else if (tid == null && id != null) // non transient id field
                        {
                            generator.write("id", String.valueOf(getFieldValue(f, relationObject)));
                        } 
                    }
                    generator.writeStartObject("attributes");
                    Map<String,Object> attributes = getRelationshipsAttributes(relationObject);
                    for(Entry entry : attributes.entrySet())
                    {
                       generator.write((String)entry.getKey(),String.valueOf(entry.getValue())); 
                    }
                    generator.writeEnd();
                    generator.writeEnd();
                }
            }
            generator.writeEnd(); // end included array        
        }
    }
    
    protected List<Object> setRelationshipsTransientId(Object relationship,int id_counter) throws IllegalArgumentException, IllegalAccessException
    {
        List<Object> relations = new ArrayList();        
        if (!(relationship instanceof List)) {
            Annotation a = relationship.getClass().getAnnotation(Type.class);
            if (a != null) {
                Field[] fields = relationship.getClass().getDeclaredFields();
                for (Field f : fields) {
                    Annotation idAnnotation = f.getAnnotation(Id.class);
                    Annotation transientId = f.getAnnotation(TransientId.class);
                    if (idAnnotation != null && transientId != null) {
                        f.setAccessible(true);
                        f.set(relationship, String.valueOf(id_counter));
                    }
                }
            }            
            id_counter++;
            relations.add(relationship);
        } 
        else if(relationship instanceof List)// list of relationships objects found per source object
        {
            for (Object relObject : (List) relationship) {
                Annotation a = relObject.getClass().getAnnotation(Type.class);
                if (a != null) {
                    Field[] fields = relObject.getClass().getDeclaredFields();
                    for (Field f : fields) {
                        Annotation idAnnotation = f.getAnnotation(Id.class);
                        Annotation transientId = f.getAnnotation(TransientId.class);
                        if (idAnnotation != null && transientId != null) {
                            f.setAccessible(true);
                            f.set(relObject, String.valueOf(id_counter));
                        }
                    }
                }                
                id_counter++;
                relations.add(relObject);
            }
        }      
        return relations;
    }
}
