/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import java.text.SimpleDateFormat;
import com.mobiweb.completeit.annotations.NotEmptyAttribute;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.annotations.JsonDateAttribute;
import com.mobiweb.completeit.common.Utilities;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Represents an outbound json profile body sent to clients.
 * @author ic
 */
@Type("profiles")
public class OutboundProfile
{
    private final Object ip;    
    
    public OutboundProfile() throws UnknownHostException, SocketException {
        //this.ip = InetAddress.getLocalHost();
        this.ip = Utilities.getLocalAddress().getHostAddress();        
        this.id = "";
    }
        
    public OutboundProfile(String id, String nickname, String picture, String dob, 
            String location, String language, String email, String password) throws UnknownHostException, SocketException {
        //this.ip = InetAddress.getLocalHost();
        this.ip = Utilities.getLocalAddress().getHostAddress();
        this.id = "";
    }    
    
    public OutboundProfile(Profile profile) throws UnknownHostException, SocketException {
        //this.ip = InetAddress.getLocalHost().getHostAddress();
        ip = Utilities.getLocalAddress().getHostAddress();
        
        final SimpleDateFormat sdfSlash = new SimpleDateFormat("dd/MM/yyyy");  
        final SimpleDateFormat sdfDash = new SimpleDateFormat("dd-MM-yyyy");  
        
        this.id = "";        
        this.id = Long.toString(profile.getId());
        this.nickname = profile.getNickname();
        //this.pictureURL = "http://10.0.1.21:8080/profiles/images" + profile.getPictureFilepath();
        //this.pictureURL = "http://10.0.1.134:8080/profiles/images" + profile.getPictureFilepath();
        this.pictureURL = "http://" + ip + ":8080/profiles/images" + profile.getPictureFilepath();
        if(Utilities.isEmpty(profile.getPictureFilepath())){
           this.pictureURL = null;
        }
        
        this.dob = profile.getDob() != null ? sdfDash.format(profile.getDob()) : null ;
        this.language = profile.getLanguage();
        this.email = profile.getEmail();        
        this.longitude = Double.toString(profile.getLocation().getX());
        this.latitude = Double.toString(profile.getLocation().getY());
        this.score = Long.toString(profile.getScore());
        this.level = Integer.toString(profile.getLevel());
        this.mobileNo = profile.getMobileNo();
        this.isMobileNoVerified = Boolean.toString(profile.isMobileNoVerified());
        this.gender = profile.getGender();
    }    
        
    @Id
    @JsonInclude(Include.ALWAYS)
    private String id;  
    
    @NotEmptyAttribute(message="Nickname is required")
    @JsonInclude(Include.ALWAYS)
    private String nickname; 
    
    @NotEmptyAttribute(message="Picture profile URL is required")
    @JsonInclude(Include.ALWAYS)
    private String pictureURL; 
    
    @NotEmptyAttribute(message="Date of birth is required")
    @JsonDateAttribute
    @JsonInclude(Include.ALWAYS)
    private String dob;
    
    @NotEmptyAttribute(message="Longitude is required")
    @JsonInclude(Include.ALWAYS)
    private String longitude;
    
    @NotEmptyAttribute(message="Latitude is required")
    @JsonInclude(Include.ALWAYS)
    private String latitude;
    
    @NotEmptyAttribute(message="Language is required")
    @JsonInclude(Include.ALWAYS)
    private String language;
    
    @NotEmptyAttribute(message="Email is required")
    @JsonInclude(Include.ALWAYS)
    private String email;
    
    @NotEmptyAttribute(message="Score is required")
    @JsonInclude(Include.ALWAYS)
    private String score;
    
    @NotEmptyAttribute(message="Player is required")
    @JsonInclude(Include.ALWAYS)
    private String level;
    
    @NotEmptyAttribute(message="Mobile is required")
    @JsonInclude(Include.ALWAYS)
    private String mobileNo;
    
    @NotEmptyAttribute(message="Mobile number verification is required")
    @JsonInclude(Include.ALWAYS)
    private String isMobileNoVerified;

    @JsonInclude(Include.ALWAYS)
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {        
        this.dob = dob;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }   
    
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }    
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsMobileNoVerified() {
        return isMobileNoVerified;
    }

    public void setIsMobileNoVerified(String isMobileNoVerified) {
        this.isMobileNoVerified = isMobileNoVerified;
    }    
    
}
