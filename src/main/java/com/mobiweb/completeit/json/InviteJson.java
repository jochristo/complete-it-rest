/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.EmailAttribute;
import com.mobiweb.completeit.annotations.NotEmptyAttribute;

/**
 * Represents a JSON API request body to create an "Invite" resource object.
 * Validation rules apply.
 * @author ic
 */
@Type("invite")
public class InviteJson
{
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)    
    private String id = "";
    
    @JsonInclude(JsonInclude.Include.ALWAYS)    
    private long userid = 0;
    
    @EmailAttribute
    @NotEmptyAttribute
    @JsonInclude(JsonInclude.Include.ALWAYS)        
    private String email = "";

    public InviteJson() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
   
}
