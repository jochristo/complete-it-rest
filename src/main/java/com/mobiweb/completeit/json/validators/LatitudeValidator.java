/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import com.mobiweb.completeit.annotations.LatitudeAttribute;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author ic
 */
public class LatitudeValidator implements ConstraintValidator<LatitudeAttribute, String> {
 
    //^-?([1-8]?[0-9]\.{1}\d{1,6}$|90\.{1}0{1,6}$)
    //pattern Latitude as decimal
    private final String pattern = "^-?([1-8]?[0-9]\\.{1}\\d{1,6}$|90\\.{1}0{1,6}$)";
    
    @Override
    public boolean isValid(String object, ConstraintValidatorContext cvc)
    {
        if ( object == null ) {
            return true;
        }        
        boolean isMatch = object.matches(pattern);
        return object.matches(pattern);        
    }    

    @Override
    public void initialize(LatitudeAttribute a) {        
    }    
}
