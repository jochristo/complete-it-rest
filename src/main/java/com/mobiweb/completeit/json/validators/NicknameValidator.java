/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.mobiweb.completeit.annotations.NicknameAttribute;

/**
 *
 * @author ic
 */
public class NicknameValidator implements ConstraintValidator<NicknameAttribute, String>
{   
    // look-ahead regular expression pattern matching at least one speial character in any position
    private final String pattern = "^(?=.*[._'\\-]|[._'\\-].*)[0-9a-zA-Z._'\\-]{4,15}$";
    //private final String pattern = "^(?=[0-9a-zA-Z]{3}[._'\\-])[0-9a-zA-Z._'\\-]{4,15}$";
    
    @Override
    public void initialize(NicknameAttribute object) {
        
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext cvc)
    {
        if ( object == null ) {
            return true;
        }     
        if(object.equals("")){
            return true;
        }        
        boolean isMatch = object.matches(pattern);
        return object.matches(pattern);        
    }
    
}
