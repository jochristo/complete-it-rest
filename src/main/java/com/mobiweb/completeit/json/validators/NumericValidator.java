package com.mobiweb.completeit.json.validators;

import com.mobiweb.completeit.annotations.NumericAttribute;
import com.mobiweb.completeit.common.Utilities;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * Constraint validity class for profile properties: latitude, longitude, mobile no, and generic numbers.
 * @author ic
 */
public class NumericValidator implements ConstraintValidator<NumericAttribute, Object>
{
    //regular expressio matching patterns
    private final String LatitudePattern = "^-?([1-8]?[0-9]\\.{1}\\d{1,7}$|90\\.{1}0{1,7}$)";        
    private final String LongitudePattern = "^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\\.{1}\\d{1,7}$)|[1]?[1-8][0]\\.{1}0{1,7}$)";        
    private final String generic = "^[0]|\\d+$";
    private final String mobileNoPattern = "^[+][0-9]+\\d+$";
    
    //invalid attribute messages
    public static final String INVALID_LONGITUDE_MESSAGE = "Longitude must be between -180 and 180 degrees inclusive.";    
    public static final String INVALID_LATITUDE_MESSAGE = "Latitude must be between -90 and 90 degrees inclusive.";
    public static final String INVALID_SCORE_MESSAGE = "Score must be an unsigned integer value";
    public static final String INVALID_LEVEL_MESSAGE = "Level must be an unsigned integer value";
    public static final String INVALID_MOBILE_NO_MESSAGE = "Mobile number must be in the format: ";
    public static final String INVALID_NUMBER_GENERAL_MESSAGE = "Number must be an unsigned integer value";    
    
    //invalid attribute titles
    public static final String INVALID_LONGITUDE_TITLE  = "Invalid longitude format";
    public static final String INVALID_LATITUDE_TITLE = "Invalid latitude format";
    public static final String INVALID_SCORE_TITLE  = "Invalid score value";
    public static final String INVALID_LEVEL_TITLE  = "Invalid level value";
    public static final String INVALID_MOBILE_NO_TITLE  = "Invalid mobile number format";
    public static final String INVALID_NUMBER_GENERAL_TITLE  = "Invalid numeric value";
    
    //invalid attribute error codes
    public static final String INVALID_LONGITUDE_ERROR_CODE = "1011";
    public static final String INVALID_LATITUDE_ERROR_CODE = "1012";
    public static final String INVALID_SCORE_ERROR_CODE = "1013";
    public static final String INVALID_LEVEL_ERROR_CODE = "1014";
    public static final String INVALID_MOBILE_NO_ERROR_CODE = "1015";    
    
    //pattern to use
    private String pattern = null;        
        
    @Override
    public void initialize(NumericAttribute a) {
        
        NumericAttribute.AttributeType attributeType = a.pattern();
        switch (attributeType)
        {
            case LONGITUDE: 
                pattern = LongitudePattern;
                break;            
            case LATITUDE: 
                this.pattern = LatitudePattern;
                break;
            case SCORE: 
                pattern = generic;
                break;
            case LEVEL: 
                pattern = generic;
                break;
            case MOBILE_NO: 
                pattern = mobileNoPattern;
                break;     
            case GENERAL_NUMBER: 
                pattern = generic;
                break;                
        }   
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc) 
    {           
        if ( Utilities.isEmpty(String.valueOf(object)) )
        {
            return true;
        }
        return String.valueOf(object).matches(pattern);
    }
        
}
