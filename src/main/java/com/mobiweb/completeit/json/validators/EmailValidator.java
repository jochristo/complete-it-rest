/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.mobiweb.completeit.annotations.EmailAttribute;

/**
 *
 * @author ic
 */
public class EmailValidator implements ConstraintValidator<EmailAttribute, String> {
    
    //^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$
    private final String pattern = "^(.+)@(.+)$";    
    private boolean isEmpty = false;

    @Override
    public void initialize(EmailAttribute a) {
        
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext cvc) {
        if ( object == null ) {
            return true;
        }
        if(object.equals("")){
            return true;
        }
        return object.matches(pattern);          
    }
    
    
}
