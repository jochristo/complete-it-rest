/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import java.text.SimpleDateFormat;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.mobiweb.completeit.annotations.JsonDateAttribute;

/**
 * Validates given date string in JSON input stream against string format patterns.
 * @author ic
 */
public class JsonDateFormatValidator implements ConstraintValidator<JsonDateAttribute, String> 
{
    private final String shortDashPattern = "dd-MM-yyyy";
    private final String shortSlashPattern = "dd/MM/yyyy";
    //private final String shortDashPatternMatch = "^[0-9][0-9]-([1-9]|[0][1-9]|[1][0-2])-[1-9][0-9][0-9][0-9]$";
    private final String matcherWithDash = "^(3[01]|[12][0-9]|0[1-9])\\-(1[0-2]|0[1-9])\\-[0-9]{4}$";
    private final String matcherWithSlash = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$";
    //private final String shortSlashPatternMatch = "^[0-9][0-9]/[0-1][0-9]/[1-9][0-9][0-9][0-9]$";    
    private String pattern;
    
    @Override
    public void initialize(JsonDateAttribute annotation)
    {        
        /*
        JsonDateFormatPattern jdfp = annotation.pattern();
        if(annotation.pattern() == JsonDateAttribute.JsonDateFormatPattern.shortWithDash)
            pattern = matcherWithDash;
        else
            pattern = matcherWithSlash;
        */
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext)
    {
        if ( object == null ) {
            return true;
        }        
        if(object.equals("")){
            return true;
        }        
        pattern = (object.matches(matcherWithDash)) ? shortDashPattern : shortSlashPattern;
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        boolean isMatch = object.matches(matcherWithDash); // || object.matches(matcherWithSlash);
        return isMatch;
    }    
    
}
