/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import com.mobiweb.completeit.annotations.NotEmptyAttribute;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.mobiweb.completeit.common.Utilities;

/**
 *
 * @author ic
 */
public class JsonAttributeValidator implements ConstraintValidator<NotEmptyAttribute, String> 
{

    @Override
    public void initialize(NotEmptyAttribute annotation)
    {
        
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext)
    {
        if ( object == null ) {
            return false;
        }        
        boolean isEmpty = Utilities.isEmpty(object);
        
        return !isEmpty;
        
    }
    
}
