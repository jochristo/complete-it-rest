/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.validators;

import com.mobiweb.completeit.annotations.LongitudeAttribute;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author ic
 */
public class LongitudeValidator implements ConstraintValidator<LongitudeAttribute, String> {
    
    //pattern Longitude as decimal
    private final String pattern = "^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\\.{1}\\d{1,6}$)|[1]?[1-8][0]\\.{1}0{1,6}$)";
    
    @Override
    public boolean isValid(String object, ConstraintValidatorContext cvc)
    {
        if ( object == null ) {
            return true;
        }        
        boolean isMatch = object.matches(pattern);
        return object.matches(pattern);        
    }    

    @Override
    public void initialize(LongitudeAttribute a) {        
    }
    
}
