/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.TransientId;
import java.util.Objects;

/**
 * Represents a JSON API answered word relationship item of a particular OutboundAnswer object
 * @author ic
 */
@Type("words")
public class AnsweredWord {
    
    public AnsweredWord() {
    }
        
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @TransientId
    private String id; 
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String word;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnsweredWord other = (AnsweredWord) obj;
        return true;
    }
    
    
    
}
