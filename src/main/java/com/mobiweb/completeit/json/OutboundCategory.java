/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Category;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author ic
 */
@Type("categories")
public class OutboundCategory {
    
    private final Object ip;
    
    public OutboundCategory() throws UnknownHostException, SocketException {
        //this.ip = InetAddress.getLocalHost().getHostAddress();
        ip = Utilities.getLocalAddress().getHostAddress();
    }
    
    public OutboundCategory(Category c) throws UnknownHostException, SocketException {
        
        //this.ip = InetAddress.getLocalHost().getHostAddress();
        ip = Utilities.getLocalAddress().getHostAddress();
        
        this.id = Long.toString(c.getId());
        this.name = c.getName();
        this.description = c.getDescription();
        //this.imagePath = "http://10.0.1.21:8080/cat/images" + c.getImageRelativePath();
        //this.imagePath = "http://10.0.1.134:8080/cat/images" + c.getImageRelativePath();
        this.imagePath = "http://" + ip + ":8080/cat/images" + c.getImageRelativePath();
        if(Utilities.isEmpty(c.getImageRelativePath())){
           this.imagePath = null;
        }        
    }    
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String name; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String imagePath; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String description;   

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
