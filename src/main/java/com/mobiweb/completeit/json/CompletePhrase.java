/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.TransientId;
import java.util.UUID;

/**
 * Represents a complete phrase with gaps completed with answered words.
 * @author ic
 */
@Type("sentences")
public class CompletePhrase {

    public CompletePhrase() {
        this.id = UUID.randomUUID().toString();
        this.isCorrect = "false";
        this.clause = "";
        this.reports = null;
    }        
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @TransientId
    private String id;  
        
    // The complete clause  of the  sentence
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String clause;     
    
    // Answered by profile-user indicator
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String isCorrect;
    
    // Count of likes from other users
    @JsonInclude(JsonInclude.Include.ALWAYS)    
    private String likes;

    // Related answerid in persistent store
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String answerId;
    
    // Like indicator for profile accessing category sentences
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String isLiked;
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String reports;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = UUID.randomUUID().toString();
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause;
    }

    public String getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String phraseId) {
        this.answerId = phraseId;
    }

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    public String getReports() {
        return reports;
    }

    public void setReports(String reports) {
        this.reports = reports;
    }
    
    
}
