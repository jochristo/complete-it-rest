/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import javax.validation.constraints.NotNull;
import com.mobiweb.completeit.domain.Phrase;

/**
 *
 * @author ic
 */
@Type("phrases")
public class OutboundPhrase {
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String phrase; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String gaps;     

    public OutboundPhrase(@NotNull Phrase p)
    {
        this.id = String.valueOf(p.getId());
        this.phrase = p.getPhrase();
        this.gaps = String.valueOf(p.getGaps());
    }  

    public OutboundPhrase() {
    }
    
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getGaps() {
        return gaps;
    }

    public void setGaps(String gaps) {
        this.gaps = gaps;
    }
    
    
    
}
