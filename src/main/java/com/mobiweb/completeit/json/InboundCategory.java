/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;

/**
 *
 * @author ic
 */
@Type("categories")
public class InboundCategory {

    public InboundCategory() {
    }
    
    @Id
    @JsonInclude(Include.ALWAYS)
    private String id;  
        
    @JsonInclude(Include.ALWAYS)
    private String name; 
        
    @JsonInclude(Include.ALWAYS)
    private String imageString; 
        
    @JsonInclude(Include.ALWAYS)
    private String description;   

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
