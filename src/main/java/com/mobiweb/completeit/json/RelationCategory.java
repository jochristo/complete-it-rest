/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;

/**
 * Represents a category relationship item of a particular InboundPhrase object.
 * @author ic
 */
@Type("categories")
public class RelationCategory {

    public RelationCategory() {
    }
        
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
