/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.maps.googleapis;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ic
 */
public class Results {
    
    public List<AddressComponent> address_components = new ArrayList();
    public String formatted_address = "";

    public Results() {
    }

    public List<AddressComponent> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<AddressComponent> address_components) {
        this.address_components = address_components;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
    
    
    
}
