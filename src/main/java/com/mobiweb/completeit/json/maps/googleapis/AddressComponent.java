/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json.maps.googleapis;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ic
 */
public class AddressComponent {
 
    public String long_name = "";
    public String short_name = "";
    public List<String> types = new ArrayList();

    public AddressComponent() {
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    
    
    
}
