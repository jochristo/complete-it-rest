/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import java.text.SimpleDateFormat;
import com.mobiweb.completeit.annotations.EmailAttribute;
import com.mobiweb.completeit.annotations.JsonDateAttribute;
import com.mobiweb.completeit.annotations.NicknameAttribute;
import com.mobiweb.completeit.annotations.NumericAttribute;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.LATITUDE;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.LONGITUDE;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.SCORE;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Profile;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LATITUDE_ERROR_CODE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LATITUDE_MESSAGE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LATITUDE_TITLE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LONGITUDE_ERROR_CODE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LONGITUDE_MESSAGE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_LONGITUDE_TITLE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_ERROR_CODE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_MESSAGE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_TITLE;

/**
 * Represents an updatable json profile object.
 * @author ic
 */
@Type("profiles")
public class UpdatableProfile //extends InboundProfile {
{
    
    final SimpleDateFormat sdfDash = new SimpleDateFormat("dd-MM-yyyy");
    
    public UpdatableProfile()
    {
        
    }
    
    @Id    
    private String id = "";
    
    @NicknameAttribute(pattern = NicknameAttribute.AttributeType.NICKNAME)
    private String nickname = "";
    
    private String pictureEncodedString = null;
    
    @JsonDateAttribute    
    private String dob = "";
    
    @NumericAttribute(pattern=LONGITUDE, title=INVALID_LONGITUDE_TITLE, message=INVALID_LONGITUDE_MESSAGE, code=INVALID_LONGITUDE_ERROR_CODE)    
    private String longitude = "";        
    
    @NumericAttribute(pattern=LATITUDE, title=INVALID_LATITUDE_TITLE, message=INVALID_LATITUDE_MESSAGE, code=INVALID_LATITUDE_ERROR_CODE)
    private String latitude = "";        
    
    private String language = "";
    
    @EmailAttribute
    private String email = "";
    
    private String password = "";
        
    @NumericAttribute(pattern=SCORE, title=INVALID_SCORE_TITLE, message=INVALID_SCORE_MESSAGE, code=INVALID_SCORE_ERROR_CODE)
    private String score = "";
        
    private String level = "";
    
    private String mobileNo = "";
    
    private String isMobileNoVerified = "false";   
    
    private String gender = "";

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPictureEncodedString() {
        return pictureEncodedString;
    }

    public void setPictureEncodedString(String pictureEncodedString) {
        this.pictureEncodedString = pictureEncodedString;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsMobileNoVerified() {
        return isMobileNoVerified;
    }

    public void setIsMobileNoVerified(String isMobileNoVerified) {
        this.isMobileNoVerified = isMobileNoVerified;
    }
    
    
    public void UpdateProperties(InboundProfile p)
    {
        if(p != null)
        {
            //mandatory base properties
            if(!Utilities.isEmpty(p.getDob())){
                this.setDob(p.getDob());
            }
            if(!Utilities.isEmpty(p.getEmail())){
                this.setDob(p.getEmail());
            }
            if(!Utilities.isEmpty(p.getLanguage())){
                this.setDob(p.getLanguage());
            }
            if(!Utilities.isEmpty(p.getNickname())){
                this.setDob(p.getNickname());
            }    
            if(!Utilities.isEmpty(p.getPassword())){
                this.setDob(p.getPassword());
            }    
            if(!Utilities.isEmpty(p.getMobileNo())){
                this.setDob(p.getMobileNo());
            }              
            //optional base properties
        }
    }
    
    public void UpdateUnchangedProperties(Profile p)
    {
        if(p != null)
        {
            this.id = String.valueOf(p.getId());
            if(!Utilities.isEmpty(p.getDob())){
                if(Utilities.isEmpty(this.dob)){
                    this.setDob(sdfDash.format(p.getDob()));
                }
            }
            if(!Utilities.isEmpty(p.getEmail())){
                if(Utilities.isEmpty(this.email)){
                    this.setEmail(p.getEmail());
                }                                
            }
            if(!Utilities.isEmpty(p.getLanguage())){
                if(Utilities.isEmpty(this.language)){
                    this.setLanguage(p.getLanguage());
                }
            }
            if(!Utilities.isEmpty(p.getNickname())){
                if(Utilities.isEmpty(this.nickname)){
                    this.setNickname(p.getNickname());
                }
            }        
            if(!Utilities.isEmpty(p.getMobileNo())){
                if(Utilities.isEmpty(this.mobileNo)){
                    this.setMobileNo(p.getMobileNo());
                }                
                
            }
            if(!Utilities.isEmpty(p.getLevel())){
                if(Utilities.isEmpty(this.level)){
                    this.setLevel(Integer.toString(p.getLevel()));
                }   
            }
            if(!Utilities.isEmpty(p.getScore())){
                if(Utilities.isEmpty(this.score)){
                    this.setScore(String.valueOf(p.getScore()));
                }
            }             
            if(!Utilities.isEmpty(p.getLocation())){
                if(Utilities.isEmpty(this.longitude)){
                    double longitude = p.getLocation().getX();
                    this.setLongitude(String.valueOf(longitude));
                }
                if(Utilities.isEmpty(this.latitude)){
                    double latitude = p.getLocation().getY();
                    this.setLatitude(String.valueOf(latitude));
                }
            }
        }
    }    
    
}
