/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import com.mobiweb.completeit.domain.Phrase;

/**
 * Represents a JSON API inbound phrase HTTP request object.
 * @author ic
 */
@Type("phrases")
public class InboundPhrase {
    
    public InboundPhrase() {     
        categories = new ArrayList();
    }
    
    public InboundPhrase(@NotNull Phrase p)
    {
        this.id = String.valueOf(p.getId());
        this.phrase = p.getPhrase();
        this.gaps = String.valueOf(p.getGaps());
        if(p.getProfile()  !=  null){
            this.profileId = String.valueOf(p.getProfile().getId());
        }
        else
        {
            this.profileId = null;
        }
    }
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String id;  
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String phrase; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String gaps; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String profileId;    
    
    @Relationship(value = "categories", resolve = true)
    private List<RelationCategory> categories;    

    public String getId() {
        return id;
    }

    public void setId(String id) {        
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getGaps() {
        return gaps;
    }

    public void setGaps(String gaps) {
        this.gaps = gaps;
    } 
    
    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public List<RelationCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<RelationCategory> categories) {
        this.categories = categories;
    }
    
}
