/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.annotations.EmailAttribute;
import com.mobiweb.completeit.annotations.NotEmptyAttribute;
import com.mobiweb.completeit.annotations.NumericAttribute;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.GENERAL_NUMBER;
import static com.mobiweb.completeit.annotations.NumericAttribute.AttributeType.SCORE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_NUMBER_GENERAL_MESSAGE;
import static com.mobiweb.completeit.json.ErrorUtils.ErrorCode.INVALID_NUMBER_GENERAL;
import static com.mobiweb.completeit.json.ErrorUtils.ErrorCode.INVALID_SCORE_ERROR_CODE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_NUMBER_GENERAL_TITLE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_MESSAGE;
import static com.mobiweb.completeit.json.validators.NumericValidator.INVALID_SCORE_TITLE;
import java.util.UUID;

/**
 * Represents a JSON API request body to create a "shared" resource object.
 * Validation rules apply.
 * @author ic
 */
@Type("share")
public class Shared {
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)    
    private String id = "";
    
    @JsonInclude(JsonInclude.Include.ALWAYS)    
    private long userid = 0;
    
    @EmailAttribute
    @NotEmptyAttribute
    @JsonInclude(JsonInclude.Include.ALWAYS)        
    private String email = "";
    
    @NumericAttribute(pattern=SCORE, title=INVALID_SCORE_TITLE, message=INVALID_SCORE_MESSAGE, errorCode = INVALID_SCORE_ERROR_CODE)    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)       
    private long score = 0;
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)  
    @NumericAttribute(pattern=GENERAL_NUMBER, title=INVALID_NUMBER_GENERAL_TITLE,message="phraseId "+INVALID_NUMBER_GENERAL_MESSAGE, errorCode = INVALID_NUMBER_GENERAL)    
    private long phraseid = 0;
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)        
    @NumericAttribute(pattern=GENERAL_NUMBER, title=INVALID_NUMBER_GENERAL_TITLE, message="answerid "+INVALID_NUMBER_GENERAL_MESSAGE, errorCode = INVALID_NUMBER_GENERAL)        
    private long answerid = 0;
   

    public Shared() {
        this.id = UUID.randomUUID().toString();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {                
        this.id = UUID.randomUUID().toString();
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getPhraseid() {
        return phraseid;
    }

    public void setPhraseid(long phraseid) {
        this.phraseid = phraseid;
    }

    public long getAnswerid() {
        return answerid;
    }

    public void setAnswerid(long answerid) {
        this.answerid = answerid;
    }

}