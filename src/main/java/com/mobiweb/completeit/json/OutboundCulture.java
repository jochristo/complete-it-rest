/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import com.mobiweb.completeit.domain.Culture;

/**
 * Represents a JSON-API culture resource
 * @author ic
 */
@Type("cultures")
public class OutboundCulture {
    
    @Id
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private long id; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String code; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String name; 
    
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String displayName; 
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String iso;   
        
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String language;    

    public OutboundCulture() {
    }
    
    public OutboundCulture(Culture culture) {
        
        if(culture != null)
        {
            this.id = culture.getId();
            this.code = culture.getCode();
            this.name = culture.getCultureName();
            this.displayName = culture.getDisplayName();
            this.iso = culture.getIso();
            this.language = culture.getLanguageName();
        }
    }    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    
    
}
