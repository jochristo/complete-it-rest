package com.mobiweb.completeit.common.exception;

/**
 * Thrown when mismatch is found between the gaps property of a com.mobiweb.completeit.domain.Phrase object and given words count.
 * @author ic
 */
public class IncorrectWordGapsException extends Exception
{
    public IncorrectWordGapsException() {
    }

    public IncorrectWordGapsException(String message) {
        super(message);
    }

    public IncorrectWordGapsException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectWordGapsException(Throwable cause) {
        super(cause);
    }

    public IncorrectWordGapsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }      
    
}
