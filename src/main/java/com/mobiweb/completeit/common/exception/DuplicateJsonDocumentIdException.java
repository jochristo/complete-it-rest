package com.mobiweb.completeit.common.exception;

/**
 * Thrown when a JSON API documents contains duplicate data "id" fields.
 * @author ic
 */
public class DuplicateJsonDocumentIdException extends Exception
{
    public DuplicateJsonDocumentIdException() {
    }

    public DuplicateJsonDocumentIdException(String message) {
        super(message);
    }

    public DuplicateJsonDocumentIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateJsonDocumentIdException(Throwable cause) {
        super(cause);
    }

    public DuplicateJsonDocumentIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }      
    
}
