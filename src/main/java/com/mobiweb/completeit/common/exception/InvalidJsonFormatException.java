package com.mobiweb.completeit.common.exception;

/**
 * Exception thrown when json format is invalid after attribute validation is performed.
 * @author ic
 */
public class InvalidJsonFormatException extends Exception{
    public InvalidJsonFormatException() {
    }

    public InvalidJsonFormatException(String message) {
        super(message);
    }

    public InvalidJsonFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidJsonFormatException(Throwable cause) {
        super(cause);
    }

    public InvalidJsonFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }    
}
