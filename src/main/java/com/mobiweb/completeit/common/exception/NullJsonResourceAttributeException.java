package com.mobiweb.completeit.common.exception;

/**
 * 
 * @author ic
 */
public class NullJsonResourceAttributeException extends Exception
{
    public NullJsonResourceAttributeException() {
    }

    public NullJsonResourceAttributeException(String message) {
        super(message);
    }

    public NullJsonResourceAttributeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullJsonResourceAttributeException(Throwable cause) {
        super(cause);
    }

    public NullJsonResourceAttributeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }      
    
}
