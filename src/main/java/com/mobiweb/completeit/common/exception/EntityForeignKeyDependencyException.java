/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common.exception;

/**
 *
 * @author ic
 */
public class EntityForeignKeyDependencyException extends Exception
{
    public EntityForeignKeyDependencyException() {
    }

    public EntityForeignKeyDependencyException(String message) {
        super(message);
    }

    public EntityForeignKeyDependencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityForeignKeyDependencyException(Throwable cause) {
        super(cause);
    }

    public EntityForeignKeyDependencyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }      
}
