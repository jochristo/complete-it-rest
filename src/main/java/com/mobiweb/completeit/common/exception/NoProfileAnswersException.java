/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common.exception;

/**
 *
 * @author ic
 */
public class NoProfileAnswersException extends Exception{
    
    public NoProfileAnswersException() {
    }

    public NoProfileAnswersException(String message) {
        super(message);
    }

    public NoProfileAnswersException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoProfileAnswersException(Throwable cause) {
        super(cause);
    }

    public NoProfileAnswersException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }     
    
}
