package com.mobiweb.completeit.common.exception;

/**
 *
 * @author ic
 */
public class RequestHeadersNullException extends Exception
{
    public RequestHeadersNullException() {
    }

    public RequestHeadersNullException(String message) {
        super(message);
    }

    public RequestHeadersNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestHeadersNullException(Throwable cause) {
        super(cause);
    }

    public RequestHeadersNullException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }     
    
}
