package com.mobiweb.completeit.common.exception;

/**
 * Thrown when invalid JSON API class is found during JSON API serialization of an object.
 * i.e. the instantiated object does not contain the @Type annotation in its class declaration.
 * @author ic
 */
public class InvalidJsonApiClassException extends Exception{
    public InvalidJsonApiClassException() {
    }

    public InvalidJsonApiClassException(String message) {
        super(message);
    }

    public InvalidJsonApiClassException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidJsonApiClassException(Throwable cause) {
        super(cause);
    }

    public InvalidJsonApiClassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }      
    
}
