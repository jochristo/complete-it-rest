/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common.exception;

/**
 *
 * @author ic
 */
public class InvalidJsonAnswerFormatException extends Exception
{
    public InvalidJsonAnswerFormatException() {
    }

    public InvalidJsonAnswerFormatException(String message) {
        super(message);
    }

    public InvalidJsonAnswerFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidJsonAnswerFormatException(Throwable cause) {
        super(cause);
    }

    public InvalidJsonAnswerFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }     
    
}
