/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 *
 * @author ic
 */
public abstract class SHA256Salted {
    
    /**
     * Generate hashed password-string using SHA-256 algorithm
     * @param password
     * @param salt
     * @return 
     */
    public static String hash(String password, byte[] salt)
    {
        String hashed = null;
        try {
            // MessageDigest instance for SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //MD5
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes 
            byte[] bytes = md.digest(password.getBytes());            
            //Convert bytes it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //hashed hex format
            hashed = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
        }
        return hashed.toUpperCase();
    }
   /**
    * Get salt to use with SHA-256 algorithm
    * @return
    * @throws NoSuchAlgorithmException
    * @throws NoSuchProviderException 
    */
    public static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        // SecureRandom generator
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        // Create array for salt
        byte[] salt = new byte[16];
        // Get random salt
        sr.nextBytes(salt);        
        return salt;
    }
    
    
}
