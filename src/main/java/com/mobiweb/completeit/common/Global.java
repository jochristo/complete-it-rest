/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ic
 */
public class Global
{      
    public class AppContext
    {
        public AppContext() { 
        }
    }
    
    public static class Categories
    {
        public Categories() {
        }
        
        private final static String cat1Name = "Geography";
        private final static String cat2Name = "Transportation";
        private final static String cat3Name = "Leisure";
        private final static String cat4Name = "Calculations";
        private final static String cat5Name = "Traffic";
        private final static String cat6Name = "Prices";
        private final static String cat7Name = "Computer";
        private final static String cat8Name = "Super Market";
        private final static String cat9Name = "Random";
        private final static String cat10Name = "Shopping";
        private final static String cat11Name = "Women";
        private final static String cat12Name = "Money";
        private final static String cat13Name = "Financial";
        private final static String cat14Name = "Home";
        private final static String cat1File = "category1.png";
        private final static String cat2File = "category2.png";
        private final static String cat3File = "category3.png";
        private final static String cat4File = "category4.png";
        private final static String cat5File = "category5.png";
        private final static String cat6File = "category6.png";
        private final static String cat7File = "category7.png";
        private final static String cat8File = "category8.png";
        private final static String cat9File = "category9.png";
        private final static String cat10File = "category10.png";    
        private final static String cat11File = "category11.png";
        private final static String cat12File = "category12.png";
        private final static String cat13File = "category13.png";
        private final static String cat14File = "category14.png";
        
        public static Map<Integer,Map.Entry<String,String>> getCategories()
        {
            Map<String, String> map = new LinkedHashMap<>();
            Map<Integer, Map.Entry<String,String>> mapz = new HashMap<>();
            map.put(cat1Name, "/".concat(cat1File));
            map.put(cat2Name, "/".concat(cat2File));
            map.put(cat3Name, "/".concat(cat3File));
            map.put(cat4Name, "/".concat(cat4File));
            map.put(cat5Name, "/".concat(cat5File));
            map.put(cat6Name, "/".concat(cat6File));
            map.put(cat7Name, "/".concat(cat7File));
            map.put(cat8Name, "/".concat(cat8File));
            map.put(cat9Name, "/".concat(cat9File));
            map.put(cat10Name, "/".concat(cat10File));
            map.put(cat11Name, "/".concat(cat11File));
            map.put(cat12Name, "/".concat(cat12File));
            map.put(cat13Name, "/".concat(cat13File));
            map.put(cat14Name, "/".concat(cat14File));             
            int index = 0;
            for (HashMap.Entry<String, String> entry : map.entrySet()) {                
                mapz.put(index, entry);
                index++;
            }
            return mapz;
        }
    }
    
    public static class Cultures
    {
        private String[] codes;
        private String[] display;
        private Object[] iso;
        private Object[] culture;
        private Object[] language;
        
        public static String extractLanguage(String display)
        {
            String pattern = "-";
            String[] split = display.split(pattern); 
            if(split.length > 0)
            {
                return StringUtils.deleteWhitespace(split[0]);
            }
            return StringUtils.EMPTY;
        }
        
        private void insert()
        {
            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);
            
            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);
            
            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);

            codes[0] = "0x0436";
            display[0] = "Afrikaans - South Africa";
            iso[0] = "SQI";
            culture[0] = "af-ZA";
            language[0] = extractLanguage(display[0]);            
            
        }
        
        
/*
1	0x0436	Afrikaans - South Africa	AFK	af-ZA	
2	0x041C	Albanian - Albania	SQI	sq-AL	
3	0x1401	Arabic - Algeria	ARG	ar-DZ	
4	0x3C01	Arabic - Bahrain	ARH	ar-BH	
5	0x0C01	Arabic - Egypt	ARE	ar-EG	
6	0x0801	Arabic - Iraq	ARI	ar-IQ	
7	0x2C01	Arabic - Jordan	ARJ	ar-JO	
8	0x3401	Arabic - Kuwait	ARK	ar-KW	
9	0x3001	Arabic - Lebanon	ARB	ar-LB	
10	0x1001	Arabic - Libya	ARL	ar-LY	
11	0x1801	Arabic - Morocco	ARM	ar-MA	
12	0x2001	Arabic - Oman	ARO	ar-OM	
13	0x4001	Arabic - Qatar	ARQ	ar-QA	
14	0x0401	Arabic - Saudi Arabia	ARA	ar-SA	
15	0x2801	Arabic - Syria	ARS	ar-SY	
16	0x1C01	Arabic - Tunisia	ART	ar-TN	
17	0x3801	Arabic - United Arab Emirates	ARU	ar-AE	
18	0x2401	Arabic - Yemen	ARY	ar-YE	
19	0x042B	Armenian - Armenia		hy-AM	
20	0x082C	Azeri (Cyrillic) - Azerbaijan		Cy-az-AZ	
21	0x042C	Azeri (Latin) - Azerbaijan		Lt-az-AZ	
22	0x042D	Basque - Basque	EUQ	eu-ES	
23	0x0423	Belarusian - Belarus	BEL	be-BY	
24	0x0402	Bulgarian - Bulgaria	BGR	bg-BG	
25	0x0403	Catalan - Catalan	CAT	ca-ES	
26	0x0804	Chinese - China	CHS	zh-CN	
27	0x0C04	Chinese - Hong Kong SAR	ZHH	zh-HK	
28	0x1404	Chinese - Macau SAR		zh-MO	
29	0x1004	Chinese - Singapore	ZHI	zh-SG	
30	0x0404	Chinese - Taiwan	CHT	zh-TW	
31	0x0004	Chinese (Simplified)		zh-CHS	
32	0x7C04	Chinese (Traditional)		zh-CHT	
33	0x041A	Croatian - Croatia	HRV	hr-HR	
34	0x0405	Czech - Czech Republic	CSY	cs-CZ	
35	0x0406	Danish - Denmark	DAN	da-DK	
36	0x0465	Dhivehi - Maldives		div-MV	
37	0x0813	Dutch - Belgium	NLB	nl-BE	
38	0x0413	Dutch - The Netherlands		nl-NL	
39	0x0C09	English - Australia	ENA	en-AU	
40	0x2809	English - Belize	ENL	en-BZ	
41	0x1009	English - Canada	ENC	en-CA	
42	0x2409	English - Caribbean		en-CB	
43	0x1809	English - Ireland	ENI	en-IE	
44	0x2009	English - Jamaica	ENJ	en-JM	
45	0x1409	English - New Zealand	ENZ	en-NZ	
46	0x3409	English - Philippines		en-PH	
47	0x1C09	English - South Africa	ENS	en-ZA	
48	0x2C09	English - Trinidad and Tobago	ENT	en-TT	
49	0x0809	English - United Kingdom	ENG	en-GB	
50	0x0409	English - United States	ENU	en-US	
51	0x3009	English - Zimbabwe		en-ZW	
52	0x0425	Estonian - Estonia	ETI	et-EE	
53	0x0438	Faroese - Faroe Islands	FOS	fo-FO	
54	0x0429	Farsi - Iran	FAR	fa-IR	
55	0x040B	Finnish - Finland	FIN	fi-FI	
56	0x080C	French - Belgium	FRB	fr-BE	
57	0x0C0C	French - Canada	FRC	fr-CA	
58	0x040C	French - France		fr-FR	
59	0x140C	French - Luxembourg	FRL	fr-LU	
60	0x180C	French - Monaco		fr-MC	
61	0x100C	French - Switzerland	FRS	fr-CH	
62	0x0456	Galician - Galician		gl-ES	
63	0x0437	Georgian - Georgia		ka-GE	
64	0x0C07	German - Austria	DEA	de-AT	
65	0x0407	German - Germany		de-DE	
66	0x1407	German - Liechtenstein	DEC	de-LI	
67	0x1007	German - Luxembourg	DEL	de-LU	
68	0x0807	German - Switzerland	DES	de-CH	
69	0x0408	Greek - Greece	ELL	el-GR	
70	0x0447	Gujarati - India		gu-IN	
71	0x040D	Hebrew - Israel	HEB	he-IL	
72	0x0439	Hindi - India	HIN	hi-IN	
73	0x040E	Hungarian - Hungary	HUN	hu-HU	
74	0x040F	Icelandic - Iceland	ISL	is-IS	
75	0x0421	Indonesian - Indonesia		id-ID	
76	0x0410	Italian - Italy		it-IT	
77	0x0810	Italian - Switzerland	ITS	it-CH	
78	0x0411	Japanese - Japan	JPN	ja-JP	
79	0x044B	Kannada - India		kn-IN	
80	0x043F	Kazakh - Kazakhstan		kk-KZ	
81	0x0457	Konkani - India		kok-IN	
82	0x0412	Korean - Korea	KOR	ko-KR	
83	0x0440	Kyrgyz - Kazakhstan		ky-KZ	
84	0x0426	Latvian - Latvia	LVI	lv-LV	
85	0x0427	Lithuanian - Lithuania	LTH	lt-LT	
86	0x042F	Macedonian (FYROM)	MKD	mk-MK	
87	0x083E	Malay - Brunei		ms-BN	
88	0x043E	Malay - Malaysia		ms-MY	
89	0x044E	Marathi - India		mr-IN	
90	0x0450	Mongolian - Mongolia		mn-MN	
91	0x0414	Norwegian (Bokm?l) - Norway		nb-NO	
92	0x0814	Norwegian (Nynorsk) - Norway		nn-NO	
93	0x0415	Polish - Poland	PLK	pl-PL	
94	0x0416	Portuguese - Brazil	PTB	pt-BR	
95	0x0816	Portuguese - Portugal		pt-PT	
96	0x0446	Punjabi - India		pa-IN	
97	0x0418	Romanian - Romania	ROM	ro-RO	
98	0x0419	Russian - Russia	RUS	ru-RU	
99	0x044F	Sanskrit - India		sa-IN	
100	0x0C1A	Serbian (Cyrillic) - Serbia		Cy-sr-SP	
101	0x081A	Serbian (Latin) - Serbia		Lt-sr-SP	
102	0x041B	Slovak - Slovakia	SKY	sk-SK	
103	0x0424	Slovenian - Slovenia	SLV	sl-SI	
104	0x2C0A	Spanish - Argentina	ESS	es-AR	
105	0x400A	Spanish - Bolivia	ESB	es-BO	
106	0x340A	Spanish - Chile	ESL	es-CL	
107	0x240A	Spanish - Colombia	ESO	es-CO	
108	0x140A	Spanish - Costa Rica	ESC	es-CR	
109	0x1C0A	Spanish - Dominican Republic	ESD	es-DO	
110	0x300A	Spanish - Ecuador	ESF	es-EC	
111	0x440A	Spanish - El Salvador	ESE	es-SV	
112	0x100A	Spanish - Guatemala	ESG	es-GT	
113	0x480A	Spanish - Honduras	ESH	es-HN	
114	0x080A	Spanish - Mexico	ESM	es-MX	
115	0x4C0A	Spanish - Nicaragua	ESI	es-NI	
116	0x180A	Spanish - Panama	ESA	es-PA	
117	0x3C0A	Spanish - Paraguay	ESZ	es-PY	
118	0x280A	Spanish - Peru	ESR	es-PE	
119	0x500A	Spanish - Puerto Rico	ES	es-PR	
120	0x0C0A	Spanish - Spain		es-ES	
121	0x380A	Spanish - Uruguay	ESY	es-UY	
122	0x200A	Spanish - Venezuela	ESV	es-VE	
123	0x0441	Swahili - Kenya		sw-KE	
124	0x081D	Swedish - Finland	SVF	sv-FI	
125	0x041D	Swedish - Sweden		sv-SE	
126	0x045A	Syriac - Syria		syr-SY	
127	0x0449	Tamil - India		ta-IN	
128	0x0444	Tatar - Russia		tt-RU	
129	0x044A	Telugu - India		te-IN	
130	0x041E	Thai - Thailand	THA	th-TH	
131	0x041F	Turkish - Turkey	TRK	tr-TR	
132	0x0422	Ukrainian - Ukraine	UKR	uk-UA	
133	0x0420	Urdu - Pakistan	URD	ur-PK	
134	0x0843	Uzbek (Cyrillic) - Uzbekistan		Cy-uz-UZ	
135	0x0443	Uzbek (Latin) - Uzbekistan		Lt-uz-UZ	
136	0x042A	Vietnamese - Vietnam	VIT	vi-VN	                
                */             
    }


}
