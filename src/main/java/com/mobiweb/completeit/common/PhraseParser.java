/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mobiweb.completeit.common.exception.InvalidArgumentException;
import com.mobiweb.completeit.json.AnswerJson;
import java.util.ArrayList;

/**
 *
 * @author ic
 */
public class PhraseParser {
    
    private String _phrase = null;
    private String _answers = null;
    private int _gaps = 0;
    private ArrayList<String> _sequences;  
    private final String validExpression = "^([^\\[\\]]+|(\\[\\]))([\\s]{1,}([^\\[\\]]+|(\\[\\])))*$";    
    private final String pattern = "^((((\\w{1,}\\s{1})+(\\[\\]\\s*){1,})|(((\\[\\]\\s{1}))+\\w{1,}){1,}))(\\s((\\w{1,}|(\\[\\]){1})))*$";
    private final String PHRASE_PATTERN_DEPRECATED = "^\\s*(((([^\\s\\[\\]]*\\s{1})+(\\[\\]\\s*)*)|(((\\[\\]\\s{1}))+[^\\s\\[\\]]*)*))(\\s(([^\\s\\[\\]]*|(\\[\\]){1})))*\\s*$";
    
    // FINAL WORKING PHRASE TEXT PATTERN
    private final String PHRASE_PATTERN = "(^\\s*[^\\[\\]\\s]+(\\s+[^\\[\\]\\s]+)*(\\s+\\[\\])+((\\s+\\[\\])*(\\s+[^\\[\\]\\s]*)*)*$)|(^\\s*(\\[\\])(\\s+\\[\\])*(\\s+[^\\[\\]\\s]+)+((\\s+\\[\\])*(\\s+[^\\[\\]\\s]*)*)*$)";
    
    // Answer word pattern: Include everything but square brackets [], and white space at the beginning and end of each words of 'word' property.
    // white space - one char is allowed in between several words of 'word' property string
    private final static  String ANSWER_PATTERN_old = "^\\[({\"[w][o][r][d]\":\"[^\\s\\[\\]]+(\\s[^\\s\\[\\]]+)*\"}){1}(,{\"[w][o][r][d]\":\"[^\\s\\[\\]]+(\\s[^\\s\\[\\]]+)*\"})*\\]$";
    private final static String WORD_PATTERN = "^([^\\s\\[\\]]+(\\s+[^\\s\\[\\]]+)*){1}$";
        
    private final static String ANSWER_JSON_PATTERN = "^\\s*\\[\\s*(\\{\\s*\\\"[w][o][r][d]\\\"\\s*:\\s*\\\"[^\\s\\[\\]]+(\\s[^\\s\\[\\]]+)*\\\"\\s*\\}\\s*){1}(,\\s*\\{\\s*\\\"[w][o][r][d]\\\"\\s*:\\s*\\\"[^\\s\\[\\]]+(\\s[^\\s\\[\\]]+)*\\\"\\s*\\}\\s*)*\\]\\s*$";
        
    public PhraseParser() {
    }
    
    public PhraseParser(String phrase) {
        this._phrase = phrase;
    }    
    
    public PhraseParser(String phrase, String answers)
    {
        this._phrase = phrase;
        this._answers = answers;
    }

    public String getPhrase() {
        return _phrase;
    }

    public void setPhrase(String _phrase) {
        this._phrase = _phrase;
    }

    public int getGaps() {
        return _gaps;
    }

    protected void setGaps(int _gaps) {
        this._gaps = _gaps;
    }

    public String getAnswers() {
        return _answers;
    }

    public void setAnswers(String _answers) {
        this._answers = _answers;
    }
    
    
    
    public void parse(String phrase) throws EmptyPhraseException, PhraseParseException
    {
        if(Utilities.isEmpty(phrase)){
            throw new EmptyPhraseException("Phrase was null");
        }        
        if(!phrase.matches(PHRASE_PATTERN)){
            throw new PhraseParseException("Phrase format is invalid");
        }
        this._phrase = phrase;
        //String[] split = phrase.split("\\[\\]");
        if(!phrase.contains("[]"))
        {
           throw new PhraseParseException("Phrase does not contain any gaps"); 
        }     
        
        int counter = 0;
        int index = phrase.indexOf("[]");
        while(index >= 0) {           
           index = phrase.indexOf("[]", index+1);
           counter++;
        }               
        this._gaps = counter;
    }
    
    public String parsed(String phrase) throws EmptyPhraseException, PhraseParseException
    {
        this.parse(phrase);
        String trimmed = phrase.trim();
        String parsed = trimmed.replaceAll("\\s{2,}"," ");
        return parsed;
    }
    
    public ArrayList<String> sequences()
    {
        return this._sequences;
    }
    
    /**
     * Composes full sentence from original phrase with gaps and JSON answers string.
     * @return the composed full sentence without gaps
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyAnswersException
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     */
    public String compose() throws EmptyPhraseException,EmptyAnswersException, PhraseParseException
    {
        if(Utilities.isEmpty(this.getPhrase())){
            throw new EmptyPhraseException("Phrase was null");
        } 
        if(Utilities.isEmpty(this.getAnswers())){
            throw new EmptyAnswersException("Answers was null");
        }         
        String sentence = "";
        String phrase = this.getPhrase();
        String answers = this.getAnswers();
        String[] split = phrase.split("\\[\\]");
        if(!phrase.contains("[]"))
        {
           throw new PhraseParseException("Phrase does not contain any gaps"); 
        }
        
        try
        {
            Gson gson = new Gson();
            AnswerJson[] words = gson.fromJson(answers, AnswerJson[].class);
            int gaps = words.length;
            for(int i = 0; i < gaps; i++)
            {
                sentence += split[i].concat("[" +words[i].word + "]");
                // CONCATENATE SENTENCE WITH LAST WORD IF NOT '[]'
                if(i == gaps - 1)
                {                    
                    String lastTwoChars = 
                        String.valueOf(phrase.charAt(phrase.length() - 2)) + String.valueOf(phrase.charAt(phrase.length() - 1));
                    if(!lastTwoChars.equals("[]"))
                    {
                        sentence = sentence.concat(split[split.length - 1]);
                    }
                }
            }        
            return sentence;
        }
        catch (JsonParseException je)
        {
            throw new PhraseParseException(je.getMessage());
        }
    }
    
    
    public static boolean isJsonAnswerWordFormatValid(String answer) throws InvalidArgumentException
    {
        if(answer.isEmpty() || answer == null)
        {
            throw new InvalidArgumentException("Answer string was null");
        }
        String trimmed = answer.trim();
        String parsed = trimmed.replaceAll("\\s{2,}", " ");
        return parsed.matches(WORD_PATTERN);
    }
    
    public static boolean isJsonAnswerFormatValid(String answer) throws InvalidArgumentException
    {
        if(answer.isEmpty() || answer == null)
        {
            throw new InvalidArgumentException("Answer string was null");
        }
        return answer.matches(ANSWER_JSON_PATTERN);
    }    
    
    public class PhraseParseException extends Exception
    {
        public PhraseParseException() {
        }
        public PhraseParseException(String message) {
          super(message);
        }
        public PhraseParseException(String message, Throwable cause) {
          super(message, cause);
        }        
    }
    
    public class EmptyPhraseException extends Exception
    {
        public EmptyPhraseException() {
        }
        public EmptyPhraseException(String message) {
          super(message);
        }
        public EmptyPhraseException(String message, Throwable cause) {
          super(message, cause);
        }        
    }    
    
    public class EmptyAnswersException extends Exception
    {
        public EmptyAnswersException() {
        }
        public EmptyAnswersException(String message) {
          super(message);
        }
        public EmptyAnswersException(String message, Throwable cause) {
          super(message, cause);
        }        
    }     
}
