package com.mobiweb.completeit.common;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides generic-related class methods to retrieve runtime info:
 * Methods, fields, etc.
 * @author ic
 */
public abstract class ClassUtils {

    public static <T extends Object> List<String> getDeclaredFieldNames(T t) {
        List<String> list = new ArrayList();
        Field[] fields = t.getClass().getDeclaredFields();
        if (!Utilities.isEmpty(fields)) {
            for (Field f : fields) {
                list.add(f.getName());
            }
        }
        return list;
    }

    public static <T extends Object> Map<String, Object> getFieldNameValuePair(T object, String name) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Map<String, Object> map = null;
        for (Field field : object.getClass().getDeclaredFields()) {
            //Type annotation = field.getAnnotation(com.github.jasminb.jsonapi.annotations.Type.class);
            if (field.getName().equals(name)) {
                map = new HashMap();
                map.put(name, getFieldValue(field, object));
            }
        }
        return map;
    }

    public static <T extends Object> Object getFieldValue(Field field, T t) throws InvocationTargetException {
        // MZ: Find the correct method
        for (Method method : t.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    // Method found, run it
                    try {
                        try {
                            return method.invoke(t);
                        } catch (IllegalArgumentException | InvocationTargetException ex) {
                            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IllegalAccessException e) {
                        Logger.getLogger("Could not determine method: " + method.getName());
                    }
                }
            }
        }
        return null;
    }
    
    
    
}
