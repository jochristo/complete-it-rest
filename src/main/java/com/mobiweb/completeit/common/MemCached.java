/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.common;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import net.spy.memcached.MemcachedClient;

/**
 *
 * @author ic
 */
@Singleton
public class MemCached {
    
    private static MemcachedClient client = null;

    public MemCached() {
    }   
        
    public static void createClient() {
        try {
            client = new MemcachedClient(new InetSocketAddress("localhost", 11211));
            boolean test = client.isAlive();
        } catch (IOException ex) {
            Logger.getLogger(MemCached.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static MemcachedClient getClient() throws IOException {
        return client;
    }    
    
}
