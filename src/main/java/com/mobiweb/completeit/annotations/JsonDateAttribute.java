/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.mobiweb.completeit.json.validators.JsonDateFormatValidator;

/**
 * Custom annotation for JSON date format
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Constraint(validatedBy = JsonDateFormatValidator.class)
@Documented
@Retention(RUNTIME)
public @interface JsonDateAttribute {
    
    String message() default "JSON date format is invalid. Allowed formats: dd-MM-yyyy";
    String title() default "Invalid date format";
    String code() default "1004";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};     
    JsonDateFormatPattern pattern() default JsonDateFormatPattern.shortWithDash;
    
    public enum JsonDateFormatPattern
    {
        shortWithDash(1),
        shortWithSlash(2);      
        private JsonDateFormatPattern(int value) { this.value = value; }
        private final int value;
    }
}
