/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.mobiweb.completeit.json.validators.JsonAttributeValidator;

/**
 * Custom annotation for required JSON attributes
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = JsonAttributeValidator.class)
@Documented
public @interface NotEmptyAttribute {    
    String code() default "1001";
    String message() default "JSON attribute is empty";
    String title() default "Required attribute";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};    
}
