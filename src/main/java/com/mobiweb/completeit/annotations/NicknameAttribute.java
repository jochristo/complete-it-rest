/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import com.mobiweb.completeit.json.validators.NicknameValidator;

/**
 *
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Constraint(validatedBy = NicknameValidator.class)
@Documented
@Retention(RUNTIME)
public @interface NicknameAttribute {
    
    AttributeType pattern();
    String title() default "Invalid nickname";
    String message() default "Nickname length must be between 4 and 15 characters. "
            + "It should contain any lower/upper case latin character, number, and at least one special character (_-.)";
    String code() default "1002";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
    public enum AttributeType
    {
        NICKNAME(1);     
        private AttributeType(int value) { this.value = value; }
        private final int value;
    }    
    
}
