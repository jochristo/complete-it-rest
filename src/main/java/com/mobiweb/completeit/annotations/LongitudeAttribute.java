/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.annotations;

import com.mobiweb.completeit.json.validators.LongitudeValidator;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = LongitudeValidator.class)
@Documented
public @interface LongitudeAttribute {
    
    String message() default "Longitude must be between -180 and 180 degrees inclusive.";
    String title() default "Invalid longitude format";
    String code() default "1011";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};     
    
}
