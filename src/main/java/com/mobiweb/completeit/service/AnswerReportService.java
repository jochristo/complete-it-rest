/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.AnswerProfileReport;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@LocalBean
@Stateless
public class AnswerReportService extends BaseEntityService<AnswerProfileReport>
{   
    
    public AnswerProfileReport find(long answerId, long profileId) {
        em.flush();
        Query query =  em.createNamedQuery("AnswerProfileReport.findByPK",AnswerProfileReport.class)
            .setParameter("answerId", answerId)
            .setParameter("profileId", profileId);
        List<AnswerProfileReport> list = query.getResultList();
        return list.size() == 1 ? list.get(0) : null;        
    }
    
    public List<AnswerProfileReport> find(long answerId) {
        em.flush();
        return em.createNamedQuery("AnswerProfileReport.findByAnswerId",AnswerProfileReport.class)
            .setParameter("answerId", answerId)            
            .getResultList();
    }   
    
    @Override
    public <N extends String> AnswerProfileReport findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AnswerProfileReport findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AnswerProfileReport> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
    
    public long getCount()
    {
        Query query = em.createNamedQuery("AnswerProfileReport.count", AnswerProfileReport.class);                
        return (long)query.getSingleResult();
    }    
}
