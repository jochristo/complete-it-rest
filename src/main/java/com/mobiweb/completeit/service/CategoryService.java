package com.mobiweb.completeit.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.Query;
import com.mobiweb.completeit.domain.Category;
import javax.ejb.Stateless;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class CategoryService extends BaseEntityService<Category>
{    
    public boolean existsName(String name) {
        List<Category> c = em.createNamedQuery("Category.findByName", Category.class)
                .setParameter("name", name).getResultList();
        return c.size() > 0;
    }    
    
    public boolean existsNameExcludeId(String name, long id) {
        List<Category> c = em.createNamedQuery("Category.findByNameExcludeId", Category.class)
                .setParameter("name", name)
                .setParameter("id", id)
                .getResultList();
        return c.size() > 0;
    }    
    
    public boolean existsNameUndeleted(String name) {        
        List<Category> c = em.createNamedQuery("Category.findByNameUndeleted", Category.class)
                .setParameter("name", name).getResultList();
        return c.size() > 0;
    }     

    @Override    
    public List<Category> findAll() {
        
        Query query =  em.createNamedQuery("Category.findAll", Category.class);
	List<Category> list = new ArrayList();
	list = query.getResultList();
        if(list.size() > 0){
            em.getEntityManagerFactory().getCache().evict(Category.class);
            list = query.getResultList();
        }
	return list;
    }

    @Override    
    public <N extends String> Category findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        Class c = paramValue.getClass();
        List<Category> query = em
                .createNamedQuery(namedQuery, Category.class)
                .setParameter(paramName, paramValue)
                .getResultList();
       if(query != null){
        if(query.size() == 1){
            return query.get(0);
        }
       }
       return null;
    }
       
    public boolean profileIdExists(Object id) {
        em.flush();        
        Object obj = find(id);
        return obj != null;
    }   
    
    public boolean hasForeignKeyDependency(long categoryId)
    {
        Query query = em.createNamedQuery("Category.countByCategoryIdUndeleted", Category.class)
            .setParameter("categoryId", categoryId);
        return ((long)query.getSingleResult()>0);
    }     

    @Override
    public Category findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Category> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public int count()
    {
        return (em.createNamedQuery("Category.countUndeleted", Long.class).getSingleResult()).intValue();
    }
    
    public List<Category> findAllDistinct() {
        Query query = em.createNamedQuery("Category.findAllDistinct", Category.class);
	List<Category> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    
    
}
