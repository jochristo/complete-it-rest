/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.AnswerDummy;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class AnswerDummyService extends BaseEntityService<AnswerDummy>
{

    @Override
    public AnswerDummy findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AnswerDummy> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<AnswerDummy> getAnswersByPhraseIdExcludeReported(long phraseId)
    {        
        List<AnswerDummy> answers = em.createNamedQuery("AnswerDummy.findByPhraseIdExcludeReported", AnswerDummy.class)            
            .setParameter("phraseId", phraseId)            
            .getResultList();        
        return answers;
        
    }     
    
    public List<Answer> findAllPaginated(int page, int size)
    {        
        Query query = em.createNamedQuery("AnswerDummy.findAll", AnswerDummy.class);
        query.setFirstResult((page-1)*size);
        query.setMaxResults(size);
	List<Answer> list = new ArrayList();
	list = query.getResultList();
	return list;
    }     
}
