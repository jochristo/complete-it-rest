/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.common.DistanceCalculator;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.Location;
import com.mobiweb.completeit.domain.Profile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Predicate to filter collections of Answer objects
 * by several fields: age, distance from location of the  source profile, profile, and phrase.
 * 
 * @author ic
 */
public class AnswerFilterPredicate implements Predicate<Answer>
{
    int minAge = -1;
    int maxAge = -1;
    int minLocation = -1;
    int maxLocation = -1;    
    Location source = null;
    public Profile profile = null;
    public long phraseId = -1;
    public boolean isAgeMode = false;
    public boolean isDistanceMode = false;
    private boolean isMixedMode = false;
    private final SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");     
    
    @Override
    public boolean test(Answer answer) {
        
        answer.getProfile();
        try {            

            isMixedMode = isAgeMode==true && isDistanceMode == true;
            
            if(minAge > 0 && maxAge > 0 && isAgeMode && !isMixedMode) // age filter predicate only
            {            
                String dob = sd.format(answer.getProfile().getDob());
                Date date = sd.parse(dob);
                Instant instant = date.toInstant();
                ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
                LocalDate ld = zdt.toLocalDate();
                int age = Utilities.getAge(ld);
                if (age >= minAge && age <= maxAge) {
                    return false;
                }
            }
            else if(source != null && minLocation >= 0 && maxLocation >= 0 && isDistanceMode && !isMixedMode)
            {
                double dist = DistanceCalculator.distance(source.getY(), source.getX(),answer.getProfile().getLocation().getY(), 
                              answer.getProfile().getLocation().getX(),"K");
                
                // zero distance from current profile player                
                if(minLocation == maxLocation){
                    if(dist == minLocation ){
                        return false;
                    }                    
                }                
                else if(minLocation != maxLocation){
                    // some distance from current profile player
                    if(dist>= new Integer(minLocation).doubleValue() && dist <= new Integer(maxLocation).doubleValue()){
                        return false;
                    }
                }
            }    
            else if(profile != null && phraseId == -1) // profile mismatch
            {
                if(profile.getId() != answer.getProfile().getId()){ 
                    return false;
                }
            }            
            else if(profile != null && phraseId > 0) // profile mismatch and phrase id match
            {
                if(profile.getId() != answer.getProfile().getId() && answer.getPhrase().getId() == phraseId){ 
                    return false;
                }
            }
            else if(isMixedMode)
            {
                String dob = sd.format(answer.getProfile().getDob());
                Date date = sd.parse(dob);
                Instant instant = date.toInstant();
                ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
                LocalDate ld = zdt.toLocalDate();
                int age = Utilities.getAge(ld);  
                double dist = DistanceCalculator.distance(source.getY(), source.getX(),answer.getProfile().getLocation().getY(), 
                              answer.getProfile().getLocation().getX(),"K");
                if (age >= minAge && age <= maxAge)
                {
                    // zero distance from current profile player                
                    if(minLocation == maxLocation){
                        if(dist == minLocation ){
                            return false;
                        }                    
                    }                
                    else if(minLocation != maxLocation){
                        // some distance from current profile player
                        if(dist>= new Integer(minLocation).doubleValue() && dist <= new Integer(maxLocation).doubleValue()){
                            return false;
                        }
                    }                    
                }                
            }
        }
        catch (ParseException ex)
        {
            Logger.getLogger(AnswerFilterPredicate.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return true;
    }    
}
