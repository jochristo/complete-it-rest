/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.mobiweb.completeit.domain.Profile;

/**
 *
 * @author ic
 */
public class ProfileItem
{
    
    private long id = 0;
    private String nickname = "";
    private String pictureFilepath = "";    
    private String language = "";
    private String email = "";
    private long score = 0;
    private int level = 1;
    private String location = "";   
    private String gender = "";   
    private String status = "Active";
    private boolean deleted = false;

    public ProfileItem() {
    }

    public ProfileItem(Profile p) {
        
        id = p.getId();
        nickname = p.getNickname();
        pictureFilepath = p.getPictureFilepath();
        language = p.getLanguage();
        email = p.getEmail();
        score = p.getScore();
        level = p.getLevel();
        location = String.valueOf(p.getLocation().getY()).concat(", ").concat(String.valueOf(p.getLocation().getX()));
        gender = p.getGender();
        if(p.isDeleted()){
            status = "Suspended";
        }
        this.deleted = p.isDeleted();
    }    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPictureFilepath() {
        return pictureFilepath;
    }

    public void setPictureFilepath(String pictureFilepath) {
        this.pictureFilepath = pictureFilepath;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    
    
}
