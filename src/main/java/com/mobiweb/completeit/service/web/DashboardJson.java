/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.mobiweb.completeit.domain.CategoryProjection;
import java.util.List;

/**
 *
 * @author ic
 */
public class DashboardJson
{
    
    // totals section
    private long invites;
    private long shares;
    private long reported;
    private long liked;
    private long answered;    
    
    // domain entities counters
    private long profiles;
    private long categories;
    private long phrases;
    private long answers;
    
    //max yAxis counts
    private long maxReported;
    private long maxUnreported;
    private long maxAnswered;
    private long maxUnanswered;
    
    private ReportedAnswers reportedAnswers;
    private PhrasesAnswered phrasesAnswered;    
    private List<CategoryPhraseItem> categoryPhrases;
    private List<CategoryAnswerItem> categoryAnswers;
    private List<CategoryPhraseStatsItem> categoryPhraseStats;
    
    public ReportedAnswers getReportedAnswers() {
        return reportedAnswers;
    }

    public PhrasesAnswered getPhrasesAnswered() {
        return phrasesAnswered;
    }

    public void setPhrasesAnswered(PhrasesAnswered phrasesAnswered) {
        this.phrasesAnswered = phrasesAnswered;
    }   
    

    public void setReportedAnswers(ReportedAnswers reportedAnswers) {
        this.reportedAnswers = reportedAnswers;
    }

    public List<CategoryPhraseItem> getCategoryPhrases() {
        return categoryPhrases;
    }

    public void setCategoryPhrases(List<CategoryPhraseItem> categoryPhrases) {
        this.categoryPhrases = categoryPhrases;
    }    

    public List<CategoryAnswerItem> getCategoryAnswers() {
        return categoryAnswers;
    }

    public void setCategoryAnswers(List<CategoryAnswerItem> categoryAnswers) {
        this.categoryAnswers = categoryAnswers;
    }   

    public List<CategoryPhraseStatsItem> getCategoryPhraseStats() {
        return categoryPhraseStats;
    }

    public void setCategoryPhraseStats(List<CategoryPhraseStatsItem> categoryPhraseStats) {
        this.categoryPhraseStats = categoryPhraseStats;
    }
            
    
    public long getInvites() {
        return invites;
    }

    public void setInvites(long invites) {
        this.invites = invites;
    }

    public long getShares() {
        return shares;
    }

    public void setShares(long shares) {
        this.shares = shares;
    }

    public long getReported() {
        return reported;
    }

    public void setReported(long reported) {
        this.reported = reported;
    }

    public long getLiked() {
        return liked;
    }

    public void setLiked(long liked) {
        this.liked = liked;
    }

    public long getAnswered() {
        return answered;
    }

    public void setAnswered(long answered) {
        this.answered = answered;
    }

    public long getProfiles() {
        return profiles;
    }

    public void setProfiles(long profiles) {
        this.profiles = profiles;
    }

    public long getCategories() {
        return categories;
    }

    public void setCategories(long categories) {
        this.categories = categories;
    }

    public long getPhrases() {
        return phrases;
    }

    public void setPhrases(long phrases) {
        this.phrases = phrases;
    }

    public long getAnswers() {
        return answers;
    }

    public void setAnswers(long answers) {
        this.answers = answers;
    }

    public long getMaxReported() {
        return maxReported;
    }

    public void setMaxReported(long maxReported) {
        this.maxReported = maxReported;
    }

    public long getMaxUnreported() {
        return maxUnreported;
    }

    public void setMaxUnreported(long maxUnreported) {
        this.maxUnreported = maxUnreported;
    }

    public long getMaxAnswered() {
        return maxAnswered;
    }

    public void setMaxAnswered(long maxAnswered) {
        this.maxAnswered = maxAnswered;
    }

    public long getMaxUnanswered() {
        return maxUnanswered;
    }

    public void setMaxUnanswered(long maxUnanswered) {
        this.maxUnanswered = maxUnanswered;
    }
       
    
    
  public class ReportedAnswers
  {
      private double reported;
      private double unreported;

      public ReportedAnswers(double reported, double unreported) {
          this.reported = reported;
          this.unreported = unreported;
      }      
      
      public double getReported() {
          return reported;
      }

      public void setReported(long reported) {
          this.reported = reported;
      }

      public double getUnreported() {
          return unreported;
      }

      public void setUnreported(long unreported) {
          this.unreported = unreported;
      }
      
  }  
  
  public class ReportedAnswerItem
  {
      private double reported;
      private double unreported;

      public ReportedAnswerItem(double reported, double unreported) {
          this.reported = reported;
          this.unreported = unreported;
      }      
      
      public double getReported() {
          return reported;
      }

      public void setReported(long reported) {
          this.reported = reported;
      }

      public double getUnreported() {
          return unreported;
      }

      public void setUnreported(long unreported) {
          this.unreported = unreported;
      }
      
  }
  
  public class PhrasesAnswered
  {
      private double answered;
      private double unanswered;

      public PhrasesAnswered(double answered, double unanswered) {
          this.answered = answered;
          this.unanswered = unanswered;
      }      
      
      public double getReported() {
          return answered;
      }

      public void setReported(long answered) {
          this.answered = answered;
      }

      public double getUnreported() {
          return unanswered;
      }

      public void setUnreported(long unanswered) {
          this.unanswered = unanswered;
      }
      
  }  
  
  public class PhrasesAnsweredItem
  {
      private double answered;
      private double unanswered;

      public PhrasesAnsweredItem(double answered, double unanswered) {
          this.answered = answered;
          this.unanswered = unanswered;
      }      
      
      public double getReported() {
          return answered;
      }

      public void setReported(long answered) {
          this.answered = answered;
      }

      public double getUnreported() {
          return unanswered;
      }

      public void setUnreported(long unanswered) {
          this.unanswered = unanswered;
      }
      
  }  
  
  public class CategoryPhrases
  {
        private List<CategoryProjection> categories;

        public CategoryPhrases(List<CategoryProjection> categories) {
            this.categories = categories;
        }       

        public List<CategoryProjection> getCategories() {
          return categories;
        }

        public void setCategories(List<CategoryProjection> categories) {
              this.categories = categories;
        }      
  }
  
  public class CategoryPhraseItem implements Comparable<CategoryPhraseItem>
  {
        private double categoryId;
        private String categoryName;
        private double percentage;
        private boolean highest = false;

        public CategoryPhraseItem(double categoryId, String categoryName, double percentage) {
            this.categoryId = categoryId;
            this.categoryName = categoryName;
            this.percentage = percentage;
        }

        public double getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(double categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public double getPercentage() {
            return percentage;
        }

        public void setPercentage(double percentage) {
            this.percentage = percentage;
        }

        public boolean isHighest() {
            return highest;
        }

        public void setHighest(boolean highest) {
            this.highest = highest;
        }       
        

        @Override
        public int compareTo(CategoryPhraseItem o) {
            return Double.compare(this.getPercentage(), o.getPercentage());
        }
        
        
  }
  
  public class CategoryAnswerItem implements Comparable<CategoryAnswerItem>
  {        
        private String name;        
        private long reported;
        private long unreported;
        private boolean highestReported = false;        

        public CategoryAnswerItem(String name, long reported, long unreported) {
            this.name = name;
            this.reported = reported;
            this.unreported = unreported;
        }
       
        
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getReported() {
            return reported;
        }

        public void setReported(long reported) {
            this.reported = reported;
        }

        public long getUnreported() {
            return unreported;
        }

        public void setUnreported(long unreported) {
            this.unreported = unreported;
        }

        public boolean isHighestReported() {
            return highestReported;
        }

        public void setHighestReported(boolean highestReported) {
            this.highestReported = highestReported;
        }        

        @Override
        public int compareTo(CategoryAnswerItem o) {
            return Double.compare(this.getReported(), o.getReported());
        }
        
        
  }  
  
  public class CategoryPhraseStatsItem implements Comparable<CategoryPhraseStatsItem>
  {        
        private String name;        
        private long answered;
        private long unanswered;
        private boolean highestAnswered = false;        

        public CategoryPhraseStatsItem(String name, long reported, long unreported) {
            this.name = name;
            this.answered = reported;
            this.unanswered = unreported;
        }
       
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getAnswered() {
            return answered;
        }

        public void setAnswered(long answered) {
            this.answered = answered;
        }

        public long getUnanswered() {
            return unanswered;
        }

        public void setUnanswered(long unanswered) {
            this.unanswered = unanswered;
        }

        public boolean isHighestAnswered() {
            return highestAnswered;
        }

        public void setHighestAnswered(boolean highestAnswered) {
            this.highestAnswered = highestAnswered;
        }
              

        @Override
        public int compareTo(CategoryPhraseStatsItem o) {
            return Double.compare(this.getAnswered(), o.getAnswered());
        }
        
        
  }   
  
  
  
}
