/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.json.AnswerJson;

/**
 *
 * @author ic
 */
public class AnswerItem 
{
    private long id = 0;
    private Object answers = "";
    private String phrase = "";    
    private String email = "";    
    private long likes = 0;
    private long reports = 1;    
    private String status = "Active";  
    private boolean deleted = false;

    public AnswerItem() {
    }
    
    public AnswerItem(Answer a) throws PhraseParser.EmptyPhraseException, PhraseParser.EmptyAnswersException, PhraseParser.PhraseParseException
    {
        id = a.getId();        
        PhraseParser parser = new StyledPhraseParser(a.getPhrase().getPhrase(),a.getAnswers(), "gap");
        answers = (Object)parser.compose();
        phrase = String.valueOf(a.getPhrase().getId());
        email = a.getProfile().getEmail();
        likes = a.getLikes();
        reports = a.getReports();
        if(a.isDeleted()){
            status = "Suspended";
        }   
        deleted = a.isDeleted();
    }    

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Object getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public long getReports() {
        return reports;
    }

    public void setReports(long reports) {
        this.reports = reports;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    public class StyledPhraseParser extends PhraseParser
    {
        private String _cssClass;        

        public StyledPhraseParser(String phrase, String answers, String cssClass) {
            super(phrase,answers);            
            _cssClass = cssClass;
        }        

        @Override
        public String compose() throws EmptyPhraseException, EmptyAnswersException, PhraseParseException 
        {
            String styleStart = "<text class=\"".concat(this.getCssClass()).concat("\"").concat(">");
            String styleEnd = "</text>";
            if(Utilities.isEmpty(this.getPhrase())){
                throw new EmptyPhraseException("Phrase was null");
            } 
            if(Utilities.isEmpty(this.getAnswers())){
                throw new EmptyAnswersException("Answers was null");
            }         
            String sentence = "";
            String phrase = this.getPhrase();
            String answers = this.getAnswers();
            String[] split = phrase.split("\\[\\]");
            if(!phrase.contains("[]"))
            {
               throw new PhraseParseException("Phrase does not contain any gaps"); 
            }

            Gson gson = new Gson();
            AnswerJson[] words = gson.fromJson(answers, AnswerJson[].class);
            int gaps = words.length;
            for(int i = 0; i < gaps; i++)
            {   
                sentence += "<text>"+ split[i].concat("[</text>" +styleStart + words[i].word +styleEnd +"<text>]</text>");
                // CONCATENATE SENTENCE WITH LAST WORD IF NOT '[]'
                if(i == gaps - 1)
                {
                    String lastTwoChars = 
                            String.valueOf(phrase.charAt(phrase.length() - 2)) + String.valueOf(phrase.charAt(phrase.length() - 1));
                    if(!lastTwoChars.equals("[]"))
                    {
                        sentence = "<text>"+sentence.concat(split[split.length - 1]+"</text>");             
                    }
                }
            }
            return sentence;
        }

        public String getCssClass() {
            return _cssClass;
        }

        public void setCssClass(String _cssClass) {
            this._cssClass = _cssClass;
        }
        
        
        
    }
    
}
