/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.mobiweb.completeit.domain.Category;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 *
 * @author ic
 */
public class CategoryItem
{
    private long id = 0;
    private String category = "";    
    private String image = "";    
    private String description = "";    
    private String cultureLanguage = "";
    private long cultureId = 0;
    private String cultureDisplayName = "";
    private Collection<CultureOptionItem> cultureItems = new HashSet();

    public CategoryItem() {
    }

    public CategoryItem(Category c) {
        
        if(c != null)
        {
            this.id = c.getId();
            this.category = c.getName();
            this.image = c.getImageRelativePath();
            this.description = c.getDescription();
            this.cultureLanguage = c.getCulture().getLanguageName();
            this.cultureId = c.getCulture().getId();
            this.cultureDisplayName = c.getCulture().getDisplayName();
        }
    } 
    
    public CategoryItem(Category c, Collection<CultureOptionItem> cultureItems) {
        
        if(c != null)
        {
            this.id = c.getId();
            this.category = c.getName();
            this.image = c.getImageRelativePath();
            this.description = c.getDescription();
            this.cultureLanguage = c.getCulture().getLanguageName();
            this.cultureId = c.getCulture().getId();
            this.cultureItems = cultureItems;
            this.cultureDisplayName = c.getCulture().getDisplayName();
        }
    }    
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String categoy) {
        this.category = categoy;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCultureLanguage() {
        return cultureLanguage;
    }

    public void setCultureLanguage(String cultureLanguage) {
        this.cultureLanguage = cultureLanguage;
    }    

    public long getCultureId() {
        return cultureId;
    }

    public void setCultureId(long cultureId) {
        this.cultureId = cultureId;
    }

    public String getCultureDisplayName() {
        return cultureDisplayName;
    }

    public void setCultureDisplayName(String cultureDisplayName) {
        this.cultureDisplayName = cultureDisplayName;
    }
    
    
    
    public Collection<CultureOptionItem> getCultureItems() {
        return cultureItems;
    }

    public void setCultureItems(Collection<CultureOptionItem> cultureItems) {
        this.cultureItems = cultureItems;
    }
            

    public class CultureOptionItem
    {
        private long id;
        private String language;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public CultureOptionItem(long id, String language) {
            this.id = id;
            this.language = language;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CultureOptionItem other = (CultureOptionItem) obj;
            boolean equals = Objects.equals(this.language, other.language);
            return equals;
        }
        
        
    }    
    
}
