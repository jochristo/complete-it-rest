/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.Phrase;
import java.util.List;

/**
 * JSP view PhraseItem object
 * @author ic
 */
public class PhraseItem {
    
    private long id = 0;
    private String phrase = "";
    private int gaps = 0;
    private String categories = "";
    private String isApproved = "Pending";
    private String addingProfileEmail = "";
    private boolean status = false;

    public PhraseItem() {
    }

    public PhraseItem(Phrase phrase) {
        status = phrase.isIsApprovalPending();
        id = phrase.getId();
        this.phrase = phrase.getPhrase();
        gaps = phrase.getGaps();
        if(!phrase.isIsApprovalPending()){
            isApproved = "Approved";
        }        
        if(phrase.getProfile() != null)
        {
            this.addingProfileEmail = phrase.getProfile().getEmail();
        }
        
        String names = "";        
        List<Category> categoryList = phrase.getCategories();
        if(categoryList != null)
        {
            if(categoryList.size()>0)
            {
                int index = 1;
                int max = categoryList.size();
                for(Category c : categoryList)
                {
                    if(index < max){
                        names += c.getName().concat(", ");
                    }else if(index == max)
                    {
                        names += c.getName();
                    }
                    index++;
                }
                this.categories = names;
            }
        }        
    }
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getGaps() {
        return gaps;
    }

    public void setGaps(int gaps) {
        this.gaps = gaps;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public String getAddingProfileEmail() {
        return addingProfileEmail;
    }

    public void setAddingProfileEmail(String addingProfileEmail) {
        this.addingProfileEmail = addingProfileEmail;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
    
}
