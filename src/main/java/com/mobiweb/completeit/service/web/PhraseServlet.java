/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.exception.EntityForeignKeyDependencyException;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.common.exception.IncorrectWordGapsException;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.Phrase;
import com.mobiweb.completeit.domain.PhraseCategory;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.PhraseCategoryService;
import com.mobiweb.completeit.service.PhraseService;
import static com.mobiweb.completeit.service.web.JsonServletResponse.Code.FOREIGN_KEY_DEPENDENCY;
import static com.mobiweb.completeit.service.web.JsonServletResponse.Code.PHRASE_TEXT_INVALID;
import com.mobiweb.completeit.service.web.PhraseEditItem.CategoryOptionItem;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ic
 */
@WebServlet(name = "phrases", urlPatterns = {"/web/secured/phrases"})
public class PhraseServlet extends HttpServlet {

    private static final long serialVersionUID = -5832176047021911038L;
    public static int PAGE_SIZE = 50;        
    private @EJB PhraseService ps;     
    private @EJB CategoryService cs;  
    private @EJB PhraseCategoryService pcs;      
    private @EJB EntityCounterService ecs;
    private ErrorUtils.ErrorMessage errorMessage;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PhraseServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PhraseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){
              //process(request, response);        
                getAjax(request, response);
            }
            else
            {
              //process(request, response);
                getPhrases(request, response);
            }            
        } catch (Exception ex) {
            Logger.getLogger(PhraseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException
    {
        try {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){              
                postAjax(request, response);
            }            
            //process(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PhraseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void process(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, EntityNotFoundException
    {
        Map<String, String[]> map = request.getParameterMap();
        String action = request.getParameter("action");        
        if ("Add".equals(action))
        {
            String text = request.getParameter("text");
            long category = Long.parseLong(request.getParameter("category"));
            Category cat = cs.find(category);
            
            // parse text against regular expression to match input mask
            PhraseParser parser = new PhraseParser();
            parser.parse(text);
            
            Phrase phrase = new Phrase();
            phrase.setPhrase(text);
            phrase.setGaps(parser.getGaps());
            phrase.setProfile(null);        

            Phrase inserted = ps.created(phrase);
            //save PhraseCategory foreign keys                        
            pcs.addPhraseCategory(inserted, cat);
            inserted.getPhraseCategory().add(pcs.find(inserted.getId(), cat.getId()));
            inserted.setPhraseCategory(inserted.getPhraseCategory());
            ps.update(inserted);
            
            //update entity counter table
            ecs.updateCounter(1, Phrase.class);              
            
            List<Phrase> phrases = ps.findAll();
            List<Phrase> range = new ArrayList();
            List<PhraseItem> items = new  ArrayList();
            range = phrases;
            for (Phrase p : range) {
                PhraseItem item = new PhraseItem(p);
                items.add(item);
            }
  
            
            request.setAttribute("phrases", items);                     
            // set new attribute with item
            PhraseItem item = new PhraseItem(inserted);
            request.setAttribute("item", item);
            Gson gson = new Gson();
            String json = gson.toJson(item, PhraseItem.class);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);            
        }
        else if("Delete".equals(action))
        {
            long phraseId = Long.parseLong(request.getParameter("id"));
            if(ps.hasForeignKeyDependency(phraseId))
            {
                // return error string
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");                
                response.getWriter().write("denied");
            }
            else{
                // else, remove phrase
                Phrase p = ps.find(phraseId);
                //p.getPhraseCategory().clear();
                //p.setPhraseCategory(p.getPhraseCategory());
                //ps.update(p);                
                //p = ps.find(p.getId());
                //ps.remove(p);                

                List<PhraseCategory> list = pcs.find(p.getId());
                if(list.size() >0)
                {
                    list.forEach((PhraseCategory pc)->{pc.setActive(false); pcs.update(pc);});
                }
                p.setDeleted(true);
                ps.update(p);
                //ps.remove(p);   
                
                //update entity counter table
                ecs.updateCounter(1, Phrase.class);                  
                
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write("deleted");
            }
        }   
        else if("Fetch".equals(action))
        {
            Gson gson = new Gson();
            String json = "";          
            try
            {                      
                String id = request.getParameter("id");       
                long phraseId = Long.parseLong(id);                            
                Phrase phrase = ps.find(phraseId);
                PhraseItem item = new PhraseItem(phrase);
                List<Category> categories = cs.findAllDistinct();
                Map<Long,String> cmap = new LinkedHashMap();
                cmap = categories.stream().collect(Collectors.toMap(Category::getId,Category::getName));
                //List<String> names = categories.stream().map(Category::getName).collect(Collectors.toList());
                List<CategoryOptionItem> categoryOptions = new ArrayList();
                for(Entry entry : cmap.entrySet())
                {
                   CategoryOptionItem coi = new PhraseEditItem().new CategoryOptionItem((long)entry.getKey(), (String)entry.getValue());
                    categoryOptions.add(coi);
                }
                PhraseEditItem editItem = new PhraseEditItem(phrase,categoryOptions);
                json = gson.toJson(editItem, PhraseEditItem.class);                
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);                                     
            }
            catch (Exception ex)
            {                
                response.setStatus(400); // bad request
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                String message = "Error: " + ex.getMessage();
                errorMessage = new ErrorUtils().new ErrorMessage()
                    .setDetail(message)
                    .setHttpStatusCode(400)
                    .setTitle("Exception");
                JsonError je = new JsonError();
                je.setId(1);
                je.setTitle("Exception");
                je.setDetail(message);
                je.setApplicationErrorCode(0);
                je.setHttpErrorCode(400);
                json = gson.toJson(je, JsonError.class);
                response.getWriter().write(json);
            }            
        }
        else if("Update".equals(action))
        {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){
              this.postAjax(request, response);        
            }            
        }    
        else if("ApproveAll".equals(action))
        {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){
              this.updateApprovalStatusAjaxPost(request, response, false);        
            }            
        }  
        else if("DisapproveAll".equals(action))
        {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){
              this.updateApprovalStatusAjaxPost(request, response,true);        
            }            
        }         
        else
        {
            // render all phrases with paginated data table
            List<Phrase> phrases = ps.findAll();            
            List<PhraseItem> items = new  ArrayList();
            for(Phrase p : phrases)
            {
                PhraseItem item = new PhraseItem(p);
                items.add(item);
            }                                   
            boolean isAllPending = ps.isAllPending();
            boolean isAllApproved = ps.isAllApproved();
            List<Category> categories = cs.findAll();                 
            request.setAttribute("categories", categories);            
            request.setAttribute("phrases", items);   
            request.setAttribute("isAllApproved", isAllApproved);            
            request.setAttribute("isAllPending", isAllPending);   
            request.getRequestDispatcher("/secured/phrases/index.jsp").forward(request, response); 
        }
    }
        
    private void getAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";         
        JsonServletResponse jsr = new JsonServletResponse(); 
        try
        {          
            String action = request.getParameter("action");        
            String id = request.getParameter("id");        
            if("Fetch".equals(action))
            {
                long phraseId = Long.parseLong(id);
                Phrase phrase = ps.find(phraseId);
                if (phrase == null) {
                    throw new EntityNotFoundException("Phrase with id <" + phraseId + "> could not be retrieved.");
                }
                List<Category> categories = cs.findAllDistinct();
                Map<Long, String> cmap = new LinkedHashMap();
                cmap = categories.stream().collect(Collectors.toMap(Category::getId, Category::getName));
                //List<String> names = categories.stream().map(Category::getName).collect(Collectors.toList());
                List<CategoryOptionItem> categoryOptions = new ArrayList();
                for (Entry entry : cmap.entrySet()) {
                    CategoryOptionItem coi = new PhraseEditItem().new CategoryOptionItem((long) entry.getKey(), (String) entry.getValue());
                    categoryOptions.add(coi);
                }
                PhraseEditItem editItem = new PhraseEditItem(phrase, categoryOptions);
                json = gson.toJson(editItem, PhraseEditItem.class);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
            }
            else if("Delete".equals(action))
            {
                long phraseId = Long.parseLong(id);
                if(ps.hasForeignKeyDependency(phraseId))
                {
                    throw new EntityForeignKeyDependencyException("Cannot delete phrase with id <"+ phraseId + "> because it is referenced in records.");
                }
                else{
                    // else, remove phrase
                    Phrase p = ps.find(phraseId); 
                    List<PhraseCategory> list = pcs.find(p.getId());
                    if(list.size() >0)
                    {
                        list.forEach((PhraseCategory pc)->{pc.setActive(false); pcs.update(pc);});
                    }
                    p.setDeleted(true);
                    ps.update(p);
                    //update entity counter table
                    ecs.updateCounter(1, Phrase.class);                    
                    jsr.setTitle(JsonServletResponse.Result.SUCCESS_RESPONSE);
                    jsr.setDetail(JsonServletResponse.Result.DELETED_RESPONSE);
                    jsr.setApplicationErrorCode(0);
                    jsr.setHttpStatus(200);
                    json = gson.toJson(jsr, JsonServletResponse.class);
                    response.getWriter().write(json);                     
                }
            }             
        }
        catch (Exception ex)
        { 
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setApplicationErrorCode(0);
            if(ex instanceof EntityForeignKeyDependencyException){
                jsr.setApplicationErrorCode(FOREIGN_KEY_DEPENDENCY);
            }
            jsr.setDetail(message);            
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);             
        }
    }      
    
    private void postAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";  
        JsonServletResponse jsr = new JsonServletResponse();        
        try
        {          
            String action = request.getParameter("action");        
            String id = request.getParameter("id");  
            if ("Add".equals(action))
            {
                String text = request.getParameter("text");
                long category = Long.parseLong(request.getParameter("category"));
                Category cat = cs.find(category);

                // parse text against regular expression to match input mask
                PhraseParser parser = new PhraseParser();                
                text = parser.parsed(text);

                Phrase phrase = new Phrase();
                phrase.setPhrase(text);
                phrase.setGaps(parser.getGaps());
                phrase.setProfile(null);        

                Phrase inserted = ps.created(phrase);
                //save PhraseCategory foreign keys                        
                pcs.addPhraseCategory(inserted, cat);
                inserted.getPhraseCategory().add(pcs.find(inserted.getId(), cat.getId()));
                inserted.setPhraseCategory(inserted.getPhraseCategory());
                ps.update(inserted);

                //update entity counter table
                ecs.updateCounter(1, Phrase.class);              

                List<Phrase> phrases = ps.findAll();
                List<Phrase> range = new ArrayList();
                List<PhraseItem> items = new  ArrayList();
                range = phrases;
                for (Phrase p : range) {
                    PhraseItem item = new PhraseItem(p);
                    items.add(item);
                }


                request.setAttribute("phrases", items);                     
                // set new attribute with item
                PhraseItem item = new PhraseItem(inserted);
                request.setAttribute("item", item);                
                json = gson.toJson(item, PhraseItem.class);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);            
            }            
            else if ("Fetch".equals(action))
            {                          
                long phraseId = Long.parseLong(id);                            
                Phrase phrase = ps.find(phraseId);
                PhraseItem item = new PhraseItem(phrase);
                json = gson.toJson(item, PhraseItem.class);                
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);                         
            }
            else if("Update".equals(action))
            {   
                //get params                
                long categoryId = Long.parseLong(request.getParameter("categoryId"));
                long phraseId = Long.parseLong(id);
                String phraseText = request.getParameter("phraseText");
                long approvalStatus = Long.parseLong(request.getParameter("approvalStatus"));
                Phrase phrase = ps.find(phraseId);
                Category category = cs.find(categoryId);
                if(phrase == null)
                {
                    throw new EntityNotFoundException("Phrase with id <" + phraseId + "> could not be retrieved.");
                }
                if(category == null)
                {
                    throw new EntityNotFoundException("Category with id <" + categoryId + "> could not be retrieved.");
                }
                
                //check if answers exist in case gaps are different after update
                PhraseParser parser = new PhraseParser(phraseText);
                parser.parse(phraseText);
                int gaps = parser.getGaps();
                Phrase match = ps.findPhraseInAnswers(phraseId);
                if(match != null)
                {
                    if(match.getGaps() != gaps)
                    {
                        throw new IncorrectWordGapsException("Phrase gaps could not be changed because phrase contains answers from users");                        
                    }
                }                
                phrase.setPhrase(phraseText);
                phrase.setGaps(gaps);
                boolean isApprovalPending = approvalStatus == 0 ? true : false ;
                phrase.setIsApprovalPending(isApprovalPending);                
                Phrase updated = ps.updated(phrase);
                
                // process phrase category
                PhraseCategory pc = pcs.findOptionally(phrase.getId(), categoryId);                
                Set<PhraseCategory> existingCategories = phrase.getPhraseCategory();
                if(pc == null)
                {                    
                    for(PhraseCategory item : existingCategories)
                    {
                        pcs.deletePhraseCategory(item.getPhrase(), item.getCategory());                        
                    }  
                    pcs.create(new PhraseCategory(phrase, category));                    
                }
                else
                {
                    //remove other categories
                    for(PhraseCategory item : existingCategories)
                    {
                        if(!item.equals(pc)){
                            pcs.deletePhraseCategory(item.getPhrase(), item.getCategory());                        
                        }
                    }                    
                }               

                updated = ps.find(id);                
                if(updated != null)
                {
                    PhraseItem item = new PhraseItem(updated);                    
                    json = gson.toJson(item, PhraseItem.class);                                       
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");                    
                    response.getWriter().write(json);
                }
                else
                {
                    response.getWriter().write("ERROR");
                }                                      
            }
            else if("ApproveAll".equals(action))
            {
                updateApprovalStatusAjaxPost(request, response, false);        
            }  
            else if("DisapproveAll".equals(action))
            { 
                this.updateApprovalStatusAjaxPost(request, response,true);  
            }             
        }         
        catch (Exception ex)
        {  
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setApplicationErrorCode(0);
            if(ex instanceof IncorrectWordGapsException)
            {
                jsr.setTitle(JsonServletResponse.Result.PHRASE_TEXT_CHANGED_FAILED);  
                jsr.setApplicationErrorCode(PHRASE_TEXT_INVALID);
            }
            jsr.setDetail(message);            
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);            
        }    
    }   
    
    private void updateApprovalStatusAjaxPost(HttpServletRequest request, HttpServletResponse response, boolean isPending) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";  
        JsonServletResponse jsr = new JsonServletResponse();
        jsr.setId(1);
        try
        {   
            // change approval pending status to false
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");  
            boolean result = ps.updateAllApprovalStatusBulk(isPending);
            if (result) {
                jsr.setTitle(JsonServletResponse.Result.SUCCESS_RESPONSE);
                jsr.setDetail(JsonServletResponse.Result.SUCCESS_RESPONSE);
                jsr.setApplicationErrorCode(0);
                jsr.setHttpStatus(200);
                json = gson.toJson(jsr, JsonServletResponse.class);
                response.getWriter().write(json);                
            } else {
                jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
                jsr.setDetail(JsonServletResponse.Result.ERROR_RESPONSE);
                jsr.setApplicationErrorCode(0);
                jsr.setHttpStatus(400);
                json = gson.toJson(jsr, JsonServletResponse.class);
                response.getWriter().write(json); 
            }
        }
        catch (Exception ex)
        {                
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setDetail(message);
            jsr.setApplicationErrorCode(0);
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);
        }      
    }    
  
    private void getPhrases(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            // render all phrases with paginated data table
            List<Phrase> phrases = ps.findAll();            
            List<PhraseItem> items = new  ArrayList();
            for(Phrase p : phrases)
            {
                PhraseItem item = new PhraseItem(p);
                items.add(item);
            }                                   
            boolean isAllPending = ps.isAllPending();
            boolean isAllApproved = ps.isAllApproved();
            List<Category> categories = cs.findAll();   
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");            
            request.setAttribute("categories", categories);            
            request.setAttribute("phrases", items);   
            request.setAttribute("isAllApproved", isAllApproved);            
            request.setAttribute("isAllPending", isAllPending);   
            request.getRequestDispatcher("/secured/phrases/index.jsp").forward(request, response);         
    }

}
