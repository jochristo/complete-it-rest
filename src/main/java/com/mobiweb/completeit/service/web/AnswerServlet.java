/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.service.AnswerService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ic
 */
@WebServlet(name = "answers", urlPatterns = {"/web/secured/answers"})
public class AnswerServlet extends HttpServlet
{   
    @EJB private AnswerService answerService; 
    private ErrorUtils.ErrorMessage errorMessage;    

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            boolean isAjaxRequest = "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));
            if(isAjaxRequest){              
                postAjax(req, resp);
            }                        
        } catch (Exception ex) {
            Logger.getLogger(PhraseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    
        List<Answer> answers = answerService.findAllInclDeleted(); 
        List<AnswerItem> items = new  ArrayList();
        JsonServletResponse jsr = new JsonServletResponse();
        Gson gson = new Gson();
        String json = "";  
        int  counter = 0;
        for(Answer a : answers)
        {
            AnswerItem item;
            try {
                
                item = new AnswerItem(a);
                items.add(item);
                counter ++;
            } catch (Exception ex) {
                resp.setStatus(400); // bad request
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                String message = "Error: " + ex.getMessage();
                jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
                jsr.setApplicationErrorCode(0);
                jsr.setDetail(message + " - counter : "+counter);            
                jsr.setHttpStatus(400);
                json = gson.toJson(jsr, JsonServletResponse.class);
                resp.getWriter().write(json);  
            }

            
        }
        resp.setContentType("application/json; charset=UTF-8");
        req.setAttribute("answers", items);        
        req.getRequestDispatcher("/secured/answers/index.jsp").forward(req, resp); 
    }

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }

    private void postAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";  
        JsonServletResponse jsr = new JsonServletResponse();        
        try
        {          
            String action = request.getParameter("action");                    
            String status = request.getParameter("status");            
            if("Update".equals(action))
            {   
                //get params                
                long answerId = Long.parseLong(request.getParameter("answerId"));
                Answer answer = answerService.find(answerId);
                if(answer == null)
                {
                    throw new EntityNotFoundException("Answer with id <" + answerId + "> could not be retrieved.");
                }              
                // set opposite status to already existing
                if("Suspended".equals(status))
                {
                    answer.setDeleted(false);
                }
                else if("Active".equals(status))
                {
                   answer.setDeleted(true); 
                }
                answer = answerService.updated(answer);
                if(answer != null)
                {
                    AnswerItem item = new AnswerItem(answer);                    
                    json = gson.toJson(item, AnswerItem.class);                                       
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");                    
                    response.getWriter().write(json);
                }
                else
                {
                    response.getWriter().write("ERROR");
                }                
            }
        }         
        catch (Exception ex)
        {  
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setApplicationErrorCode(0);
            jsr.setDetail(message);            
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);            
        }    
    }      
    
}
