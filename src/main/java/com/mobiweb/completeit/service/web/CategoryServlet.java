/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.EntityExistsException;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.common.exception.InvalidArgumentException;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.Culture;
import com.mobiweb.completeit.domain.PhraseCategory;
import com.mobiweb.completeit.service.CategoryService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.json.ErrorUtils.ErrorMessage;
import com.mobiweb.completeit.service.CultureService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.PhraseCategoryService;
import com.mobiweb.completeit.service.web.CategoryItem.CultureOptionItem;
import static com.mobiweb.completeit.service.web.JsonServletResponse.Code.ENTITY_EXISTS;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


/**
 *
 * @author ic
 */
@WebServlet(name = "categories", urlPatterns = {"/web/secured/categories"})
@MultipartConfig
public class CategoryServlet extends HttpServlet
{   
    private String uploadPath;
    private String tomeePath;    
    private File file ;            
    @EJB private CategoryService categoryService;
    @EJB private PhraseCategoryService pcs;
    private ErrorMessage errorMessage;
    private @EJB EntityCounterService ecs;
    private @EJB CultureService cultureService;

    @Override
    public void init() throws ServletException {
        
        //get context's category images path
        this.uploadPath = getServletContext().getInitParameter("Category.Images.Path");        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {        
        try
        {
            boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(isAjaxRequest){              
                getAjax(request, response);
            }
            else
            {              
                getCategories(request, response);
            }            
        } catch (Exception ex) {
            Logger.getLogger(PhraseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {     
        this.delete(req, resp);
    }

    @Override    
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {      
      boolean isAjaxRequest = "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));
      if(isAjaxRequest){
        postAjax(req, resp);        
      }
      else
      {
        post(req, resp);
      }
    } 
    
    private void postAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {   
        long id = 0;
        String categoryName = "";        
        String description = "";
        long cultureId = 0;        
        Gson gson = new Gson();
        String json = "";
        JsonServletResponse jsr = new JsonServletResponse();                
        tomeePath = System.getProperty("catalina.base").concat((String)getServletContext().getInitParameter("Category.Images.Path"));
        //tomeePath = System.getProperty("catalina.base").concat((String)getServletContext().getInitParameter("Images.Categories.Path"));
        File f = new File(tomeePath).getAbsoluteFile();        
        String fullPath = f.getAbsolutePath();                                  
        String fileName = "";
        String action = "";
        if(ServletFileUpload.isMultipartContent(request))
        {
            try
            {               
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);                
                List<FileItem> cache = new ArrayList();                
                for(FileItem item : multiparts)
                {
                    if(!item.isFormField())
                    {
                        String contentType = item.getContentType();                        
                        String type = contentType.split("/")[0];
                        if(!type.equals("image")){
                            throw new InvalidArgumentException("Expected image content type but found: "+type);
                        }
                        // image file is included
                        if(!"".equals(item.getName())){
                            String name = new File(item.getName()).getName();                        
                            File myFile = new File(fullPath + File.separator + name);
                            if(myFile.exists())
                            {
                                name = Utilities.randomFilename(item.getName()).concat(".png");
                            }                            
                            fileName = name;
                            cache.add(item);                            
                        }
                    }
                    else
                    {      
                        //iterate over other multipart data
                        String fieldName = item.getFieldName();
                        if(null != fieldName)
                        switch (fieldName) {
                            case "category":
                                categoryName  = item.getString();
                                //if(categoryService.existsName(categoryName)){
                                //    throw new InvalidArgumentException("Category '"+categoryName +"' exists");
                                //}   
                                break;
                            case "action":
                                action  = item.getString();
                                break;
                            case "id":
                                id = Long.parseLong(item.getString());
                                break;
                            case "updatedName":
                                categoryName = item.getString();                                
                                break;                                 
                            case "description":
                                if(!Strings.isNullOrEmpty(item.getString())){
                                    description = item.getString();
                                }   break;
                            case "culture":
                                if(!Strings.isNullOrEmpty(item.getString())){
                                    cultureId =  Long.parseLong(item.getString());
                                }   break;                                
                            default:
                                break;
                        }
                    }                        
                }
                
                //add or update
                Category category = null;
                if("Add".equals(action))
                {
                    category = new Category();
                    category.setName(categoryName);
                    if(categoryService.existsNameUndeleted(categoryName)){
                        throw new EntityExistsException("Category '"+categoryName +"' exists");
                    }                    
                    category.setImageRelativePath("");
                    category.setDescription("");
                    if (!fileName.equals("")) {
                        category.setImageRelativePath("/".concat(fileName));
                    }
                    Culture culture = cultureService.find(cultureId);
                    if(culture == null)
                    {
                        throw new EntityNotFoundException("Culture with id <" + cultureId + "> could not be retrieved");
                    }                    
                    category.setCulture(culture);
                    categoryService.create(category);
                    category = categoryService.find(category.getId());
                    if(category != null)
                    {
                        //update entity counter table
                        ecs.updateCounter(1, Category.class);
                        if(cache.size() == 1){
                            cache.get(0).write( new File(fullPath + File.separator + fileName));
                        }                         
                    }                                     
                }
                else if("Update".equals(action))
                {
                    category = categoryService.find(id);
                    if(category == null)
                    {
                        throw new EntityNotFoundException("Category with id <" + id + "> could not be retrieved");
                    } 
                    if(categoryService.existsNameExcludeId(categoryName, id))
                    {
                        throw new EntityExistsException("Category '"+categoryName +"' exists");
                    }     
                    Culture culture = cultureService.find(cultureId);
                    if(culture == null)
                    {
                        throw new EntityNotFoundException("Culture with id <" + cultureId + "> could not be retrieved");
                    }                    
                    category.setCulture(culture);
                    category.setName(categoryName);
                    category.setDescription(description);
               
                    if (!fileName.equals("")) {
                        category.setImageRelativePath("/".concat(fileName));
                    }
                    category = categoryService.updated(category);
                    //write new file
                    if (category != null && !fileName.equals("")) {
                        if(cache.size() == 1){
                            cache.get(0).write( new File(fullPath + File.separator + fileName));
                        }                         
                    }
                    
                }                
                CategoryItem item = new CategoryItem(category);
                json = gson.toJson(item, CategoryItem.class);
                response.setStatus(201);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8"); 
                response.getWriter().write(json);             
            }
            catch (Exception ex)
            {                   
                String message = "Category creation failed due to: " + ex.getMessage();
                response.setStatus(400); // bad request
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");                
                jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
                jsr.setApplicationErrorCode(0);
                if(ex instanceof EntityExistsException)
                {
                    jsr.setApplicationErrorCode(ENTITY_EXISTS);
                }
                jsr.setDetail(message);            
                jsr.setHttpStatus(400);
                json = gson.toJson(jsr, JsonServletResponse.class);
                response.getWriter().write(json); 
            }           
        }           
    }    
    
    private void post(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        String categoryName = "";
        Gson gson = new Gson();
        String json = "";       
        if(ServletFileUpload.isMultipartContent(request))
        {
            try
            {
                tomeePath = System.getProperty("catalina.base").concat((String)getServletContext().getInitParameter("Category.Images.Path"));
                File f = new File(tomeePath).getAbsoluteFile();
                String fullPath = f.getAbsolutePath();                                  
                String fileName = "";                
                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                List<FileItem> cache = new ArrayList();                
                for(FileItem item : multiparts)
                {
                    if(!item.isFormField() && "file".equals(item.getFieldName()))
                    {
                        // image file is included
                        if(!"".equals(item.getName())){
                            String name = new File(item.getName()).getName();                        
                            File myFile = new File(fullPath + File.separator + name);
                            if(myFile.exists())
                            {
                                name = Utilities.randomFilename(item.getName()).concat(".png");
                            }
                            //item.write( new File(fullPath + File.separator + name));  
                            fileName = name;
                            cache.add(item);
                        }
                    }
                    else
                    {      
                        String cat = item.getFieldName();
                        if("category".equals(cat))
                        {
                            categoryName  = item.getString();
                            if(categoryService.existsNameUndeleted(categoryName)){
                                throw new EntityExistsException("Category '"+categoryName +"' exists");
                            }
                        }
                    }                        
                }
                Category category = new Category();
                category.setName(categoryName);
                category.setImageRelativePath("");
                if (!fileName.equals("")) {
                    category.setImageRelativePath("/".concat(fileName));
                }
                categoryService.create(category);
                Object loaded = categoryService.find(category.getId());
                if(loaded != null)
                {                    
                   if(cache.size() == 1){
                    cache.get(0).write( new File(fullPath + File.separator + fileName));
                    json = gson.toJson(loaded,Category.class);
                    response.setStatus(201);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8"); 
                    response.getWriter().write(json);
                   }                   
                }
            }
            catch (Exception ex)
            {                
                response.setStatus(400); // bad request
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");     
                String message = "Category creation failed due to: " + ex.getMessage();
                errorMessage = new ErrorUtils(). new ErrorMessage()
                .setDetail(message)
                .setHttpStatusCode(400)
                .setTitle("Exception");                
                json = gson.toJson(errorMessage, ErrorMessage.class);                
                response.getWriter().write(json); //"Category creation failed due to: " + ex.getMessage());
            }
        }        
    }
    
    private void getAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    { 
        Gson gson = new Gson();
        String json = "";         
        JsonServletResponse jsr = new JsonServletResponse(); 
        try
        {          
            String action = request.getParameter("action");        
            String id = request.getParameter("id");        
            if("Fetch".equals(action))
            {
                long categoryId = Long.parseLong(id);
                Category category = categoryService.find(categoryId);
                if (category == null) {
                    throw new EntityNotFoundException("Category with id <" + categoryId + "> could not be retrieved.");
                }
                List<Culture> cultures = cultureService.findAllDistinct();
                HashSet<CultureOptionItem> cultureOptions = new HashSet();
                if(cultures.size() > 0)
                {
                    Map<Long, String> cmap = new LinkedHashMap();
                    cmap = cultures.stream().collect(Collectors.toMap(Culture::getId, Culture::getDisplayName));                       
                    for (Entry entry : cmap.entrySet()) {
                        CultureOptionItem coi = new CategoryItem().new CultureOptionItem((long) entry.getKey(), (String) entry.getValue());
                        cultureOptions.add(coi);
                    }                    
                }
                CategoryItem editItem = new CategoryItem(category, cultureOptions);
                json = gson.toJson(editItem, CategoryItem.class);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
            }

        }
        catch (Exception ex)
        { 
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setApplicationErrorCode(0);
            jsr.setDetail(message);            
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);             
        }        
    }    
    
    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {   
        List<Category> categories = categoryService.findAll();     
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("categories", categories);        
        request.getRequestDispatcher("/secured/categories/index.jsp").forward(request, response);       
    }
    
    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";          
        try
        {          
            long categoryId = Long.parseLong(req.getParameter("id"));
            if (categoryService.hasForeignKeyDependency(categoryId) == true)
            {
                // return error string
                resp.setContentType("text/plain");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write("denied");
            } 
            else 
            {
                // else, update removal indicator in db
                Category c = categoryService.find(categoryId);                
                List<PhraseCategory> list = pcs.findAllByCategoryId(categoryId);
                if(list.size() > 0){
                    list.forEach((PhraseCategory pc)->{pc.setActive(false); pcs.update(pc);});
                }                
                c.setDeleted(true);                
                categoryService.update(c);      
                //update entity counter table
                ecs.updateCounter(-1, Category.class);                 
                resp.setContentType("text/plain");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write("deleted");
            }             
        }
        catch (NumberFormatException | IOException ex)
        {                
            resp.setStatus(400); // bad request
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            String message = "Category creation failed due to: " + ex.getMessage();
            errorMessage = new ErrorUtils().new ErrorMessage()
                .setDetail(message)
                .setHttpStatusCode(400)
                .setTitle("Exception");
            JsonError je = new JsonError();
            je.setId(1);
            je.setTitle("Exception");
            je.setDetail(message);
            je.setApplicationErrorCode(0);
            je.setHttpErrorCode(400);
            json = gson.toJson(je, JsonError.class);
            resp.getWriter().write(json);
        }            
      
    }    
    
    private void getCategories(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        List<Category> categories = categoryService.findAll();     
        List<Culture> cultures = cultureService.findAllDistinct();        
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("categories", categories);        
        request.setAttribute("cultures", cultures);        
        request.getRequestDispatcher("/secured/categories/index.jsp").forward(request, response);           
    }
}
