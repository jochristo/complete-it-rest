/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.service.ProfileService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ic
 */
@WebServlet(name = "profiles", urlPatterns = {"/web/secured/profiles"})
public class ProfileServlet extends HttpServlet
{
    @EJB private ProfileService profileService;
    private ErrorUtils.ErrorMessage errorMessage;    

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.process(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            boolean isAjaxRequest = "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));
            if(isAjaxRequest){              
                postAjax(req, resp);
            }                        
        } catch (Exception ex) {
            Logger.getLogger(PhraseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.process(req, resp);
    }
            
    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {        
        String method = request.getMethod();
        if(method.equals("GET"))
        {
            //fetch profiles
            List<Profile> list = profileService.findAllInclDeleted();
            List<ProfileItem> profiles = new ArrayList();
            for(Profile p : list)
            {
                ProfileItem item = new ProfileItem(p);
                profiles.add(item);
            }             
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8"); 
            request.setAttribute("profiles", profiles);        
            request.getRequestDispatcher("/secured/profiles/index.jsp").forward(request, response);       
        }
        else if(method.equals("DELETE"))
        {
            //delete ...
            if(profileService.hasForeignKey(1))
            {
                
            }
        }
    }
    
    private void postAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {        
        Gson gson = new Gson();
        String json = "";  
        JsonServletResponse jsr = new JsonServletResponse();        
        try
        {          
            String action = request.getParameter("action");                    
            String status = request.getParameter("status");            
            if("Update".equals(action))
            {   
                //get params                
                long profileId = Long.parseLong(request.getParameter("profileId"));
                Profile profile = profileService.find(profileId);
                if(profile == null)
                {
                    throw new EntityNotFoundException("Profile with id <" + profileId + "> could not be retrieved.");
                }              
                // set opposite status to already existing
                if("Suspended".equals(status))
                {
                    profile.setDeleted(false);
                }
                else if("Active".equals(status))
                {
                   profile.setDeleted(true); 
                }
                profile = profileService.updated(profile);
                if(profile != null)
                {
                    ProfileItem item = new ProfileItem(profile);                    
                    json = gson.toJson(item, ProfileItem.class);                                       
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");                    
                    response.getWriter().write(json);
                }
                else
                {
                    response.getWriter().write("ERROR");
                }                
            }
        }         
        catch (Exception ex)
        {  
            response.setStatus(400); // bad request
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String message = "Error: " + ex.getMessage();
            jsr.setTitle(JsonServletResponse.Result.ERROR_RESPONSE);
            jsr.setApplicationErrorCode(0);
            jsr.setDetail(message);            
            jsr.setHttpStatus(400);
            json = gson.toJson(jsr, JsonServletResponse.class);
            response.getWriter().write(json);            
        }    
    }       
}
