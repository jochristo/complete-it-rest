/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

/**
 *
 * @author ic
 */
public class JsonServletResponse {
    
    private long id;
    private String title;
    private String detail;
    private int applicationErrorCode;
    private int httpStatus;

    public JsonServletResponse() {
    }    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = 1;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getApplicationErrorCode() {
        return applicationErrorCode;
    }

    public void setApplicationErrorCode(int applicationErrorCode) {
        this.applicationErrorCode = applicationErrorCode;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }
    
    
    public class Result
    {
        public static final String SUCCESS_RESPONSE = "Success";
        public static final String ERROR_RESPONSE = "Error";
        public static final String CREATED_RESPONSE = "Created";
        public static final String UPDATED_RESPONSE = "Updated";        
        public static final String DELETED_RESPONSE = "Deleted";        
        public static final String PHRASE_TEXT_CHANGED_FAILED = "Answer exists";        
    }
    
    public class Code
    {
        public static final int PHRASE_TEXT_INVALID = 100;
        public static final int FOREIGN_KEY_DEPENDENCY = 200;
        public static final int ENTITY_EXISTS = 300;
    }
    
}
