/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ic
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req,resp);
    }
    
    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {        
        String action = request.getParameter("action");
        if(action != null)
        {
            if(action.equals("login"))
            {
                //check credentials agains tomcat realm
                
                request.getRequestDispatcher("/web/secured/phrases").forward(request, response);                
            }
        }
        else
        {
            response.sendRedirect("/complete-it/login.jsp");
        }

    }
    
}
