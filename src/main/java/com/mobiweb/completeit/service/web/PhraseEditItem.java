/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.mobiweb.completeit.domain.Phrase;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ic
 */
public class PhraseEditItem extends PhraseItem
{
    private List<CategoryOptionItem> categoryItems = new ArrayList();

    public PhraseEditItem() {
    }

    public PhraseEditItem(Phrase phrase, List<CategoryOptionItem> categoryItems) {        
        super(phrase);
        this.categoryItems = categoryItems;
    }

    public List<CategoryOptionItem> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(List<CategoryOptionItem> categoryItems) {
        this.categoryItems = categoryItems;
    }             
    
    public class CategoryOptionItem
    {
        private long id;
        private String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        
        public CategoryOptionItem() {
        }

        public CategoryOptionItem(long id, String name) {
            this.id = id;
            this.name = name;
        }
        
        
        
    }
    
}
