/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service.web;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.CategoryProjection;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.service.AnswerService;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.PhraseCategoryService;
import com.mobiweb.completeit.service.PhraseService;
import com.mobiweb.completeit.service.ProfileService;
import com.mobiweb.completeit.service.web.DashboardJson.CategoryAnswerItem;
import com.mobiweb.completeit.service.web.DashboardJson.CategoryPhraseItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ic
 */
@WebServlet(name = "dashboard", urlPatterns = {"/web/secured/dashboard"})
public class DashboardServlet extends HttpServlet
{
    
    @EJB private CategoryService categoryService;
    @EJB private ProfileService profileService;
    @EJB private PhraseCategoryService pcs;    
    @EJB private AnswerService answerService;
    @EJB private PhraseService phraseService;    
    private ErrorUtils.ErrorMessage errorMessage;
    private @EJB EntityCounterService ecs;

    @Override
    public void init() throws ServletException 
    {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.get(req, resp);
    }

    @Override    
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {      
      boolean isAjaxRequest = "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));
      if(isAjaxRequest){
        postAjax(req, resp);        
      }

    }     
    
    private void get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {   
      boolean isAjaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
      if(isAjaxRequest){
        postAjax(request, response);        
      }            
        /*
        long profiles = ecs.findByEntityType("Profile").getCounter();
        long categories = ecs.findByEntityType("Category").getCounter();
        long phrases = ecs.findByEntityType("Phrase").getCounter();
        long answers = ecs.findByEntityType("Answer").getCounter();
        */        
        long profiles = profileService.count();
        long categories = categoryService.count();
        long phrases = phraseService.count();
        long answers =answerService.count();                 
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("profiles", profiles);        
        request.setAttribute("categories", categories);        
        request.setAttribute("phrases", phrases);        
        request.setAttribute("answers", answers);        
        request.getRequestDispatcher("/secured/dashboard.jsp").forward(request, response);
        
    }    
    
    private void postAjax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {     
        Gson gson = new Gson();
        DashboardJson dj = new DashboardJson();
        /*
        long profiles = ecs.findByEntityType("Profile").getCounter();
        long categories = ecs.findByEntityType("Category").getCounter();
        long phrases = ecs.findByEntityType("Phrase").getCounter();
        long answers = ecs.findByEntityType("Answer").getCounter();
        */
        long profiles = profileService.count();
        long categories = categoryService.count();
        long phrases = phraseService.count();
        long answers =answerService.count();         

        // set totals
        dj.setAnswered(ecs.AnsweredPhrases());
        dj.setInvites(ecs.Invites());
        dj.setLiked(ecs.Likes());
        dj.setReported(ecs.Reports());
        dj.setShares(ecs.Shares());
        
        // set domain entities counters
        dj.setAnswers(answers);
        dj.setProfiles(profiles);
        dj.setPhrases(phrases);
        dj.setCategories(categories);
        
        // get charts data
        long totalAnswered = answerService.getCountDistinctAnsweredPhrases();
        long totalUnanswered = answerService.countUnansweredPhrases();        
        long totalReported = answerService.countReported();
        long totalUnreported = answerService.countUnreported();
        long totalAnswers = totalReported + totalUnreported;
        
        double reported = totalAnswers >0 ? (double)(((double)totalReported / (double)totalAnswers)*100) : 0;
        reported = Utilities.roundHalfUp((double)reported, 2);        
        double unreported = totalAnswers >0 ? (double)(((double)totalUnreported / (double)totalAnswers)*100) : 0;
        unreported = Utilities.roundHalfUp((double)unreported, 2);        
        double answered = phrases >0 ? (double)(((double)totalAnswered / (double)phrases)*100) : 0;
        answered = Utilities.roundHalfUp((double)answered, 2);        
        double unanswered = phrases >0 ? (double)(((double)totalUnanswered / (double)phrases)*100) : 0;        
        unanswered = Utilities.roundHalfUp((double)unanswered, 2);
        
        // set json charts data
        DashboardJson.ReportedAnswers reportedAnswers = new DashboardJson().new ReportedAnswers(reported,unreported);        
        DashboardJson.PhrasesAnswered phrasesAnswered = new DashboardJson().new PhrasesAnswered(answered,unanswered);
        dj.setReportedAnswers(reportedAnswers);
        dj.setPhrasesAnswered(phrasesAnswered);
        
        List<CategoryProjection> categoryProjection = pcs.getCountGroupByCategory();
        List<DashboardJson.CategoryPhraseItem> phraseItems = new ArrayList();        
        long totalPhrases = phraseService.count();
        if(totalPhrases > 0){
        for(CategoryProjection pj : categoryProjection)
        {        
            double percentage = (double)(((double)pj.getCount() / (double)totalPhrases)*100);
            percentage = Utilities.roundHalfUp((double)percentage, 2);            
            DashboardJson.CategoryPhraseItem categoryPhrases = 
                    new DashboardJson().new CategoryPhraseItem(pj.getCategoryId(), pj.getCategoryName(), percentage);
            phraseItems.add(categoryPhrases);            
        }
        }
        if(phraseItems.size() > 0)
        {
            CategoryPhraseItem itemMax = Collections.max(phraseItems);
            int index = phraseItems.indexOf(itemMax);
            phraseItems.get(index).setHighest(true);        
        }
        dj.setCategoryPhrases(phraseItems);
        
        // answers in categories
        List<CategoryProjection> groupedUnreportedAnswers = answerService.countUnReportedGroupByCategory();
        List<CategoryProjection> groupedReportedAnswers = answerService.countReportedGroupByCategory();
        List<DashboardJson.CategoryAnswerItem> answerItems = new ArrayList();
        
        Map<Long,Long> reportedMap = new LinkedHashMap();
        Map<Long,Long> unreportedMap = new LinkedHashMap();
        for(CategoryProjection cp : groupedReportedAnswers)
        {
            if(!reportedMap.containsKey(cp.getCategoryId()))
            {
                reportedMap.put(cp.getCategoryId(), cp.getCount());
            }
        }
        for(CategoryProjection cp : groupedUnreportedAnswers)
        {
            if(!unreportedMap.containsKey(cp.getCategoryId()))
            {
                unreportedMap.put(cp.getCategoryId(), cp.getCount());
            }
        }        
        
        List<Category> cats = categoryService.findAllDistinct();
        for(Category c : cats)
        {
            long rValue = reportedMap.containsKey(c.getId()) ? reportedMap.get(c.getId()) : 0;
            long unrValue = unreportedMap.containsKey(c.getId()) ? unreportedMap.get(c.getId()) : 0;
            DashboardJson.CategoryAnswerItem item = 
            new DashboardJson().new CategoryAnswerItem(c.getName(), rValue, unrValue);
            answerItems.add(item);            
        }
        //Collections.sort(answerItems);               
        //Collections.sort(answerItems, Collections.reverseOrder());
        dj.setCategoryAnswers(answerItems);
                
        // phrases (Answered-unanswered) in categories
        List<CategoryProjection> groupedUnAnswered = phraseService.countUnansweredGroupByCategory();
        List<CategoryProjection> groupedAnswered = phraseService.countAnsweredGroupByCategory();
        List<DashboardJson.CategoryPhraseStatsItem> phraseStatsItems = new ArrayList();
        
        Map<Long,Long> answeredMap = new LinkedHashMap();
        Map<Long,Long> unansweredMap = new LinkedHashMap();
        for(CategoryProjection cp : groupedAnswered)
        {
            if(!answeredMap.containsKey(cp.getCategoryId()))
            {
                answeredMap.put(cp.getCategoryId(), cp.getCount());
            }
        }
        for(CategoryProjection cp : groupedUnAnswered)
        {
            if(!unansweredMap.containsKey(cp.getCategoryId()))
            {
                unansweredMap.put(cp.getCategoryId(), cp.getCount());
            }
        }        
                
        for(Category c : cats)
        {
            long aValue = answeredMap.containsKey(c.getId()) ? answeredMap.get(c.getId()) : 0;
            long unValue = unansweredMap.containsKey(c.getId()) ? unansweredMap.get(c.getId()) : 0;
            DashboardJson.CategoryPhraseStatsItem item = 
            new DashboardJson().new CategoryPhraseStatsItem(c.getName(), aValue, unValue);
            phraseStatsItems.add(item);            
        }
        //Collections.sort(phraseStatsItems);               
        //Collections.sort(phraseStatsItems, Collections.reverseOrder());
        dj.setCategoryPhraseStats(phraseStatsItems);    
        
        //find max yAxis values for multiseries charts data:
        CategoryProjection max = null;
        if(groupedReportedAnswers.size() > 0)
        {
            max = Collections.max(groupedReportedAnswers);
            long maxReported = max.getCount();
            dj.setMaxReported(maxReported);
        }
        if(groupedUnreportedAnswers.size() > 0){
            max = Collections.max(groupedUnreportedAnswers);
            long maxUnreported = max.getCount(); 
            dj.setMaxUnreported(maxUnreported);
        }
        if(groupedAnswered.size() > 0){
            max = groupedAnswered.size() > 0 ? Collections.max(groupedAnswered) : null;
            long maxAnswered =  max != null ? max.getCount() : 0;
            dj.setMaxAnswered(maxAnswered);
        }
        if(groupedUnAnswered.size() > 0){
            max = groupedUnAnswered.size() > 0 ? Collections.max(groupedUnAnswered) : null;
            long maxUnanswered = max != null ? max.getCount() : 0;
            dj.setMaxUnanswered(maxUnanswered);
        }
        
        String json = gson.toJson(dj, DashboardJson.class);        
        response.setStatus(200);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8"); 
        response.getWriter().write(json);     
    }
   
}
