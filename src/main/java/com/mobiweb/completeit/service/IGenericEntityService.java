/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import java.io.Serializable;
import java.util.List;

/**
 * Contains common methods used by multiple persistence class data-access-layer services.
 * @author ic
 * @param <T>
 */
public interface IGenericEntityService<T extends Serializable> {
    
    public T find(Object id);   
    public T findByParameterSingle(Object param);
    public List<T> findByParameterMany(Object param);
    public List<T> findAll();
    public List<T> findRange(int[] range);
    public List<T> findAllCb();
    public int count();
    public <N extends String> T findSingleByParam(N namedQuery, N paramName, Object paramValue);
    public <N extends String> List<T> findManyByParam(N namedQuery, N paramName, Object paramValue);
    public <N extends String> T findSingleByTwoParams(N namedQuery, List<N> paramNames, List<Object> paramValues);  
    public T created(T entity);
    public T updated(T entity); 
    public void remove(T entity);
    public void create(T entity);
    public void update(T entity);
    public boolean exists(Object id);
}
