/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.AnswerProfileLike;
import com.mobiweb.completeit.domain.Phrase;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@LocalBean
@Stateless
public class AnswerLikeService extends BaseEntityService<AnswerProfileLike>
{    
    public AnswerProfileLike find(long answerId, long profileId) {
        em.flush();
        Query query =  em.createNamedQuery("AnswerProfileLike.findByPK",AnswerProfileLike.class)
            .setParameter("answerId", answerId)
            .setParameter("profileId", profileId);
        List<AnswerProfileLike> list = query.getResultList();
        return list.size() == 1 ? list.get(0) : null;        
    }
    
    public List<AnswerProfileLike> find(long answerId) {
        em.flush();
        return em.createNamedQuery("AnswerProfileLike.findByAnswerId",AnswerProfileLike.class)
            .setParameter("answerId", answerId)            
            .getResultList();
    }    
    
    //@Override
    public boolean exists(Object id) {
        Phrase p = em.find(Phrase.class, id);
        return p != null;
    }

    @Override
    public AnswerProfileLike find(Object id) {
        em.flush();
        return em.find(AnswerProfileLike.class, id);
    }
    
    public boolean existsLike(long answerId) {
        return em.createNamedQuery("AnswerProfileLike.findByAnswerId",AnswerProfileLike.class)
            .setParameter("answerId", answerId)            
            .getResultList().size()>0;
    }    
    
    public boolean existsLike(long answerId, long profileId) {
        return em.createNamedQuery("AnswerProfileLike.findByProfileIdAndAnswerId",AnswerProfileLike.class)
            .setParameter("answerId", answerId)
            .setParameter("profileId", profileId)
            .getResultList().size()>0;
    }     

    @Override
    public List<AnswerProfileLike> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AnswerProfileLike findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AnswerProfileLike> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
    
    public long getCount()
    {
        Query query = em.createNamedQuery("AnswerProfileLike.count", AnswerProfileLike.class);                
        return (long)query.getSingleResult();
    }     

}
