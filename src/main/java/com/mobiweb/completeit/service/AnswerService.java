/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.common.Utilities;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.Query;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.CategoryProjection;
import com.mobiweb.completeit.domain.Location;
import com.mobiweb.completeit.domain.Phrase;
import java.util.Collection;
import java.util.LinkedList;
import javax.ejb.Stateless;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class AnswerService extends BaseEntityService<Answer>        
{
    
    @Override    
    public List<Answer> findAll() {
        em.getEntityManagerFactory().getCache().evict(Answer.class);
        Query query = em.createNamedQuery("Answer.findAll", Answer.class);
	List<Answer> list = new ArrayList();
	list = query.getResultList();
	return list;
    }
    
    public List findAllInclDeleted() {
        em.getEntityManagerFactory().getCache().evict(Answer.class);
        List<Answer> query = em.createNamedQuery("Answer.findAllInclDeleted", Answer.class).getResultList();
        return query;        
    }     
    
    @Override
    public <N extends String> Answer findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        List<Answer> query = em
                .createNamedQuery(namedQuery, Answer.class)
                .setParameter(paramName, paramValue)
                .getResultList();
       if(query != null){
        if(query.size() == 1){
            return query.get(0);
        }
       }
       return null;
    }
    
    public boolean existsAnswerProfile(String answers, long profileId)
    {
        List<Phrase> query = em
                .createNamedQuery("Phrase.findByAnswerAndProfileId", Phrase.class)
                .setParameter("answers", answers)
                .setParameter("profileId", profileId)
                .getResultList();
        return query.size() > 0;
    }    
   
    public boolean existsAnswer(long phraseId, long profileId)
    {        
        List<Answer> query = em
                .createNamedQuery("Answer.findByPhraseIdAndProfileId", Answer.class)
                .setParameter("phraseId", phraseId)
                .setParameter("profileId", profileId)
                .getResultList();
        return query.size() > 0;
    }  
    
    public boolean existsAnswer(String answers, long phraseId, long profileId)
    {        
        List<Answer> query = em
                .createNamedQuery("Answer.findByAnswersAndPhraseIdAndProfileId", Answer.class)
                .setParameter("answers", answers)
                .setParameter("phraseId", phraseId)
                .setParameter("profileId", profileId)
                .getResultList();
        return query.size() > 0;
    }     
    
    public boolean existsAnswerByPhraseId(long phraseId)
    {
        
        List<Answer> query = em
                .createNamedQuery("Answer.findByPhraseId", Answer.class)
                .setParameter("phraseId", phraseId)                
                .getResultList();
        return query.size() > 0;
    }     
    
    @Deprecated
    public List<Answer> getfilteredResults(String gender, List<Integer> ages, List<Integer> locations, Location profileLocation)
    {
       List<Answer> phrases = new LinkedList();
       List<Answer> filtered = new ArrayList();
       int minAge = -1;
       int maxAge = -1;
       int minLocation = -1;
       int maxLocation = -1;
       if(!Utilities.isEmpty(ages))
       {
           if(ages.size() == 2)
           {
               minAge = ages.get(0);
               maxAge = ages.get(1);
           }
       }
       if(!Utilities.isEmpty(locations))
       {
           if(locations.size() == 2)
           {
               minLocation = locations.get(0);
               maxLocation = locations.get(1);
           }
       }
       boolean isGenderFiltered = gender != null; //!Utilities.isEmpty(gender);       
       boolean isDistanceFiltered = minLocation >= 0 && (maxLocation >=minLocation && maxLocation >= 0);
       boolean isAgeFiltered = minAge > 0 && (maxAge >=minAge && maxAge > 0);       
       if(isGenderFiltered)
       {
           boolean isWhitespace = gender.matches("^\\s*$");
           if(!isWhitespace){
            phrases = em.createNamedQuery("Answer.findByGender", Answer.class).setParameter("gender", gender).getResultList();
           }
           else{
               phrases = em.createNamedQuery("Answer.findAll", Answer.class).getResultList();
           }
       }
       else{
           phrases = em.createNamedQuery("Answer.findAll", Answer.class).getResultList();
       }
           if(phrases.size() > 0)
           {              
              filtered = new ArrayList(phrases);
              AnswerFilterPredicate predicate = null;
              if(isDistanceFiltered && isAgeFiltered)
              {
                  predicate = new AnswerFilterPredicate();
                  predicate.minLocation = minLocation;
                  predicate.maxLocation = maxLocation;
                  predicate.source = profileLocation;
                  predicate.isDistanceMode = true;
                  predicate.minAge = minAge;
                  predicate.maxAge = maxAge;
                  predicate.isAgeMode = true;                  
                  filtered.removeIf(predicate);
              }              
              else if(isDistanceFiltered && !isAgeFiltered)
              {   
                  predicate = new AnswerFilterPredicate();
                  predicate.minLocation = minLocation;
                  predicate.maxLocation = maxLocation;
                  predicate.source = profileLocation;
                  predicate.isDistanceMode = true;                  
                  filtered.removeIf(predicate);                  
              }
              else if(isAgeFiltered && !isDistanceFiltered)
              {
                  predicate = new AnswerFilterPredicate();
                  predicate.minAge = minAge;
                  predicate.maxAge = maxAge;
                  predicate.isAgeMode = true;                  
                  filtered.removeIf(predicate);
              }
              return filtered;
           }       
       return phrases;
    }
    
    public List<Answer> getAnswersByCategoryExcludeProfile(long categoryId, long profileId)
    {
        List<Answer> answers = new LinkedList();
        answers = em.createNamedQuery("Answer.findByCategoryIdExcludeProfileId", Answer.class)
                .setParameter("profileId", profileId)
                //.setParameter("categoryId", categoryId)
                .getResultList();
        List<Answer> finals = new ArrayList();
        for(Answer a : answers)
        {
            Collection<Category> categories = a.getPhrase().getCategories();
            for(Category c : categories)
            {
                if(c.getId() == categoryId)
                {
                    finals.add(a);
                }
            }
        }
        return finals;
    }
    
    public List<Answer> getAnswersByProfile(long categoryId, long profileId)
    {
        List<Answer> answers = new LinkedList();
        answers = em.createNamedQuery("Answer.findByProfileId", Answer.class)
        //answers = em.createNamedQuery("Answer.findByProfileIdFetchJoinLikes", Answer.class)
                .setParameter("profileId", profileId)                
                .getResultList();
        return answers;
    }    
    
    public List<Answer> getAnswersByProfilePaginated(long categoryId, long profileId, int page, int size)
    {
        List<Answer> answers = new LinkedList();
        answers = em.createNamedQuery("Answer.findByProfileId", Answer.class)
        //answers = em.createNamedQuery("Answer.findByProfileIdFetchJoinLikes", Answer.class)                
                .setFirstResult((page-1)*size)
                .setMaxResults(size)                 
                .setParameter("profileId", profileId)                
                .getResultList();
        return answers;
    }   
    
    public long getCountAnswersByProfile(long profileId)
    {        
        Query query = em.createNamedQuery("Answer.countByProfileId", Answer.class)
                .setParameter("profileId", profileId);                                
        return (long)query.getSingleResult();
    }    
    
    public List<Answer> getAnswersByProfileIdAndAnswerId(long answerId, long profileId)
    {
        List<Answer> answers = new LinkedList();
        answers = em.createNamedQuery("Answer.findByAnswerIdAndProfileId", Answer.class)
                .setParameter("profileId", profileId) 
                .setParameter("answerId", answerId) 
                .getResultList();
        return answers;
    }    

    public Answer getByPhraseIdAndProfileId(long phraseId, long profileId)
    {
        Answer answer = em.createNamedQuery("Answer.findByPhraseIdAndProfileId", Answer.class)
                .setParameter("profileId", profileId) 
                .setParameter("phraseId", phraseId) 
                .getSingleResult();
        return answer;
    }
        
    public List<Answer> getGroupedFilteredResults(String gender, List<Integer> ages, List<Integer> locations, Location profileLocation)
    {
       List<Answer> phrases = new LinkedList();
       List<Answer> filtered = new ArrayList();
       int minAge = -1;
       int maxAge = -1;
       int minLocation = -1;
       int maxLocation = -1;
       if(!Utilities.isEmpty(ages))
       {
           if(ages.size() == 2)
           {
               minAge = ages.get(0);
               maxAge = ages.get(1);
           }
       }
       if(!Utilities.isEmpty(locations))
       {
           if(locations.size() == 2)
           {
               minLocation = locations.get(0);
               maxLocation = locations.get(1);
           }
       }
       boolean isGenderFiltered = gender != null; //!Utilities.isEmpty(gender);       
       boolean isDistanceFiltered = minLocation >= 0 && (maxLocation >=minLocation && maxLocation >= 0);
       boolean isAgeFiltered = minAge > 0 && (maxAge >=minAge && maxAge > 0);       
       if(isGenderFiltered)
       {
           boolean isWhitespace = gender.matches("^\\s*$");
           if(!isWhitespace){
            phrases = em.createNamedQuery("Answer.findByGenderGroupByPhraseAndProfile", Answer.class).setParameter("gender", gender).getResultList();
           }
           else{
               phrases = em.createNamedQuery("Answer.findAllGroupByPhraseAndProfile", Answer.class).getResultList();
           }
       }
       else{
           phrases = em.createNamedQuery("Answer.findAllGroupByPhraseAndProfile", Answer.class).getResultList();
       }
           if(phrases.size() > 0)
           {              
              filtered = new ArrayList(phrases);
              AnswerFilterPredicate predicate = null;
              if(isDistanceFiltered && isAgeFiltered)
              {
                  predicate = new AnswerFilterPredicate();
                  predicate.minLocation = minLocation;
                  predicate.maxLocation = maxLocation;
                  predicate.source = profileLocation;
                  predicate.isDistanceMode = true;
                  predicate.minAge = minAge;
                  predicate.maxAge = maxAge;
                  predicate.isAgeMode = true;                  
                  filtered.removeIf(predicate);
              }              
              else if(isDistanceFiltered && !isAgeFiltered)
              {   
                  predicate = new AnswerFilterPredicate();
                  predicate.minLocation = minLocation;
                  predicate.maxLocation = maxLocation;
                  predicate.source = profileLocation;
                  predicate.isDistanceMode = true;                  
                  filtered.removeIf(predicate);                  
              }
              else if(isAgeFiltered && !isDistanceFiltered)
              {
                  predicate = new AnswerFilterPredicate();
                  predicate.minAge = minAge;
                  predicate.maxAge = maxAge;
                  predicate.isAgeMode = true;                  
                  filtered.removeIf(predicate);
              }
              return filtered;
           }       
       return phrases;
    } 
    
    public List<Answer> findAllGroupByPhraseAndProfile()
    {
        
            Query query = em.createNamedQuery("Answer.findAllGroupByPhraseAndProfile", Answer.class);
            List<Answer> list = new ArrayList();
            list = query.getResultList();
            return list;
    }
        
    public List<Answer> getAnswersByPhraseExcludeReported(long id)
    {
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseIdExcludeReported", Answer.class)
                .setParameter("phraseId", id)                
                .getResultList();        
        return answers;
    }  
    
    public long getCountAnswersByPhrase(long id)
    {
        Query query = em.createNamedQuery("Answer.countByPhraseId", Answer.class)
                .setParameter("phraseId", id);                                        
        return (long)query.getSingleResult();
    } 
    
    public long getCountAnswersByPhraseExcludeReported(long id)
    {
        Query query = em.createNamedQuery("Answer.countByPhraseIdExcludeReported", Answer.class)
                .setParameter("phraseId", id);                                        
        return (long)query.getSingleResult();
    }   
    
    public long getCountByPhraseExcludeReportingProfileAndManyReports(long phraseId, long profileId)
    {
        Query query = em.createNamedQuery("Answer.countByPhraseIdExcludeReportedAndReportingProfile", Answer.class)
                .setParameter("phraseId", phraseId)
                .setParameter("profileId", profileId);                                        
        return (long)query.getSingleResult();
    }    
    
    public List<Answer> getAnswersByPhrasePaginated(long id,int page, int size)
    {        
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseIdExcludeReported", Answer.class)        
            .setFirstResult((page-1)*size)
            .setMaxResults(size)                
            .setParameter("phraseId", id)        
            .getResultList();                
        return answers;
        
    }   
    
    public List<Answer> getAnswersByPhrasePaginatedExcludeReported(long phraseId, long profileId, int page, int size)
    {        
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseIdExcludeReportedAndReportingProfile", Answer.class)        
            .setFirstResult((page-1)*size)
            .setMaxResults(size)                
            .setParameter("phraseId", phraseId)
            .setParameter("profileId", profileId)
            .getResultList();                
        return answers;
        
    }    
    
    public List<Answer> getAnswersByPhraseIdExcludeReported(long phraseId, long profileId)
    {        
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseIdExcludeReportedAndReportingProfile", Answer.class)           
            .setParameter("phraseId", phraseId)
            .setParameter("profileId", profileId)
            .getResultList();        
        return answers;
        
    }     
    
    public List<Answer> findAllPaginated(int page, int size)
    {        
        Query query = em.createNamedQuery("Answer.findAll", Answer.class);
        query.setFirstResult((page-1)*size);
        query.setMaxResults(size);
	List<Answer> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    

    @Override
    public Answer findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Answer> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns count of Answer entities with DELETED flag set to false.
     * @return 
     */
    @Override
    public int count()
    {
        return (em.createNamedQuery("Answer.countUndeleted", Long.class).getSingleResult()).intValue();
    }
    
    /**
     * Returns count of answered distinct phrases (undeleted answers only)
     * @return 
     */
        public long getCountDistinctAnsweredPhrases()
    {
        Query query = em.createNamedQuery("Answer.countDistinctPhrases", Answer.class);                
        return (long)query.getSingleResult();
    } 

    public long countReported()
    {
        Query query = em.createNamedQuery("Answer.countReported", Answer.class);                
        return (long)query.getSingleResult();
    } 

    public long countUnreported()
    {
        Query query = em.createNamedQuery("Answer.countUnreported", Answer.class);                
        return (long)query.getSingleResult();
    }    

    public long countUnansweredPhrases()
    {
        Query query = em.createNamedQuery("Phrase.countUndeleted", Phrase.class);
        long total = (long)query.getSingleResult();
        return total <= 0 ? 0 : total - this.getCountDistinctAnsweredPhrases();
    }  
    
    public List<CategoryProjection> countUnReportedGroupByCategory()
    {
        Query query = em.createNamedQuery("Answer.countUnreportedGroupByCategory", CategoryProjection.class);                
        return query.getResultList();
    }     
    
    public List<CategoryProjection> countReportedGroupByCategory()
    {
        Query query = em.createNamedQuery("Answer.countReportedGroupByCategory", CategoryProjection.class);                
        return query.getResultList();
    }
    
    
    
}
