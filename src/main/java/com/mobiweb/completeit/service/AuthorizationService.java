/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.google.gson.Gson;
import com.mobiweb.completeit.common.MemCached;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.oauth.TokenInfo;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.mobiweb.completeit.json.ErrorUtils;
import net.spy.memcached.MemcachedClient;

/**
 * Grants authorization and returns response.
 * Provides grant and persistence-related validation and verification.
 Provides access to _authorized Profile requesting a resource or other service via get property method.
 * @author ic
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AuthorizationService {
       
    // AUTHORIZATION RESULT
    private boolean _authorized = false;
    // ERROR MESSAGE
    private final ErrorUtils.ErrorMessage _errorMessage;
    // CACHE CLIENT
    private final MemcachedClient cache;
    // PROFILE SERVICE
    private @EJB ProfileService profileService;
    // AUTHORIZED PROFILE
    private Profile _profile = null;

    public AuthorizationService() throws IOException {
        cache = MemCached.getClient();
        _errorMessage = new ErrorUtils().new ErrorMessage();
    }

    public boolean isAuthorized() {
        return _authorized;
    }

    public void setAuthorized(boolean _isAuthorized) {
        this._authorized = _isAuthorized;
    }    
        
    public Response authorized(String authorization)
    {
        if(authorization == null)
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Detail.AUTHORIZATION_HEADER_MISSING)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            this._authorized = false;
            return Response.status(Response.Status.BAD_REQUEST).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        
        boolean isauthorizationHeaderValid = authorization.matches("^Bearer \\w+$");
        if(!isauthorizationHeaderValid)
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Detail.INVALID_AUTHORIZATION_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            this._authorized = false;            
            return Response.status(Response.Status.BAD_REQUEST).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        String parsed[] = authorization.split("Bearer ");
        authorization = parsed[1];
                
        // MEMCACHED LOOKUP
        Gson gson = new Gson();                  
        Object value = null;
        try
        {
            value = cache.get(authorization);
        }
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.BAD_REQUEST).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }            
        // TOKEN NOT FOUND
        if(value==null)
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.UNAUTHORIZED).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        
        // INVALID TOKEN
	TokenInfo tokenInfo = gson.fromJson(value.toString(), TokenInfo.class); 
        value = tokenInfo.getAccess_token();      
        if(value == null || !value.equals(authorization))
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.UNAUTHORIZED).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }  
        // MISMATCHED PROFILE AND TOKEN PAIR        
        String tokenInfoProfileId = tokenInfo.getId();
        if(Utilities.isEmpty(tokenInfoProfileId))
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.UNAUTHORIZED).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                 
        }
        
        // COPY PROFILE TO MEMCACHED TO IMPROVE PERFORMANCE
        String cachedProfile = String.valueOf(Long.parseLong(tokenInfoProfileId));
        //Profile copy key : profileID+email  
        String profileCopyKey = cachedProfile.concat(tokenInfo.getEmail());
        Profile profile = null;
        try
        {
            if(cache.get(profileCopyKey) == null)
            {                              
                cache.set(profileCopyKey, 86400, profileService.find(Long.parseLong(tokenInfoProfileId)));
                //cache.set(cachedProfile, 86400, profileService.find(Long.parseLong(tokenInfoProfileId)));
                profile = profileService.find(Long.parseLong(tokenInfoProfileId));
            }
            else
            {                
                profile = (Profile)cache.get(profileCopyKey);
            }
        }   
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            _errorMessage
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.BAD_REQUEST).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }       
        
        if(profile == null)
        {           
            _errorMessage
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested profile id was not found : "+tokenInfoProfileId)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            this._authorized = false;
            return Response.status(Response.Status.NOT_FOUND).entity(_errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }        
        this._authorized = true;   
        setProfile(profile);
        return null;
    }

    public Profile getProfile() {
        return _profile;
    }

    private void setProfile(Profile _profile) {
        this._profile = _profile;
    }
}
