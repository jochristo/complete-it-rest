/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.Culture;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class CultureService extends BaseEntityService<Culture> 
{

    public List<Culture> findAllDistinct() {
        Query query = em.createNamedQuery("Culture.findAllDistinct", Culture.class);
	List<Culture> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    public Culture findByIso(String iso)
    {
        Query query = em.createNamedQuery("Culture.findByIso", Culture.class)
                .setParameter("iso", iso);
	List<Culture> list = new ArrayList();
	list = query.getResultList();
        if(!list.isEmpty() && list.size() == 1){
            return list.get(0);
        }
	return null;
    }
    
    public Culture findByCode(String code)
    {
        Query query = em.createNamedQuery("Culture.findByCode", Culture.class)
                .setParameter("code", code);
	List<Culture> list = new ArrayList();
	list = query.getResultList();
        if(!list.isEmpty() && list.size() == 1){
            return list.get(0);
        }
	return null;
    }

        public Culture findByName(String cultureName)
    {
        Query query = em.createNamedQuery("Culture.findByCultureName", Culture.class)
                .setParameter("cultureName", cultureName);
	List<Culture> list = new ArrayList();
	list = query.getResultList();
        if(!list.isEmpty() && list.size() == 1){
            return list.get(0);
        }
	return null;
    }
    
    @Override
    public Culture findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Culture> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean populate()
    {
        Culture c = new Culture("0x0436", "Afrikaans - South Africa", "AFK", "af-ZA");
        this.create(c);
        c = new Culture("0x041C", "Albanian - Albania", "SQI", "sq-AL");
        this.create(c);
        c = new Culture("0x1401", "Arabic - Algeria", "ARG", "ar-DZ");
        this.create(c);
        c = new Culture("0x3C01", "Arabic - Bahrain", "ARH", "ar-BH");
        this.create(c);
        c = new Culture("0x0C01", "Arabic - Egypt", "ARE", "ar-EG");
        this.create(c);
        c = new Culture("0x0801", "Arabic - Iraq", "ARI", "ar-IQ");
        this.create(c);
        c = new Culture("0x2C01", "Arabic - Jordan", "ARJ", "ar-JO");
        this.create(c);
        c = new Culture("0x3401", "Arabic - Kuwait", "ARK", "ar-KW");
        this.create(c);
        c = new Culture("0x3001", "Arabic - Lebanon", "ARB", "ar-LB");
        this.create(c);
        c = new Culture("0x1001", "Arabic - Libya", "ARL", "ar-LY");
        this.create(c);
        c = new Culture("0x1801", "Arabic - Morocco", "ARM", "ar-MA");
        this.create(c);
        c = new Culture("0x2001", "Arabic - Oman", "ARO", "ar-OM");
        this.create(c);
        c = new Culture("0x4001", "Arabic - Qatar", "ARQ", "ar-QA");
        this.create(c);
        c = new Culture("0x0401", "Arabic - Saudi Arabia", "ARA", "ar-SA");
        this.create(c);
        c = new Culture("0x2801", "Arabic - Syria", "ARS", "ar-SY");
        this.create(c);
        c = new Culture("0x1C01", "Arabic - Tunisia", "ART", "ar-TN");
        this.create(c);
        c = new Culture("0x3801", "Arabic - United Arab Emirates", "ARU", "ar-AE");
        this.create(c);
        c = new Culture("0x2401", "Arabic - Yemen", "ARY", "ar-YE");
        this.create(c);
        c = new Culture("0x042B", "Armenian - Armenia", "", "hy-AM");
        this.create(c);
        c = new Culture("0x082C", "Azeri (Cyrillic) - Azerbaijan", "", "Cy-az-AZ");
        this.create(c);
        c = new Culture("0x042C", "Azeri (Latin) - Azerbaijan", "", "Lt-az-AZ");
        this.create(c); 
        c = new Culture("0x042D", "Basque - Basque", "EUQ", "eu-ES");
        this.create(c);         
        c = new Culture("0x0423", "Belarusian - Belarus", "BEL", "be-BY");
        this.create(c);
        c = new Culture("0x0402", "Bulgarian - Bulgaria", "BGR", "bg-BG");
        this.create(c);
        c = new Culture("0x0403", "Catalan - Catalan", "CAT", "ca-ES");
        this.create(c);
        c = new Culture("0x0804", "Chinese - China", "CHS", "zh-CN");
        this.create(c);         

        c = new Culture("0x0C04", "Chinese - Hong Kong SAR", "ZHH", "zh-HK");
        this.create(c);         
 
        c = new Culture("0x1404", "Chinese - Macau SAR", "", "zh-MO");
        this.create(c);         
 
        c = new Culture("0x1004", "Chinese - Singapore", "ZHI", "zh-SG");
        this.create(c);         
 
        c = new Culture("0x0404", "Chinese - Taiwan", "CHT", "zh-TW");
        this.create(c); 
 
        c = new Culture("0x0004", "Chinese (Simplified)", "", "zh-CHS");
        this.create(c);         
 
        c = new Culture("0x7C04", "Chinese (Traditional)", "", "zh-CHT");
        this.create(c);         
 
        c = new Culture("0x041A", "Croatian - Croatia", "HRV", "hr-HR");
        this.create(c);         
 
        c = new Culture("0x0405", "Czech - Czech Republic", "CSY", "cs-CZ");
        this.create(c);         

        c = new Culture("0x0406", "Danish - Denmark", "DAN", "da-DK");
        this.create(c);         

        c = new Culture("0x0465", "Dhivehi - Maldives", "", "div-MV");
        this.create(c);         

        c = new Culture("0x0813", "Dutch - Belgium", "NLB", "nl-BE");
        this.create(c);         

        c = new Culture("0x0413", "Dutch - The Netherlands", "", "nl-NL");
        this.create(c);         

        c = new Culture("0x0C09", "English - Australia", "ENA", "en-AU");
        this.create(c);         

        c = new Culture("0x2809", "English - Belize", "ENL", "en-BZ");
        this.create(c);         

        c = new Culture("0x1009", "English - Canada", "ENC", "en-CA");
        this.create(c);         

        c = new Culture("0x2409", "English - Caribbean", "", "en-CB");
        this.create(c);         

        c = new Culture("0x1809", "English - Ireland", "ENI", "en-IE");
        this.create(c);         

        c = new Culture("0x2009", "English - Jamaica", "ENJ", "en-JM");
        this.create(c);         

        c = new Culture("0x1409", "English - New Zealand", "ENZ", "en-NZ");
        this.create(c);          

        c = new Culture("0x3409", "English - Philippines", "", "en-PH");
        this.create(c);          
 
        c = new Culture("0x1C09", "English - South Africa", "ENS", "en-ZA");
        this.create(c);          
 
        c = new Culture("0x2C09", "English - Trinidad and Tobago", "ENT", "en-TT");
        this.create(c);          

        c = new Culture("0x0809", "English - United Kingdom", "ENG", "en-GB");
        c.setActive(true);
        this.create(c);          

        c = new Culture("0x0409", "English - United States", "ENU", "en-US");
        c.setActive(true);
        this.create(c);          

        c = new Culture("0x3009", "English - Zimbabwe", "", "en-ZW");
        this.create(c);          

        c = new Culture("0x0425", "Estonian - Estonia", "ETI", "et-EE");
        this.create(c);          

        c = new Culture("0x0438", "Faroese - Faroe Islands", "FOS", "fo-FO");
        this.create(c);          
 
        c = new Culture("0x0429", "Farsi - Iran", "FAR", "fa-IR");
        this.create(c);          
 
        c = new Culture("0x040B", "Finnish - Finland", "FIN", "fi-FI");
        this.create(c);          
 
        c = new Culture("0x080C", "French - Belgium", "FRB", "fr-BE");
        this.create(c);          
 
        c = new Culture("0x0C0C", "French - Canada", "FRC", "fr-CA");
        this.create(c);          
 
        c = new Culture("0x040C", "French - France", "", "fr-FR");
        this.create(c);          
 
        c = new Culture("0x140C", "French - Luxembourg", "FRL", "fr-LU");
        this.create(c);          
 
        c = new Culture("0x180C", "French - Monaco", "", "fr-MC");
        this.create(c);          
 
        c = new Culture("0x100C", "French - Switzerland", "FRS", "fr-CH");
        this.create(c);          
 
        c = new Culture("0x0456", "Galician - Galician", "", "gl-ES");
        this.create(c);          
 
        c = new Culture("0x0437", "Georgian - Georgia", "", "ka-GE");
        this.create(c);          
 
        c = new Culture("0x0C07", "German - Austria", "DEA", "de-AT");
        this.create(c);          
 
        c = new Culture("0x0407", "German - Germany", "", "de-DE");
        this.create(c);          
 
        c = new Culture("0x1407", "German - Liechtenstein", "DEC", "de-LI");
        this.create(c);          
 
        c = new Culture("0x1007", "German - Luxembourg", "DEL", "de-LU");
        this.create(c);          
 
        c = new Culture("0x0807", "German - Switzerland", "DES", "de-CH");
        this.create(c);          
 
        c = new Culture("0x0408", "Greek - Greece", "ELL", "el-GR");
        c.setActive(true);
        this.create(c);          
 
        c = new Culture("0x0447", "Gujarati - India", "", "gu-IN");
        this.create(c);          
 
        c = new Culture("0x040D", "Hebrew - Israel", "HEB", "he-IL");
        this.create(c);          
 
        c = new Culture("0x0439", "Hindi - India", "HIN", "hi-IN");
        this.create(c);          
 
        c = new Culture("0x040E", "Hungarian - Hungary", "HUN", "hu-HU");
        this.create(c);          
 
        c = new Culture("0x040F", "Icelandic - Iceland", "ISL", "is-IS");
        this.create(c);          
 
        c = new Culture("0x0421", "Indonesian - Indonesia", "", "id-ID");
        this.create(c);          
 
        c = new Culture("0x0410", "Italian - Italy", "", "it-IT");
        this.create(c);          
 
        c = new Culture("0x0810", "Italian - Switzerland", "ITS", "it-CH");
        this.create(c);          
 
        c = new Culture("0x0411", "Japanese - Japan", "JPN", "ja-JP");
        this.create(c);          
 
        c = new Culture("0x044B", "Kannada - India", "", "kn-IN");
        this.create(c);          
 
        c = new Culture("0x043F", "Kazakh - Kazakhstan", "", "kk-KZ");
        this.create(c);          
 
        c = new Culture("0x0457", "Konkani - India", "", "kok-IN");
        this.create(c);          
 
        c = new Culture("0x0412", "Korean - Korea", "KOR", "ko-KR");
        this.create(c);          
 
        c = new Culture("0x0440", "Kyrgyz - Kazakhstan", "", "ky-KZ");
        this.create(c);          
 
        c = new Culture("0x0426", "Latvian - Latvia", "LVI", "lv-LV");
        this.create(c);          
 
        c = new Culture("0x0427", "Lithuanian - Lithuania", "LTH", "lt-LT");
        this.create(c);          
 
        c = new Culture("0x042F", "Macedonian (FYROM)", "MKD", "mk-MK");
        this.create(c);          
 
        c = new Culture("0x083E", "Malay - Brunei", "", "ms-BN");
        this.create(c);          
 
        c = new Culture("0x043E", "Malay - Malaysia", "", "ms-MY");
        this.create(c);          
 
        c = new Culture("0x044E", "Marathi - India", "", "mr-IN");
        this.create(c);          
 
        c = new Culture("0x0450", "Mongolian - Mongolia", "", "mn-MN");
        this.create(c);          
 
        c = new Culture("0x0414", "Norwegian (Bokm?l) - Norway", "", "nb-NO");
        this.create(c);          
 
        c = new Culture("0x0814", "Norwegian (Nynorsk) - Norway", "", "nn-NO");
        this.create(c);          
 
        c = new Culture("0x0415", "Polish - Poland", "PLK", "pl-PL");
        this.create(c);          
 
        c = new Culture("0x0416", "Portuguese - Brazil", "PTB", "pt-BR");
        this.create(c);          
 
        c = new Culture("0x0816", "Portuguese - Portugal", "", "pt-PT");
        this.create(c);          
 
        c = new Culture("0x0446", "Punjabi - India", "", "pa-IN");
        this.create(c);          
 
        c = new Culture("0x0418", "Romanian - Romania", "ROM", "ro-RO");
        this.create(c);          
 
        c = new Culture("0x0419", "Russian - Russia", "RUS", "ru-RU");
        this.create(c);          
 
        c = new Culture("0x044F", "Sanskrit - India", "", "sa-IN");
        this.create(c);          

        c = new Culture("0x0C1A", "Serbian (Cyrillic) - Serbia", "", "Cy-sr-SP");
        this.create(c);          

        c = new Culture("0x081A", "Serbian (Latin) - Serbia", "", "Lt-sr-SP");
        this.create(c);          

        c = new Culture("0x041B", "Slovak - Slovakia", "SKY", "sk-SK");
        this.create(c);          

        c = new Culture("0x0424", "Slovenian - Slovenia", "SLV", "sl-SI");
        this.create(c);          
 
        c = new Culture("0x2C0A", "Spanish - Argentina", "ESS", "es-AR");
        this.create(c);          
 
        c = new Culture("0x400A", "Spanish - Bolivia", "ESB", "es-BO");
        this.create(c);          
 
        c = new Culture("0x340A", "Spanish - Chile", "ESL", "es-CL");
        this.create(c);          
 
        c = new Culture("0x240A", "Spanish - Colombia", "ESO", "es-CO");
        this.create(c);          
 
        c = new Culture("0x140A", "Spanish - Costa Rica", "ESC", "es-CR");
        this.create(c);          
 
        c = new Culture("0x1C0A", "Spanish - Dominican Republic", "ESD", "es-DO");
        this.create(c);          
 
        c = new Culture("0x300A", "Spanish - Ecuador", "ESF", "es-EC");
        this.create(c);          
 
        c = new Culture("0x440A", "Spanish - El Salvador", "ESE", "es-SV");
        this.create(c);          
 
        c = new Culture("0x100A", "Spanish - Guatemala", "ESG", "es-GT");
        this.create(c);          
 
        c = new Culture("0x480A", "Spanish - Honduras", "ESH", "es-HN");
        this.create(c);          
 
        c = new Culture("0x080A", "Spanish - Mexico", "ESM", "es-MX");
        this.create(c);          

        c = new Culture("0x4C0A", "Spanish - Nicaragua", "ESI", "es-NI");
        this.create(c);          

        c = new Culture("0x180A", "Spanish - Panama", "ESA", "es-PA");
        this.create(c);          

        c = new Culture("0x3C0A", "Spanish - Paraguay", "ESZ", "es-PY");
        this.create(c);          

        c = new Culture("0x280A", "Spanish - Peru", "ESR", "es-PE");
        this.create(c);          

        c = new Culture("0x500A", "Spanish - Puerto Rico", "ES", "es-PR");
        this.create(c);          

        c = new Culture("0x0C0A", "Spanish - Spain", "", "es-ES");
        this.create(c);          

        c = new Culture("0x380A", "Spanish - Uruguay", "ESY", "es-UY");
        this.create(c);          

        c = new Culture("0x200A", "Spanish - Venezuela", "ESV", "es-VE");
        this.create(c);          

        c = new Culture("0x0441", "Swahili - Kenya", "", "sw-KE");
        this.create(c);          
 
        c = new Culture("0x081D", "Swedish - Finland", "SVF", "sv-FI");
        this.create(c);          

        c = new Culture("0x041D", "Swedish - Sweden", "", "sv-SE");
        this.create(c);          
 
        c = new Culture("0x045A", "Syriac - Syria", "", "syr-SY");
        this.create(c);          

        c = new Culture("0x0449", "Tamil - India", "", "ta-IN");
        this.create(c);          

        c = new Culture("0x0444", "Tatar - Russia", "", "tt-RU");
        this.create(c);          

        c = new Culture("0x044A", "Telugu - India", "", "te-IN");
        this.create(c);          

        c = new Culture("0x041E", "Thai - Thailand", "THA", "th-TH");
        this.create(c);          

        c = new Culture("0x041F", "Turkish - Turkey", "TRK", "tr-TR");
        this.create(c);          

        c = new Culture("0x0422", "Ukrainian - Ukraine", "UKR", "uk-UA");
        this.create(c);          

        c = new Culture("0x0420", "Urdu - Pakistan", "URD", "ur-PK");
        this.create(c);          

        c = new Culture("0x0843", "Uzbek (Cyrillic) - Uzbekistan", "", "Cy-uz-UZ");
        this.create(c);          

        c = new Culture("0x0443", "Uzbek (Latin) - Uzbekistan", "", "Lt-uz-UZ");
        this.create(c);          

        c = new Culture("0x042A", "Vietnamese - Vietnam", "VIT", "vi-VN");
        this.create(c);          
                
        return true;
    }
    
}
