/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import com.mobiweb.completeit.domain.Profile;
import javax.ejb.Stateless;


/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class ProfileService extends BaseEntityService<Profile>
{
    
    public boolean contains(Profile profile)
    {
        return em.contains(profile);
    }

    @Override
    public Profile findSingleByParam(String namedQuery, String paramName, Object paramValue)
    {
        Class c = paramValue.getClass();
        Profile query = em
                .createNamedQuery(namedQuery, Profile.class)
                .setParameter(paramName, paramValue)
                .getSingleResult();
        if(query != null)
        {
            return query;
        }
        return null;    
    }
    
    public Profile publicFind(Object id) { 
        return em.find(Profile.class, id);
    }   
    
    public List<Profile> findAllByNickname(String nickname)
    {
       List<Profile> query = 
               em.createNamedQuery("Profile.findByNickname", Profile.class)
               .setParameter("nickname", nickname)
               .getResultList();
        return (query != null && query.size() > 0) ? query : null;
    }
  
    public List<Profile> findAllByEmail(String email)
    {
       List<Profile> query = 
               em.createNamedQuery("Profile.findByEmail", Profile.class)
               .setParameter("email", email)
               .getResultList();
        return (query != null && query.size() > 0) ? query : null;
    }  

    
    public List<Profile> findByEmailAndNickname(String email, String nickname)
    {
       List<Profile> query = 
               em.createNamedQuery("Profile.findByEmailAndNickname", Profile.class)
               .setParameter("email", email)
               .setParameter("nickname",nickname)
               .getResultList();
        return (query != null && query.size() > 0) ? query : null;
    }   
   
    /**
     * Finds and returns a list of all entities in the database.
     * @return List<T>
     */
    @Override
    public List findAll() {
        em.getEntityManagerFactory().getCache().evict(Profile.class);
        List<Profile> query = em.createNamedQuery("Profile.findAll", Profile.class).getResultList();		
	return query;
    }
    
    public List findAllInclDeleted() {
        em.getEntityManagerFactory().getCache().evict(Profile.class);
        List<Profile> query = em.createNamedQuery("Profile.findAllInclDeleted", Profile.class).getResultList();
        return query;        
    }    
    
    public Profile findSingleByEmail(String email)
    {     
        List<Profile> query = em.createNamedQuery("Profile.findByEmail", Profile.class)
               .setParameter("email", email)
               .getResultList();       
       /*
        CriteriaBuilder cb = em.getCriteriaBuilder(); 
        CriteriaQuery<Profile> q = cb.createQuery(Profile.class);
        Root<Profile> c = q.from(Profile.class);
        ParameterExpression<String> p = cb.parameter(String.class);
        q.select(c).where(cb.equal(c.get("email"), p));              
        TypedQuery<Profile> tq = em.createQuery(q);
        tq.setParameter(p, email);        
        List<Profile> myQuery = tq.getResultList();
        
       if(myQuery != null){
        if(myQuery.size() == 1){
            return myQuery.get(0);
        }
       }
       */
       
       if(query != null){
        if(query.size() == 1){
            Profile p = em.find(Profile.class,query.get(0).getId());
            em.refresh(p);
            return p;            
        }
       }
       
       return null;
    }   

    public Profile findSingleByNickname(String nickname)
    {       
        CriteriaBuilder cb = em.getCriteriaBuilder(); 
        CriteriaQuery<Profile> q = cb.createQuery(Profile.class);
        Root<Profile> c = q.from(Profile.class);
        ParameterExpression<String> p = cb.parameter(String.class);
        q.select(c).where(cb.equal(c.get("nickname"), p));              
        TypedQuery<Profile> tq = em.createQuery(q);
        tq.setParameter(p, nickname);        
        List<Profile> myQuery = tq.getResultList();
        
       if(myQuery != null){
        if(myQuery.size() == 1){
            return myQuery.get(0);
        }
       }       
       return null;              
    }    
      
   public List<Profile> searchByEmail(String email)
   {
       List<Profile> query = 
               em.createNamedQuery("Profile.findByEmail", Profile.class)
               .setParameter("email", email)
               .getResultList();
        return query;
   }    
      
   public List<Profile> searchByNickname(String nickname)
   {
       List<Profile> query = 
               em.createNamedQuery("Profile.findByNickname", Profile.class)               
               .setParameter("nickname",nickname)
               .getResultList();
        return query;             
   }    
   
    public List<Profile> findTopTen()
   {
       List<Profile> query = 
               em.createNamedQuery("Profile.findTopTen", Profile.class)               
               .setMaxResults(10)
               .getResultList();
        return query;             
   } 

   public List<Profile> findExcluded(long profileId)
   {
       List<Profile> query = 
               em.createNamedQuery("Profile.findAllExcludeProfile", Profile.class)                              
               .setParameter("profileId", profileId)
               .getResultList();
        return query;             
   }       
   
   public boolean hasForeignKey(Object id)
   {
       Profile profile = this.find(id);
       if(profile!=null)
       {   
           if(profile.getLikes().size() > 0 || profile.getReports().size() > 0)
           {
               return true;
           }
       }
       return false;
   }

    @Override
    public Profile findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Profile> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Returns count of Profile entities with DELETED flag set to false.
     * @return 
     */
    @Override
    public int count()
    {
        return (em.createNamedQuery("Profile.countUndeleted",Long.class).getSingleResult()).intValue();
    }    
}
