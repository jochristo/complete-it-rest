/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.Invite;
import com.mobiweb.completeit.domain.ReferredUser;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class InviteService extends BaseEntityService<Invite>
{ 
    @Override
    public <N extends String> Invite findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    public Invite findByProfileId(long userid) {
        Query query  = em.createNamedQuery("Invite.findByProfileId", ReferredUser.class)
            .setParameter("userid", userid);
        if(query.getResultList().size() > 0)
        {
            return (Invite)query.getResultList().get(0);
        }
        return null;
    }  
    
    public Invite findByEmail(String email) {
        Query query  = em.createNamedQuery("Invite.findByEmail", ReferredUser.class)
            .setParameter("email", email);
        if(query.getResultList().size() > 0)
        {
            return (Invite)query.getResultList().get(0);
        }
        return null;
    }   
    
    public Invite findByProfileIdAndEmail(long profileId, String email) {
        Query query  = em.createNamedQuery("Invite.findByProfileIdAndEmail", Invite.class)
            .setParameter("profileId", profileId)
            .setParameter("email", email);
        if(query.getResultList().size() > 0)
        {
            return (Invite)query.getResultList().get(0);
        }
        return null;
    }     
    
    public boolean existsUserIdAndEmail(long profileId, String email) {
        Query query  = em.createNamedQuery("Invite.findByProfileIdAndEmail", Invite.class)
            .setParameter("profileId", profileId)
            .setParameter("email", email);
        return query.getResultList().size() > 0;
    }     

    @Override
    public Invite findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Invite> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public long getCount()
    {
        Query query = em.createNamedQuery("Invite.count", Invite.class);                
        return (long)query.getSingleResult();
    }    
}
