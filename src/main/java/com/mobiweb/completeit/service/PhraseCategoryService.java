/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.CategoryProjection;
import com.mobiweb.completeit.domain.Phrase;
import com.mobiweb.completeit.domain.PhraseCategory;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class PhraseCategoryService extends BaseEntityService<PhraseCategory>
{    
    @EJB PhraseService ps;
    private @EJB CategoryService cs;

    public PhraseCategory find(long pid, long cid) {
        em.flush();
        return em.createNamedQuery("PhraseCategory.findByPK",PhraseCategory.class)
            .setParameter("phraseId", pid)
            .setParameter("categoryId", cid)
            .getSingleResult();
    }
    
    public PhraseCategory findOptionally(long pid, long cid) {
        em.flush();
        List<PhraseCategory> list = 
        em.createNamedQuery("PhraseCategory.findByPK",PhraseCategory.class)
            .setParameter("phraseId", pid)
            .setParameter("categoryId", cid).getResultList();
        return list.size() > 0 ? list.get(0) : null ;
    }    
    
    public PhraseCategory findOptionally(Phrase phrase, Category category) {
        em.flush();
        List<PhraseCategory> list = 
        em.createNamedQuery("PhraseCategory.findByCompositeKey",PhraseCategory.class)            
            .setParameter("phrase", phrase)
                .setParameter("category", category)
                .getResultList();
        return list.size() > 0 ? list.get(0) : null ;
    }    
    
    public List<PhraseCategory> find(long pid) {
        em.flush();
        return em.createNamedQuery("PhraseCategory.findByPhraseId",PhraseCategory.class)
            .setParameter("phraseId", pid)            
            .getResultList();
    }  
    
    public List<PhraseCategory> findAllByCategoryId(long cid) {
        em.flush();
        return em.createNamedQuery("PhraseCategory.findByCategoryId",PhraseCategory.class)
            .setParameter("categoryId", cid)            
            .getResultList();
    }     
    
    
    public boolean existsPhraseId(Object id) {
        Phrase p = em.find(Phrase.class, id);
        return p != null;
    }

    @Override
    public <N extends String> PhraseCategory findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)      
    public void addPhraseCategory(Phrase p, Category c) throws EntityNotFoundException
    {
        if( Utilities.isEmpty(p) || Utilities.isEmpty(c) ) {
            throw new NullPointerException("Null phrase and/or category found");
        }             
        Phrase loadedPhrase = em.find(Phrase.class, p.getId());
        if( !Utilities.isEmpty(loadedPhrase))
        {
            PhraseCategory pc = new  PhraseCategory(p, c);
            //p.getCategories().add(c);
            //p.setCategories(p.getCategories());
            //ps.update(p);
            this.create(pc);       
            String temp = "";
        }   
        else{
            throw new EntityNotFoundException("Phrase id No : "+loadedPhrase.getId() +" not found");
        }        
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)      
    public void deletePhraseCategory(Phrase p, Category c) throws EntityNotFoundException
    {
        if( Utilities.isEmpty(p) || Utilities.isEmpty(c) ) {
            throw new NullPointerException("Null phrase and/or category found");
        }             
        Phrase loadedPhrase = em.find(Phrase.class, p.getId());        
        if( !Utilities.isEmpty(loadedPhrase))
        {
            PhraseCategory pc = this.find(p.getId(), c.getId());
            if(this.em.contains(pc))
            {                
                remove(pc);
            }
        }   
        else{
            throw new EntityNotFoundException("Phrase id No : "+loadedPhrase.getId() +" not found");
        }        
    }    

    @Override
    public PhraseCategory findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PhraseCategory> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<CategoryProjection> getCountGroupByCategory() {
        Query query = em.createNamedQuery("PhraseCategory.countGroupByCategory",CategoryProjection.class);                        
	return query.getResultList();
    }     
    
}
