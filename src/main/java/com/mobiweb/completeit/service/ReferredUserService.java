/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.ReferredUser;
import com.mobiweb.completeit.domain.SharedContent;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class ReferredUserService extends BaseEntityService<ReferredUser>
{
    @Override
    public List<ReferredUser> findAll() {
        return em.createNamedQuery("ReferredUser.findAll", ReferredUser.class).getResultList();
    }

    @Override
    public <N extends String> ReferredUser findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    public ReferredUser findByProfileId(long userid) {
        Query query  = em.createNamedQuery("ReferredUser.findByProfileId", ReferredUser.class)
            .setParameter("userid", userid);
        if(query.getResultList().size() > 0)
        {
            return (ReferredUser)query.getResultList().get(0);
        }
        return null;
    }  
    
    public ReferredUser findByEmail(String email) {
        Query query  = em.createNamedQuery("ReferredUser.findByEmail", ReferredUser.class)
            .setParameter("email", email);
        if(query.getResultList().size() > 0)
        {
            return (ReferredUser)query.getResultList().get(0);
        }
        return null;
    }   
    
    public ReferredUser findByProfileIdAndEmail(long profileId, String email) {
        Query query  = em.createNamedQuery("ReferredUser.findByProfileIdAndEmail", ReferredUser.class)
            .setParameter("profileId", profileId)
            .setParameter("email", email);
        if(query.getResultList().size() > 0)
        {
            return (ReferredUser)query.getResultList().get(0);
        }
        return null;
    }     
    
    public boolean existsUserIdAndEmail(long profileId, String email) {
        Query query  = em.createNamedQuery("ReferredUser.findByProfileIdAndEmail", ReferredUser.class)
            .setParameter("profileId", profileId)
            .setParameter("email", email);
        return query.getResultList().size() > 0;
    }   

    @Override
    public ReferredUser findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ReferredUser> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean addSharedContent(ReferredUser referredUser, SharedContent content)
    {
        if(content != null)
        {
            this.em.persist(content);
            List<SharedContent> list = em.createNamedQuery("SharedContent.findByProfileId", SharedContent.class)
                    .setParameter("profileId", referredUser.getProfileId())
                    .getResultList();
            if(list.size() >=1)
            {
                SharedContent sc = list.get(0);
                referredUser.getSharedContent().add(sc);
                referredUser.setSharedContent(referredUser.getSharedContent());
                this.update(referredUser);
                return true;
            }
        }
        return false;
    }
   
    public long getCount()
    {
        Query query = em.createNamedQuery("ReferredUser.count", ReferredUser.class);                
        return (long)query.getSingleResult();
    }     
}
