/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.CategoryProjection;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.persistence.Query;
import com.mobiweb.completeit.domain.Phrase;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.function.Predicate;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class PhraseService extends BaseEntityService<Phrase>
{   
    
    @EJB AnswerService as;   
    @Resource EJBContext ejbContext;
    
    public void setRollBackOnly()
    {
        ejbContext.setRollbackOnly();
    }
    
    @Override    
    public List<Phrase> findAll() {        
        em.getEntityManagerFactory().getCache().evict(Phrase.class);
        Query query = em.createNamedQuery("Phrase.findAll", Phrase.class);
        return query.getResultList();       
    }

    @Override
    public <N extends String> Phrase findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        Class c = paramValue.getClass();
        List<Phrase> query = em
                .createNamedQuery(namedQuery, Phrase.class)
                .setParameter(paramName, paramValue)
                .getResultList();
       if(query != null){
        if(query.size() == 1){
            return query.get(0);
        }
       }
       return null;
    }   

    public boolean existsPhraseProfile(String phrase, long profileId)
    {
        List<Phrase> query = em
                .createNamedQuery("Phrase.findByPhraseAndProfileId", Phrase.class)
                .setParameter("phrase", phrase)
                .setParameter("profileId", profileId)
                .getResultList();
        return query.size() > 0;
    }
    
    public boolean isPhraseAnsweredExcludingProfileId(long phraseId, long profileId)
    {
        List<Answer> query = as.getEntityManager()
                .createNamedQuery("Answer.findByPhraseIdAndNotProfileId", Answer.class)
                .setParameter("phraseId", phraseId) 
                .setParameter("profileId", profileId) 
                .getResultList();
        return query.size() > 0;
    }
    
    public boolean isApprovalPending(Object id)
    {
        Phrase query = em
                .createNamedQuery("Phrase.findById", Phrase.class)
                .setParameter("id", id)                
                .getSingleResult();
        return query.isIsApprovalPending();
    }    
    
    public double getMaleCompletionPercentage(long id)
    {
        double percentage = 0.00;
        int total = getTotalAnswersPerPhrase(id);
        int maleTotal = getTotalAnswersPerPhraseAndGender(id, "Male");                
        if(total > 0)
        {
            percentage = (double) maleTotal / (double)total;            
            percentage = (double)(percentage*100);
        }
        return percentage;
    }
    
    public double getFemaleCompletionPercentage(long id)
    {
        double percentage = 0.00;        
        int total = getTotalAnswersPerPhrase(id);                
        int femaleTotal = getTotalAnswersPerPhraseAndGender(id, "Female");
        if(total > 0)
        {
            percentage = (double)femaleTotal / (double)total;            
            percentage = (double)(percentage*100);
        }
        return percentage;
    }    
    
    public int getTotalAnswersPerPhrase(long id)
    {
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseId", Answer.class)
                .setParameter("phraseId", id)                
                .getResultList();        
        return answers.size();
    }
    
    public int getTotalAnswersPerPhraseNativeSql(long id)
    {
        Query answers = em.createNativeQuery("SELECT * FROM answer WHERE an_phrase_id = "+id);                               
        return answers.getResultList().size();
    }    
    
    private int getTotalAnswersPerPhraseAndGender(long id, String gender)
    {
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseIdAndProfileGender", Answer.class)
                .setParameter("phraseId", id) 
                .setParameter("gender", gender)
                .getResultList();        
        return answers.size();
    }
    
        
    private int getTotalAnswersPerPhraseAndGenderNativeSql(long id, String gender)
    {
        Query answers = em.createNativeQuery("SELECT count(an_profile_id) FROM answer"
                + " INNER JOIN user_profile ON answer.an_profile_id = user_profile.pr_id AND user_profile.pr_id = answer.an_profile_id"
                + " WHERE an_phrase_id = "+id + " AND pr_gender = \""+gender+"\"");        
        return answers.getResultList().size();
    }    

    public long getMeanAgePerPhrase(long id) throws ParseException
    {
        
        List<Answer> answers = em.createNamedQuery("Answer.findByPhraseId", Answer.class)
            .setParameter("phraseId", id)             
            .getResultList();
        
        List ages = new ArrayList();
        /*
        Query answers = em.createNativeQuery("SELECT pr_dob as dob FROM answer"
                + " INNER JOIN user_profile ON answer.an_profile_id = user_profile.pr_id AND user_profile.pr_id = answer.an_profile_id"
                + " WHERE an_phrase_id = "+id);          
        */
        if(answers.size() > 0)
        {
            SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");            
            for (Answer a : answers)                        
            {   
                String dob = sd.format(a.getProfile().getDob());                
                Date date = sd.parse(dob);
                Instant instant = date.toInstant();
                ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
                LocalDate ld = zdt.toLocalDate();                
                int age = Utilities.getAge(ld);
                ages.add(age);
            }
            
            if(ages.size() > 0){
                return Math.round(mean(ages));
            }
        }
        return 0;
    }       
    
    public List<Phrase> findByCategoryIdPaginated(long categoryId, int page, int size) {
        Query query = em.createNamedQuery("Phrase.findByCategoryId", Phrase.class)
            .setFirstResult((page-1)*size)
            .setMaxResults(size)
            .setParameter("categoryId", categoryId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    public List<Phrase> findByCategoryIdUnPaginated(long categoryId) {
        Query query = em.createNamedQuery("Phrase.findByCategoryId", Phrase.class)            
            .setParameter("categoryId", categoryId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }     
    
    public List<Phrase> findByCategoryIdOrderedByCreationDescPaginated(long categoryId, int page, int size) {
        Query query = em.createNamedQuery("Phrase.findByCategoryIdOrderedByCreationDesc", Phrase.class)        
            .setFirstResult((page-1)*size)
            .setMaxResults(size)
            .setParameter("categoryId", categoryId);            
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }        
    
    public long getCountByCategoryId(long categoryId) {
        Query query = em.createNamedQuery("Phrase.countByCategoryId", Phrase.class)            
            .setParameter("categoryId", categoryId);            
	return (long)query.getSingleResult();
    }     
    
   
    public List<Phrase> findAllByCategoryIdNotInAnswersUnpaginated(long categoryId, long profileId) {
        Query query = em.createNamedQuery("Phrase.findByCategoryIdNotInAnswersNEW", Phrase.class)            
            .setParameter("categoryId", categoryId)
            .setParameter("profileId", profileId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }     
    
    public long getCountByCategoryIdNotInAnswersUnpaginated(long categoryId, long profileId) {
        Query query = em.createNamedQuery("Phrase.countByCategoryIdNotInAnswersNEW", Phrase.class)            
            .setParameter("categoryId", categoryId)
            .setParameter("profileId", profileId);		
	return (long)query.getSingleResult();
    }           
     
    public List<Phrase> findByCategoryIdNotInAnswersPaginatedOrderedByCreationDesc(long categoryId, long profileId, int page, int size) {
        Query query = em.createNamedQuery("Phrase.findByCategoryIdNotInAnswersOrderedByCreationDescNEW", Phrase.class)        
            .setFirstResult((page-1)*size)
            .setMaxResults(size)
            .setParameter("categoryId", categoryId)
            .setParameter("profileId", profileId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }       
        
    public long getCount() {
        Query query = em.createNamedQuery("Phrase.count", Phrase.class);                        
	return (long)query.getSingleResult();
    }     
    
    public List<Object[]> getCountGroupByCategory() {
        Query query = em.createNamedQuery("Phrase.countGroupByCategory", Phrase.class);                        
	return query.getResultList();
    }    
    
    public List<Phrase> findAllPaginated( int page, int size) {
        Query query = em.createNamedQuery("Phrase.findAll", Phrase.class)
            .setFirstResult((page-1)*size)
            .setMaxResults(size);                
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    
    public List<Phrase> findRange(String field, String searchTerm, int firstResult, int maxResults) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Phrase> cq = qb.createQuery(Phrase.class);
        Root<Phrase> root = cq.from(Phrase.class);
        EntityType<Phrase> type = em.getMetamodel().entity(Phrase.class);
        //Path<String> path = root.get(type.getDeclaredSingularAttribute(field, String.class));
        //Predicate condition = (Predicate) qb.like(path, "%" + searchTerm + "%");
        //cq.where((Expression<Boolean>) condition);
        TypedQuery<Phrase> q = em.createQuery(cq);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();
    }    
    
    public int count(String field, String searchTerm) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        Root<Phrase> root = cq.from(Phrase.class);
        EntityType<Phrase> type = em.getMetamodel().entity(Phrase.class);
        Path<String> path = root.get(type.getDeclaredSingularAttribute(field, String.class));
        Predicate condition = (Predicate) qb.like(path, "%" + searchTerm + "%");
        cq.select(qb.count(root));
        cq.where((Expression<Boolean>) condition);
        return em.createQuery(cq).getSingleResult().intValue();
    }    
    
    public boolean hasForeignKeyDependency(long phraseId)
    {        
        Query query = em.createNamedQuery("Answer.countByPhraseId", Answer.class)
            .setParameter("phraseId", phraseId);
        return ((long)query.getSingleResult()>0);
    }    
    
    public double mean (List<Integer> a){
        int sum = sum(a);
        double mean = 0;
        mean = sum / (a.size() * 1.0);
        return mean;
    }
    
    public double median (List<Integer> a){
        int middle = a.size()/2;
 
        if (a.size() % 2 == 1) {
            return a.get(middle);
        } else {
           return (a.get(middle-1) + a.get(middle)) / 2.0;
        }
    }
    
    public int sum (List<Integer> a){
        if (a.size() > 0) {
            int sum = 0;
 
            for (Integer i : a) {
                sum += i;
            }
            return sum;
        }
        return 0;
    }    

    @Override
    public Phrase findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Phrase> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public int count()
    {
        return (em.createNamedQuery("Phrase.countUndeleted", Long.class).getSingleResult()).intValue();
    }   
    
    public List<CategoryProjection> countUnansweredGroupByCategory()
    {
        Query query = em.createNamedQuery("Phrase.countUnansweredGroupByCategory", CategoryProjection.class);                
        return query.getResultList();
    }     
    
    public List<CategoryProjection> countAnsweredGroupByCategory()
    {
        Query query = em.createNamedQuery("Phrase.countAnsweredGroupByCategory", CategoryProjection.class);                
        return query.getResultList();
    }     
    
    public Phrase findPhraseInAnswers(long phraseId) {
        Query query = em.createNamedQuery("Phrase.findPhraseInAnswers", Phrase.class)            
            .setParameter("phraseId", phraseId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list.size() == 1 ? list.get(0): null;
    }    
    
    public List<Phrase> findAllApprovedPaginated( int page, int size) {
        Query query = em.createNamedQuery("Phrase.findAllApproved", Phrase.class)
            .setFirstResult((page-1)*size)
            .setMaxResults(size);                
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    public List<Phrase> findAllApproved() {
        Query query = em.createNamedQuery("Phrase.findAllApproved", Phrase.class);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list;
    }    
    
    public boolean updateAllAprovalPendingStatus(boolean status)
    {
        try{
        List<Phrase> list = this.findAll();
        for(Phrase p : list)
        {            
            //Phrase ref = em.find(Phrase.class, p.getId());
            p.setIsApprovalPending(status);
            update(p);            
        }        
        return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    
    public boolean isAllApproved()
    {
        long countApproved = (long)em.createNamedQuery("Phrase.countApproved", Long.class).getSingleResult();
        return this.count() == countApproved;
    }  
    
    public boolean isAllPending()
    {
        long countPending = (long)em.createNamedQuery("Phrase.countPending", Long.class).getSingleResult();
        return this.count() == countPending;
    }
    
    public boolean updateAllApprovalStatusBulk(boolean status)
    {
        List<Phrase> list = this.findAll();  
        int statusValue = status == true ? 1 : 0;
        for(Phrase p : list)
        {            
            //bulk update
            Query query = em.createQuery("UPDATE Phrase p SET p.isApprovalPending = ?1 WHERE p.id = ?2");
            query.setParameter(1, statusValue);
            query.setParameter(2, p.getId());
            query.executeUpdate();
        }                
        return true;
      
    }
    
    public Phrase update(long phraseId) {
        Query query = em.createNamedQuery("Phrase.findPhraseInAnswers", Phrase.class)            
            .setParameter("phraseId", phraseId);
	List<Phrase> list = new ArrayList();
	list = query.getResultList();
	return list.size() == 1 ? list.get(0): null;
    }     
}
