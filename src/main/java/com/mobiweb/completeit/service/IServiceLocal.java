/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author ic
 * @param <T>
 */

public interface IServiceLocal<T extends Object> 
{
    boolean exists(Object id);    
    T find(Object id);
    List<T> findAll();      
    T inserted(T entity);    
    T updated(T entity);       
    EntityManager getEntityManager();
    public <N extends String> T findSingleByParam(N namedQuery, N paramName, Object paramValue);    
    public void create(T entity);
    public void remove(T entity);    
    public void update(T entity);     
   
}
