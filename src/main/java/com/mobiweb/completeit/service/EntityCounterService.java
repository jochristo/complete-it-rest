/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.service;

import com.mobiweb.completeit.domain.EntityCounter;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author ic
 */
@Stateless
@LocalBean
public class EntityCounterService extends BaseEntityService<EntityCounter>
{
    private @EJB CategoryService categoryService;
    private @EJB ProfileService profileService;
    private @EJB PhraseService phraseService;
    private @EJB AnswerService answerService;
    private @EJB InviteService inviteService;
    private @EJB AnswerLikeService answerLikeService;
    private @EJB AnswerReportService answerReportService;
    private @EJB ReferredUserService referredUserService;

    @Override
    public <N extends String> EntityCounter findSingleByParam(N namedQuery, N paramName, Object paramValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EntityCounter findByParameterSingle(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntityCounter> findByParameterMany(Object param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EntityCounter findByEntityType(String entityType)
    {
        //return em.createNamedQuery("EntityCounter.findByEntityType", EntityCounter.class)
        //    .setParameter("entityType", entityType).getSingleResult();
        
        Query query = em.createNamedQuery("EntityCounter.findByEntityType", EntityCounter.class)
            .setParameter("entityType", entityType);
        List<EntityCounter> list = query.getResultList();
        if(list.size() == 1){
            return list.get(0);
        }
        return null;
    }
    
    public long countByEntityType(String entityType)
    {
        //return em.createNamedQuery("EntityCounter.findByEntityType", EntityCounter.class)
        //    .setParameter("entityType", entityType).getSingleResult();
        
        Query query = em.createNamedQuery("EntityCounter.countByEntityType", EntityCounter.class)
            .setParameter("entityType", entityType);        
        return (long)query.getSingleResult();
    }    
    
    public void updateCounter(long value, Class<? extends Serializable> clazz)
    {
        String name = clazz.getSimpleName();
        EntityCounter ec = this.findByEntityType(name);
        if(ec != null)
        {            
            long count = 0;
            switch(name)
            {
                case "Profile"  :  count = profileService.count();
                    break;
                case "Category" :  count = categoryService.count();
                    break;
                case "Phrase"   :  count = phraseService.count();
                    break;
                case "Answer"   :  count = answerService.count();
                    break;                
            }            
            ec.setCounter(count);
            this.update(ec);                
        }
    }
    
    public long Invites()
    {
        return inviteService.getCount();
    }
    
    public long Shares()
    {
        return referredUserService.getCount();
    }    
    
    public long Reports()
    {
        return answerReportService.getCount();
    }    
    
    public long Likes()
    {
        return answerLikeService.getCount();
    }    
    
    public long AnsweredPhrases()
    {
        return answerService.getCountDistinctAnsweredPhrases();
    }
    
}
