/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.resource;

import com.google.gson.JsonParseException;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.EntityNotFoundException;
import com.mobiweb.completeit.common.exception.InvalidJsonApiClassException;
import com.mobiweb.completeit.common.exception.InvalidJsonFormatException;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.Phrase;
import com.mobiweb.completeit.domain.PhraseCategory;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.json.AnsweredWord;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.json.ErrorUtils.ErrorMessage;
import com.mobiweb.completeit.json.InboundAnswer;
import com.mobiweb.completeit.json.InboundCategory;
import com.mobiweb.completeit.json.InboundPhrase;
import com.mobiweb.completeit.json.JsonApiStreamGenerator;
import com.mobiweb.completeit.json.JsonParser;
import com.mobiweb.completeit.json.OutboundCategory;
import com.mobiweb.completeit.json.OutboundPhrase;
import com.mobiweb.completeit.json.OutboundProfile;
import com.mobiweb.completeit.json.RelationCategory;
import com.mobiweb.completeit.service.AnswerService;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.PhraseCategoryService;
import com.mobiweb.completeit.service.PhraseService;
import com.mobiweb.completeit.service.ProfileService;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Administrator services endpoint.
 * Authenticates against users (in 'completeit-admin' role-name) realm in TomEE server.
 * @author ic
 */
@Path("/secured")
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@DeclareRoles(value={"completeit-admin"})
public class AdministrationResource
{
    private @EJB ProfileService profileService;  
    private @EJB CategoryService cs;  
    private @EJB PhraseService ps;
    private @EJB AnswerService as;  
    private @EJB PhraseCategoryService pcs;  
    private @Context ServletContext context;
    private @Resource EJBContext ejbContext;   
    private ErrorUtils.ErrorMessage errorMessage;  
    private String imagesPath = null;   
    private @EJB EntityCounterService ecs;
    
    /**
     * Returns all profiles in JSON format.
     * @param httpServletRequest
     * @return
     * @throws IOException 
     * @throws java.lang.IllegalAccessException 
     * @throws java.lang.NoSuchFieldException 
     * @throws java.lang.reflect.InvocationTargetException 
     */
    @GET
    @Produces("application/vnd.api+json")
    @Path("/profiles")
    @RolesAllowed("completeit-admin")    
    public Response getAllProfiles(@Context HttpServletRequest httpServletRequest) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {
        List<Profile> profiles = profileService.findAll();
        List<OutboundProfile> list = new ArrayList();
        for(Profile profile : profiles){
            OutboundProfile body = new OutboundProfile(profile);
            list.add(body);
        }
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(list, null);
        return Response.ok(generator.writeStream()).build(); 
    }  
    
    /**
     * Deletes  a profile resource.
     * @param id The profile resource id to search and delete.
     * @param httpServletRequest
     * @return 
     */
    @DELETE
    @Consumes("application/vnd.api+json")
    @Produces("application/vnd.api+json")
    @Path("/profiles/{id}")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)    
    @RolesAllowed("completeit-admin")
    public Response deleteProfile(@PathParam("id") long id,@Context HttpServletRequest httpServletRequest)
    {
        
        // SEARCH profile ID
        Profile p = profileService.find(id);
        if(p == null)
        {
            // profile not found, return no content
            return Response.status(Response.Status.NOT_FOUND).build();
        }         
        
        try
        {            
            profileService.remove(p);
            if(profileService.contains(p))
            {   
                // deletion success, return no content status.
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        }
        catch(IllegalArgumentException ex)
        {
            return null;
        }            
        
        return null;
    }   
    /**
     * Endpoint to create and post a new category object.
     * @param httpServletRequest
     * @param is
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException 
     */
    
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/categories")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)  
    @RolesAllowed("completeit-admin")    
    public Response createCategory(@Context HttpServletRequest httpServletRequest, InputStream is) throws IOException, IllegalAccessException             
    {    
        InboundCategory inbound = new InboundCategory();
        try
        {                      
            inbound = JsonParser.deserialized(is, InboundCategory.class);                        
            if(inbound != null)
            {
                // prepare profile picture processings
                boolean isImageNull = Utilities.isEmpty(inbound.getImageString());
                Category category = new Category();
                String base64EndodedString = null;
                String imagePath = null;
                String imageFilename = null;
                if(!isImageNull)
                {               
                    imagePath = getCategoryImagesAbsolutePath()+ "\\" + inbound.getName() + "\\";                           
                    String random = Utilities.randomFilename(inbound.getName());            
                    imageFilename = random + ".png";
                    boolean existsFile = new File(imagePath,imageFilename).exists();  
                    while(existsFile){
                        random = Utilities.randomFilename(inbound.getName());
                        imageFilename = random + ".png";
                        existsFile = new File(imagePath,imageFilename).exists(); 
                    }
                    String relativePath = new File(getCategoryImagesAbsolutePath()).toURI().relativize(new File(imagePath,imageFilename).toURI()).getPath();                    
                    category.setImageRelativePath("/".concat(relativePath));                                    
                    base64EndodedString = inbound.getImageString();
                }
                
                // PERSIST NEW CATEGORY
                category.setName(inbound.getName());
                category.setDescription(inbound.getDescription());                
                
                if(isImageNull)
                {
                    category.setImageRelativePath(null);
                }
                
                Category c = null;
                try
                {
                    c = cs.created(category);
                    //update entity counter table
                    ecs.updateCounter(1, Category.class);                    
                } catch (Exception t)
                {
                    ejbContext.setRollbackOnly();
                    //return Response.status(Response.Status.BAD_REQUEST).entity(t.getMessage()).type(MediaType.APPLICATION_JSON).build();                                        
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(t.getMessage())
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
                }
                
                // write image to server file system
                if(!isImageNull)
                {
                    try
                    {
                        BufferedImage image = Utilities.decodeToImage(base64EndodedString); 
                        Utilities.writeImageToFile(image, imagePath,imageFilename);
                    } catch (IllegalArgumentException ex)
                    {
                        // rollback the update process, invalid image found
                        ejbContext.setRollbackOnly();
                        //return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.APPLICATION_JSON).build();                        
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                        .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_IMAGE_STRING.concat(" : ").concat(ex.getMessage()))
                        .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_IMAGE_FORMAT.getCode())
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                        
                    }            
                }

                OutboundCategory out = new OutboundCategory(c);
                String jsonResponse = null;        
                jsonResponse = JsonParser.serialized(out, OutboundCategory.class);          
                if(c != null)
                {
                    try{                  
                        URI locationUri = new URI("categories/"+String.valueOf(c.getId()));
                        return Response.created(new URI("categories/"+String.valueOf(c.getId())))
                            .header("Location", locationUri)
                            .entity(jsonResponse)
                            .build();
                     } catch (URISyntaxException ex) {
                        Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);
                     }
                }
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);
            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());           
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        return Response.noContent().build();
    }  
    
    /**
     * Endpoint to create and post a phrase object.
     * @param httpServletRequest
     * @param is
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException 
     */
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/phrases")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @RolesAllowed("completeit-admin")     
    public Response createPhrase(@Context HttpServletRequest httpServletRequest, InputStream is) throws IOException, IllegalAccessException, EntityNotFoundException
    {    
        InboundCategory inboundCategory = new InboundCategory();
        InboundPhrase inboundPhrase = new InboundPhrase();        
        try
        {   
            inboundPhrase = JsonParser.deserializedWithRelationship(is, InboundPhrase.class, RelationCategory.class);               
            if(inboundCategory != null)
            {
                Category category = null;
                Phrase phrase = null;
                try
                {   
                    long id = Long.parseUnsignedLong(inboundPhrase.getProfileId());                    
                    PhraseParser parser = new PhraseParser();
                    parser.parse(inboundPhrase.getPhrase());                    
                    Profile requestingPofile = profileService.find(id);
                    if(requestingPofile == null)
                    {           
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                        .setDetail("Profile id does not exist : "+ id)
                        .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                        return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
                    }                    
                    if(ps.existsPhraseProfile(inboundPhrase.getPhrase(),id))
                    {
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                        .setDetail("Profile id <" + inboundPhrase.getProfileId() + "> and phrase exist : "+ inboundPhrase.getPhrase())
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                  
                    }
                    
                    inboundPhrase.setId(UUID.randomUUID().toString());
                    List<Category> categories = new ArrayList();                    
                    for(RelationCategory rc : inboundPhrase.getCategories())
                    {                        
                        category = cs.find(Long.parseLong(rc.getId()));
                        if(category == null)
                        {           
                            errorMessage = new ErrorUtils().new ErrorMessage()
                            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                            .setDetail("The requested category id was not found : "+ rc.getId())
                            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
                        }                         
                        categories.add(category);                       
                    }
                                  
                    //set phrase entity properties from json
                    phrase = new Phrase();
                    phrase.setPhrase(inboundPhrase.getPhrase());
                    phrase.setGaps(parser.getGaps());
                    inboundPhrase.setGaps(String.valueOf(parser.getGaps()));                    
                    phrase.setProfile(requestingPofile);                    
                    Phrase p = ps.created(phrase);     
                    for(Category c : categories)
                    {
                        Category cat = cs.find(c.getId());                       
                        p.getPhraseCategory().add(new PhraseCategory(p,cat));
                    }                    
                    p.setPhraseCategory(p.getPhraseCategory());
                    ps.update(p);
                    //update entity counter table
                    ecs.updateCounter(1, Phrase.class);                      
                    inboundPhrase.setId(String.valueOf(phrase.getId()));
                    
                } catch (NumberFormatException | PhraseParser.EmptyPhraseException | PhraseParser.PhraseParseException t)
                {
                    ejbContext.setRollbackOnly();
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(t.getMessage())
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
                }
                String jsonResponse = null;        
                jsonResponse = JsonParser.serializedWithRelatioship(inboundPhrase, InboundPhrase.class, RelationCategory.class);
                if(category != null)
                {
                    return Response.status(Response.Status.CREATED).entity(jsonResponse).build();   
                }
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());           
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
            
        }        
        return Response.noContent().build();
    } 

    /**
     * Returns all phrases in JSON API format.
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     * @throws java.text.ParseException 
     * @throws java.lang.NoSuchFieldException 
     */
    @GET
    @Produces("application/vnd.api+json")
    @Path("/phrases")
    @RolesAllowed("completeit-admin")   
    
    public Response getAllPhrases() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, ParseException, NoSuchFieldException
    {   
        List<Phrase> phrases = ps.findAll(); 
        List<OutboundPhrase> jsonPhrases = new ArrayList(); 
        for (Phrase phr : phrases) {
            OutboundPhrase body = new OutboundPhrase(phr);
            jsonPhrases.add(body);
        }
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonPhrases, OutboundPhrase.class);
        return Response.status(Response.Status.OK).entity(generator.writeStream()).build();     
    } 
    
    @GET
    @Produces("application/vnd.api+json")
    @Path("/answers")
    @RolesAllowed("completeit-admin")
    public Response getAllAnswers() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, InvalidJsonApiClassException, NoSuchFieldException
    {
        long id = 0;
        String answerText = "";
        try
            {
            List<Answer> answers = as.findAll();
            List<InboundAnswer> outboundAnswers = new ArrayList();

            for (Answer answer : answers)
            {
                id = answer.getId();
                answerText = answer.getAnswers();
                InboundAnswer outboundAnswer = new InboundAnswer(answer);            
                outboundAnswers.add(outboundAnswer);
            }

                JsonApiStreamGenerator generator = new JsonApiStreamGenerator(outboundAnswers, AnsweredWord.class);               
                return Response.status(Response.Status.OK).entity(generator.writeStream()).build();     
        }
        catch (Exception ex)
        {
            String detail = "";
            if(ex instanceof JsonParseException)
            {
                Throwable cause = ex.getCause();
                detail = cause.getMessage();
            }            
                        
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT + " answerid - "+String.valueOf(id))
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            if(ex instanceof JsonParseException)
            {
                String escaped = answerText.replace("\"","\\\"");
                errorMessage.setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT 
                        + " - answer id < "+ id +" >, answer json text (" + escaped + ") : " + detail);
            }            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
    }      
    
    
    /**
     * Returns all categories in JSON API format.
     * @return
     * @throws IOException 
     * @throws java.lang.IllegalAccessException 
     * @throws java.lang.reflect.InvocationTargetException 
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException 
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     */
    @GET
    @Produces("application/vnd.api+json")
    @Path("/categories")
    @RolesAllowed("completeit-admin")  
    
    public Response getAllCategories() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, NoSuchFieldException
    {           
        List<Category> categories = cs.findAll(); 
        List<OutboundCategory> jsonCategories = new ArrayList(); 
        for (Category cat : categories) {
            OutboundCategory body = new OutboundCategory(cat);
            jsonCategories.add(body);
        }       
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonCategories, null);        
        String json = generator.writeStream();        
        return Response.status(Response.Status.OK).entity(json).build();         
    }     
    
    /**
     * Returns  context's parameter holding the absolute path to profile images upload server location.
     * @return String
     */
    private String getCategoryImagesAbsolutePath()
    {
        imagesPath = System.getProperty("catalina.base").concat((String)context.getInitParameter("Category.Images.Path"));                
        if(!imagesPath.isEmpty())
        {
            File f = new File(imagesPath).getAbsoluteFile();              
            return f.getAbsolutePath();
        }
        return null;
    }    
    
}
