/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.mobiweb.completeit.common.MemCached;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.DuplicateJsonDocumentIdException;
import com.mobiweb.completeit.common.exception.IncorrectWordGapsException;
import com.mobiweb.completeit.common.exception.InvalidArgumentException;
import com.mobiweb.completeit.common.exception.InvalidJsonApiClassException;
import com.mobiweb.completeit.common.exception.InvalidJsonFormatException;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.AnswerProfileReport;
import com.mobiweb.completeit.domain.Category;
import com.mobiweb.completeit.domain.Culture;
import com.mobiweb.completeit.domain.Invite;
import com.mobiweb.completeit.domain.Phrase;
import com.mobiweb.completeit.domain.PhraseCategory;
import com.mobiweb.completeit.domain.PhraseCategoryPK;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.domain.ReferredUser;
import com.mobiweb.completeit.domain.SharedContent;
import com.mobiweb.completeit.json.AnswerJson;
import com.mobiweb.completeit.json.AnsweredWord;
import com.mobiweb.completeit.json.CompletePhrase;
import com.mobiweb.completeit.json.InboundAnswer;
import com.mobiweb.completeit.json.InboundCategory;
import com.mobiweb.completeit.json.InboundPhrase;
import com.mobiweb.completeit.json.JsonApiStreamGenerator;
import com.mobiweb.completeit.json.OutboundCategory;
import com.mobiweb.completeit.json.JsonParser;
import com.mobiweb.completeit.json.OutboundAnswer;
import com.mobiweb.completeit.json.OutboundPhrase;
import com.mobiweb.completeit.json.OutboundProfileAnswer;
import com.mobiweb.completeit.json.OutboundStatisticsPhrase;
import com.mobiweb.completeit.json.RelationCategory;
import com.mobiweb.completeit.json.RelationGaps;
import com.mobiweb.completeit.json.StatisticsItem;
import com.mobiweb.completeit.service.AnswerService;
import com.mobiweb.completeit.service.AuthorizationService;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.PhraseService;
import com.mobiweb.completeit.service.ProfileService;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ws.rs.QueryParam;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.json.ErrorUtils.ErrorMessage;
import com.mobiweb.completeit.json.InviteJson;
import com.mobiweb.completeit.json.OutboundCulture;
import com.mobiweb.completeit.json.Shared;
import com.mobiweb.completeit.service.AnswerLikeService;
import com.mobiweb.completeit.service.CultureService;
import com.mobiweb.completeit.service.EntityCounterService;
import com.mobiweb.completeit.service.InviteService;
import com.mobiweb.completeit.service.ReferredUserService;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.annotation.security.DeclareRoles;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import net.spy.memcached.MemcachedClient;

/**
 * Restful endpoint to provide services to handle category, phrase, and answer requests.
 * @author ic
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@DeclareRoles(value={"completeit-admin"})
public class NormalModeResource {
    
    private @EJB CategoryService cs;
    private @EJB PhraseService ps;
    private @EJB ProfileService profileService;
    private @EJB AnswerService as;
    private @EJB AuthorizationService authorizationService;        
    private @EJB ReferredUserService referredUserService;
    private @EJB InviteService inviteService;
    private @EJB AnswerLikeService als;
    private @EJB CultureService cultureService;
    private @Context ServletContext context;
    private @Resource EJBContext ejbContext;        
    private String imagesPath = null;   
    private ErrorUtils.ErrorMessage errorMessage;
    private final MemcachedClient cache;    
    private @EJB EntityCounterService ecs;
    
    public NormalModeResource() throws IOException {
        cache = MemCached.getClient();
    }   
        
    /**
     * Endpoint to create and post a new category object.
     * @param httpServletRequest
     * @param is
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException 
     */
    
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/categories")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)    
    public Response createCategory(@Context HttpServletRequest httpServletRequest, InputStream is) throws IOException, IllegalAccessException             
    {    
        InboundCategory inbound = new InboundCategory();
        try
        {                      
            inbound = JsonParser.deserialized(is, InboundCategory.class);                        
            if(inbound != null)
            {
                // prepare profile picture processings
                boolean isImageNull = Utilities.isEmpty(inbound.getImageString());
                Category category = new Category();
                String base64EndodedString = null;
                String imagePath = null;
                String imageFilename = null;
                if(!isImageNull)
                {               
                    imagePath = getCategoryImagesAbsolutePath()+ "\\" + inbound.getName() + "\\";                           
                    String random = Utilities.randomFilename(inbound.getName());            
                    imageFilename = random + ".png";
                    boolean existsFile = new File(imagePath,imageFilename).exists();  
                    while(existsFile){
                        random = Utilities.randomFilename(inbound.getName());
                        imageFilename = random + ".png";
                        existsFile = new File(imagePath,imageFilename).exists(); 
                    }
                    String relativePath = new File(getCategoryImagesAbsolutePath()).toURI().relativize(new File(imagePath,imageFilename).toURI()).getPath();                    
                    category.setImageRelativePath("/".concat(relativePath));                                    
                    base64EndodedString = inbound.getImageString();
                }
                
                // PERSIST NEW CATEGORY
                category.setName(inbound.getName());
                category.setDescription(inbound.getDescription());                
                
                if(isImageNull)
                {
                    category.setImageRelativePath(null);
                }
                
                Category c = null;
                try
                {
                    c = cs.created(category);
                    //update entit counter table
                    ecs.updateCounter(1, Category.class);                            ;
                } catch (Exception t)
                {
                    ejbContext.setRollbackOnly();
                    //return Response.status(Response.Status.BAD_REQUEST).entity(t.getMessage()).type(MediaType.APPLICATION_JSON).build();                                        
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(t.getMessage())
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
                }
                
                // write image to server file system
                if(!isImageNull)
                {
                    try
                    {
                        BufferedImage image = Utilities.decodeToImage(base64EndodedString); 
                        Utilities.writeImageToFile(image, imagePath,imageFilename);
                    } catch (IllegalArgumentException ex)
                    {
                        // rollback the update process, invalid image found
                        ejbContext.setRollbackOnly();
                        //return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.APPLICATION_JSON).build();                        
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                        .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_IMAGE_STRING.concat(" : ").concat(ex.getMessage()))
                        .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_IMAGE_FORMAT.getCode())
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                        
                    }            
                }

                OutboundCategory out = new OutboundCategory(c);
                String jsonResponse = null;        
                jsonResponse = JsonParser.serialized(out, OutboundCategory.class);          
                if(c != null)
                {
                    try{                  
                        URI locationUri = new URI("categories/"+String.valueOf(c.getId()));
                        return Response.created(new URI("categories/"+String.valueOf(c.getId())))
                            .header("Location", locationUri)
                            .entity(jsonResponse)
                            .build();
                     } catch (URISyntaxException ex) {
                        Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);
                     }
                }
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);
            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());           
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        return Response.noContent().build();
    }
    
    /**
     * Returns  context's parameter holding the absolute path to profile images upload server location.
     * @return String
     */
    private String getCategoryImagesAbsolutePath()
    {
        imagesPath = System.getProperty("catalina.base").concat((String)context.getInitParameter("Category.Images.Path"));                
        if(!imagesPath.isEmpty())
        {
            File f = new File(imagesPath).getAbsoluteFile();              
            return f.getAbsolutePath();
        }
        return null;
    } 
    
    @GET       
    @Produces("application/vnd.api+json")        
    public Response get(@PathParam("cat_name") String cat_name) throws IOException
    {                  
        // profile not found, return no content            
        Category c = cs.findSingleByParam("Category.findByName", "name", cat_name);
        if(c == null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested category was not found : "+cat_name)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }                      
        try{
            OutboundCategory body = new OutboundCategory(c);
            String jsonResponse = null;
            jsonResponse = JsonParser.serialized(body, OutboundCategory.class);
            return Response.ok(jsonResponse).build();              
        } catch (Exception ex){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
    }     
    
    @GET       
    @Path("/categories/{cat_id}")
    @Produces("application/vnd.api+json")
    public Response getCategory(@PathParam("cat_id") long cat_id) throws IOException
    {                  
        // category not found, return no content            
        Category c = cs.find(cat_id);
        if(c == null)
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested category id was not found : "+cat_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }                      
        try{
            OutboundCategory body = new OutboundCategory(c);
            String jsonResponse = null;
            jsonResponse = JsonParser.serialized(body, OutboundCategory.class);
            List<OutboundCategory> list = new ArrayList();
            list.add(body);
            jsonResponse = JsonParser.toJsonAPI(list, OutboundCategory.class);
            return Response.ok(jsonResponse).build();              
        } catch (UnknownHostException | SocketException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        catch(JsonProcessingException ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();              
        }
    }    
    
    /**
     * Returns all categories in JSON API format.
     * @return
     * @throws IOException 
     * @throws java.lang.IllegalAccessException 
     * @throws java.lang.reflect.InvocationTargetException 
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException 
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     * @throws java.lang.NoSuchFieldException 
     */
    @GET
    @Produces("application/vnd.api+json")
    @Path("/categories")    
    public Response getAllCategories() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, NoSuchFieldException
    {           
        List<Category> categories = cs.findAll(); 
        List<OutboundCategory> jsonCategories = new ArrayList(); 
        for (Category cat : categories) {
            OutboundCategory body = new OutboundCategory(cat);
            jsonCategories.add(body);
        }       
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonCategories, null);        
        String json = generator.writeStream();        
        return Response.status(Response.Status.OK).entity(json).build();         
    } 
    
    /**
     * Endpoint to create and post a phrase object.
     * @param httpServletRequest
     * @param is
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException 
     */
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/phrases")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response createPhrase(@Context HttpServletRequest httpServletRequest, InputStream is) throws IOException, IllegalAccessException             
    {    
        InboundCategory inboundCategory = new InboundCategory();
        InboundPhrase inboundPhrase = new InboundPhrase();        
        try
        {   
            inboundPhrase = JsonParser.deserializedWithRelationship(is, InboundPhrase.class, RelationCategory.class);               
            if(inboundCategory != null)
            {
                Category category = null;
                Phrase phrase = null;
                try
                {   
                    long id = Long.parseUnsignedLong(inboundPhrase.getProfileId());                    
                    PhraseParser parser = new PhraseParser();                    
                    // parse-validate against pattern and trim phrase text, eg. : word [] [] etc.,  [] word etc.
                    String parsed = parser.parsed(inboundPhrase.getPhrase());
                    inboundPhrase.setPhrase(parsed);
                    Profile existsProfile = profileService.find(id);
                    if(existsProfile == null)
                    {           
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                        .setDetail("Profile id does not exist : "+ id)
                        .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                        return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
                    }                    
                    if(ps.existsPhraseProfile(inboundPhrase.getPhrase(),id))
                    {
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                        .setDetail("Profile id <" + inboundPhrase.getProfileId() + "> and phrase exist : "+ inboundPhrase.getPhrase())
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                  
                    }
                    
                    inboundPhrase.setId(UUID.randomUUID().toString());
                    List<Category> categories = new ArrayList();                    
                    for(RelationCategory rc : inboundPhrase.getCategories())
                    {                        
                        category = cs.find(Long.parseLong(rc.getId()));
                        if(category == null)
                        {           
                            errorMessage = new ErrorUtils().new ErrorMessage()
                            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                            .setDetail("The requested category id was not found : "+ rc.getId())
                            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
                        }                         
                        categories.add(category);                       
                    }
                                  
                    //set phrase entity properties from json
                    phrase = new Phrase();
                    phrase.setPhrase(inboundPhrase.getPhrase());
                    phrase.setGaps(parser.getGaps());
                    inboundPhrase.setGaps(String.valueOf(parser.getGaps()));                    
                    phrase.setProfile(existsProfile);
                    // temporarily set true
                    //phrase.setIsAdminAdded(true);
                    phrase.setIsApprovalPending(false);
                    //
                    Phrase p = ps.created(phrase);                    
                    for(Category c : categories)
                    {
                        Category cat = cs.find(c.getId());
                        PhraseCategory ps = new PhraseCategory();
                        ps.setPhrase(p);
                        ps.setCategory(cat);
                        ps.setPhraseCategoryPK(new PhraseCategoryPK(p.getId(),cat.getId()));
                        p.getPhraseCategory().add(ps);                        
                    }
                    p.setPhraseCategory(p.getPhraseCategory());
                    ps.update(p);
                    //update entity counter table
                    ecs.updateCounter(1, Phrase.class);                    
                    inboundPhrase.setId(String.valueOf(phrase.getId()));
                    
                } catch (NumberFormatException | PhraseParser.EmptyPhraseException | PhraseParser.PhraseParseException t)
                {
                    ejbContext.setRollbackOnly();
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(t.getMessage())
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
                }
                String jsonResponse = null;        
                jsonResponse = JsonParser.serializedWithRelatioship(inboundPhrase, InboundPhrase.class, RelationCategory.class);
                if(category != null)
                {
                    return Response.status(Response.Status.CREATED).entity(jsonResponse).build();   
                }
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());           
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
            
        }        
        return Response.noContent().build();
    }    
    
    /**
     * Endpoint to create and post an answer object.
     * @param httpServletRequest
     * @param is
     * @return Response in JSON API format 
     * @throws IOException
     * @throws IllegalAccessException 
     * @throws java.lang.reflect.InvocationTargetException 
     */
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/answers")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response createAnswer(@Context HttpServletRequest httpServletRequest, InputStream is) throws IOException, IllegalAccessException, InvocationTargetException, InvalidArgumentException             
    {   
        // CHECK AUTHORIZATION HEADER AND ITS FORMAT
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }        
        
        InboundAnswer inboundAnswer = new InboundAnswer();                
        try
        {   
            // DESERIALIZE JSON INPUT INTO INBOUND ANSWER POJO
            inboundAnswer = JsonParser.deserializedWithRelationship(is, InboundAnswer.class, RelationGaps.class);                            
            if(inboundAnswer != null)
            {
                Phrase phrase = null;
                Answer answer  = new Answer();                
                try
                {   
                    long profileId = profile.getId();                    
                    long phraseId = Long.parseUnsignedLong(inboundAnswer.getPhraseId());  
                    phrase = ps.find(phraseId);
                    if(!ps.exists(phraseId)) // PHRASE DOES NOT EXIST
                    {
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                        .setDetail("Phrase id does not exist : "+ phraseId)
                        .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                        return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                  
                    }         
                    
                    // EXTRACT WORDS FROM JSON ANSWER
                    inboundAnswer.setId(UUID.randomUUID().toString());                    
                    List<AnswerJson> jsonAnswers = new ArrayList();
                    AnswerJson answerJson = null;
                    
                    if(JsonParser.hasDuplicatesJsonTypeId(inboundAnswer.getGaps())){
                        throw new DuplicateJsonDocumentIdException("The gaps list contains duplicate id");
                    }                    
                    
                    List<ErrorMessage> errorMessages = new ArrayList<>();
                    String errorsJson = "";                    
                    for(RelationGaps gap : inboundAnswer.getGaps())
                    {   
                        answerJson = new AnswerJson();                        
                        String trimmed = gap.getWord().trim();
                        String replaced = trimmed.replaceAll("\\s{2,}", " ");
                        answerJson.word = replaced;                        
                        jsonAnswers.add(answerJson);                        
                        gap.setWord(replaced);
                        
                        //VALIDATE JSON ANSWER TEXT                                                
                        if(!PhraseParser.isJsonAnswerWordFormatValid(gap.getWord()))
                        {
                            
                            ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                            .setTitle("Invalid word format")
                            .setDetail("Word field value with gaps id [" + gap.getId() + "] must not contain brackets - []")
                            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_ANSWER_ATTRIBUTE_ERROR_CODE.getCode());
                            errorMessages.add(errorMessage);                                                                               
                        }
                    }
                       
                    // return error response of answer text is invalid
                    if(!errorMessages.isEmpty())
                    {                        
                        errorsJson = ErrorUtils.errors(errorMessages);                        
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorsJson).type(MediaType.APPLICATION_JSON).build(); 
                    }                      
                    
                    // CHECK ANSWER GAPS TO MATCH GIVEN WORDS COUNT
                    int noOfGaps = phrase.getGaps();
                    int wordCount = inboundAnswer.getGaps().size();
                    if(noOfGaps != wordCount){
                        throw new IncorrectWordGapsException("The phrase to answer contains "+ noOfGaps+" gaps. Given words : "+wordCount);
                    }
                    // SET ANSWER PROPERTIES
                    answer.setPhrase(phrase);
                    answer.setProfile(profile);
                    Gson gson = new Gson();
                    String answers = gson.toJson(jsonAnswers, ArrayList.class);
                    
                    // CHECK FOR DUPLICATE ANSWER(string) from same profile and phrase
                    if(as.existsAnswer(answers,phraseId,profileId)) // EXISTS PHRASE-PROFILE PAIR
                    {
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                        .setDetail(ErrorUtils.ErrorResponse.Detail.DUPLICATE_ANSWERS_FOUND)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(ErrorUtils.ErrorCode.DUPLICATE_ANSWERS.getCode());            
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                  
                    }                                        
                    answer.setAnswers(answers);                                        
                    answer = as.created(answer);
                    //update entit counter table
                    ecs.updateCounter(1, Answer.class);
                    
                    inboundAnswer.setId(String.valueOf(answer.getId()));                  
                    // INCREMENT ID VALUE BY ONE EACH TIME                    
                    int i = 1;
                    for(RelationGaps gap : inboundAnswer.getGaps())
                    {
                        gap.setId(String.valueOf(i));
                        i++;
                    }
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException | DuplicateJsonDocumentIdException | IncorrectWordGapsException t)
                {
                    ejbContext.setRollbackOnly();
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                    .setDetail(t.getMessage())
                    .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                
                }
                String jsonResponse = null;
                //jsonResponse = JsonParser.serializedWithRelatioship(inboundAnswer, InboundAnswer.class, RelationGaps.class);
                jsonResponse = JsonParser.serializedWithIncludedRelatioship(inboundAnswer, InboundAnswer.class, RelationGaps.class);
                if(!Utilities.isEmpty(jsonResponse))
                {
                    return Response.status(Response.Status.CREATED).entity(jsonResponse).build();   
                }
                return Response.noContent().build();
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            Logger.getLogger(NormalModeResource.class.getName()).log(Level.SEVERE, null, ex);            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                        
        }        
        return Response.noContent().build();
    }   
    
    /**
     * Returns all phrases in JSON API format.
     * @return Response in JSON API format
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     */
    //@GET
    //@Produces("application/vnd.api+json")
    //@Path("/phrases")
    //@RolesAllowed("completeit-admin")    
    public Response getAllPhrases() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, ParseException, NoSuchFieldException
    {   
        List<Phrase> phrases = ps.findAll(); 
        List<OutboundPhrase> jsonPhrases = new ArrayList(); 
        for (Phrase phr : phrases) {
            OutboundPhrase body = new OutboundPhrase(phr);
            jsonPhrases.add(body);
        }
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonPhrases, OutboundPhrase.class);
        String json = generator.writeStream();
        return Response.status(Response.Status.OK).entity(json).build();     
    }     
    
    //@GET
    //@Produces("application/vnd.api+json")
    //@Path("/answers")
    //@RolesAllowed("completeit-admin")
    public Response getAllAnswers() throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, PhraseParser.EmptyPhraseException, PhraseParser.PhraseParseException, InvalidJsonApiClassException, NoSuchFieldException
    {   
        List<Answer> answers = as.findAll();        
        String json = "\"data\": [";        
        List<OutboundAnswer> outboundAnswers = new ArrayList();
        int index = 0;
        int limit = answers.size() - 1;
        for (Answer answer : answers)
        {
            OutboundAnswer outboundAnswer = new OutboundAnswer(answer);            
            outboundAnswers.add(outboundAnswer);            
            if(index < limit)
            {
                json += ",";
            }
            index++;            
        }
        json += "]}";                        
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(outboundAnswers, AnsweredWord.class);
        json = generator.writeStream();                
        return Response.status(Response.Status.OK).entity(json).build();     
    }    
 
    
    @GET       
    @Path("/answers/{an_id}")
    @Produces("application/vnd.api+json")
    public Response getAnswer(@PathParam("an_id") long an_id) throws IOException
    {                  
        // Phrase not found, return no content                    
        Answer answer = as.find(an_id);
        if(answer == null)
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested answer id was not found : "+an_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }                      
        try{            
            InboundAnswer outboundAnswer = new InboundAnswer(answer);
            String jsonResponse = null;                        
            JsonApiStreamGenerator generator = new JsonApiStreamGenerator(outboundAnswer, RelationGaps.class);            
            jsonResponse = generator.writeStream();
            return Response.ok(jsonResponse).build();              
        } 
        catch (Exception ex)
        {
            String detail = "";
            if(ex instanceof JsonParseException)
            {
                Throwable cause = ex.getCause();
                detail = cause.getMessage();
            }
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());                
            if(ex instanceof JsonParseException)
            {
                errorMessage.setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT + " - answer json text < "+ answer.getAnswers() +" > : " + detail);
            }           
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
    } 
  
    
    /**
     * Returns all sentences (complete phrases with answers) by requested phrase id.
     * @param httpServletRequest
     * @param phrase_id requested uncompleted phrase
     * @param page requested page of results
     * @param size requested page size of results
     * @return
     * @throws IOException 
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException 
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyAnswersException 
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     */
    @GET
    @Path("/phrases/{phrase_id}/sentences")
    @Produces("application/vnd.api+json")    
    public Response getSentencesByPhrasePaginated(@Context HttpServletRequest httpServletRequest, 
            @PathParam("phrase_id") long phrase_id,
            @QueryParam("page") int page, @QueryParam("size") int size) 
            throws IOException, PhraseParser.EmptyPhraseException, PhraseParser.EmptyAnswersException, PhraseParser.PhraseParseException
    {
        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }        
        
        // PHRASE NOT FOUND                    
        Phrase phrase = ps.find(phrase_id);
        if(phrase == null)
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested phrase id was not found : "+ phrase_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }  
        
        try
        {              
            boolean isPaginationRequested = (page > 0 && size > 0);
            List<Answer> answers = new ArrayList();            
            long count = 0;            
            if(isPaginationRequested)
            {
                //answers = as.getAnswersByPhrasePaginated(phrase_id, page, size); 
                answers = as.getAnswersByPhrasePaginatedExcludeReported(phrase_id, profile.getId(), page, size); 
                
            }
            else if(!isPaginationRequested)
            {
                //answers = as.getAnswersByPhraseExcludeReported(phrase_id);
                answers = as.getAnswersByPhraseIdExcludeReported(phrase_id, profile.getId());
                
            }            

            String json = "";                                    
            List<OutboundProfileAnswer> jsonAnswers = new ArrayList();
            PhraseParser parser = new PhraseParser();
            Map<Profile, List<Answer>> map = new HashMap();            
            for (Answer answer : answers)
            {
                Set<AnswerProfileReport> reportsSet = answer.getReportsCollection();
                if(!reportsSet.contains(new  AnswerProfileReport(answer,profile)))
                {
                    List<Answer> setOfAnswers = new ArrayList();                    
                    if (!map.containsKey(answer.getProfile()))
                    {
                        setOfAnswers.add(answer);
                        map.put(answer.getProfile(), setOfAnswers);
                    }
                    else
                    {
                        map.get(answer.getProfile()).add(answer);
                    }
                }
            } 
            List<CompletePhrase> sentences = new ArrayList();   
            //long startTime  = System.nanoTime();
            for(Entry entry : map.entrySet())
            {                
                Profile pr = (Profile)entry.getKey();
                List<Answer> listOfAnswers = (List<Answer> )entry.getValue();                
                sentences = new ArrayList();                    
                for(Answer answer : listOfAnswers)
                {   
                    Answer refreshed = as.find(answer.getId());
                    CompletePhrase cp = new CompletePhrase(); 
                    cp.setId("");
                    cp.setIsCorrect(null);
                    cp.setLikes(String.valueOf(refreshed.getLikes()));
                    cp.setAnswerId(String.valueOf(refreshed.getId()));
                    parser.setPhrase(refreshed.getPhrase().getPhrase());
                    parser.setAnswers(refreshed.getAnswers());
                    cp.setClause(parser.compose());  
                    
                    boolean existsLike = als.existsLike(answer.getId(), profile.getId());                    
                    cp.setIsLiked("false");
                    if (existsLike)                                            
                    {
                        
                        cp.setIsLiked("true");
                    }
                    sentences.add(cp);
                }                               
                OutboundProfileAnswer opa = new OutboundProfileAnswer(pr, sentences);
                jsonAnswers.add(opa);                
            }            
            JsonApiStreamGenerator generator = null;
            if(!isPaginationRequested){
                generator = new JsonApiStreamGenerator(jsonAnswers, CompletePhrase.class);
            }
            else
            {
                //count = as.getCountAnswersByPhraseExcludeReported(phrase_id);
                count = as.getCountByPhraseExcludeReportingProfileAndManyReports(phrase_id,profile.getId());
                generator = new JsonApiStreamGenerator(jsonAnswers, CompletePhrase.class, count);
                //set pagination properties
                generator.page = page;
                generator.size = size;
                generator.linksBaseUrl = httpServletRequest.getRequestURL().toString();                
            }                       
            json = generator.writeStream(); 
            return Response.status(Response.Status.OK).entity(json).build(); 
        }
        catch (IllegalArgumentException 
                | IllegalAccessException | InvocationTargetException | IOException | NoSuchFieldException ex)
        {
            String detail = ex.getMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
            }
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                        
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
    }
    
        /**
     * Returns all phrases in given category id.
     * @param httpServletRequest
     * @param cat_id
     * @param page
     * @param size
     * @return Response in JSON API format
     * @throws IOException 
     */
    @GET       
    @Path("/categories/{cat_id}/phrases")
    @Produces("application/vnd.api+json")    
    public Response getPhrasesStatsByCategoryIdPaginated(@Context HttpServletRequest httpServletRequest, 
            @PathParam("cat_id") long cat_id, 
            @QueryParam("page") int page, @QueryParam("size") int size) throws IOException
    {           
        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }        
        
        // CATEGORY NOT FOUND            
        Category category = cs.find(cat_id);
        if(category == null)
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested category id was not found : "+cat_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }                      
        try
        {            
            // CHECK CATEGORY PHRASES COLLECTION, CHECK APPROVAL STATUS AND EXISTING ANSWER FOR PROFILE ID            
            boolean isPaginationRequested = (page>0 && size > 0);
            long count = 0; 
            List<Phrase> phrases = new ArrayList();
            if(!isPaginationRequested)
            {
                //phrases = ps.findAllByCategoryIdNotInAnswersUnpaginated(cat_id, profile.getId());
                
                //request to included answered phrase and profile submitting it.
                phrases = ps.findByCategoryIdUnPaginated(cat_id);
                
            }                                 
            else if(isPaginationRequested)
            {                
                //phrases = ps.findByCategoryIdNotInAnswersPaginatedOrderedByCreationDesc(cat_id, profile.getId(), page, size);                
                
                //request to included answered phrase and profile submitting it.
                phrases = ps.findByCategoryIdOrderedByCreationDescPaginated(cat_id, page, size);                
            }            
            
            List<OutboundStatisticsPhrase> jsonStatsPhrases = new ArrayList(); 
            for (Phrase phrase : phrases)
            {   
                StatisticsItem stats = new StatisticsItem();
                OutboundStatisticsPhrase statisticsPhrase = null;
                stats.setId("1");
                String males = String.format(Locale.ROOT, "%.2f", ps.getMaleCompletionPercentage(phrase.getId()));
                stats.setMales(males);
                String females = String.format(Locale.ROOT, "%.2f", ps.getFemaleCompletionPercentage(phrase.getId()));
                stats.setFemales(females);
                stats.setAverageAge(String.valueOf(ps.getMeanAgePerPhrase(phrase.getId())));
                stats.setCompletions(String.valueOf(ps.getTotalAnswersPerPhrase(phrase.getId())));
                statisticsPhrase = new OutboundStatisticsPhrase(phrase, stats);
                jsonStatsPhrases.add(statisticsPhrase);
            }                        
            
            JsonApiStreamGenerator generator  = null;
            if(!isPaginationRequested)
            {
                generator = new JsonApiStreamGenerator(jsonStatsPhrases, StatisticsItem.class);
            }
            else{
                //count = ps.getCountByCategoryIdNotInAnswersUnpaginated(cat_id, profile.getId());
                //request to included answered phrase and profile submitting it.
                count = ps.getCountByCategoryId(cat_id);
                generator = new JsonApiStreamGenerator(jsonStatsPhrases, StatisticsItem.class, count);
                //set page, size, and links base url properties
                generator.page = page;
                generator.size = size;
                generator.linksBaseUrl = httpServletRequest.getRequestURL().toString();                      
            }
            String json = generator.writeStream();
            return Response.status(Response.Status.OK).entity(json).type(MediaType.APPLICATION_JSON).build();             
            
        } catch (ParseException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | IOException | NoSuchFieldException ex){
            String detail = ex.getMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
            }
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
    } 
        
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/share")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response share(@Context HttpServletRequest httpServletRequest, InputStream is)    
    {
        try 
        {
            String authorization = httpServletRequest.getHeader("Authorization");
            Response response = authorizationService.authorized(authorization);
            Profile profile = null;
            if(authorizationService.isAuthorized() && response == null)
            {
                profile = authorizationService.getProfile();
            }
            else if(!authorizationService.isAuthorized() && response != null)
            {
                return response;
            }            
            
            // get Shared JSON from input's body
            Shared shared = new Shared();
            shared = JsonParser.deserialized(is, Shared.class);
            
            //validate
            if(null != shared)
            {                
                ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
                Validator validator = vf.getValidator();             
                Set<ConstraintViolation<Shared>> violations = validator.validate(shared);
                List<ErrorUtils.ErrorMessage> errorMessages = new ArrayList<>(); 
                if(!violations.isEmpty())
                {                    
                    for ( ConstraintViolation<Shared> v : violations){                      
                        
                        Map<String,Object> map = v.getConstraintDescriptor().getAttributes();
                        String title = (String)map.get("title");
                        String message = (String)map.get("message");   
                        String code = (String)map.get("code");  
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(title)
                        .setDetail(message)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(Integer.parseInt(code));            
                        errorMessages.add(errorMessage);                        
                    }                    
                    if(errorMessages.size() > 0){                                                
                        return Response.status(Response.Status.BAD_REQUEST).entity(ErrorUtils.errors(errorMessages)).type(MediaType.APPLICATION_JSON).build();                         
                    }                    
                }                
            }
            ReferredUser ru = new ReferredUser();
            shared.setId("");
            ru.setProfileId(profile.getId());
            ru.setEmail(shared.getEmail());
            SharedContent content = new SharedContent();
            if(shared.getPhraseid() > 0 || shared.getAnswerid()> 0 || shared.getScore()> 0)
            {
                //create share content db entry                                 
                if(shared.getPhraseid()> 0)
                {
                    if(ps.find(shared.getPhraseid()) == null){
                        throw new InvalidArgumentException("Phrase id does not exist: "+shared.getPhraseid());
                    }                    
                    content.setPhraseid(shared.getPhraseid());
                }
                if(shared.getAnswerid()> 0)
                {
                    if(as.find(shared.getAnswerid()) == null){
                        throw new InvalidArgumentException("Answer id does not exist: "+shared.getAnswerid());
                    }                    
                    content.setAnswerid(shared.getAnswerid());
                }
                if(shared.getScore()> 0)
                {
                    content.setScore(shared.getScore());
                }
            }             
            ReferredUser ref = referredUserService.findByProfileIdAndEmail(profile.getId(), shared.getEmail());
            if(ref == null)
            {
                ref = referredUserService.created(ru);

            }            

            content.setReferredUser(ref);
            referredUserService.addSharedContent(ref, content);         
            shared.setUserid(profile.getId());
            return Response.status(Response.Status.CREATED).entity(Utilities.jsonApiSerialized(shared, Shared.class)).build();
        }
        catch(InvalidArgumentException | InvalidJsonFormatException | NumberFormatException | IllegalAccessException | JsonProcessingException ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage();
            String detail = ex.getMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }
            errorMessage.setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail).setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();              
        }
    }
    
    @POST        
    @Produces("application/vnd.api+json")
    @Path("/invite")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response invite(@Context HttpServletRequest httpServletRequest, InputStream is)    
    {
        try 
        {
            String authorization = httpServletRequest.getHeader("Authorization");
            Response response = authorizationService.authorized(authorization);
            Profile profile = null;
            if(authorizationService.isAuthorized() && response == null)
            {
                profile = authorizationService.getProfile();
            }
            else if(!authorizationService.isAuthorized() && response != null)
            {
                return response;
            }            
            
            // get Invite JSON from input's body
            InviteJson inviteJson = new InviteJson();
            inviteJson = JsonParser.deserialized(is, InviteJson.class);
            
            //validate
            if(null != inviteJson)
            {                
                ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
                Validator validator = vf.getValidator();             
                Set<ConstraintViolation<InviteJson>> violations = validator.validate(inviteJson);
                List<ErrorUtils.ErrorMessage> errorMessages = new ArrayList<>(); 
                if(!violations.isEmpty())
                {                    
                    for ( ConstraintViolation<InviteJson> v : violations){                      
                        
                        Map<String,Object> map = v.getConstraintDescriptor().getAttributes();
                        String title = (String)map.get("title");
                        String message = (String)map.get("message");   
                        String code = (String)map.get("code");  
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(title)
                        .setDetail(message)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(Integer.parseInt(code));            
                        errorMessages.add(errorMessage);                        
                    }                    
                    if(errorMessages.size() > 0){                                                
                        return Response.status(Response.Status.BAD_REQUEST).entity(ErrorUtils.errors(errorMessages)).type(MediaType.APPLICATION_JSON).build();                         
                    }                    
                }                
            }
            Invite invite = new Invite();
            inviteJson.setId("");
            invite.setProfileId(profile.getId());
            invite.setEmail(inviteJson.getEmail());                        
            if(inviteService.findByProfileIdAndEmail(profile.getId(), inviteJson.getEmail()) == null)
            {
                Invite inv = inviteService.created(invite); 
                inviteJson.setUserid(profile.getId());
                return Response.status(Response.Status.CREATED).entity(Utilities.jsonApiSerialized(inviteJson, InviteJson.class)).build();                                
            }
            else
            {
                return Response.status(Response.Status.NO_CONTENT).build();
            }

        }
        catch(InvalidJsonFormatException | NumberFormatException | IllegalAccessException | JsonProcessingException ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage();
            String detail = ex.getMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }
            errorMessage.setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail).setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();              
        }  
        
    }      
    
      
    @GET
    @Path("/cultures")
    @Produces("application/vnd.api+json")
    public Response getAllCultures(@Context HttpServletRequest httpServletRequest, @QueryParam("name") String name, @QueryParam("iso") String iso,@QueryParam("code") String code)
    {
        try
        {            
            Map<String,String[]> map = httpServletRequest.getParameterMap();
            String json = "";
            if(map.isEmpty())
            {
                List<Culture> cultures = cultureService.findAll();
                List<OutboundCulture> jsonCultures = new ArrayList();         
                for (Culture c : cultures) {
                    OutboundCulture oc = new OutboundCulture(c);
                    jsonCultures.add(oc);
                }       
                JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonCultures, null); 
                json = generator.writeStream();   
            }
            else
            {
                String nameParam = httpServletRequest.getParameter("name");
                String isoParam = httpServletRequest.getParameter("iso");
                String codeParam = httpServletRequest.getParameter("code");
                String paramValue = "";
                Culture culture = null;
                if(!Strings.isNullOrEmpty(nameParam) && Strings.isNullOrEmpty(isoParam) && Strings.isNullOrEmpty(codeParam))
                {
                    culture = cultureService.findByName(nameParam);
                    paramValue = nameParam;
                }
                else if(!Strings.isNullOrEmpty(isoParam) && Strings.isNullOrEmpty(nameParam) && Strings.isNullOrEmpty(codeParam))
                {
                   culture = cultureService.findByIso(isoParam); 
                   paramValue = isoParam;
                }
                else if(!Strings.isNullOrEmpty(codeParam) && Strings.isNullOrEmpty(isoParam) && Strings.isNullOrEmpty(nameParam))
                {
                    culture = cultureService.findByCode(codeParam);
                    paramValue = codeParam;
                }                
                
                if(culture == null)
                {           
                    errorMessage = new ErrorUtils().new ErrorMessage()
                    .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
                    .setDetail("The requested culture parameter value was not found : "+paramValue)
                    .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
                    return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
                }
                OutboundCulture oc = new OutboundCulture(culture); 
                JsonApiStreamGenerator generator = new JsonApiStreamGenerator(oc, null); 
                json = generator.writeStream();                 
            }
            return Response.status(Response.Status.OK).entity(json).build();         
        }
        catch (Exception ex)
        {
            String detail = ex.getMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
            }
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();               
        }
    }

}
