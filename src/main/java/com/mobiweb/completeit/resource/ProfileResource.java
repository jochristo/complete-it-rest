/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import com.mobiweb.completeit.annotations.PATCH;
import com.mobiweb.completeit.common.MemCached;
import com.mobiweb.completeit.common.PBKDF2SHA1;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.InvalidJsonFormatException;
import com.mobiweb.completeit.domain.Location;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.json.InboundProfile;
import com.mobiweb.completeit.json.InboundScore;
import com.mobiweb.completeit.json.JsonApiStreamGenerator;
import com.mobiweb.completeit.json.OutboundProfile;
import com.mobiweb.completeit.json.UpdatableProfile;
import com.mobiweb.completeit.oauth.TokenInfo;
import com.mobiweb.completeit.service.AuthorizationService;
import com.mobiweb.completeit.service.ProfileService;
import java.lang.reflect.InvocationTargetException;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import com.mobiweb.completeit.json.ErrorUtils;
import com.mobiweb.completeit.service.EntityCounterService;
import net.spy.memcached.MemcachedClient;

/**
 * Provides services to register and update profiles resources.
 * @author ic
 */
@Path("/profiles")
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ProfileResource {   
   
    // inject local requestedProfile service bean
    private @EJB ProfileService profileService;      
    private @EJB AuthorizationService authorizationService; 
    private @Context ServletContext context;
    private @Resource EJBContext ejbContext;      
    private String imagesPath = null;    
    private final MemcachedClient cache;     
    private ErrorUtils.ErrorMessage errorMessage = null;
    private @EJB EntityCounterService ecs;

    public ProfileResource() throws IOException {
        cache = MemCached.getClient();        
    }
    
    /**
     * Returns all profiles in JSON format.
     * @param httpServletRequest
     * @return
     * @throws IOException 
     * @throws java.lang.IllegalAccessException 
     * @throws java.lang.NoSuchFieldException 
     * @throws java.lang.reflect.InvocationTargetException 
     */
    @GET
    @Produces("application/vnd.api+json")
    @Path("/secured")
    //@RolesAllowed("completeit-admin")
    public Response getAll(@Context HttpServletRequest httpServletRequest) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
    {
        List<Profile> profiles = profileService.findAll();
        List<OutboundProfile> list = new ArrayList();
        for(Profile profile : profiles){
            OutboundProfile body = new OutboundProfile(profile);
            list.add(body);
        }
        JsonApiStreamGenerator generator = new JsonApiStreamGenerator(list, null);
        return Response.ok(generator.writeStream()).build(); 
    }

    @GET   
    @Path("/")
    @Produces("application/vnd.api+json")        
    public Response get(@Context HttpServletRequest httpServletRequest) throws IOException
    {                  
        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }       
        
        try
        {
            OutboundProfile body = new OutboundProfile(profile);
            String jsonResponse = null;
            jsonResponse = Utilities.jsonApiSerialized(body, OutboundProfile.class);
            return Response.ok(jsonResponse).build();                          
        }
        catch (UnknownHostException | SocketException | IllegalAccessException | JsonProcessingException ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
    }  
    
    /**
     * Deletes  a profile resource.
     * @param id The profile resource id to search and delete.
     * @param httpServletRequest
     * @return 
     */
    //@DELETE
    //@Consumes("application/vnd.api+json")
    //@Produces("application/vnd.api+json")
    //@Path("/{id}")
    //@Path("/secured/{id}")
    //@RolesAllowed("completeit-admin")
    public Response delete(@PathParam("id") long id,@Context HttpServletRequest httpServletRequest)
    {
        /*
        // check for correct Accept header
        String acceptHeader = httpServletRequest.getHeader("Accept");
        if(!acceptHeader.equals("application/vnd.api+json"))
        {        
            // responde according to JSON API 1.0 spec with 406 code.
            return  Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }        
        */
        
        // SEARCH profile ID
        Profile p = profileService.find(id);
        if(p == null)
        {
            // profile not found, return no content
            return Response.status(Response.Status.NOT_FOUND).build();
        }         
        
        try
        {            
            profileService.remove(p);
            //update entity counter table
            ecs.updateCounter(-1, Profile.class);
            if(profileService.contains(p))
            {   
                // deletion success, return no content status.
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        }
        catch(IllegalArgumentException ex)
        {
            return Response.status(Response.Status.NOT_FOUND).build(); 
        }            
        
        return Response.status(Response.Status.NOT_FOUND).build();
    }    
    
    /**
     * Returns  context's parameter holding the absolute path to profile images upload server location.
     * @return String
     */
    private String getProfileImagesAbsolutePath()
    {
        imagesPath = System.getProperty("catalina.base").concat((String)context.getInitParameter("Profile.Images.Path"));                
        if(!imagesPath.isEmpty())
        {
            File f = new File(imagesPath).getAbsoluteFile();              
            return f.getAbsolutePath();
        }
        return null;
    }

    
    /**
     * Endpoint to update-patch profile password.
     * @param id
     * @param httpServletRequest
     * @param old_password
     * @param new_password
     * @param repeat_password
     * @param is
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws URISyntaxException
     * @throws ParseException 
     */
    @PATCH
    @Produces("application/vnd.api+json")
    @Path("password/{id}")
    public Response updatePassword(@PathParam("id") long id, @Context HttpServletRequest httpServletRequest, 
            @FormParam("old_password") String old_password, 
            @FormParam("new_password") String new_password, 
            @FormParam("repeat_password") String repeat_password,InputStream is) 
            throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, IllegalAccessException, URISyntaxException, ParseException
    {
        
        //CHECK AUTHORIZATION FIRST
        String authorization = httpServletRequest.getHeader("Authorization");
        if(authorization == null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.AUTHORIZATION_HEADER_MISSING)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        
        boolean isAuthorizationHeaderValid = authorization.matches("^Bearer \\w+$");
        if(!isAuthorizationHeaderValid)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_AUTHORIZATION_FORMAT)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        
        // CHECK FORM PARAMS FORMAT
        if(old_password == null || old_password.isEmpty()){              
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)                    
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                                     
        }
        else if(new_password == null || new_password.isEmpty()){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        else if(repeat_password == null || repeat_password.isEmpty()){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_REQUEST)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }

        //CHECK NEW AND REPEAT PASSWORDS
        if(!Utilities.isEqual(new_password,repeat_password))
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.PASSWORD_MISMATCH)
            .setDetail(ErrorUtils.ErrorResponse.Detail.PASSWORD_MISMATCH_DETAILS)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.PASSWORD_MISMATCH.getCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        
        //CHECK GIVEN PROFILE ID
        Profile p = profileService.find(id);
        if(p == null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail(ErrorUtils.ErrorResponse.Detail.PROFILE_NOT_FOUND)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }        
        
        // LOOK IT UP IN MEMCACHED       
        String parsed[] = authorization.split("Bearer ");
        authorization = parsed[1];
        Gson gson = new Gson();                  
        Object value = null;
        try{
            value = cache.get(authorization);
        }
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }            
        
        if(value==null)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        
	TokenInfo tokenInfo = gson.fromJson(value.toString(), TokenInfo.class); 
        value = tokenInfo.getAccess_token();      
        if(value == null || !value.equals(authorization))
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }         
        
        String profileId = String.valueOf(p.getId());
        String tokenInfoId = tokenInfo.getId();
        if(profileId.compareTo(tokenInfoId) != 0)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.UNAUTHORIZED_ACCESS)
            .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_GRANT)
            .setHttpStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());            
            return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                              
        }        
                
        // CHECK GIVEN PASSWORD
        boolean isPasswordValid = PBKDF2SHA1.isPasswordValid(old_password,p.getPassword());        
        if(!isPasswordValid){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.WRONG_PASSWORD_DETAILS)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INCORRECT_PASSWORD.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        else{
            
            String hashed = PBKDF2SHA1.getHash(new_password);
            p.setPassword(hashed);       
        }
        
        
        Profile result = profileService.updated(p);         
        String cachedProfile = String.valueOf(result.getId()); 
        String profileCopyKey = cachedProfile.concat(result.getEmail());
        try
        {
            //update memcache profile                       
            //cache.set(cachedProfile, 86400, result);             
            cache.set(profileCopyKey, 86400, result); 
        }   
        catch(net.spy.memcached.OperationTimeoutException ote)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ote.getCause().toString().concat(" ".concat(ote.getMessage())))
            .setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }        
        OutboundProfile jp = new OutboundProfile(p);
        String jsonResponse = Utilities.jsonApiSerialized(jp, OutboundProfile.class);          
        if(result != null)
        {
            URI locationUri = new URI("profiles/"+String.valueOf(result.getId()));
            return Response.ok().header("Location", locationUri).entity(jsonResponse).build();
        }        
        return Response.noContent().build();       
    } 
    
    //@POST
    //@Path("/{id}")
    //@Consumes({MediaType.MULTIPART_FORM_DATA, "application/vnd.api+json"})    
    //@Produces("application/vnd.api+json")
    public Response uploadProfilePicture(@FormParam("stream") InputStream inputStream,@Context HttpHeaders httpHeaders,
            @PathParam("id") long id,@Context HttpServletRequest httpServletRequest) throws IllegalAccessException, JsonProcessingException
    {
        // check for correct Accept header
        String acceptHeader = httpServletRequest.getHeader("Accept");
        if(!acceptHeader.equals(MediaType.MULTIPART_FORM_DATA))
        {        
            // responde according to JSON API 1.0 spec with 406 code.            
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
        
        Profile p = profileService.find(id);
        if(p == null)
        {
            // profile not found, return no content            
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }                
        
        // GET FILE FROM POST RESUEST, SAVE TO SERVER, UPDATE PATH TO DATABASE
        String nickname  = p.getNickname();
        MultivaluedMap headers = httpHeaders.getRequestHeaders();
        String filename = Utilities.getFileName(headers);
        String fullImagePath = getProfileImagesAbsolutePath().concat("\\").concat(nickname).concat(filename);        
        Utilities.writeToFile(inputStream, fullImagePath);
        p.setPictureFilepath((String)context.getInitParameter("Profile.Images.Path").concat("/").concat(nickname).concat(filename));
        Profile updated = profileService.updated(p);
        
        // JSON serializer - message body write 
        InboundProfile jp = new InboundProfile(p);           
        String jsonResponse = null;
        jsonResponse = Utilities.jsonApiSerialized(jp, InboundProfile.class);          
        if(updated != null)
        {
            try {                    
                return Response.created(new URI("profiles/"+String.valueOf(updated.getId()))).entity(jsonResponse).build();
            }  catch (URISyntaxException ex) {
                
               }
        }               

        return Response.status(Response.Status.BAD_REQUEST).build();
    }   
    
    @PATCH    
    @Produces("application/vnd.api+json")
    @Path("/{id}")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response patch(@PathParam("id") long id, @Context HttpServletRequest httpServletRequest, InputStream is) 
            throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException, MalformedURLException, URISyntaxException, IOException, IllegalAccessException 
    {   
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }        
        
        // PROCESS RAW JSON INPUT STREAM
        UpdatableProfile updatable = new UpdatableProfile();        
        String base64EndodedString = null;
        List<ErrorUtils.ErrorMessage> errorMessages = new ArrayList<>(); 
        try
        {            
            //de-serialize input stream into json profile            
            updatable = Utilities.deserialized(is, UpdatableProfile.class);            
            if(updatable != null)
            {
                // validate json string for required attribute values
                ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
                Validator validator = vf.getValidator();             
                Set<ConstraintViolation<UpdatableProfile>> violations = validator.validate(updatable); 
                if(!violations.isEmpty())
                {                    
                    for ( ConstraintViolation<UpdatableProfile> v : violations){                        
                        Map<String,Object> map = v.getConstraintDescriptor().getAttributes();
                        String title = (String)map.get("title");
                        String message = (String)map.get("message");
                        String code = (String)map.get("code");                        
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(title)
                        .setDetail(message)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(Integer.parseInt(code));            
                        errorMessages.add(errorMessage);                        
                    }                    
                    if(errorMessages != null){                        
                        String errorsJson = ErrorUtils.errors(errorMessages);
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorsJson).type(MediaType.APPLICATION_JSON).build();                         
                    } 
                }
            }
        }
        catch (InvalidJsonFormatException ex)
        {   
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }       
                
        // PROCESS PROFILE IMAGE STRING
        boolean isProfilePictureNull = Utilities.isEmpty(updatable.getPictureEncodedString());        
        String profileImagePath = null;
        String imageFilename = null;                
        if(!isProfilePictureNull)            
        {   
            profileImagePath = getProfileImagesAbsolutePath() + "\\" + Utilities.getPath(updatable.getNickname().toLowerCase()) + "\\";                           
            String random = Utilities.randomFilename(updatable.getNickname());            
            imageFilename = random + ".png";
            boolean existsFile = new File(profileImagePath,imageFilename).exists();  
            while(existsFile){
                random = Utilities.randomFilename(updatable.getNickname());
                imageFilename = random + ".png";
                existsFile = new File(profileImagePath,imageFilename).exists(); 
            }
            String relativePath = new File(getProfileImagesAbsolutePath()).toURI().relativize(new File(profileImagePath,imageFilename).toURI()).getPath();                    
            profile.setPictureFilepath("/".concat(relativePath)); 
            base64EndodedString = updatable.getPictureEncodedString();            
        }        
        
        // CHECK FOR DUPLICATE EMAIL/NICKNAME 
        List<Profile> emailMatches = profileService.searchByEmail(updatable.getEmail());
        List<Profile> nicknameMatches = profileService.searchByNickname(updatable.getNickname());
        boolean isEmailMatch = emailMatches != null && emailMatches.size() >= 1  ;
        boolean isNicknameMatch = nicknameMatches != null && nicknameMatches.size() >= 1  ;
        boolean isDifferentProfileEmail = emailMatches.size() == 1 && emailMatches.get(0).getId() != id;
        boolean isDifferentProfileNickname = nicknameMatches.size() == 1 && nicknameMatches.get(0).getId() != id;
        if(isEmailMatch && isDifferentProfileEmail)
        {              
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.DUPLICATE_EMAIL_FOUND)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.DUPLICATE_EMAIL.getCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        //if(isNicknameMatch && p.getId() != id)
        if(isNicknameMatch && isDifferentProfileNickname)            
        {            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.DUPLICATE_NICKNAME_FOUND)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.DUPLICATE_NICKNAME.getCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                          
        } 
        
        // UPDATE PROFILE AND PERSIST 
        profile.setNickname(updatable.getNickname());
        profile.setLanguage(updatable.getLanguage());
        profile.setEmail(updatable.getEmail());        
        double longitude = (!Utilities.isEmpty(updatable.getLongitude())) ? Double.parseDouble(updatable.getLongitude()) : 0;
        double latitude = (!Utilities.isEmpty(updatable.getLatitude())) ? Double.parseDouble(updatable.getLatitude()) : 0;
        profile.setLocation(new Location(longitude,latitude));        
        Date date = Utilities.convertToDate(updatable.getDob());
        profile.setDob(date);        
        profile.setMobileNo(updatable.getMobileNo());
        profile.setIsMobileNoVerified(Boolean.valueOf(updatable.getIsMobileNoVerified()));               
        profile.setGender(updatable.getGender());
        
        Profile result = null;
        try {            
            result = profileService.updated(profile);                    
            
            // DELETE PROFILE PICTURE FROM FILE SYSTEM IF NOT PRESENT IN JSON STREAM
            // CHOOSE NOT TO DELETE FILE FROM SERVER, SET ENTITY VALUE ONLY TO NULL
            if(isProfilePictureNull){
                profileImagePath = getProfileImagesAbsolutePath() + "\\" +profile.getPictureFilepath();            
                //boolean isDeleted  = Utilities.deleteFile(profileImagePath);
                //if(isDeleted){
                    //profile.setPictureFilepath(null);
                    Profile updated = profileService.find(result.getId());
                    updated.setPictureFilepath(null);
                    result = profileService.updated(updated); 
                //}                
            }
                        
            //update memcache profile
            String cachedProfile = String.valueOf(result.getId());
            String profileCopyKey = cachedProfile.concat(result.getEmail());            
            cache.set(profileCopyKey, 86400, result);         
            
        } catch (Exception t)
        {
            String detail = t.getMessage();
            if(t instanceof net.spy.memcached.OperationTimeoutException){
                detail = t.getCause().toString().concat(" ".concat(t.getMessage()));
            }            
            ejbContext.setRollbackOnly();
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
            if(t instanceof net.spy.memcached.OperationTimeoutException){
                errorMessage.setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode());
            }
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }

        // CREATE PROFILE IMAGE FILE IN FILE SYSTEM      
        if(!isProfilePictureNull)            
        {
            try{
                BufferedImage image = Utilities.decodeToImage(base64EndodedString); 
                Utilities.writeImageToFile(image, profileImagePath,imageFilename);
            } catch (IllegalArgumentException ex)
            {                
                // rollback the update process, invalid image found
                ejbContext.setRollbackOnly();
                errorMessage = new ErrorUtils().new ErrorMessage()
                .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                .setDetail(ex.getMessage())
                .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                 
            }            
        }
        
        // SUCCESS + RESPONSE         
        OutboundProfile jp = new OutboundProfile(profile);
        String jsonResponse = null;
        jsonResponse = Utilities.jsonApiSerialized(jp, OutboundProfile.class);          
        if(result != null)
        {
            URI locationUri = new URI("profiles/"+String.valueOf(result.getId()));
            return Response.ok().header("Location", locationUri).entity(jsonResponse).build();
        }
        
        return Response.noContent().build();
    }   
    
    /**
     * Creates new Profile from given request.
     * @param httpServletRequest
     * @param is
     * @return Response 
     * @throws java.security.NoSuchAlgorithmException 
     * @throws java.security.spec.InvalidKeySpecException 
     * @throws java.text.ParseException 
     * @throws java.net.MalformedURLException 
     * @throws java.net.URISyntaxException 
     * @throws java.lang.IllegalAccessException 
     */
    @POST        
    @Produces("application/vnd.api+json")
    @Path("")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response create(@Context HttpServletRequest httpServletRequest, InputStream is) 
            throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException, MalformedURLException, URISyntaxException, IOException, IllegalAccessException 
    {   
        InboundProfile inboundJsonProfile = new InboundProfile();                        
        String errorsJson = "";
        List<ErrorUtils.ErrorMessage> errorMessages = new ArrayList<>();             
        try
        {
            // deserialize json string into Profile object                         
            inboundJsonProfile = Utilities.deserialized(is, InboundProfile.class);                        
            if(inboundJsonProfile != null)
            {
                // validate json string for required attribute values
                ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
                Validator validator = vf.getValidator();             
                Set<ConstraintViolation<InboundProfile>> violations = validator.validate(inboundJsonProfile); 
                if(!violations.isEmpty())
                {                    
                    for ( ConstraintViolation<InboundProfile> v : violations){                      
                        
                        Map<String,Object> map = v.getConstraintDescriptor().getAttributes();
                        String title = (String)map.get("title");
                        String message = (String)map.get("message");   
                        String code = (String)map.get("code");  
                        ErrorUtils.ErrorMessage errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(title)
                        .setDetail(message)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(Integer.parseInt(code));            
                        errorMessages.add(errorMessage);                        
                    }                    
                    if(errorMessages != null){                        
                        errorsJson = ErrorUtils.errors(errorMessages);
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorsJson).type(MediaType.APPLICATION_JSON).build();                         
                    }                    
                }
            }
        }
        catch (InvalidJsonFormatException ex )
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
        
        // prepare profile picture processings
        boolean isProfilePictureNull = Utilities.isEmpty(inboundJsonProfile.getPictureEncodedString());
        Profile p = new Profile();
        String base64EndodedString = null;
        String profileImagePath = null;
        String imageFilename = null;        
        if(!isProfilePictureNull)
        {               
            profileImagePath = getProfileImagesAbsolutePath() + "\\" + Utilities.getPath(inboundJsonProfile.getNickname().toLowerCase()) + "\\";                           
            String random = Utilities.randomFilename(inboundJsonProfile.getNickname());            
            imageFilename = random + ".png";
            boolean existsFile = new File(profileImagePath,imageFilename).exists();  
            while(existsFile){
                random = Utilities.randomFilename(inboundJsonProfile.getNickname());
                imageFilename = random + ".png";
                existsFile = new File(profileImagePath,imageFilename).exists(); 
            }
            String relativePath = new File(getProfileImagesAbsolutePath()).toURI().relativize(new File(profileImagePath,imageFilename).toURI()).getPath();                    
            p.setPictureFilepath("/".concat(relativePath));                                    
            base64EndodedString = inboundJsonProfile.getPictureEncodedString();
        }
       
        // persist new Profile        
        p.setNickname(inboundJsonProfile.getNickname());
        p.setLanguage(inboundJsonProfile.getLanguage());
        p.setEmail(inboundJsonProfile.getEmail());        
        double longitude = (!Utilities.isEmpty(inboundJsonProfile.getLongitude())) ? Double.parseDouble(inboundJsonProfile.getLongitude()) : 0;
        double latitude = (!Utilities.isEmpty(inboundJsonProfile.getLatitude())) ? Double.parseDouble(inboundJsonProfile.getLatitude()) : 0;
        p.setLocation(new Location(longitude,latitude));        
        Date date = Utilities.convertToDate(inboundJsonProfile.getDob());
        p.setDob(date);        
        p.setMobileNo(inboundJsonProfile.getMobileNo());
        p.setIsMobileNoVerified(Boolean.valueOf(inboundJsonProfile.getIsMobileNoVerified()));             
        p.setGender(inboundJsonProfile.getGender());
        
        // hash password
        String password = inboundJsonProfile.getPassword();
        String hashed = PBKDF2SHA1.getHash(password);
        p.setPassword(hashed);       
                
        // check for duplicate email-nickname pair in database        
        boolean isEmailMatch = profileService.findSingleByEmail(p.getEmail()) != null;
        boolean isNicknameMatch = profileService.findSingleByNickname(p.getNickname())  != null;
        if(isEmailMatch){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.DUPLICATE_EMAIL_FOUND)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.DUPLICATE_EMAIL.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
        if(isNicknameMatch){
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ErrorUtils.ErrorResponse.Detail.DUPLICATE_NICKNAME_FOUND)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.DUPLICATE_NICKNAME.getCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }         

        if(isProfilePictureNull)
        {
            p.setPictureFilepath(null);
        }         
        
        Profile result = null;
        try
        {
            result = profileService.created(p);
            //update memcache profile
            String cachedProfile = String.valueOf(result.getId());
            String profileCopyKey = cachedProfile.concat(result.getEmail());         
            cache.set(profileCopyKey, 86400, result);  
            
            //update entity counter table
            ecs.updateCounter(1, Profile.class);
        }
        catch (Exception t)
        {  
            String detail = t.getMessage();
            if(t instanceof net.spy.memcached.OperationTimeoutException)
            {
                detail = t.getCause().toString().concat(" ".concat(t.getMessage()));
            }             
            
            ejbContext.setRollbackOnly();
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            if(t instanceof net.spy.memcached.OperationTimeoutException)
            {
                errorMessage.setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode());
            }            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build(); 
        }        
        
        // Create profile picture in server file system
        if(!isProfilePictureNull)
        {
            try{
                BufferedImage image = Utilities.decodeToImage(base64EndodedString); 
                Utilities.writeImageToFile(image, profileImagePath,imageFilename);
            } catch (IllegalArgumentException ex)
            {
                // rollback the update process, invalid image found
                ejbContext.setRollbackOnly();
                errorMessage = new ErrorUtils().new ErrorMessage()
                .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
                .setDetail(ErrorUtils.ErrorResponse.Detail.INVALID_IMAGE_STRING.concat(" : ").concat(ex.getMessage()))
                .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_IMAGE_FORMAT.getCode())
                .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());                
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();                
            }            
        }                
        
        OutboundProfile out = new OutboundProfile(p);
        String jsonResponse = null;        
        jsonResponse = Utilities.jsonApiSerialized(out, OutboundProfile.class);            
        if(result != null)
        {
            try{                  
                URI locationUri = new URI("profiles/"+String.valueOf(result.getId()));
                return Response.created(new URI("profiles/"+String.valueOf(result.getId())))
                    .header("Location", locationUri)
                    .entity(jsonResponse)
                    .build();
             } catch (URISyntaxException ex) {                
             }
        }        
        return Response.noContent().build();
    }     
    
    @PATCH    
    @Produces("application/vnd.api+json")
    @Path("/{id}/score")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response patchScore(@PathParam("id") long id, @Context HttpServletRequest httpServletRequest, InputStream is) 
            throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException, MalformedURLException, URISyntaxException, IOException, IllegalAccessException 
    {   
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }        
        
        // PROCESS RAW JSON INPUT STREAM
        InboundScore updatable = new InboundScore();                
        List<ErrorUtils.ErrorMessage> errorMessages = new ArrayList<>();
        long supplementalScore = 0;
        try
        {            
            //de-serialize input stream into json profile            
            updatable = Utilities.deserialized(is, InboundScore.class);            
            if(updatable != null)
            {
                // validate json string for required attribute values
                ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
                Validator validator = vf.getValidator();             
                Set<ConstraintViolation<InboundScore>> violations = validator.validate(updatable); 
                if(!violations.isEmpty())
                {                    
                    for ( ConstraintViolation<InboundScore> v : violations){                        
                        Map<String,Object> map = v.getConstraintDescriptor().getAttributes();
                        String title = (String)map.get("title");
                        String message = (String)map.get("message");
                        String code = (String)map.get("code");                        
                        errorMessage = new ErrorUtils().new ErrorMessage()
                        .setTitle(title)
                        .setDetail(message)
                        .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .setApplicationErrorCode(Integer.parseInt(code));            
                        errorMessages.add(errorMessage);                        
                    }                    
                    if(!Utilities.isEmpty(errorMessages)){                        
                        String errorsJson = ErrorUtils.errors(errorMessages);
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorsJson).type(MediaType.APPLICATION_JSON).build();                         
                    } 
                }
                supplementalScore = Long.parseLong(updatable.getScore());
            }
        }
        catch (InvalidJsonFormatException ex)
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT)
            .setDetail(ex.getMessage())
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }       

        // update SCORE                
        profile.setScore(profile.getScore() + supplementalScore);        
        Profile result = null;
        try
        {            
            result = profileService.updated(profile);
            //update memcache profile
            String cachedProfile = String.valueOf(result.getId());
            String profileCopyKey = cachedProfile.concat(result.getEmail());             
            cache.set(profileCopyKey, 86400, result);               
        }
        catch (Exception t)
        {
            String detail = t.getMessage();
            if(t instanceof net.spy.memcached.OperationTimeoutException){
                detail = t.getCause().toString().concat(" ".concat(t.getMessage()));
            }             
            ejbContext.setRollbackOnly();
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            if(t instanceof net.spy.memcached.OperationTimeoutException){
                errorMessage.setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode());
            }            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }                
        
        // SUCCESS + RESPONSE         
        OutboundProfile jp = new OutboundProfile(profile);
        String jsonResponse = null;
        jsonResponse = Utilities.jsonApiSerialized(jp, OutboundProfile.class);          
        if(result != null)
        {
            URI locationUri = new URI("profiles/"+String.valueOf(result.getId()));
            return Response.ok().header("Location", locationUri).entity(jsonResponse).build();
        }
        
        return Response.noContent().build();
    }     
    

    
}
