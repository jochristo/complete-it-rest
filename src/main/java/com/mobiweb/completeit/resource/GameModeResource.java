/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.resource;

import com.google.gson.Gson;
import com.mobiweb.completeit.annotations.PATCH;
import com.mobiweb.completeit.common.MemCached;
import com.mobiweb.completeit.common.PhraseParser;
import com.mobiweb.completeit.common.PhraseParser.EmptyAnswersException;
import com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException;
import com.mobiweb.completeit.common.PhraseParser.PhraseParseException;
import com.mobiweb.completeit.common.Utilities;
import com.mobiweb.completeit.common.exception.InvalidArgumentException;
import com.mobiweb.completeit.common.exception.InvalidJsonFormatException;
import com.mobiweb.completeit.common.exception.NoProfileAnswersException;
import com.mobiweb.completeit.domain.Answer;
import com.mobiweb.completeit.domain.AnswerProfileLike;
import com.mobiweb.completeit.domain.AnswerProfileReport;
import com.mobiweb.completeit.domain.Phrase;
import com.mobiweb.completeit.domain.Profile;
import com.mobiweb.completeit.json.CompletePhrase;
import com.mobiweb.completeit.json.JsonApiStreamGenerator;
import com.mobiweb.completeit.json.OutboundProfile;
import com.mobiweb.completeit.json.OutboundProfileAnswer;
import com.mobiweb.completeit.json.maps.googleapis.AddressComponent;
import com.mobiweb.completeit.json.maps.googleapis.Coordinates;
import com.mobiweb.completeit.service.AnswerFilterPredicate;
import com.mobiweb.completeit.service.AnswerLikeService;
import com.mobiweb.completeit.service.AnswerReportService;
import com.mobiweb.completeit.service.AnswerService;
import com.mobiweb.completeit.service.AuthorizationService;
import com.mobiweb.completeit.service.CategoryService;
import com.mobiweb.completeit.service.ProfileService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.mobiweb.completeit.json.ErrorUtils;
import net.spy.memcached.MemcachedClient;

/**
 * Restful endpoint to provide services to handle game mode requests.
 * @author ic
 */
@Path("game")
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class GameModeResource
{   
    // fields
    private @EJB CategoryService cs;
    private @EJB ProfileService profileService;
    private @EJB AnswerService as;
    private @EJB AuthorizationService authorizationService; 
    private ErrorUtils.ErrorMessage errorMessage;
    private final MemcachedClient cache;
    private @EJB AnswerLikeService als;
    private @EJB AnswerReportService ars;
    

    public GameModeResource() throws IOException {
        cache = MemCached.getClient();
    } 
    
    
    @GET
    @Path("/stats")
    @Produces("application/vnd.api+json")
    
    public Response getStats()
    {
        try
        {
            List<Profile> profiles = profileService.findTopTen();
            List<OutboundProfile> jsonProfiles = new ArrayList(); 
            for (Profile pr : profiles) {
                OutboundProfile body = new OutboundProfile(pr);
                jsonProfiles.add(body);
            }      
            JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonProfiles, null);        
            String json = generator.writeStream();        
            return Response.status(Response.Status.OK).entity(json).build();                        
        }
        catch (WebApplicationException | NoSuchFieldException | IOException | IllegalArgumentException | IllegalAccessException | InvocationTargetException  ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ex.getMessage())            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        } 
    }    
    
    
    
    @GET       
    @Path("/profiles/sentences")
    @Produces("application/vnd.api+json")    
    public Response getSentencesByProfilePaginated(@Context HttpServletRequest httpServletRequest,
        @QueryParam("page") int page, @QueryParam("size") int size) throws IOException
    {    
        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }  
                     
        try
        {  
            boolean isPaginationRequested = (page > 0 && size > 0);
            List<Answer> answers = new ArrayList();            
            long count = 0;
            if(isPaginationRequested)
            {
                answers = as.getAnswersByProfilePaginated(1, profile.getId(), page, size);                
            }
            else if(!isPaginationRequested)
            {
                answers = as.getAnswersByProfile(1 , profile.getId());
            }
            
            String json = "";            
            List<OutboundProfileAnswer> jsonAnswers = new ArrayList();
            if(answers.isEmpty()){                
                json = "{\n\"data\": []\n}";
                return Response.status(Response.Status.OK).entity(json).build();                 
            }
            
            List<CompletePhrase> sentences = new ArrayList();
            PhraseParser parser = new PhraseParser();
            for(Answer answer : answers )
            {
                parser.setPhrase(answer.getPhrase().getPhrase());
                parser.setAnswers(answer.getAnswers());
                String sentence = parser.compose();
                CompletePhrase cp = new CompletePhrase();
                cp.setClause(sentence);                
                cp.setLikes(String.valueOf(answer.getLikes()));
                sentences.add(cp);                                
            }
            
            OutboundProfileAnswer opa = new OutboundProfileAnswer(profile, sentences);
            jsonAnswers.add(opa);
            
            JsonApiStreamGenerator generator = null;
            if(!isPaginationRequested){
                generator = new JsonApiStreamGenerator(jsonAnswers, CompletePhrase.class);
            }
            else
            {
                count = as.getCountAnswersByProfile(profile.getId());
                generator = new JsonApiStreamGenerator(jsonAnswers, CompletePhrase.class, count);
                //set pagination properties
                generator.page = page;
                generator.size = size;
                generator.linksBaseUrl = httpServletRequest.getRequestURL().toString();                
            }            
            
            json = generator.writeStream();
            return Response.status(Response.Status.OK).entity(json).build();            
            
        } catch (EmptyPhraseException | EmptyAnswersException | PhraseParseException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | IOException | NoSuchFieldException ex){
            String detail = ex.getMessage();
            errorMessage = new ErrorUtils().new ErrorMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }   
            errorMessage
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }
    }        

    
    @PATCH
    @Path("/sentences/{answer_id}/like")
    @Produces("application/vnd.api+json")    
    public Response likeSentence(@Context HttpServletRequest httpServletRequest, @PathParam("answer_id") long answer_id)
    {
        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }  
        
        // ANSWER ID NOT FOUND
        if(!as.exists(answer_id))
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested answer id was not found : "+ answer_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }        
        try
        {            
            Answer answer = as.find(answer_id);
            if(answer != null)
            {
                
                AnswerProfileLike apl = als.find(answer_id, profile.getId());
                if(apl != null)
                {
                    if(!apl.isActive()){
                        //update isActive state
                        apl.setActive(true);
                        als.update(apl);
                        return Response.status(Response.Status.OK).build();
                    }else{
                        return Response.status(Response.Status.NO_CONTENT).build();
                    }                    
                }                
                else
                {
                // new many-to-many objects
                Set<AnswerProfileLike> likesSet =  answer.getLikesCollection();                                
                if(!likesSet.contains(apl))
                {
                    apl = new AnswerProfileLike(answer,profile);
                    long likes = answer.getLikes() + 1;
                    answer.setLikes(likes);                    
                    answer.getLikesCollection().add(apl);
                    answer.setLikesCollection(answer.getLikesCollection());
                    as.update(answer);
                    Profile pr = profileService.find(answer.getProfile().getId());
                    long score = pr.getScore();
                    pr.setScore(score + 5);
                    Profile updatedProfile = profileService.updated(pr);      
                    
                    //update memcache profile
                    String cachedProfile = String.valueOf(updatedProfile.getId());
                    String profileCopyKey = cachedProfile.concat(updatedProfile.getEmail());                    
                    cache.set(profileCopyKey, 86400, updatedProfile);                     
                    
                    return Response.status(Response.Status.OK).build();
                }
                }                
                return Response.status(Response.Status.NO_CONTENT).build();
            }            
        }
        catch (Exception ex)
        {
            String detail = ex.getMessage();
            errorMessage = new ErrorUtils().new ErrorMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }
            else if(ex instanceof net.spy.memcached.OperationTimeoutException){
                detail = ex.getCause().toString().concat(" ".concat(ex.getMessage()));
            }            
            errorMessage
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            if(ex instanceof net.spy.memcached.OperationTimeoutException){
                errorMessage.setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode());
            }             
            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        
        return null;
    }
    
    @PATCH
    @Path("/sentences/{answer_id}/unlike")
    @Produces("application/vnd.api+json") 
    public Response unlikeSentence(@Context HttpServletRequest httpServletRequest, @PathParam("answer_id") long answer_id)
    {        
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }         
        
        // ANSWER ID NOT FOUND
        if(!as.exists(answer_id))
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested answer id was not found : "+ answer_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }
        
        try
        {            
            Answer answer = as.find(answer_id);
            if(answer != null)
            {  
                // new many-to-many objects
                Set<AnswerProfileLike> likesSet =  answer.getLikesCollection();                            
                AnswerProfileLike apl = als.find(answer_id, profile.getId());
                if(apl == null)
                {
                    return Response.status(Response.Status.NO_CONTENT).build();                    
                }
                else
                {                
                    if(likesSet.contains(apl))
                    {
                        if(apl.isActive()) // is active, unset
                        {
                            long likes = answer.getLikes() > 0 ? answer.getLikes() - 1 : 0;
                            answer.setLikes(likes);
                            //answer.getLikesCollection().remove(apl);
                            as.update(answer);
                            // mark inactive instead
                            //als.remove(apl);                    
                            apl.setActive(false);
                            als.update(apl);
                            Profile pr = profileService.find(answer.getProfile().getId());
                            long score = pr.getScore()>=5 ? pr.getScore() - 5: 0;
                            pr.setScore(score);
                            Profile updatedProfile = profileService.updated(pr);      
                            
                            //update memcache profile
                            String cachedProfile = String.valueOf(updatedProfile.getId());
                            String profileCopyKey = cachedProfile.concat(updatedProfile.getEmail());             
                            cache.set(profileCopyKey, 86400, updatedProfile);
                    
                            return Response.status(Response.Status.OK).build();
                        }
                        else
                        {   //already inactive, no changes
                            return Response.status(Response.Status.NO_CONTENT).build();
                        }
                    }
                }
                return Response.status(Response.Status.NO_CONTENT).build();
            }            
        }
        catch (Exception ex)
        {
            String detail = ex.getMessage();
            errorMessage = new ErrorUtils().new ErrorMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }   
            else if(ex instanceof net.spy.memcached.OperationTimeoutException){
                detail = ex.getCause().toString().concat(" ".concat(ex.getMessage()));
            }             
            errorMessage
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode()); 
            if(ex instanceof net.spy.memcached.OperationTimeoutException){
                errorMessage.setApplicationErrorCode(com.mobiweb.completeit.json.ErrorUtils.ErrorCode.MEMCACHED_TIMEOUT.getCode());
            }
            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }        
        
        return null;
    }    
    
    @PATCH
    @Path("/sentences/{answer_id}/report")
    @Produces("application/vnd.api+json") 
    public Response reportSentence(@Context HttpServletRequest httpServletRequest, @PathParam("answer_id") long answer_id)
    {
        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }  
         
        // ANSWER ID NOT FOUND
        if(!as.exists(answer_id))
        {           
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(ErrorUtils.ErrorResponse.Title.NOT_FOUND)
            .setDetail("The requested answer id was not found : "+ answer_id)
            .setHttpStatusCode(Response.Status.NOT_FOUND.getStatusCode());            
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();          
        }
        
        try
        {            
            Answer answer = as.find(answer_id);
            if(answer != null)
            {
                // new many-to-many objects
                AnswerProfileReport apr = ars.find(answer_id,profile.getId());
                if(apr != null)
                {
                    //update isActive state
                    apr.setActive(true);
                    ars.update(apr);
                    //already reported - no content
                    return Response.status(Response.Status.NO_CONTENT).build();
                }
                else
                {
                    Set<AnswerProfileReport> reportsSet =  answer.getReportsCollection();                    
                    if(!reportsSet.contains(apr))
                    {
                        apr = new AnswerProfileReport(answer,profile);
                        long reports = answer.getReports() + 1;
                        if(reports >= 3){
                            answer.setDeleted(true);
                        }
                        answer.setReports(reports);
                        answer.getReportsCollection().add(apr);
                        answer.setReportsCollection(answer.getReportsCollection());
                        as.update(answer);                   
                        return Response.status(Response.Status.OK).build();
                    }
                }
                return Response.status(Response.Status.NO_CONTENT).build();
            }            
        }
        catch (Exception ex)
        {
            String detail = ex.getMessage();
            errorMessage = new ErrorUtils().new ErrorMessage();
            if(ex instanceof InvalidJsonFormatException){
                detail = ErrorUtils.ErrorResponse.Title.INVALID_JSON_FORMAT;
                errorMessage.setApplicationErrorCode(ErrorUtils.ErrorCode.INVALID_JSON_FORMAT.getCode());
            }   
            errorMessage
            .setTitle(ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(detail)
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        }        
        
        return null;
    }    
    
    
    @GET
    @Path("/stats/{country}")
    @Produces("application/vnd.api+json")
    
    public Response getCountryStats(@PathParam("country") String country)
    {
        try
        {   
            Client client = ClientBuilder.newClient();
            WebTarget target = null;
            List<Profile> allProfiles = profileService.findAll();
            Set<Profile> set = new LinkedHashSet();
            Gson gson = new Gson();
            List<Profile> countryProfiles = new ArrayList();
            for(Profile p : allProfiles)
            {
                Coordinates coords = new Coordinates();
                String coordinates = String.valueOf(p.getLocation().getY()).concat(",").concat(String.valueOf(p.getLocation().getX()));
                
                /*
                Future<Coordinates> future1 = client.target("http://maps.googleapis.com/maps/api/geocode/json")
                .queryParam("latlng", coordinates)
                .queryParam("sensor", "false").request().async().get(Coordinates.class);
                */
                
                String json = readUrl("http://maps.googleapis.com/maps/api/geocode/json"
                          + "?latlng="+ coordinates + "&sensor=false");
                
                //ObjectMapper mapper = new ObjectMapper();
                //coords = mapper.readValue("http://maps.googleapis.com/maps/api/geocode/json"
                //          + "?latlng="+ coordinates + "&sensor=false", Coordinates.class);
                                
                //Coordinates res1 = future1.get();
                
                //Future<String> geocode = geocodeAsync(coordinates);
                coords = gson.fromJson(json, Coordinates.class);                
                if(coords.results.size() > 0)
                {                    
                    List<AddressComponent> components = coords.results.get(0).getAddress_components();
                    String formattedAddress = coords.results.get(0).getFormatted_address();
                    if(!Utilities.isEmpty(formattedAddress))
                    {
                        String[] split = formattedAddress.split(",");
                        if(split.length >0)
                        {
                            String coun = split[split.length-1];
                            String theCountry = coun.trim();
                            if(theCountry.equals(country)){
                                countryProfiles.add(p);
                           }                            
                        }
                    }
                    
                    /*
                    if(components.size() >0)
                    {
                        for(AddressComponent c : components){
                        List<String> types = c.getTypes();
                        if(types.size() >0)
                        {
                            for(String type : types)
                            {
                                if(type.equals("country")){
                                String longName = c.getLong_name();
                                if(longName.equals(country)){
                                    countryProfiles.add(p);
                                }
                                }
                            }
                        }
                        
                        }
                    }
                    */
                }
            }
            //target = client.target("http://maps.googleapis.com/maps/api/geocode/json?latlng=55.0,10.801158&sensor=false")            
            List<Profile> profiles = profileService.findTopTen();
            List<OutboundProfile> jsonProfiles = new ArrayList(); 
            for (Profile pr : countryProfiles) {
                OutboundProfile body = new OutboundProfile(pr);
                jsonProfiles.add(body);
            }      
            JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonProfiles, null);        
            String json = generator.writeStream();        
            return Response.status(Response.Status.OK).entity(json).build();                        
        }
        catch (WebApplicationException | NoSuchFieldException | IOException | IllegalArgumentException | IllegalAccessException | InvocationTargetException  ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ex.getMessage())            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        } catch (Exception ex) {
            Logger.getLogger(GameModeResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }    
   
    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read); 

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    } 
    
    /*
    //@GET
    //@Path("/stats/async/{country}")  
    //@Produces("application/vnd.api+json")
    @Asynchronous
    public void getCountryStatsAsync(@Suspended final AsyncResponse asyncResponse, @PathParam("country") String country)
    {
        Response result = getCountryStats(country);
        asyncResponse.resume(result);
    }      
    */
    private Callable<String> callable(String coordinates)
    {
        String apiKey = "AIzaSyDrA3FIIkrvuoLVuinVUbe0DSxmei8Gxkk";
        return () -> {        
            return readUrl("https://maps.googleapis.com/maps/api/geocode/json" + "?latlng="+ coordinates + "&sensor=false&key="+apiKey);
        };        
    }
    
    @GET
    @Path("/geocode/{country}")
    @Produces("application/vnd.api+json")        
    public Response getGeocode(@PathParam("country") String country)
    {
        try
        {   
            List<Profile> allProfiles = profileService.findAll();
            Gson gson = new Gson();
            List<Profile> countryProfiles = new ArrayList();
            ExecutorService executor = Executors.newWorkStealingPool();
            for(Profile p : allProfiles)
            {
                Coordinates coords = new Coordinates();
                String coordinates = String.valueOf(p.getLocation().getY()).concat(",").concat(String.valueOf(p.getLocation().getX()));
                //String json = readUrl("https://maps.googleapis.com/maps/api/geocode/json" + "?latlng="+ coordinates + "&sensor=false");                
                List<Callable<String>> callables = Arrays.asList(callable(coordinates));
                String json = executor.invokeAny(callables);                
                coords = gson.fromJson(json, Coordinates.class);                
                if(coords.results.size() > 0)
                {                    
                    //List<AddressComponent> components = coords.results.get(0).getAddress_components();
                    String formattedAddress = coords.results.get(0).getFormatted_address();
                    if(!Utilities.isEmpty(formattedAddress))
                    {
                        String[] split = formattedAddress.split(",");
                        if(split.length >0)
                        {
                            String coun = split[split.length-1];
                            String theCountry = coun.trim();
                            if(theCountry.equals(country)){
                                countryProfiles.add(p);
                           }                            
                        }
                    }
                }
            }            
            List<Profile> profiles = profileService.findTopTen();
            List<OutboundProfile> jsonProfiles = new ArrayList(); 
            for (Profile pr : countryProfiles) {
                OutboundProfile body = new OutboundProfile(pr);
                jsonProfiles.add(body);
            }      
            JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonProfiles, null);        
            String json = generator.writeStream();        
            return Response.status(Response.Status.OK).entity(json).build();                        
        }
        catch (WebApplicationException | NoSuchFieldException | IOException | IllegalArgumentException | IllegalAccessException | InvocationTargetException  ex)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ex.getMessage())            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        } catch (Exception ex) {
            Logger.getLogger(GameModeResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }    
    
    //http://maps.googleapis.com/maps/api/geocode/json?latlng=55.0,10.801158&sensor=false      
    
    /**
     * Returns a random list of 10 profiles with a set of 3 sentences one of which is answered by related profile.
     * @param httpServletRequest
     * @param gender
     * @param minAge
     * @param maxAge
     * @param minLocation
     * @param maxLocation
     * @return
     * @throws IOException
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyPhraseException
     * @throws com.mobiweb.completeit.common.PhraseParser.EmptyAnswersException
     * @throws com.mobiweb.completeit.common.PhraseParser.PhraseParseException 
     */
    @POST       
    @Path("/sentences")
    @Produces("application/vnd.api+json")       
    public Response getSentences(@Context HttpServletRequest httpServletRequest,
            @FormParam("gender") String gender, @FormParam("minAge") String minAge,
            @FormParam("maxAge") String maxAge, @FormParam("minLocation") String minLocation, 
            @FormParam("maxLocation") String maxLocation) throws IOException, EmptyPhraseException, EmptyAnswersException, PhraseParseException
    {            

        String authorization = httpServletRequest.getHeader("Authorization");
        Response response = authorizationService.authorized(authorization);
        Profile profile = null;
        if(authorizationService.isAuthorized() && response == null)
        {
            profile = authorizationService.getProfile();
        }
        else if(!authorizationService.isAuthorized() && response != null)
        {
            return response;
        }         
      
        try
        {
            int maxSentences = 2;
            int maxResults = 10;
            List<Integer> ageRange = new ArrayList();
            List<Integer> locationRange = new ArrayList();
            if(!Utilities.isEmpty(minAge) && !Utilities.isEmpty(maxAge))
            {                
                ageRange.add(Integer.parseInt(minAge));
                ageRange.add(Integer.parseInt(maxAge));                
                if(ageRange.get(1) <= 0 && ageRange.get(0) <= 0){
                    throw new InvalidArgumentException("Min and max age must me greater than zero");                    
                }
                else if(ageRange.get(1) <= 0)
                {
                    throw new InvalidArgumentException("Max age must me greater than zero");
                }
                else if(ageRange.get(0) <= 0)
                {
                    throw  new InvalidArgumentException("Min age must me greater than zero");
                }                
                if(ageRange.get(0)  > ageRange.get(1)){
                    throw  new InvalidArgumentException("Min age must me less than or equal than max age");
                }
            }
            if(!Utilities.isEmpty(minLocation) && !Utilities.isEmpty(maxLocation))
            {
                locationRange.add(Integer.parseInt(minLocation));
                locationRange.add(Integer.parseInt(maxLocation));                
                if(locationRange.get(0)  > locationRange.get(1)){
                    throw  new InvalidArgumentException("Min location must me less than or equal than max location");
                }               
            }
            
            // COPY answers to memcached for speed
            List<Answer> allAnswers = null;
            List<Answer> filteredAnswers = null;                        
            filteredAnswers = as.getGroupedFilteredResults(gender, ageRange, locationRange, profile.getLocation());
            allAnswers = as.findAllGroupByPhraseAndProfile();
            if(allAnswers.isEmpty()){
                throw new NoProfileAnswersException("The system does not contain any answers");
            }            
            // random mode - no filters
            boolean isRandomMode = (Utilities.isEmpty(gender) || gender.matches("^\\s*$")) && Utilities.isEmpty(minAge) 
                                   && Utilities.isEmpty(maxAge) && Utilities.isEmpty(minLocation) 
                                   && Utilities.isEmpty(maxLocation);
            if(isRandomMode){
                filteredAnswers = allAnswers;
            }
            
            List<OutboundProfileAnswer> jsonAnswers = new ArrayList();
            if(filteredAnswers.isEmpty()){                
                
                String json = "{\n\"data\": []\n}";
                return Response.status(Response.Status.OK).entity(json).build();                 
            }
            
            // FILTER ANSWERS FROM CURRENT PLAYER
            AnswerFilterPredicate profilePredicate = new AnswerFilterPredicate();
            profilePredicate.profile = profile; // profile predicate
            List<Answer> exclusiveAnswers = new ArrayList(filteredAnswers);             
            exclusiveAnswers.removeIf(profilePredicate); // remove answers from current profile
                        
            //copy to set
            Set<Answer> answerSet = new LinkedHashSet(exclusiveAnswers);
            
            // MAKE A MAP OF PROFILE-ANSWERS PAIRS
            Map<Phrase,List<Answer>> phraseAnswersMap = new LinkedHashMap();
            Map<Profile,Set<Answer>> profileAnswersMap = new LinkedHashMap();            
            for(Answer ans : exclusiveAnswers)
            {                
                Phrase phrase = ans.getPhrase(); 
                List<Answer> answers = new ArrayList();
                if(!phraseAnswersMap.containsKey(phrase))
                {                    
                    answers.add(ans);
                    phraseAnswersMap.put(phrase, answers);                    
                }
                else
                {
                    phraseAnswersMap.get(phrase).add(ans);                    
                }                
            }
            // REMOVE ANSWER ENTRIES WITH LESS THAN 3 PROFILE ANSWERS
            for(Entry entry : phraseAnswersMap.entrySet())
            {
                List<Answer> values = (List)entry.getValue();
                if(values.size()<3)
                {                
                    values = (List<Answer>)entry.getValue();                    
                    for(Answer a : values)
                    {
                        answerSet.remove(a);                        
                    }
                }
            }    
            
            // BUILD A PROFILE ANSWERS MAP LOOKUP
            Map<Phrase,Set<Answer>> phraseMap = new LinkedHashMap();
            for(Answer ans : answerSet)
            {
                Profile pr = ans.getProfile();
                Set<Answer> setOfanswers = new LinkedHashSet<>();
                if(!profileAnswersMap.containsKey(pr))
                {                    
                    setOfanswers.add(ans);
                    profileAnswersMap.put(pr, setOfanswers);                    
                }
                else
                {
                    profileAnswersMap.get(pr).add(ans);                    
                }
                
                Set<Answer> setOfAnswersPerPhrase = new LinkedHashSet<>();
                if(!phraseMap.containsKey(ans.getPhrase()))
                {                    
                    setOfAnswersPerPhrase.add(ans);
                    phraseMap.put(ans.getPhrase(), setOfAnswersPerPhrase);                    
                }
                else
                {
                    phraseMap.get(ans.getPhrase()).add(ans);                    
                }                 
            }
            
            Object[] keys = profileAnswersMap.keySet().toArray(); // GET PROFILE KEYS
            int length = keys.length;
            
            // NEW : Need to solve problem with no data at very start of the running app
            if(length < 1)
            {
                String json = "{\n\"data\": []\n}";
                return Response.status(Response.Status.OK).entity(json).build();                 
            }       
            
            int randomIndex = 0;            
            randomIndex = ThreadLocalRandom.current().nextInt(0, length);
            List<Integer> positions = new ArrayList();
            Set<Integer> uniques = new LinkedHashSet();            
            int max = length <10 ? length : 10;            
            for(int i = 0; i < max; i++)
            {
                randomIndex = ThreadLocalRandom.current().nextInt(0, length);                
                while(uniques.contains(randomIndex))
                {
                    randomIndex = ThreadLocalRandom.current().nextInt(0, length);
                }
                uniques.add(randomIndex);
            }
            
            // prepare random access index list to select answers with
            positions.addAll(uniques);            
            if(positions.size() < maxResults)
            {
                String json = "{\n\"data\": []\n}";
                return Response.status(Response.Status.OK).entity(json).build();                 
            }            
            
            // GET RANDOM TEN ELEMENTS to DISPLAY
            PhraseParser parser = new PhraseParser();
            for(Integer index : positions)
            {  
                Profile pr = (Profile)keys[index];
                Set<Answer> answers = profileAnswersMap.get(pr);
                List<Answer> answersList = new ArrayList(answers);
                randomIndex = ThreadLocalRandom.current().nextInt(0, answers.size());
                List<CompletePhrase> sentences = new ArrayList();
                parser.setPhrase(answersList.get(randomIndex).getPhrase().getPhrase());
                parser.setAnswers(answersList.get(randomIndex).getAnswers());
                String sentence = parser.compose();
                CompletePhrase cp = new CompletePhrase();
                cp.setClause(sentence);                                
                cp.setIsCorrect("true");
                //cp.setAnswerId(String.valueOf(answersList.get(randomIndex).getId()));
                sentences.add(cp);                 
                
                Set<Answer> matchedAnswers = phraseMap.get(answersList.get(randomIndex).getPhrase());
                List<Answer> list = new ArrayList(matchedAnswers);
                for(int i = 0 ; i < maxSentences; i++)
                {
                    int random = ThreadLocalRandom.current().nextInt(0, list.size());
                    Profile p = list.get(random).getProfile();
                    while(p == pr)
                    {
                        random = ThreadLocalRandom.current().nextInt(0, list.size());
                        p = list.get(random).getProfile();
                    }
                    parser.setPhrase(list.get(random).getPhrase().getPhrase());
                    parser.setAnswers(list.get(random).getAnswers());
                    sentence = parser.compose();
                    cp = new CompletePhrase();
                    cp.setClause(sentence);                                
                    cp.setIsCorrect("false");
                    //cp.setAnswerId(String.valueOf(list.get(random).getId()));
                    sentences.add(cp);                    
                }
                
                Collections.shuffle(sentences);
                OutboundProfileAnswer opa = new OutboundProfileAnswer(pr, sentences);
                jsonAnswers.add(opa);                
            }
            
            JsonApiStreamGenerator generator = new JsonApiStreamGenerator(jsonAnswers, CompletePhrase.class);
            String json = generator.writeStream();
            return Response.status(Response.Status.OK).entity(json).build(); 
        }
        catch (NumberFormatException | InvalidArgumentException e)
        {
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(e.getMessage())            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();            
        } catch (IllegalArgumentException | IllegalAccessException 
                | InvocationTargetException | NoSuchFieldException | NoProfileAnswersException ex)
        {            
            errorMessage = new ErrorUtils().new ErrorMessage()
            .setTitle(com.mobiweb.completeit.json.ErrorUtils.ErrorResponse.Title.BAD_REQUEST)
            .setDetail(ex.getMessage())            
            .setHttpStatusCode(Response.Status.BAD_REQUEST.getStatusCode());            
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.response()).type(MediaType.APPLICATION_JSON).build();             
        }
    }  
    
}
