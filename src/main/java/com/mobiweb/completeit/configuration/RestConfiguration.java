package com.mobiweb.completeit.configuration;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import com.mobiweb.completeit.oauth.OAuthResource;
import com.mobiweb.completeit.resource.AdministrationResource;
import com.mobiweb.completeit.resource.GameModeResource;
import com.mobiweb.completeit.resource.NormalModeResource;
import com.mobiweb.completeit.resource.ProfileResource;

/**
 * Configuration class for Restful web services in this war project.
 * Services url path relative to this webapp: e.g. //localhost:8080/project-name/services
 * @author John christodoulou
 */
@ApplicationPath("services")
public class RestConfiguration extends Application
{
    public RestConfiguration() {
        
    }
   
    @Override
    public Set<Class<?>> getClasses() {
        
        Set<Class<?>> classes = new HashSet();                
        classes.add(OAuthResource.class);
        classes.add(ProfileResource.class);
        classes.add(NormalModeResource.class);
        classes.add(GameModeResource.class);
        classes.add(AdministrationResource.class);
        return classes;
    } 
}
