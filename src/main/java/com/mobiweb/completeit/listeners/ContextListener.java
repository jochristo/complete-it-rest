/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.listeners;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.mobiweb.completeit.common.MemCached;

/**
 * Creates MemCached client
 * @author ic
 */
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {      
        
        MemCached.createClient();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
     
        try {
            MemCached.getClient().shutdown();
        } catch (IOException ex) {
            Logger.getLogger(ContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
