/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.logging;

import java.io.IOException;
import java.util.Date;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ic
 */
public class PayloadLoggingFilter implements Filter{

    @Override
    public void init(FilterConfig fc) throws ServletException {
        
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		
		//Get the IP address of client machine.
		String ipAddress = request.getRemoteAddr();
		
		//Log the IP address and current timestamp.
		System.out.println("IP "+ipAddress + ", Time " 
							+ new Date().toString());
		
		chain.doFilter(req, res);                        
    }

    @Override
    public void destroy() {
        
    }


    

    
}
