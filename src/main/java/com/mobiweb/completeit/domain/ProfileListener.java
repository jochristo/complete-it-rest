/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Profile entity callback listener class.
 * @author ic
 */
public class ProfileListener
{    
        
    @PrePersist
    public void prePersist(Profile profile) {
        updateLevel(profile); 
    }
    
    @PreUpdate
    public void preUpdate(Profile profile) {
        updateLevel(profile);
    }   
    
    /**
     * Updates profile level property based on profile score.
     * @param profile 
     */
    private void updateLevel(Profile profile)
    {
        // update SCORE + LEVEL     
        long score = profile.getScore();
        final int minLevel  = 1;        
        final int maxLevel = 10;
        final int maxLevelPoints = 1000;        
        int level = profile.getLevel();
        if (score == 0) {
            level = 1;
        } else {
            int levelPoints = 0;
            for (int i = minLevel; i <= maxLevel; i++) {
                levelPoints += maxLevelPoints;
                if (score > levelPoints && score < levelPoints + maxLevelPoints + 1) {
                    level = ((levelPoints) / maxLevelPoints) + 1;
                    if (level > maxLevel) {
                        level = maxLevel;
                    }

                }
            }
        }
        profile.setLevel(level);
    }
}
