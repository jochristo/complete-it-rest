/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 * Represents an AnswerProfile entity record holding reported answer and reporting profile pair.
 * @author ic
 */
@Entity
@Table(name="answer_profile_reports")
@NamedQueries({
    @NamedQuery(name="AnswerProfileReport.findByPK", 
        query="SELECT apr FROM AnswerProfileReport apr WHERE apr.answerProfileReportPK.answerId =:answerId "
                + "AND apr.answerProfileReportPK.profileId =:profileId"),
    @NamedQuery(name="AnswerProfileReport.findByAnswerId", 
        query="SELECT apr FROM AnswerProfileReport apr WHERE apr.answerProfileReportPK.answerId =:answerId"),   
    @NamedQuery(name="AnswerProfileReport.findByProfileId", 
        query="SELECT apr FROM AnswerProfileReport apr WHERE apr.answerProfileReportPK.profileId =:profileId"),
    @NamedQuery(name="AnswerProfileReport.count", 
        query="SELECT COUNT(apr.answerProfileReportPK) FROM AnswerProfileReport apr"),
}) 
public class AnswerProfileReport implements Serializable 
{ 
    @EmbeddedId
    protected AnswerProfileReportPK answerProfileReportPK;
    
    @ManyToOne
    @JoinColumn(name="FK_answer_id", referencedColumnName = "an_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKey_answer_id", columnNames = {"FK_answer_id"}, 
        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)             
    private Answer answer;
    
    @ManyToOne
    @JoinColumn(name="FK_profile_id", referencedColumnName = "pr_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKey_profile_id", columnNames = {"FK_profile_id"}, 
        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)             
    private Profile profile;     

    @Basic(optional = false)
    @Column(name="is_active", nullable=false, columnDefinition = "BIT(1) DEFAULT b'1'")    
    private boolean active = true;    
    
    public AnswerProfileReport() {
    }
    
    public AnswerProfileReport(Answer answer, Profile profile) {
        this.answerProfileReportPK = new AnswerProfileReportPK(answer.getId(), profile.getId());
        this.answer = answer;
        this.profile = profile;
    }     
    
    public AnswerProfileReportPK getAnswerProfileReportPK() {
        return answerProfileReportPK;
    }

    public void setAnswerProfileReportPK(AnswerProfileReportPK answerProfileReportPK) {
        this.answerProfileReportPK = answerProfileReportPK;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }        

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.answer);
        hash = 71 * hash + Objects.hashCode(this.profile);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnswerProfileReport other = (AnswerProfileReport) obj;
        return Objects.equals(this.answerProfileReportPK, other.answerProfileReportPK);
    }    
}
