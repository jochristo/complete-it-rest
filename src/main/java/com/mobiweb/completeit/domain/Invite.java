/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *  Represents a Referred user entity-record.
 * @author ic
 */
@Entity
@Table(name="invite")
@NamedQueries({
@NamedQuery(name="Invite.findAll", query="SELECT i FROM Invite i"),
@NamedQuery(name="Invite.findByProfileId", query="SELECT i FROM Invite i WHERE i.profileId =:profileId"),    
@NamedQuery(name="Invite.findByEmail", query="SELECT i FROM Invite i WHERE i.email =:email"),    
@NamedQuery(name="Invite.findByProfileIdAndEmail", query="SELECT i FROM Invite i WHERE i.profileId =:profileId AND i.email =:email"),    
@NamedQuery(name="Invite.count", query="SELECT COUNT(i.id) FROM Invite i"),
})
public class Invite implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name="inv_id") 
    private long id;
    
    @Basic(optional = false)
    @Column(name="inv_profile_id", nullable=false)    
    private long profileId;
    
    @Basic(optional = false)
    @Column(name="inv_email", nullable=false)    
    private String email;
    
    @Column(name="inv_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());

    public Invite() {
    }    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 79 * hash + (int) (this.profileId ^ (this.profileId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Invite other = (Invite) obj;
        return !(this.profileId != other.profileId && !Objects.equals(this.email, other.email));
    }

    
}
