/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Represents composite primary key of an AnswerProfileReport pair.
 * @author ic
 */
@Embeddable
public class AnswerProfileReportPK implements Serializable 
{
    @Basic(optional = false)
    @NotNull
    @Column(name = "FK_answer_id")    
    private long answerId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FK_profile_id")    
    private long profileId; 

    public AnswerProfileReportPK() {
    }

    public AnswerProfileReportPK(long answerId, long profileId) {
        this.answerId = answerId;
        this.profileId = profileId;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }   
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (this.answerId ^ (this.answerId >>> 32));
        hash = 59 * hash + (int) (this.profileId ^ (this.profileId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnswerProfileReportPK other = (AnswerProfileReportPK) obj;
        return !(this.answerId != other.answerId && this.profileId != other.profileId);
    }       
}
