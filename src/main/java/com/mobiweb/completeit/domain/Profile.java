package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;


/**
 *  Represents a Profile entity and record.
 * @author ic
 */
@Entity
@EntityListeners({ProfileListener.class})
@Table(name="user_profile", 
        uniqueConstraints={
            @UniqueConstraint(columnNames={"pr_nickname"}),
            @UniqueConstraint(columnNames={"pr_email"})
        })
@NamedQueries({
        @NamedQuery(name="Profile.findAll", query="SELECT pr FROM Profile pr WHERE pr.deleted = FALSE"),
        @NamedQuery(name="Profile.findAllInclDeleted", query="SELECT pr FROM Profile pr ORDER BY pr.id"),
        @NamedQuery(name="Profile.findAllExcludeProfile", query="SELECT pr FROM Profile pr WHERE pr.id <> :profileId"), 
        @NamedQuery(name="Profile.findById", query="SELECT pr FROM Profile pr WHERE pr.id = :id"),    
        @NamedQuery(name="Profile.findByNickname", query="SELECT pr FROM Profile pr WHERE pr.nickname = :nickname"),
        @NamedQuery(name="Profile.findByEmail", query="SELECT pr FROM Profile pr WHERE pr.email = :email"),
        @NamedQuery(name="Profile.findByDob", query="SELECT pr FROM Profile pr WHERE pr.dob = :dob"),        
        @NamedQuery(name="Profile.findByEmailAndNickname", query="SELECT pr FROM Profile pr WHERE pr.email = :email AND pr.nickname = :nickname"),
        @NamedQuery(name="Profile.findByGender", query="SELECT pr FROM Profile pr WHERE pr.gender = :gender"),
        @NamedQuery(name="Profile.findTopTen", query="SELECT pr FROM Profile pr WHERE pr.deleted=FALSE AND pr.score > 0 AND pr.level>=1 ORDER BY pr.level DESC, pr.score DESC"),
        @NamedQuery(name="Profile.count", query="SELECT COUNT(pr.id) FROM Profile pr"),
        @NamedQuery(name="Profile.countUndeleted", query="SELECT COUNT(pr.id) FROM Profile pr WHERE pr.deleted = FALSE"),
        
})
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L; 

    public Profile() {      
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name="pr_id")  
    private long id;    
    
    @Column(name="pr_nickname", nullable=false, unique = true) 
    @NotNull(message="Nickname cannot be null")    
    private String nickname;
    
    //@Basic(optional = false)
    @Column(name="pr_picture_filepath", nullable=true)    
    private String pictureFilepath = null;
    
    @Column(name="pr_dob", nullable=false)
    @Temporal(TemporalType.DATE)    
    private Date dob;    
       
    @Basic(optional = false)
    @Column(name="pr_language", nullable=false)
    private String language;
    
    @Basic(optional = false)
    @Column(name="pr_email", nullable=false, unique = true)
    private String email;
    
    @Basic(optional = false)
    @Column(name="pr_password", nullable=false)
    private String password; 
      
    @Basic(optional = false)
    @Column(name="pr_score", nullable=false, columnDefinition = "int(11) DEFAULT 0")
    private long score = 0;
        
    @Basic(optional = false)
    @Column(name="pr_level", nullable=false, columnDefinition = "int(11) DEFAULT 1")    
    private int level = 1;
    
    @Basic(optional = false)
    @Column(name="pr_mobileNo", nullable=false)
    private String mobileNo = "";  
    
    @Basic(optional = false)
    @Column(name="pr_isMobileNoVerified", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")
    private boolean isMobileNoVerified = false;      
    
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "pr_longitude",nullable=false)),
            @AttributeOverride(name = "y", column = @Column(name = "pr_latitude",nullable=false))
    })
    private Location location = new Location();
    
    @Basic(optional = false)
    @Column(name="pr_gender", nullable=false)
    private String gender = "Male";   
    
    @OneToMany(mappedBy="profile",fetch=FetchType.LAZY, cascade={CascadeType.MERGE})
    @OrderBy("id, phrase")
    private Collection<Phrase> phrases = new ArrayList();   
      
    @Transient
    private List<Answer> answerLikes = new ArrayList();    
    
    @Transient    
    private List<Answer> answerReports = new ArrayList();  
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "profile", fetch=FetchType.LAZY)
    private Set<AnswerProfileLike> likes = new HashSet();
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "profile", fetch=FetchType.LAZY)
    private Set<AnswerProfileReport> reports = new HashSet();    
        
    @Basic(optional = false)
    @Column(name="pr_deleted", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean deleted = false; 
    
    @Basic(optional = false)
    @Column(name="pr_is_dummy", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean dummy = false;     
    
    @Column(name="pr_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());     
    
    public String getPictureFilepath() {
        return pictureFilepath;
    }    
    
    public void setPictureFilepath(String filename) {
        this.pictureFilepath = filename;
    }
    
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }    
    
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public boolean isMobileNoVerified() {
        return isMobileNoVerified;
    }

    public void setIsMobileNoVerified(boolean isMobileNoVerified) {
        this.isMobileNoVerified = isMobileNoVerified;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Collection<Phrase> getPhrases() {
        return phrases;
    }

    public void setPhrases(Collection<Phrase> phrases) {
        this.phrases = phrases;
    }

    public List<Answer> getAnswerLikes() {
        return answerLikes;
    }

    public void setAnswerLikes(List<Answer> answerLikes) {
        this.answerLikes = answerLikes;
    }

    public List<Answer> getAnswerReports() {
        return answerReports;
    }

    public void setAnswerReports(List<Answer> answerReports) {
        this.answerReports = answerReports;
    }
    
    //new staff to map foreign keys properly in aanswer likes+reports

    public Set<AnswerProfileLike> getLikes() {
        return likes;
    }

    public void setLikes(Set<AnswerProfileLike> likes) {
        this.likes = likes;
    }

    public Set<AnswerProfileReport> getReports() {
        return reports;
    }

    public void setReports(Set<AnswerProfileReport> reports) {
        this.reports = reports;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }    

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    
        
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 53 * hash + Objects.hashCode(this.nickname);
        hash = 53 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Profile other = (Profile) obj;
        if (this.id != other.id && !Objects.equals(this.nickname, other.nickname) && !Objects.equals(this.email, other.email))
        {
            return false;
        }

        return true;
    }
        
    
}
