/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 * Represents a PhraseCategory pair.
 * @author ic
 */
@Entity
@Table(name="phrase_category")
@NamedQueries({
    @NamedQuery(name="PhraseCategory.findByCompositeKey", 
        query="SELECT pc FROM PhraseCategory pc WHERE pc.phrase =:phrase and pc.category =:category"),    
    @NamedQuery(name="PhraseCategory.findByPK", 
        query="SELECT pc FROM PhraseCategory pc WHERE pc.phraseCategoryPK.phraseId =:phraseId AND pc.phraseCategoryPK.categoryId =:categoryId"),
@NamedQuery(name="PhraseCategory.findByPhraseId", 
        query="SELECT pc FROM PhraseCategory pc WHERE pc.phraseCategoryPK.phraseId =:phraseId"),  
@NamedQuery(name="PhraseCategory.findByCategoryId", 
        query="SELECT pc FROM PhraseCategory pc WHERE pc.phraseCategoryPK.categoryId =:categoryId"), 
@NamedQuery(name="PhraseCategory.countGroupByCategory", 
        query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(pc.category.id, pc.category.name, COUNT(pc.phrase.id)) "
                + "FROM PhraseCategory pc "
                + "WHERE pc.phrase.deleted = FALSE AND pc.phrase.isApprovalPending = FALSE "
                + "AND pc.category.deleted = FALSE GROUP BY pc.category.id, pc.category.name ORDER BY pc.category.name ASC"), 
})
public class PhraseCategory implements Serializable
{    
    @EmbeddedId
    protected PhraseCategoryPK phraseCategoryPK;
    
    @ManyToOne
    @JoinColumn(name="FK_phrase_id", referencedColumnName = "ph_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKey_phrase_id", columnNames = {"FK_phrase_id"}, 
        deleteAction=ForeignKeyAction.CASCADE, updateAction = ForeignKeyAction.CASCADE)    
    private Phrase phrase;
    
    @ManyToOne
    @JoinColumn(name="FK_category_id", referencedColumnName = "cat_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKey_category_id", columnNames = {"FK_category_id"}, 
        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)     
    private Category category;
    
    @Basic(optional = false)
    @Column(name="is_active", nullable=false, columnDefinition = "BIT(1) DEFAULT b'1'")    
    private boolean active = true;
    
    public PhraseCategory() {
    }

    public PhraseCategory(Phrase phrase, Category category) {
        this.phrase = phrase;
        this.category = category;
        this.phraseCategoryPK = new PhraseCategoryPK(phrase.getId(),category.getId());
    }   

    public PhraseCategoryPK getPhraseCategoryPK() {
        return phraseCategoryPK;
    }

    public void setPhraseCategoryPK(PhraseCategoryPK phraseCategoryPK) {
        this.phraseCategoryPK = phraseCategoryPK;
    }

    public Phrase getPhrase() {
        return phrase;
    }

    public void setPhrase(Phrase phrase) {
        this.phrase = phrase;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.phraseCategoryPK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhraseCategory other = (PhraseCategory) obj;
        boolean isEquals = Objects.equals(this.phraseCategoryPK, other.phraseCategoryPK);
        return Objects.equals(this.phraseCategoryPK, other.phraseCategoryPK);        
    }    
}
