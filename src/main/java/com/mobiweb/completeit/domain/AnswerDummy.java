/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Dummy Answer entity to hold dummy data where nominal real answer data is present.
 * @author ic
 */
@Entity
@Table(name="answer_dummy")
@NamedQueries({
@NamedQuery(name="AnswerDummy.findAll", query="SELECT a FROM AnswerDummy a WHERE a.deleted = FALSE"),   
@NamedQuery(name="AnswerDummy.findById", query="SELECT a FROM AnswerDummy a WHERE a.id = :id"),    
@NamedQuery(name="AnswerDummy.findByAnswerDummy", query="SELECT a FROM AnswerDummy a WHERE a.answers = :answers"),
@NamedQuery(name="Phrase.findByAnswerDummyAndProfileId", query="SELECT a FROM AnswerDummy a WHERE a.answers = :answers AND a.profileId = :profileId"),
@NamedQuery(name="AnswerDummy.findByProfileId", query="SELECT a FROM AnswerDummy a WHERE a.profileId = :profileId"),
@NamedQuery(name="AnswerDummy.countByProfileId", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.profileId = :profileId"),
@NamedQuery(name="AnswerDummy.findByPhraseIdAndProfileId", 
        query="SELECT a FROM AnswerDummy a WHERE a.phraseId = :phraseId AND a.profileId = :profileId"),
@NamedQuery(name="AnswerDummy.findByAnswerDummysAndPhraseIdAndProfileId", 
        query="SELECT a FROM AnswerDummy a WHERE a.answers =:answers AND a.phraseId = :phraseId AND a.profileId = :profileId"),
@NamedQuery(name="AnswerDummy.findByPhraseId", query="SELECT a FROM AnswerDummy a WHERE a.phraseId = :phraseId"),
@NamedQuery(name="AnswerDummy.findByPhraseIdExcludeReported", query="SELECT a FROM AnswerDummy a WHERE a.deleted=FALSE AND a.phraseId = :phraseId AND a.reports < 3 ORDER BY a.created DESC"),
@NamedQuery(name="AnswerDummy.countByPhraseId", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.phraseId = :phraseId"),
@NamedQuery(name="AnswerDummy.countByPhraseIdExcludeReported", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.phraseId = :phraseId AND a.reports < 3"),
/*
@NamedQuery(name="AnswerDummy.findByPhraseIdAndProfileGender", 
        query="SELECT a FROM AnswerDummy a WHERE a.phrase.id = :phraseId AND a.profile.gender = :gender"),
@NamedQuery(name="AnswerDummy.findDistinctPhraseId", 
        query="SELECT a FROM AnswerDummy a WHERE a.phrase.id = :phraseId AND a.profile.gender = :gender"),
*/
@NamedQuery(name="AnswerDummy.findByPhraseIdAndNotProfileId", 
        query="SELECT a FROM AnswerDummy a WHERE a.phraseId = :phraseId AND a.profileId <> :profileId"),
//@NamedQuery(name="AnswerDummy.findByGender", query="SELECT a FROM AnswerDummy a WHERE a.profile.gender = :gender"),
@NamedQuery(name="AnswerDummy.findByCategoryIdExcludeProfileId", 
        query="SELECT a FROM AnswerDummy a WHERE a.profileId <> :profileId"),
@NamedQuery(name="AnswerDummy.findAllGroupByPhraseAndProfile", 
        query="SELECT a FROM AnswerDummy a GROUP BY a.phraseId, a.profileId ORDER BY a.phraseId, a.profileId "),
//@NamedQuery(name="AnswerDummy.findByGenderGroupByPhraseAndProfile", 
//        query="SELECT a FROM AnswerDummy a WHERE a.profile.gender = :gender GROUP BY a.phrase.id, a.profile.id ORDER BY a.phrase.id, a.profile.id "),
@NamedQuery(name="AnswerDummy.findByAnswerDummyIdAndProfileId", query="SELECT a FROM AnswerDummy a WHERE a.id = :answerId AND a.profileId =:profileId"),
@NamedQuery(name="AnswerDummy.count", query="SELECT COUNT(a.id) FROM AnswerDummy a"),
@NamedQuery(name="AnswerDummy.countUndeleted", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.deleted = FALSE"),
@NamedQuery(name="AnswerDummy.countByPhraseId", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.phraseId =:phraseId"),
@NamedQuery(name="AnswerDummy.countDistinctPhrases", query="SELECT COUNT(DISTINCT a.phraseId) FROM AnswerDummy a WHERE a.deleted = false"),
@NamedQuery(name="AnswerDummy.countReported", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.deleted = false AND a.reports > 0"),
@NamedQuery(name="AnswerDummy.countUnreported", query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.deleted = false AND a.reports = 0"),
//@NamedQuery(name="AnswerDummy.findByPhraseIdExcludeReportedAndReportingProfile", query="SELECT a FROM AnswerDummy a WHERE (a.deleted=FALSE AND a.phraseId =:phraseId AND a.reports <3 AND NOT EXISTS(SELECT apr.answerProfileReportPK.answerId FROM AnswerDummyProfileReport apr where apr.answerProfileReportPK.answerId = a.id AND apr.answerProfileReportPK.profileId =:profileId)) ORDER BY a.created DESC"),
/*
@NamedQuery(name="AnswerDummy.countByPhraseIdExcludeReportedAndReportingProfile", 
        query="SELECT COUNT(a.id) FROM AnswerDummy a WHERE a.deleted=FALSE AND a.phraseId =:phraseId AND a.reports <3 "
                + "AND NOT EXISTS(SELECT apr FROM AnswerDummyProfileReport apr where apr.answerProfileReportPK.answerId = a.id AND apr.answerProfileReportPK.profileId =:profileId)"),       
@NamedQuery(name="AnswerDummy.countUnreportedGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(pc.category.id, pc.category.name, COUNT(a.id)) "
                    + "FROM AnswerDummy a INNER JOIN a.phrase p INNER JOIN p.phraseCategory pc "
                    + "WHERE a.deleted = FALSE and a.reports <= 0 and pc.category.deleted = FALSE "
                    + "GROUP BY pc.category.id, pc.category.name ORDER BY pc.category.name ASC, a.id"),                    
@NamedQuery(name="AnswerDummy.countReportedGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(pc.category.id, pc.category.name, COUNT(a.id)) "
                    + "FROM AnswerDummy a INNER JOIN a.phrase p INNER JOIN p.phraseCategory pc "
                    + "WHERE a.deleted = FALSE and a.reports > 0 and pc.category.deleted = FALSE "
                    + "GROUP BY pc.category.id, pc.category.name ORDER BY pc.category.name ASC, a.id"),
*/
})
public class AnswerDummy implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="an_id", nullable=false) 
    private long id;
    
    @Column(name="an_answers", nullable=true)
    @Basic(optional = false) 
    @NotNull(message="Answers cannot be null")     
    private String answers;    
    
    /*
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.ALL},targetEntity=Phrase.class)
    @JoinColumn(name="an_phrase_id",nullable = false)    
    @ForeignKey(name ="an_phrase_id_FK", columnNames = {"an_phrase_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)  
    */
    @Transient
    private Phrase phrase;
        
   
    
    /*
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.ALL},targetEntity=Profile.class)
    @JoinColumn(name="an_profile_id",nullable = false)    
    @ForeignKey(name ="an_profile_id_FK", columnNames = {"an_profile_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)    
    */
    @Transient
    private Profile profile;   
    
    @Column(name="an_likes", nullable=true, columnDefinition = "int(11) DEFAULT 0")    
    private long likes = 0;
    
    @Basic(optional = false) 
    @Column(name="an_phrase_id", nullable=true) 
    private long phraseId;     
    
    @Basic(optional = false) 
    @Column(name="an_profile_id", nullable=true)     
    private long profileId;    
    
    @Column(name="an_reports", nullable=true, columnDefinition = "int(11) DEFAULT 0")    
    private long reports = 0;  
    
    @Column(name="an_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime()); 
       
    @Basic(optional = false)
    @Column(name="an_deleted", nullable=true, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean deleted = false;    
    
    public AnswerDummy() {
        phrase = new Phrase();
        profile = new Profile();     
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public Phrase getPhrase() {
        return phrase;
    }

    public void setPhrase(Phrase phrase) {
        this.phrase = phrase;
    }

    public long getPhraseId() {
        return phraseId;
    }

    public void setPhraseId(long phraseId) {
        this.phraseId = phraseId;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public long getReports() {
        return reports;
    }

    public void setReports(long reports) {
        this.reports = reports;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + Objects.hashCode(this.answers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnswerDummy other = (AnswerDummy) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    } 
    
}
