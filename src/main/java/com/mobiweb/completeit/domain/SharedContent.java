/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 *  Represents a Referred-User shared content record.
 * @author ic
 */
@Entity
@Table(name="shared_content")
@NamedQueries({
@NamedQuery(name="SharedContent.findAll", query="SELECT sc FROM SharedContent sc"),
@NamedQuery(name="SharedContent.findByProfileId", query="SELECT sc FROM SharedContent sc WHERE sc.referredUser.profileId =:profileId"),    
})
public class SharedContent implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name="id") 
    private long id;   
    
    @Basic(optional = false)
    @Column(name="score", nullable=true)    
    private long score;   
    
    @Basic(optional = false)
    @Column(name="phraseid", nullable=true)    
    private long phraseid;

    @Basic(optional = false)
    @Column(name="answerid", nullable=true)    
    private long answerid;
    
    @Column(name="created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());     
    
    @ManyToOne(fetch = FetchType.LAZY, 
            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, targetEntity = ReferredUser.class)
    @JoinColumn(name="referred_user_id",referencedColumnName = "refu_id", nullable = false)    
    @ForeignKey(name ="FK_ReferredUserId", columnNames = {"referred_user_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)
    private ReferredUser referredUser;    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getPhraseid() {
        return phraseid;
    }

    public void setPhraseid(long phraseid) {
        this.phraseid = phraseid;
    }

    public long getAnswerid() {
        return answerid;
    }

    public void setAnswerid(long answerid) {
        this.answerid = answerid;
    }

    public ReferredUser getReferredUser() {
        return referredUser;
    }

    public void setReferredUser(ReferredUser referredUser) {
        this.referredUser = referredUser;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));        
        hash = 97 * hash + (int) (this.phraseid ^ (this.phraseid >>> 32));
        hash = 97 * hash + (int) (this.answerid ^ (this.answerid >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SharedContent other = (SharedContent) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.referredUser.getId(), other.referredUser.getId());
    }
    
    
    
}
