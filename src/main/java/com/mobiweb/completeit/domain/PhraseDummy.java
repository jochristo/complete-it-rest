/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ic
 */
@Entity
@Table(name="phrase_dummy")
@NamedQueries({
    @NamedQuery(name="PhraseDummy.findAll", query="SELECT p FROM PhraseDummy p WHERE p.deleted = FALSE ORDER BY p.id"), 
    @NamedQuery(name="PhraseDummy.findById", query="SELECT p FROM PhraseDummy p WHERE p.id = :id"),    
    @NamedQuery(name="PhraseDummy.findByPhraseDummy", query="SELECT p FROM PhraseDummy p WHERE p.phrase = :phrase"),
    @NamedQuery(name="PhraseDummy.findByPhraseDummyAndProfileId", query="SELECT p FROM PhraseDummy p WHERE p.phrase = :phrase AND p.profileId = :profileId"),
    @NamedQuery(name="PhraseDummy.findByProfileId", query="SELECT p FROM PhraseDummy p WHERE p.profileId = :profileId"),
    @NamedQuery(name="PhraseDummy.IsApprovalPending", query="SELECT p FROM PhraseDummy p WHERE p.profileId = :profileId"),
    //@NamedQuery(name="PhraseDummy.findByIdAndProfileGender", query="SELECT p FROM PhraseDummy p WHERE p.id = :id AND p.profile.gender = :gender"),
    //@NamedQuery(name="PhraseDummy.findByGenderAgeRangeLocation", query="SELECT p FROM PhraseDummy p WHERE p.profile.gender = :gender "
    //        + "AND p.profile.dob BETWEEN :low AND :high"),
    //@NamedQuery(name="PhraseDummy.findByGender", query="SELECT p FROM PhraseDummy p WHERE p.profile.gender = :gender"),    
    @NamedQuery(name="PhraseDummy.findPhraseDummysNotInAnswers", 
            query="SELECT p FROM PhraseDummy p WHERE NOT EXISTS (select a from Answer a WHERE a.phrase.id = p.id)"),

   
    //@NamedQuery(name="PhraseDummy.findByCategoryId", 
    //        query="SELECT p from PhraseDummy p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.id =:categoryId"),    
    //@NamedQuery(name="PhraseDummy.findByCategoryIdOrderedByCreationDesc", 
    //    query="SELECT p from PhraseDummy p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.phraseCategoryPK.categoryId =:categoryId ORDER BY p.created DESC, p.id ASC"),    
    //@NamedQuery(name="PhraseDummy.countByCategoryId", 
    //    query="SELECT COUNT(p.id) from PhraseDummy p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.id =:categoryId"),    
    
    @NamedQuery(name="PhraseDummy.count", query="SELECT COUNT(p.id) FROM PhraseDummy p"),  
    @NamedQuery(name="PhraseDummy.countUndeleted", query="SELECT COUNT(p.id) FROM PhraseDummy p WHERE p.deleted = FALSE"),    
    // NEW: fetch data from m:m PhraseDummyCategory relation with FKs.
    /*
    @NamedQuery(name="PhraseDummy.findByCategoryIdNotInAnswersNEW", 
        query="SELECT p from PhraseDummy p INNER JOIN p.phraseCategory c WHERE p.isApprovalPending = FALSE AND c.category.id =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)"),    
    @NamedQuery(name="PhraseDummy.findByCategoryIdNotInAnswersOrderedByCreationDescNEW", 
        query="SELECT p from PhraseDummy p INNER JOIN p.phraseCategory c WHERE (p.isApprovalPending = FALSE AND c.phraseCategoryPK.categoryId =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)) ORDER BY p.created DESC"),   
    @NamedQuery(name="PhraseDummy.countByCategoryIdNotInAnswersNEW", 
        query="SELECT COUNT(p.id) from PhraseDummy p INNER JOIN p.phraseCategory c WHERE p.isApprovalPending = FALSE AND c.category.id =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)"),
    @NamedQuery(name="PhraseDummy.countGroupByCategory", 
            query="SELECT c.phraseCategoryPK.categoryId, COUNT(c.phraseCategoryPK.phraseId) FROM PhraseDummy p INNER JOIN p.phraseCategory c "
                    + "WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.deleted = FALSE GROUP BY c.phraseCategoryPK.categoryId, c.phraseCategoryPK.phraseId "
                    + "ORDER BY c.phraseCategoryPK.categoryId ASC"),                    
    
    @NamedQuery(name="PhraseDummy.countAnsweredGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(c.phraseCategoryPK.categoryId, c.category.name, COUNT(c.phraseCategoryPK.phraseId)) FROM PhraseDummy p INNER JOIN p.phraseCategory c "
                    + "WHERE (p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.deleted = FALSE "
                    + "AND EXISTS(SELECT a.id FROM Answer a WHERE a.phrase.id = p.id)) GROUP BY c.phraseCategoryPK.categoryId, c.category.name ORDER BY c.category.name, p.id ASC"),                    
    @NamedQuery(name="PhraseDummy.countUnansweredGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(c.phraseCategoryPK.categoryId, c.category.name, COUNT(c.phraseCategoryPK.phraseId)) FROM PhraseDummy p INNER JOIN p.phraseCategory c "
                    + "WHERE (p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.deleted = FALSE "
                    + "AND NOT EXISTS(SELECT a.id FROM Answer a WHERE a.phrase.id  = p.id)) GROUP BY c.phraseCategoryPK.categoryId, c.category.name ORDER BY c.category.name, p.id ASC"),                        
    */
})
public class PhraseDummy implements Serializable {

    public PhraseDummy() {       
        this.categories = new ArrayList();
        this.profile = new Profile();
        this.created = new Timestamp(new Date().getTime());
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="ph_id", nullable=false) 
    private long id;
    
    @Column(name="ph_phrase", nullable=true) 
    @Basic(optional = false) 
    @NotNull(message="Phrase cannot be null")     
    private String phrase;    

    @Column(name="ph_gaps", nullable=true) 
    @Basic(optional = false) 
    @NotNull(message="Phrase gaps cannot be null")    
    private int gaps;
        
    @Transient
    private List<Category> categories;    
    
    @Column(name="ph_profile_id", nullable=true) 
    @Basic(optional = false)     
    private int profileId;    
    
    //@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.ALL},targetEntity=Profile.class)
    //@JoinColumn(name="ph_profile_id",nullable = true)    
    //@ForeignKey(name ="ph_profile_id_FK", columnNames = {"ph_profile_id"}, 
    //        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)
    @Transient
    private Profile profile;  
    
    @Column(name="ph_isAdminAdded", nullable=true, columnDefinition = "BIT(1) DEFAULT b'1'") 
    @Basic(optional = false) 
    @NotNull(message="isAdminAdded cannot be null")     
    private boolean isAdminAdded = true;
    
    @Column(name="ph_isApprovalPending", nullable=true, columnDefinition = "BIT(1) DEFAULT 0") 
    @Basic(optional = false) 
    @NotNull(message="isApprovalPending cannot be null")     
    private boolean isApprovalPending = false;
    
    @Column(name="ph_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());   
      
    //@OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "phrase", fetch=FetchType.EAGER)
    @Transient
    private Set<PhraseCategory> phraseCategory = new HashSet();        
                
    @Basic(optional = false)
    @Column(name="ph_deleted", nullable=true, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean deleted = false;     

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getGaps() {
        return gaps;
    }

    public void setGaps(int gaps) {
        this.gaps = gaps;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public boolean isIsAdminAdded() {
        return isAdminAdded;
    }

    public void setIsAdminAdded(boolean isAdminAdded) {
        this.isAdminAdded = isAdminAdded;
    }

    public boolean isIsApprovalPending() {
        return isApprovalPending;
    }

    public void setIsApprovalPending(boolean isApprovalPending) {
        this.isApprovalPending = isApprovalPending;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Set<PhraseCategory> getPhraseCategory() {
        return phraseCategory;
    }

    public void setPhraseCategory(Set<PhraseCategory> phraseCategory) {
        this.phraseCategory = phraseCategory;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
   
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 59 * hash + Objects.hashCode(this.phrase);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhraseDummy other = (PhraseDummy) obj;
        return this.id == other.id;
    }
    
    
    
}
