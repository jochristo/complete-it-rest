/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import static com.mobiweb.completeit.common.Global.Cultures.extractLanguage;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author ic
 */
@Entity
@Table(name="language_culture")
@NamedQueries({
    @NamedQuery(name="Culture.findAllDistinct", query="SELECT DISTINCT c FROM Culture c ORDER BY c.displayName"),
    @NamedQuery(name="Culture.findByCode", query="SELECT c FROM Culture c WHERE c.code =:code ORDER BY c.displayName"),
    @NamedQuery(name="Culture.findByIso", query="SELECT DISTINCT c FROM Culture c WHERE c.iso =:iso ORDER BY c.displayName"),
    @NamedQuery(name="Culture.findByCultureName", query="SELECT DISTINCT c FROM Culture c WHERE c.cultureName =:cultureName ORDER BY c.displayName"),
    @NamedQuery(name="Culture.findByLanguageName", query="SELECT DISTINCT c FROM Culture c WHERE c.languageName =:languageName ORDER BY c.languageName"),
})
public class Culture implements Serializable
{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="id", nullable=false) 
    private long id;
    
    @Column(name="culture_code", nullable=true)         
    private String code;
            
    @Column(name="display_name", nullable=true)     
    private String displayName;     
    
    @Column(name="iso_value", nullable=true)         
    private String iso;
            
    @Column(name="language_culture_name", nullable=true)     
    private String cultureName;     
    
    @Column(name="language_name", nullable=true)     
    private String languageName;
    
    @Column(name="is_active", nullable=true, columnDefinition = "BIT(1) DEFAULT 0")     
    private boolean active = false;

    public Culture() {
    }

    public Culture(String code, String displayName, String iso, String cultureName) {
        
        this.code = code;
        this.displayName = displayName;
        this.iso = iso;
        this.cultureName = cultureName;
        this.languageName = extractLanguage(displayName);
    }
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getCultureName() {
        return cultureName;
    }

    public void setCultureName(String cultureName) {
        this.cultureName = cultureName;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Culture other = (Culture) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.code, other.code);
    }
    
    
}

