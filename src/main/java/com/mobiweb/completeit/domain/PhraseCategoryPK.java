/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Represents composite primary key of a PhraseCategory pair.
 * @author ic
 */
@Embeddable
public class PhraseCategoryPK implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FK_phrase_id")    
    private long phraseId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "FK_category_id")    
    private long categoryId;     
  

    public PhraseCategoryPK() {
    }

    public PhraseCategoryPK(long phrase,long category) {
        this.categoryId = category;
        this.phraseId = phrase;
    }
    

    public long getPhraseId() {
        return phraseId;
    }

    public void setPhraseId(long phrase) {
        this.phraseId = phrase;
    }    

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long category) {
        this.categoryId = category;
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (this.phraseId ^ (this.phraseId >>> 32));
        hash = 59 * hash + (int) (this.categoryId ^ (this.categoryId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhraseCategoryPK other = (PhraseCategoryPK) obj;
        boolean isEquals = !(this.phraseId != other.phraseId && this.categoryId != other.categoryId);
        return this.phraseId == other.phraseId && this.categoryId == other.categoryId;
    }     
}
