/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 *
 * @author ic
 */
@Entity
@Table(name="answer" 
        //removed: user can post more than one answers
        //uniqueConstraints = @UniqueConstraint(name ="an_phrase_id_an_profile_id_UniqueConstaint",
        //columnNames = {"an_profile_id", "an_phrase_id"})
        )
@NamedQueries({
@NamedQuery(name="Answer.findAll", query="SELECT a FROM Answer a WHERE a.deleted = FALSE"),   
    @NamedQuery(name="Answer.findAllInclDeleted", query="SELECT a FROM Answer a ORDER BY a.id"),   
@NamedQuery(name="Answer.findById", query="SELECT a FROM Answer a WHERE a.id = :id"),    
@NamedQuery(name="Answer.findByAnswer", query="SELECT a FROM Answer a WHERE a.answers = :answers"),
@NamedQuery(name="Phrase.findByAnswerAndProfileId", query="SELECT a FROM Answer a WHERE a.answers = :answers AND a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByProfileId", query="SELECT a FROM Answer a WHERE a.profile.id = :profileId"),
@NamedQuery(name="Answer.countByProfileId", query="SELECT COUNT(a.id) FROM Answer a WHERE a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByPhraseIdAndProfileId", 
        query="SELECT a FROM Answer a WHERE a.phrase.id = :phraseId AND a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByAnswersAndPhraseIdAndProfileId", 
        query="SELECT a FROM Answer a WHERE a.answers =:answers AND a.phrase.id = :phraseId AND a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByPhraseId", query="SELECT a FROM Answer a WHERE a.phrase.id = :phraseId"),
@NamedQuery(name="Answer.findByPhraseIdExcludeReported", query="SELECT a FROM Answer a WHERE a.deleted=FALSE AND a.phrase.id = :phraseId AND a.reports < 3 ORDER BY a.created DESC"),
@NamedQuery(name="Answer.countByPhraseId", query="SELECT COUNT(a.id) FROM Answer a WHERE a.phrase.id = :phraseId"),
@NamedQuery(name="Answer.countByPhraseIdExcludeReported", query="SELECT COUNT(a.id) FROM Answer a WHERE a.phrase.id = :phraseId AND a.reports < 3"),
@NamedQuery(name="Answer.findByPhraseIdAndProfileGender", 
        query="SELECT a FROM Answer a WHERE a.phrase.id = :phraseId AND a.profile.gender = :gender"),
@NamedQuery(name="Answer.findDistinctPhraseId", 
        query="SELECT a FROM Answer a WHERE a.phrase.id = :phraseId AND a.profile.gender = :gender"),
@NamedQuery(name="Answer.findByPhraseIdAndNotProfileId", 
        query="SELECT a FROM Answer a WHERE a.phrase.id = :phraseId AND a.profile.id <> :profileId"),
@NamedQuery(name="Answer.findByGender", query="SELECT a FROM Answer a WHERE a.profile.gender = :gender"),
@NamedQuery(name="Answer.findByCategoryIdExcludeProfileId", 
        query="SELECT a FROM Answer a WHERE a.profile.id <> :profileId"),
@NamedQuery(name="Answer.findAllGroupByPhraseAndProfile", 
        query="SELECT a FROM Answer a GROUP BY a.phrase.id, a.profile.id ORDER BY a.phrase.id, a.profile.id "),
@NamedQuery(name="Answer.findByGenderGroupByPhraseAndProfile", 
        query="SELECT a FROM Answer a WHERE a.profile.gender = :gender GROUP BY a.phrase.id, a.profile.id ORDER BY a.phrase.id, a.profile.id "),
@NamedQuery(name="Answer.findByAnswerIdAndProfileId", query="SELECT a FROM Answer a WHERE a.id = :answerId AND a.profile.id =:profileId"),
@NamedQuery(name="Answer.count", query="SELECT COUNT(a.id) FROM Answer a"),
@NamedQuery(name="Answer.countUndeleted", query="SELECT COUNT(a.id) FROM Answer a WHERE a.deleted = FALSE"),
@NamedQuery(name="Answer.countByPhraseId", query="SELECT COUNT(a.id) FROM Answer a WHERE a.phrase.id =:phraseId"),
@NamedQuery(name="Answer.countDistinctPhrases", query="SELECT COUNT(DISTINCT a.phrase.id) FROM Answer a WHERE a.deleted = false"),
@NamedQuery(name="Answer.countReported", query="SELECT COUNT(a.id) FROM Answer a WHERE a.deleted = false AND a.reports > 0"),
@NamedQuery(name="Answer.countUnreported", query="SELECT COUNT(a.id) FROM Answer a WHERE a.deleted = false AND a.reports = 0"),
@NamedQuery(name="Answer.findByPhraseIdExcludeReportedAndReportingProfile", query="SELECT a FROM Answer a WHERE (a.deleted=FALSE AND a.phrase.id =:phraseId AND a.reports <3 AND NOT EXISTS(SELECT apr.answerProfileReportPK.answerId FROM AnswerProfileReport apr where apr.answerProfileReportPK.answerId = a.id AND apr.answerProfileReportPK.profileId =:profileId)) ORDER BY a.created DESC"),
@NamedQuery(name="Answer.countByPhraseIdExcludeReportedAndReportingProfile", 
        query="SELECT COUNT(a.id) FROM Answer a WHERE a.deleted=FALSE AND a.phrase.id =:phraseId AND a.reports <3 "
                + "AND NOT EXISTS(SELECT apr FROM AnswerProfileReport apr where apr.answerProfileReportPK.answerId = a.id AND apr.answerProfileReportPK.profileId =:profileId)"),       
@NamedQuery(name="Answer.countUnreportedGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(pc.category.id, pc.category.name, COUNT(a.id)) "
                    + "FROM Answer a INNER JOIN a.phrase p INNER JOIN p.phraseCategory pc "
                    + "WHERE a.deleted = FALSE and a.reports <= 0 and pc.category.deleted = FALSE "
                    + "GROUP BY pc.category.id, pc.category.name ORDER BY pc.category.name ASC, a.id"),                    
@NamedQuery(name="Answer.countReportedGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(pc.category.id, pc.category.name, COUNT(a.id)) "
                    + "FROM Answer a INNER JOIN a.phrase p INNER JOIN p.phraseCategory pc "
                    + "WHERE a.deleted = FALSE and a.reports > 0 and pc.category.deleted = FALSE "
                    + "GROUP BY pc.category.id, pc.category.name ORDER BY pc.category.name ASC, a.id"),

//FETCH JOINS to get relationships loaded
@NamedQuery(name="Answer.findByProfileIdFetchJoinLikes", query="SELECT a FROM Answer a JOIN FETCH a.likesCollection WHERE a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByProfileIdFetchJoinReports", query="SELECT a FROM Answer a JOIN FETCH a.reportsCollection WHERE a.profile.id = :profileId"),
@NamedQuery(name="Answer.findByPhraseIdExcludeReportedAndReportingProfileFetchJoinLikes", query="SELECT a FROM Answer a JOIN FETCH a.likesCollection WHERE (a.deleted=FALSE AND a.phrase.id =:phraseId AND a.reports <3 AND NOT EXISTS(SELECT apr.answerProfileReportPK.answerId FROM AnswerProfileReport apr where apr.answerProfileReportPK.answerId = a.id AND apr.answerProfileReportPK.profileId =:profileId)) ORDER BY a.created DESC"),

})
public class Answer implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="an_id", nullable=false) 
    private long id;
    
    @Column(name="an_answers", nullable=false)
    @Basic(optional = false) 
    @NotNull(message="Answers cannot be null")     
    private String answers;    
    
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.ALL},targetEntity=Phrase.class)
    @JoinColumn(name="an_phrase_id",nullable = false)    
    @ForeignKey(name ="an_phrase_id_FK", columnNames = {"an_phrase_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)    
    private Phrase phrase;
    
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.ALL},targetEntity=Profile.class)
    @JoinColumn(name="an_profile_id",nullable = false)    
    @ForeignKey(name ="an_profile_id_FK", columnNames = {"an_profile_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)    
    private Profile profile;
    
    @Column(name="an_likes", nullable=true, columnDefinition = "int(11) DEFAULT 0")    
    private long likes = 0;
    
    @Column(name="an_reports", nullable=true, columnDefinition = "int(11) DEFAULT 0")    
    private long reports = 0;

    @Transient
    private Set<Profile> profileLikes;    
    
    @Transient
    private LinkedHashSet<Profile> profileReports;   
    
    @Column(name="an_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime()); 
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "answer", fetch=FetchType.LAZY)
    private Set<AnswerProfileLike> likesCollection = new HashSet();
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "answer", fetch=FetchType.LAZY)
    private Set<AnswerProfileReport> reportsCollection = new HashSet();     
    
    @Basic(optional = false)
    @Column(name="an_deleted", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")  //columnDefinition = "BIT(1) DEFAULT b'1'"  
    private boolean deleted = false;    
    
    @Basic(optional = false)
    @Column(name="an_is_dummy", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean dummy = false;     
    
    public Answer() {
        phrase = new Phrase();
        profile = new Profile();     
    }
        
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Phrase getPhrase() {
        return phrase;
    }

    public void setPhrase(Phrase phrase) {
        this.phrase = phrase;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public Set<Profile> getProfileLikes() {
        return profileLikes;
    }

    public void setProfileLikes(Set<Profile> profileLikes) {
        this.profileLikes = profileLikes;
    }

    public long getReports() {
        return reports;
    }

    public void setReports(long reports) {
        this.reports = reports;
    }

    public LinkedHashSet<Profile> getProfileReports() {
        return profileReports;
    }

    public void setProfileReports(LinkedHashSet<Profile> profileReports) {
        this.profileReports = profileReports;
    }    

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
        //this.created = new Timestamp(new Date().getTime()); 
    }
    
    //new staff to map foreign keys properly in aanswer likes+reports

    public Set<AnswerProfileLike> getLikesCollection() {
        return likesCollection;
    }

    public void setLikesCollection(Set<AnswerProfileLike> likesCollection) {
        this.likesCollection = likesCollection;
    }

    public Set<AnswerProfileReport> getReportsCollection() {
        return reportsCollection;
    }

    public void setReportsCollection(Set<AnswerProfileReport> reportsCollection) {
        this.reportsCollection = reportsCollection;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }
        
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + Objects.hashCode(this.answers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Answer other = (Answer) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
}
