/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;

/**
 * Represents longitude and latitude points.
 * @author ic
 */
@Embeddable
public class Location implements Serializable {

    public Location(double x, double y) {        

        this.x = x;        
        this.y = y;
    }


    public Location() {
    }       
    
    @Basic(optional = false)
    private double x;  
    
    @Basic(optional = false)
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    
    
}
