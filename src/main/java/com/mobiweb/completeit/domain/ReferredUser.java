/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *  Represents a Referred user entity-record.
 * @author ic
 */
@Entity
@Table(name="referred_users")
@NamedQueries({
@NamedQuery(name="ReferredUser.findAll", query="SELECT ru FROM ReferredUser ru"),
@NamedQuery(name="ReferredUser.findByProfileId", query="SELECT ru FROM ReferredUser ru WHERE ru.profileId =:profileId"),    
@NamedQuery(name="ReferredUser.findByEmail", query="SELECT ru FROM ReferredUser ru WHERE ru.email =:email"),    
@NamedQuery(name="ReferredUser.findByProfileIdAndEmail", query="SELECT ru FROM ReferredUser ru WHERE ru.profileId =:profileId AND ru.email =:email"),    
@NamedQuery(name="ReferredUser.count", query="SELECT COUNT(ru.id) FROM ReferredUser ru"),
})
public class ReferredUser implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name="refu_id") 
    private long id;
    
    @Basic(optional = false)
    @Column(name="refu_profile_id", nullable=false)    
    private long profileId;
    
    @Basic(optional = false)
    @Column(name="refu_email", nullable=false)    
    private String email;
    
    @Column(name="refu_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());     
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "referredUser", fetch=FetchType.EAGER)
    //@OrderBy("id")
    private Set<SharedContent> sharedContent = new HashSet();    

    public ReferredUser() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long userid) {
        this.profileId = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<SharedContent> getSharedContent() {
        return sharedContent;
    }

    public void setSharedContent(Set<SharedContent> sharedContent) {
        this.sharedContent = sharedContent;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }          

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.profileId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReferredUser other = (ReferredUser) obj;
        return !(!Objects.equals(this.profileId, other.profileId) && !Objects.equals(this.email, other.email));
    }
    
    
    
}
