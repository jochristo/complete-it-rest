/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 *
 * @author ic
 */
@SqlResultSetMapping(name="PhraseSqlResultSetMapping",
    entities = @EntityResult(entityClass = Phrase.class,
    fields = {
        @FieldResult(name = "id", column = "ph_id"),
        @FieldResult(name = "created", column = "ph_created"),
        @FieldResult(name = "deleted", column = "ph_deleted"),
        @FieldResult(name = "phrase", column = "ph_phrase"),
        @FieldResult(name = "gaps", column = "ph_gaps"),
        @FieldResult(name = "isAdminAdded", column = "ph_isAdminAdded"),
        @FieldResult(name = "isApprovalPending", column = "ph_isApprovalPending"),
        @FieldResult(name = "profile", column = "ph_profile_id"),
    }))
@NamedNativeQueries({
    @NamedNativeQuery(name="PhraseNative.findAll", 
        query="SELECT p.ph_id, p.ph_created, p.ph_deleted, p.ph_phrase, p.ph_gaps, p.ph_isAdminAdded, p.ph_isApprovalPending, p.ph_profile_id"
                + " FROM phrase p WHERE p.ph_deleted = FALSE ORDER BY p.ph_id",
        resultClass = Phrase.class, resultSetMapping = "PhraseSqlResultSetMapping"),
})
@Entity
@Table(name="phrase")
@NamedQueries({
    @NamedQuery(name="Phrase.findAll", query="SELECT p FROM Phrase p WHERE p.deleted = FALSE ORDER BY p.id"), 
    @NamedQuery(name="Phrase.findAllApproved", query="SELECT p FROM Phrase p WHERE p.deleted = FALSE AND p.isApprovalPending=FALSE ORDER BY p.id"),    
    @NamedQuery(name="Phrase.findById", query="SELECT p FROM Phrase p WHERE p.id = :id"),    
    @NamedQuery(name="Phrase.findByPhrase", query="SELECT p FROM Phrase p WHERE p.phrase = :phrase"),
    @NamedQuery(name="Phrase.findByPhraseAndProfileId", query="SELECT p FROM Phrase p WHERE p.phrase = :phrase AND p.profile.id = :profileId"),
    @NamedQuery(name="Phrase.findByProfileId", query="SELECT p FROM Phrase p WHERE p.profile.id = :profileId"),
    @NamedQuery(name="Phrase.IsApprovalPending", query="SELECT p FROM Phrase p WHERE p.profile.id = :profileId"),
    @NamedQuery(name="Phrase.findByIdAndProfileGender", query="SELECT p FROM Phrase p WHERE p.id = :id AND p.profile.gender = :gender"),
    @NamedQuery(name="Phrase.findByGenderAgeRangeLocation", query="SELECT p FROM Phrase p WHERE p.profile.gender = :gender "
            + "AND p.profile.dob BETWEEN :low AND :high"),
    @NamedQuery(name="Phrase.findByGender", query="SELECT p FROM Phrase p WHERE p.profile.gender = :gender"),    
    @NamedQuery(name="Phrase.findPhrasesNotInAnswers", 
            query="SELECT p FROM Phrase p WHERE NOT EXISTS (select a from Answer a WHERE a.phrase.id = p.id)"),    
    @NamedQuery(name="Phrase.findPhraseInAnswers", 
            query="SELECT p FROM Phrase p WHERE EXISTS (select a from Answer a WHERE a.phrase.id = p.id) and p.id =:phraseId"),    
   
    @NamedQuery(name="Phrase.findByCategoryId", 
            query="SELECT p from Phrase p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.id =:categoryId"),    
    @NamedQuery(name="Phrase.findByCategoryIdOrderedByCreationDesc", 
        query="SELECT p from Phrase p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.phraseCategoryPK.categoryId =:categoryId ORDER BY p.created DESC, p.id ASC"),    
    @NamedQuery(name="Phrase.countByCategoryId", 
        query="SELECT COUNT(p.id) from Phrase p INNER JOIN p.phraseCategory c WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.id =:categoryId"),    
    
    @NamedQuery(name="Phrase.count", query="SELECT COUNT(p.id) FROM Phrase p"),  
    @NamedQuery(name="Phrase.countUndeleted", query="SELECT COUNT(p.id) FROM Phrase p WHERE p.deleted = FALSE"),    
    // NEW: fetch data from m:m PhraseCategory relation with FKs.
    @NamedQuery(name="Phrase.findByCategoryIdNotInAnswersNEW", 
        query="SELECT p from Phrase p INNER JOIN p.phraseCategory c WHERE p.isApprovalPending = FALSE AND c.category.id =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)"),    
    @NamedQuery(name="Phrase.findByCategoryIdNotInAnswersOrderedByCreationDescNEW", 
        query="SELECT p from Phrase p INNER JOIN p.phraseCategory c WHERE (p.isApprovalPending = FALSE AND c.phraseCategoryPK.categoryId =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)) ORDER BY p.created DESC"),   
    @NamedQuery(name="Phrase.countByCategoryIdNotInAnswersNEW", 
        query="SELECT COUNT(p.id) from Phrase p INNER JOIN p.phraseCategory c WHERE p.isApprovalPending = FALSE AND c.category.id =:categoryId "
                + "AND NOT EXISTS(select a from Answer a WHERE a.phrase = p AND a.profile.id =:profileId)"),
    @NamedQuery(name="Phrase.countGroupByCategory", 
            query="SELECT c.phraseCategoryPK.categoryId, COUNT(c.phraseCategoryPK.phraseId) FROM Phrase p INNER JOIN p.phraseCategory c "
                    + "WHERE p.deleted=FALSE AND p.isApprovalPending = FALSE AND c.category.deleted = FALSE GROUP BY c.phraseCategoryPK.categoryId, c.phraseCategoryPK.phraseId "
                    + "ORDER BY c.phraseCategoryPK.categoryId ASC"),                    
    @NamedQuery(name="Phrase.countApproved", query="SELECT COUNT(p.id) FROM Phrase p WHERE p.deleted = FALSE AND p.isApprovalPending=FALSE"),        
    @NamedQuery(name="Phrase.countPending", query="SELECT COUNT(p.id) FROM Phrase p WHERE p.deleted = FALSE AND p.isApprovalPending=TRUE"),            
    
    @NamedQuery(name="Phrase.countAnsweredGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(c.phraseCategoryPK.categoryId, c.category.name, COUNT(c.phraseCategoryPK.phraseId)) FROM Phrase p INNER JOIN p.phraseCategory c "
                    + "WHERE (p.deleted=FALSE AND c.category.deleted = FALSE "
                    + "AND EXISTS(SELECT a.id FROM Answer a WHERE a.phrase.id = p.id)) GROUP BY c.phraseCategoryPK.categoryId, c.category.name ORDER BY c.category.name, p.id ASC"),                    
@NamedQuery(name="Phrase.countUnansweredGroupByCategory", 
            query="SELECT NEW com.mobiweb.completeit.domain.CategoryProjection(c.phraseCategoryPK.categoryId, c.category.name, COUNT(c.phraseCategoryPK.phraseId)) FROM Phrase p INNER JOIN p.phraseCategory c "
                    + "WHERE (p.deleted=FALSE AND c.category.deleted = FALSE "
                    + "AND NOT EXISTS(SELECT a.id FROM Answer a WHERE a.phrase.id  = p.id)) GROUP BY c.phraseCategoryPK.categoryId, c.category.name ORDER BY c.category.name, p.id ASC"),                        
})
public class Phrase implements Serializable {

    public Phrase() {       
        this.categories = new ArrayList();
        this.profile = new Profile();
        this.created = new Timestamp(new Date().getTime());
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="ph_id", nullable=false) 
    private long id;
    
    @Column(name="ph_phrase", nullable=false) 
    @Basic(optional = false) 
    @NotNull(message="Phrase cannot be null")     
    private String phrase;    

    @Column(name="ph_gaps", nullable=false) 
    @Basic(optional = false) 
    @NotNull(message="Phrase gaps cannot be null")    
    private int gaps;
        
    @Transient
    private List<Category> categories;    
    
    @ManyToOne(fetch=FetchType.EAGER, cascade={CascadeType.ALL},targetEntity=Profile.class)
    @JoinColumn(name="ph_profile_id",nullable = true)    
    @ForeignKey(name ="ph_profile_id_FK", columnNames = {"ph_profile_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)
    private Profile profile;  
    
    @Column(name="ph_isAdminAdded", nullable=false, columnDefinition = "BIT(1) DEFAULT 0") 
    @Basic(optional = false) 
    @NotNull(message="isAdminAdded cannot be null")     
    private boolean isAdminAdded = true;
    
    @Column(name="ph_isApprovalPending", nullable=false, columnDefinition = "BIT(1) DEFAULT 0") 
    @Basic(optional = false) 
    @NotNull(message="isApprovalPending cannot be null")     
    private boolean isApprovalPending = false;
    
    @Column(name="ph_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());   
      
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "phrase", fetch=FetchType.EAGER,orphanRemoval = false)    
    private Set<PhraseCategory> phraseCategory = new HashSet();        
    
    @Transient
    protected List<Category> _categories = new ArrayList();
        
    @Basic(optional = false)
    @Column(name="ph_deleted", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean deleted = false;     
    
    @Basic(optional = false)
    @Column(name="ph_is_dummy", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean dummy = false;     
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getGaps() {
        return gaps;
    }

    public void setGaps(int gaps) {
        this.gaps = gaps;
    }
    
    public List<Category> getCategories() {
        
        List<Category> cats = new ArrayList();        
        Set<PhraseCategory> set = this.getPhraseCategory();
        for(PhraseCategory pc : set)
        {
            cats.add(pc.getCategory());            
        }
        return cats;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
    
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public boolean isIsAdminAdded() {
        return isAdminAdded;
    }

    public void setIsAdminAdded(boolean isAdminAdded) {
        this.isAdminAdded = isAdminAdded;
    }

    public boolean isIsApprovalPending() {
        return isApprovalPending;
    }

    public void setIsApprovalPending(boolean isApprovalPending) {
        this.isApprovalPending = isApprovalPending;
    }
    
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }    

    public Set<PhraseCategory> getPhraseCategory() {
        return phraseCategory;
    }

    public void setPhraseCategory(Set<PhraseCategory> phraseCategory) {
        this.phraseCategory = phraseCategory;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 59 * hash + Objects.hashCode(this.phrase);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Phrase other = (Phrase) obj;
        return this.id == other.id;
    }
    
    
    
}
