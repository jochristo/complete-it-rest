/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

/**
 *
 * @author ic
 */
public class CategoryProjection implements Comparable<CategoryProjection>
{
    
    private long categoryId;
    private String categoryName;
    private long count;

    public CategoryProjection() {
    }

    public CategoryProjection(long categoryId, String categoryName, long count) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.count = count;
    }
    

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (this.categoryId ^ (this.categoryId >>> 32));
        hash = 37 * hash + (int) (this.count ^ (this.count >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategoryProjection other = (CategoryProjection) obj;
        return this.categoryId == other.categoryId;
    }

        @Override
        public int compareTo(CategoryProjection o) {
            return Long.compare(this.getCount(), o.getCount());
        }
    
    
    
}
