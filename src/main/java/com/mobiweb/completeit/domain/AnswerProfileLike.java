/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 * Represents composite primary key of an Answer Profile pair.
 * @author ic
 */
@Entity
@Table(name="answer_profile_likes")
@NamedQueries({
    @NamedQuery(name="AnswerProfileLike.findByPK", 
        query="SELECT apl FROM AnswerProfileLike apl WHERE apl.answerProfileLikePK.answerId =:answerId "
                + "AND apl.answerProfileLikePK.profileId =:profileId"),
@NamedQuery(name="AnswerProfileLike.findByAnswerId", 
        query="SELECT apl FROM AnswerProfileLike apl WHERE apl.answerProfileLikePK.answerId =:answerId"),   
@NamedQuery(name="AnswerProfileLike.findByProfileId", 
        query="SELECT apl FROM AnswerProfileLike apl WHERE apl.answerProfileLikePK.profileId =:profileId"),
    @NamedQuery(name="AnswerProfileLike.count", 
        query="SELECT COUNT(apl.answerProfileLikePK) FROM AnswerProfileLike apl WHERE apl.active = TRUE"),
@NamedQuery(name="AnswerProfileLike.findByProfileIdAndAnswerId", 
        query="SELECT apl FROM AnswerProfileLike apl WHERE apl.answerProfileLikePK.profileId =:profileId AND apl.answerProfileLikePK.answerId =:answerId"),    
}) 
public class AnswerProfileLike implements Serializable 
{    
    @EmbeddedId
    protected AnswerProfileLikePK answerProfileLikePK;    
        
    @ManyToOne
    @JoinColumn(name="like_answer_id", referencedColumnName = "an_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKeya_answer_id", columnNames = {"like_answer_id"}, 
        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)             
    private Answer answer;
    
    @ManyToOne
    @JoinColumn(name="like_profile_id", referencedColumnName = "pr_id", nullable = false, insertable=false, updatable= false)
    @ForeignKey(name ="ForeignKeyp_profile_id", columnNames = {"like_profile_id"}, 
        deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)             
    private Profile profile; 
    
    @Basic(optional = false)
    @Column(name="is_active", nullable=false, columnDefinition = "BIT(1) DEFAULT b'1'")    
    private boolean active = true;    

    public AnswerProfileLike() {
    }

    public AnswerProfileLike(Answer answer, Profile profile) {
        this.answerProfileLikePK = new AnswerProfileLikePK(answer.getId(), profile.getId());
        this.answer = answer;
        this.profile = profile;
    }   

    public AnswerProfileLikePK getAnswerProfileLikePK() {
        return answerProfileLikePK;
    }

    public void setAnswerProfileLikePK(AnswerProfileLikePK answerProfileLikePK) {
        this.answerProfileLikePK = answerProfileLikePK;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }   
    
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.answerProfileLikePK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnswerProfileLike other = (AnswerProfileLike) obj;
        return Objects.equals(this.answerProfileLikePK, other.answerProfileLikePK);
    }

    
}
