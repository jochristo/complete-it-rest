/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

/**
 * LikedAnswer projection POJO.
 * @author ic
 */
@SqlResultSetMapping(name="LikedAnswerMapping",
    entities = @EntityResult(entityClass = LikedAnswer.class,
    fields = {
        @FieldResult(name = "an_id", column = "an_id"),
        @FieldResult(name = "an_answers", column = "an_answers"),
        @FieldResult(name = "ph_phrase", column = "ph_phrase"),
        @FieldResult(name = "an_profile_id", column = "an_profile_id"),
        @FieldResult(name = "an_likes", column = "an_likes"),
        @FieldResult(name = "an_reports", column = "an_reports"),
        @FieldResult(name = "like_profile_id", column = "like_profile_id"),
    }))
@Entity
@NamedNativeQueries({
    @NamedNativeQuery(name="LikedAnswersExcludeReported", 
        query="select an_id, an_answers, ph_phrase, an_profile_id, an_likes, an_reports, profile_likes_profile_id as like_profile_id "
        + "FROM answer a INNER JOIN phrase p on ph_id = an_phrase_id LEFT JOIN profile_likes pl on an_id = profile_likes_answer_id "
        + "WHERE profile_likes_profile_id is not null AND an_profile_id = ?1 AND an_reports < 3 "
        + "ORDER BY an_id",
        resultClass = LikedAnswer.class, resultSetMapping = "LikedAnswerMapping"),
    
    @NamedNativeQuery(name="LikedAnswer.allLikedAnswersExcludeReportedByPhraseId", 
        query="select * from (select an_id, an_answers, ph_phrase, an_profile_id, an_likes, an_reports, profile_likes_profile_id as like_profile_id, ph_id "
        + "FROM answer a INNER JOIN phrase p on ph_id = an_phrase_id LEFT JOIN profile_likes pl on an_id = profile_likes_answer_id "
        + " AND an_reports < 3 "
        + "ORDER BY ph_id) as s where s.ph_id = ?1",
        resultClass = LikedAnswer.class, resultSetMapping = "LikedAnswerMapping"),    
})
public class LikedAnswer implements Serializable {
     
    // answer id
    @Id
    private long an_id;
    // answers string
    private String an_answers;
    // incomplete phrase
    private String ph_phrase;
    // profile id of the answer
    private long an_profile_id;
    // count of likes
    private long an_likes;
    // count of reports
    private long an_reports;
    // profile id which liked the answer
    private long like_profile_id;


    public LikedAnswer(long an_id, String an_answers, String ph_phrase, long an_profile_id, long an_likes, long an_reports, long like_profile_id) {
        this.an_id = an_id;
        this.an_answers = an_answers;
        this.ph_phrase = ph_phrase;
        this.an_profile_id = an_profile_id;
        this.an_likes = an_likes;
        this.an_reports = an_reports;
        this.like_profile_id = like_profile_id;
    }

    public LikedAnswer() {
    }  

    public long getAn_id() {
        return an_id;
    }

    public void setAn_id(long an_id) {
        this.an_id = an_id;
    }

    public String getAn_answers() {
        return an_answers;
    }

    public void setAn_answers(String an_answers) {
        this.an_answers = an_answers;
    }

    public String getPh_phrase() {
        return ph_phrase;
    }

    public void setPh_phrase(String ph_phrase) {
        this.ph_phrase = ph_phrase;
    }

    public long getAn_profile_id() {
        return an_profile_id;
    }

    public void setAn_profile_id(long an_profile_id) {
        this.an_profile_id = an_profile_id;
    }

    public long getAn_likes() {
        return an_likes;
    }

    public void setAn_likes(long an_likes) {
        this.an_likes = an_likes;
    }

    public long getAn_reports() {
        return an_reports;
    }

    public void setAn_reports(long an_reports) {
        this.an_reports = an_reports;
    }

    public long getLike_profile_id() {
        return like_profile_id;
    }

    public void setLike_profile_id(long like_profile_id) {
        this.like_profile_id = like_profile_id;
    }
    
    

    
}
