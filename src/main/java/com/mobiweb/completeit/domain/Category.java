/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.apache.openjpa.persistence.jdbc.ForeignKey;
import org.apache.openjpa.persistence.jdbc.ForeignKeyAction;

/**
 *
 * @author ic    
 */
@Entity
@Table(name="category")
//,uniqueConstraints = @UniqueConstraint(name ="UniqueCategoryNameConstraint",
//        columnNames = {"cat_name"}))

@NamedQueries({
    @NamedQuery(name="Category.findAll", query="SELECT c FROM Category c WHERE c.deleted = FALSE"),
    @NamedQuery(name="Category.findAllDistinct", query="SELECT DISTINCT c FROM Category c WHERE c.deleted = FALSE ORDER BY c.name"),    
    @NamedQuery(name="Category.findById", query="SELECT c FROM Category c WHERE c.id = :id"),    
    @NamedQuery(name="Category.findByName", query="SELECT c FROM Category c WHERE c.name = :name"),
    @NamedQuery(name="Category.findByNameUndeleted", query="SELECT c FROM Category c WHERE c.name = :name AND c.deleted = false"),
    @NamedQuery(name="Category.findByNameExcludeId", query="SELECT c FROM Category c WHERE c.name = :name AND c.id <> :id"),
    @NamedQuery(name="Category.count", query="SELECT COUNT(c.id) FROM Category c"),
    @NamedQuery(name="Category.countByCategoryId", 
            query="SELECT COUNT(c.id) FROM Category c INNER JOIN c.phraseCategory p WHERE p.phraseCategoryPK.categoryId =:categoryId"),
    @NamedQuery(name="Category.countByCategoryIdUndeleted", 
            query="SELECT COUNT(c.id) FROM Category c INNER JOIN c.phraseCategory p WHERE p.phraseCategoryPK.categoryId =:categoryId "
                    + "AND p.active = TRUE"),
    @NamedQuery(name="Category.countUndeleted", query="SELECT COUNT(c.id) FROM Category c WHERE c.deleted = FALSE"),    
})
public class Category implements Serializable {

    public Category() {
        this.phrases = new ArrayList<>();
    }    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false) 
    @Column(name="cat_id", nullable=false) 
    private long id;
    
    @Column(name="cat_name", nullable=false) 
    @Basic(optional = false) 
    @NotNull(message="Category name cannot be null")     
    private String name;
            
    @Column(name="cat_description") 
    @Basic(optional = true)     
    private String description;    
    
    @Column(name="cat_img_rel_path", nullable=false) 
    @Basic(optional = false) 
    @NotNull(message="Image relative path cannot be null") 
    private String imageRelativePath;
    
    @Transient
    private List<Phrase> phrases;  
    
    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "category", fetch=FetchType.EAGER, orphanRemoval = false)
    private Set<PhraseCategory> phraseCategory = new HashSet();     
   
    @Basic(optional = false)
    @Column(name="cat_deleted", nullable=false, columnDefinition = "BIT(1) DEFAULT 0")    
    private boolean deleted = false;  
    
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, targetEntity=Culture.class)
    @JoinColumn(name="culture_id",nullable = true)    
    @ForeignKey(name ="FOREIGN_KEY_culture_id", columnNames = {"culture_id"}, 
            deleteAction=ForeignKeyAction.RESTRICT, updateAction = ForeignKeyAction.CASCADE)     
    private Culture culture;
    
    @Column(name="cat_created", nullable=true, columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)    
    private Date created = new Timestamp(new Date().getTime());     
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
    
    public String getImageRelativePath() {
        return imageRelativePath;
    }

    public void setImageRelativePath(String imageRelativePath) {
        this.imageRelativePath = imageRelativePath;
    }    

    public List<Phrase> getPhrases() {
        return phrases;
    }

    public void setPhrases(List<Phrase> phrases) {
        this.phrases = phrases;
    }    

    public Set<PhraseCategory> getPhraseCategory() {
        return phraseCategory;
    }

    public void setPhraseCategory(Set<PhraseCategory> phraseCategory) {
        this.phraseCategory = phraseCategory;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Culture getCulture() {
        return culture;
    }

    public void setCulture(Culture culture) {
        this.culture = culture;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 71 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        return !(this.id != other.id && !Objects.equals(this.name, other.name));
    }
    
    
    
}
