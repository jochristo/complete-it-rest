/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobiweb.completeit.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Represents an entity counter instance when other  new entities are created and persisted.
 * @author AdminAccount
 */
@Entity
//@EntityListeners({ProfileListener.class})
@Table(name="entity_counter", 
        uniqueConstraints={
            @UniqueConstraint(columnNames={"counter_id"}),
            @UniqueConstraint(columnNames={"entity_type"})
        })
@NamedQueries({
    @NamedQuery(name="EntityCounter.findByEntityType", query="SELECT c FROM EntityCounter c WHERE c.entity_type =:entityType"),
    @NamedQuery(name="EntityCounter.countByEntityType", query="SELECT c.counter FROM EntityCounter c WHERE c.entity_type =:entityType"),
})    
public class EntityCounter implements Serializable
{
    private static final long serialVersionUID = 1L; 
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name="counter_id")     
    private long id;
    
    @Column(name="entity_type", nullable=false, unique = true)     
    private String entity_type;
    
    @Basic(optional = false)
    @Column(name="records_counter", nullable=false)    
    private long counter = 0;
            
    
    public EntityCounter() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 43 * hash + (int) (this.counter ^ (this.counter >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityCounter other = (EntityCounter) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.entity_type, other.entity_type)) {
            return false;
        }
        return true;
    }
    
    
}
